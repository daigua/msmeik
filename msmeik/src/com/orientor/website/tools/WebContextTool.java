package com.orientor.website.tools;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

public class WebContextTool {
	public static String getCookieValue(String cookieName){
		HttpServletRequest request = ServletActionContext.getRequest();		
		Cookie[] cookies = request.getCookies();
		if(cookies == null)
			return null;
		for (int i = 0; i < cookies.length; i++) {
            Cookie cookie = cookies[i];
            if (cookieName.equals(cookie.getName())) {
                return cookie.getValue();
            }
        }
		return null;
	}
	public static Object getSessionValue(String name){
		return ServletActionContext.getContext().getSession().get(name);
	}
}
