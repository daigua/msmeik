package com.orientor.website.tools;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Map;

import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class FreemarkerTool{
	private static FreeMarkerConfigurer freemarkerConfigurer;
	
	//���ɾ�̬�ļ�
	public static void geneHtml(BufferedWriter out,Map model,String viewFtl){
		Configuration cfg=freemarkerConfigurer.getConfiguration();
		cfg.setObjectWrapper(new DefaultObjectWrapper());
		Template t;
		try {
			t = cfg.getTemplate(viewFtl);
			t.process(model, out);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TemplateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static String getHtml(Map model,String viewFtl){
		Template t=null;
		ByteArrayOutputStream baos=new ByteArrayOutputStream();
		Writer out=new BufferedWriter(new OutputStreamWriter(baos));
		Configuration cfg=freemarkerConfigurer.getConfiguration();
		String html="";
		try {
			t = cfg.getTemplate(viewFtl);
			t.process(model, out);

			html=new String(baos.toByteArray());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";			
		} 			
		return html;
	}

	public FreeMarkerConfigurer getFreemarkerConfigurer() {
		return freemarkerConfigurer;
	}

	public void setFreemarkerConfigurer(FreeMarkerConfigurer freemarkerConfigurer) {
		this.freemarkerConfigurer = freemarkerConfigurer;
	}
	
	
}
