package com.orientor.website.tools;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

public class SendMailTool {
	private static JavaMailSenderImpl mailSender;
	
	public static void sendMimeMail(String to,String from,String subject,String text){
		MimeMessage mimeMsg=mailSender.createMimeMessage();
		try {
			MimeMessageHelper helper=new MimeMessageHelper(mimeMsg,false);
			helper.setTo(to);
			helper.setSubject(subject);
			helper.setFrom(from);
			helper.setText(text,true);				
			mailSender.send(mimeMsg);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			System.out.println("�ʼ�����ʧ��");
		}
	}

	public JavaMailSenderImpl getMailSender() {
		return mailSender;
	}

	public void setMailSender(JavaMailSenderImpl mailSender) {
		this.mailSender = mailSender;
	}
	
	
}
