package com.orientor.website.tools;

import java.util.Date;
import java.text.SimpleDateFormat;

public class UtilTools {
	/**
	 * 将日期按格式化输出成字符串
	 * */
	public static String formatDate(Date date,String format){
		return new SimpleDateFormat(format).format(date);
	}
	/**
	 * 取出字符串当中多余的空格，回车换行等空白字符
	 * */
	public static String replaceWhite(String str){
		return str.trim().replaceAll("\\s{1,}", "");
	}
	/**
	 * 获取去除html标签，去除回车换行，去除空格后的内容
	 * */
	public static String getHtmlText(String html){
		if(html == null || html.equals(""))
			return html;
		html=html.replaceAll("</?[^>]+>","");   //剔出了<html>的标签
		html=html.replace("&nbsp;","");
		html=html.replace(".","");
		html=html.replace("\"","‘");
		html=html.replace("'","‘");
		html=html.replaceAll("\\s*|\t|\r|\n","");//去除字符串中的空格,回车,换行符,制表符
		return html;
	}
}
