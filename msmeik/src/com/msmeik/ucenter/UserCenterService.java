package com.msmeik.ucenter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.msmeik.authentication.AuthenticationService;
import com.msmeik.member.User;
import com.msmeik.ucenter.client.Client;
import com.msmeik.ucenter.util.XMLHelper;

public class UserCenterService {
	
	private static Client client = new Client();
	
	/**
	 * user ucenter api
	 * */
	/**
	 * 使用ucenter 登录
	 * @return Map
	 * */
	public static Map userLoginFromUcenter(User user){
		String result = client.uc_user_login(user.getAccount(), user.getPassword());
		List<String> rs = XMLHelper.uc_unserialize(result);
		Map p = new HashMap();
		p.put("status", -1);
		if(rs.size()>0){
			int userId = Integer.parseInt(rs.get(0));
			String account = rs.get(1);
			String password = rs.get(2);
			User loginUser = new User();
			if(userId > 0) {
				System.out.println("登录成功From ucenter");					
				System.out.println("开始同步登陆");
				String $ucsynlogin = client.uc_user_synlogin(userId);
				System.out.println("同步登录成功"+$ucsynlogin);
				loginUser.setId(userId);
				loginUser.setAccount(account);
				loginUser.setPassword(password);
				p.put("ucsynlogin", $ucsynlogin);
				p.put("user", loginUser);
				p.put("status", 1);
			}			
		}
		return p;
	}
	
	/**
	 * 使用ucenter同步登出
	 * @return 返回同步登出代码
	 * */
	public static String userLogoutFromUcenter(){
		System.out.println("登出美食美客，开始同步登出");
		String result = client.uc_user_synlogout();
		System.out.println("取得同步登出代码"+result);
		return result;
	}
	
	/**
	 * 积分兑换，把美客积分兑换成为积分
	 * */
	public static String exchangeMeikeCreditToHomeCredit(int uid, int from ,int to, int amount){
		System.out.println("正在将"+uid+"的美客积分"+amount+"兑换成家园积分");
		String status = client.uc_credit_exchange_request(uid, from, to, 2, amount);
		if(status.equals("1")){
			System.out.println("兑换成功"+uid+"的美客积分，兑换"+amount+"积分");
		}else{
			System.out.println("兑换失败"+uid+"的美客积分，兑换"+amount+"积分");
		}
		return status;
	}
	
	/**
	 * 保存用户登录信息
	 * */
	public static void saveLoginStatus(User user){
		AuthenticationService.setUserLoginStatus(user);
	}
	/**
	 * 登出用户信息
	 * */
	public static void logout(){
		AuthenticationService.logout();
	}
}
