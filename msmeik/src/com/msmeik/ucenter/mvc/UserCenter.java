package com.msmeik.ucenter.mvc;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.msmeik.member.User;
import com.msmeik.ucenter.UserCenterService;
import com.msmeik.ucenter.client.Client;

public class UserCenter {
	
	private String code;
	private String result = "";
	
	public String execute(){
		if(code == null){
			result = this.API_RETURN_FAILED;
			return "success";
		}
		Map<String,String> get = new HashMap();
		
		code = new Client().uc_authcode(code, "DECODE");		
		this.parse_str(code, get);
		if(get.isEmpty()){
			result = "Invalid Request";
			return "success";
		}
		System.out.println(get);
		String action = get.get("action");
		if(action == null){
			result = this.API_RETURN_FAILED;
			return "success";
		}
		if(action.equals("test")){
			result = this.API_RETURN_SUCCEED;
			return "success";
		}else 
		if(action.equals("deleteUser")){
			result = this.API_RETURN_SUCCEED;
			return "success";
		}else
		if(action.equals("renameuser")){
			result = this.API_RETURN_SUCCEED;
			return "success";
		}else
		if(action.equals("gettag")){
			if(!API_GETTAG)
				result = this.API_RETURN_FORBIDDEN;
			else
				result = this.API_RETURN_SUCCEED;
			return "success";
		}else
		if(action.equals("synlogin")){
			if(!API_SYNLOGIN){
				result = this.API_RETURN_FORBIDDEN;
				return "success";
			}
			User user = new User();
			user.setAccount(get.get("username"));
			user.setId(Integer.parseInt(get.get("uid")));
			UserCenterService.saveLoginStatus(user);
			System.out.println("接受ucenter登录信息成功");
		}else
		if(action.equals("synlogout")){
			if(!API_SYNLOGOUT){
				result = this.API_RETURN_FORBIDDEN;
				return "success";
			}
			System.out.println("开始接受ucenter发出的登出信息，准备登出");
			UserCenterService.logout();
			System.out.println("接受ucenter登出信息成功");
		}else
		if(action.equals("updateclient")){
			result = this.API_RETURN_FORBIDDEN;
			return "success";
		}else
		if(action.equals("updatepw")){
			result = this.API_RETURN_FORBIDDEN;
			return "success";
		}else
		if(action.equals("updatebadwords")){
			result = this.API_RETURN_FORBIDDEN;
			return "success";
		}else
		if(action.equals("updatehosts")){
			result = this.API_RETURN_FORBIDDEN;
			return "success";
		}else
		if(action.equals("updateapps")){
			result = this.API_RETURN_FORBIDDEN;
			return "success";
		}else
		if(action.equals("updatecredit")){
			if(!this.API_UPDATECREDITSETTINGS)
				result = this.API_RETURN_FORBIDDEN;
			System.out.println("收到ucenter发出的积分更新通知");
			return "success";
		}else
		if(action.equals("getcreditsettings")){
			//result = this.API_RETURN_FORBIDDEN;
			if(!this.API_UPDATECREDITSETTINGS)
				result = this.API_RETURN_FORBIDDEN;
			else
				result = Client.CREDIT_SETTINGS;
			return "success";
		}else
		if(action.equals("updatecreditsettings")){
			if(!this.API_UPDATECREDITSETTINGS)
				result = this.API_RETURN_FORBIDDEN;
			result = "1";
			return "success";
		}		
		
		return "success";
	}
	
	private void parse_str(String str, Map<String,String> sets){
		if(str==null||str.length()<1) 
			return;
		String[] ps = str.split("&");
		for(int i=0;i<ps.length;i++){
			String[] items = ps[i].split("=");
			if(items.length==2){
				sets.put(items[0], items[1]);
			}else if(items.length ==1){
				sets.put(items[0], "");
			}
		}
	}
	
	protected long time(){
		return System.currentTimeMillis()/1000;
	}
	
    private static long tolong(Object s){
        if(s!=null){
            String ss = s.toString().trim();
            if(ss.length()==0){
                return 0L;
            }else{
                return Long.parseLong(ss);
            }
        }else{
            return 0L;
        }
    }
    
    
    
    public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	

	public String getResult() {
		return result;
	}



	private static final long serialVersionUID = -7377364931916922413L;
	
	public static boolean IN_DISCUZ= true;
	public static String UC_CLIENT_VERSION="1.5.0";	//note UCenter 版本标识
	public static String UC_CLIENT_RELEASE="20100408";

	public static boolean API_DELETEUSER=true;		//note 用户删除 API 接口�?��
	public static boolean API_RENAMEUSER=true;		//note 用户改名 API 接口�?��
	public static boolean API_GETTAG=true;		//note 获取标签 API 接口�?��
	public static boolean API_SYNLOGIN=true;		//note 同步登录 API 接口�?��
	public static boolean API_SYNLOGOUT=true;		//note 同步登出 API 接口�?��
	public static boolean API_UPDATEPW=true;		//note 更改用户密码 �?��
	public static boolean API_UPDATEBADWORDS=true;	//note 更新关键字列�?�?��
	public static boolean API_UPDATEHOSTS=true;		//note 更新域名解析缓存 �?��
	public static boolean API_UPDATEAPPS=true;		//note 更新应用列表 �?��
	public static boolean API_UPDATECLIENT=true;		//note 更新客户端缓�?�?��
	public static boolean API_UPDATECREDIT=true;		//note 更新用户积分 �?��
	public static boolean API_GETCREDITSETTINGS=true;	//note �?UCenter 提供积分设置 �?��
	public static boolean API_GETCREDIT=true;		//note 获取用户的某项积�?�?��
	public static boolean API_UPDATECREDITSETTINGS=true;	//note 更新应用积分设置 �?��

	public static String API_RETURN_SUCCEED   =    "1";
	public static String API_RETURN_FAILED    =   "-1";
	public static String API_RETURN_FORBIDDEN =   "-2";
}
