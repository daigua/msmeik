package com.msmeik.member;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class MemberDao extends SqlMapClientDaoSupport{
	
	public int insertUser(Object user){
		return (Integer)this.getSqlMapClientTemplate().insert("insertUser", user);
	}
	public List selectUser(Object p){
		return this.getSqlMapClientTemplate().queryForList("selectUser", p);
	}
	public int selectUserCount(Object p){
		Object returnObj = this.getSqlMapClientTemplate().queryForObject("selectUserCount", p);
		if(returnObj == null)
			return 0;
		return (Integer)returnObj;
	}
	public int updateUser(Object user){
		return this.getSqlMapClientTemplate().update("updateUser", user);
	}
}
