package com.msmeik.member.singlepoint;

import java.util.HashMap;
import java.util.Map;

import com.msmeik.authentication.AuthenticationService;
import com.msmeik.member.User;
import com.orientor.website.tools.MD5EncodeTool;

public class SingleLoginService {
	
	/**
	 * 存入用户登录的验证信息， key为MD5加密，值为 登录的User信息
	 * */
	private static Map loginAuth = new HashMap();
	private static String authKey = "com.msmeik.luling.201004142054";
	
	/**
	 * 判断用户是否在基站点登录，比如判断用户是否在http://www.msmeik.com登录
	 * 如果登录，则生成一个MD5加密的验证码存入loginAuth中，并且将这个验证码返回
	 * */
	public static String checkLogin(){
		User user = AuthenticationService.getLoginUser();
		if(user!=null){
			String authStr = SingleLoginService.authKey+System.currentTimeMillis()+user.getAccount()+user.getId();
			String auth = MD5EncodeTool.encode(authStr);
			SingleLoginService.loginAuth.put(auth, user);
			return auth;
		}
		return null;
	}
	/**
	 * 取得用户deng
	 * */
	
	/**
	 * 根据用户返回回来的auth判断是否允许其登录
	 * 如果auth正确，则写入登陆信息，并返回登录的用户信息
	 * */
	public static User login(String auth){		
		if(loginAuth.containsKey(auth) && !auth.equals("")){
			//MemberUtil1.setLogin((User)loginAuth.get(auth));
			User user = (User)loginAuth.get(auth);
			loginAuth.remove(auth);
			return user;
		}		
		return null;
	}
}
