package com.msmeik.member.singlepoint.mvc;

import com.msmeik.member.singlepoint.SingleLoginService;

public class CheckLogin {
	
	private String authCode;
	
	public String execute(){
		this.authCode = SingleLoginService.checkLogin();
		
		return "success";
	}

	public String getAuthCode() {
		return authCode;
	}
	
	
}
