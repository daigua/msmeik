package com.msmeik.member.singlepoint.mvc;

import java.util.HashMap;
import java.util.Map;

import com.msmeik.member.User;
import com.msmeik.member.singlepoint.SingleLoginService;

public class Login {
	
	private String auth;
	private Map user;
	
	public String execute(){
		User user = SingleLoginService.login(auth);
		
		if(user == null)
			return "success";
		this.user = new HashMap();
		this.user.put("userId", user.getId());
		this.user.put("account", user.getAccount());
		return "success";
	}

	public void setAuth(String auth) {
		this.auth = auth;
	}

	public Map getUser() {
		return user;
	}

	
	
}
