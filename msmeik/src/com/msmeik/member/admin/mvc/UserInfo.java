package com.msmeik.member.admin.mvc;

import com.msmeik.member.MemberService;
import com.msmeik.member.User;

public class UserInfo {
	private MemberService memberService;
	
	private int userId;
	private String account;
	
	private User user;
	
	public String execute(){
		if(this.userId>0){
			this.user = this.memberService.getUserByUserId(userId);
		}else
		if(this.account!=null || !this.account.trim().equals("")){
			this.user = this.memberService.getUserByAccount(account);
		}
		if(this.user == null)
			this.user = new User();
		return "success";
	}

	public User getUser() {
		return user;
	}

	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	
	
}
