package com.msmeik.member;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;

import com.msmeik.ucenter.util.XMLHelper;
import com.msmeik.ucenter.client.Client;
import com.orientor.website.tools.MD5EncodeTool;

public class MemberService {
	private MemberDao memberDao;
	
	/**
	 * 根据用户账号取得匹配用户的个数
	 * */
	public int getUserCountByAccount(String account){
		if(account==null || account.trim().equals(""))
			return 0;
		Map p = new HashMap();
		p.put("account", account);
		return this.memberDao.selectUserCount(p);
	}
	/**
	 * 根据用户账号和密码取得匹配用户的个数
	 * */
	public int getUserCountByAccountAndPassword(String account,String password){
		if(account==null || password==null || account.trim().equals("") || password.trim().equals(""))
			return 0;
		Map p = new HashMap();
		p.put("account", account);
		p.put("password", password);
		return this.memberDao.selectUserCount(p);
	}
	
	/**
	 * 根据用户账号和email取得匹配的用户的个数
	 * */
	public int getUserCountByAccountAndEmail(String account,String email){
		if(account == null || email == null || account.trim().equals("") || email.trim().equals(""))
			return 0;
		Map p = new HashMap();
		p.put("account", account);
		p.put("email", email);
		return this.memberDao.selectUserCount(p); 
	}
	
	/**
	 * 保存用户，并返回保存的用户id
	 * */
	public int saveUser(User user){
		return this.memberDao.insertUser(user);
	}
	
	/**
	 * 更新用户信息并返回更新的数量
	 * @param userId 要更新的用户id
	 * @param user:HashMap 要更新的用户信息
	 * */
	public int updateUser(int userId,Map user){
		user.put("id", userId);
		return this.memberDao.updateUser(user);
	}
	/**
	 * 更新用户信息并返回更新的数量
	 * @param userId 要更新的用户id
	 * @param user:User 要更新的用户信息
	 * */
	public int updateUser(int userId,User user){
		user.setId(userId);
		return this.memberDao.updateUser(user);
	}
	/**
	 * 根据账号修改密码
	 * */
	public int updateUserPasswordByAccount(String account,String password){
		if(account==null || password == null || account.trim().equals("") || password.trim().equals(""))
			return 0;
		Map p = new HashMap();
		p.put("account", account);
		p.put("newPassword", password);
		return this.memberDao.updateUser(p);
	}
	/**
	 * 根据账号和原密码和新密码修改密码
	 * */
	public int updateUserPasswordByAccountAndPassword(String account,String originalPassword,String newPassword){
		if(account == null || originalPassword==null || newPassword == null || account.trim().equals("") || originalPassword.trim().equals("") || newPassword.trim().equals(""))
			return 0;
		Map p = new HashMap();
		p.put("account", account);
		p.put("originalPassword", originalPassword);
		p.put("password", newPassword);
		return this.memberDao.updateUser(p);
	}
	/**
	 * 根据用户id和原密码和新密码修改密码
	 * */
	public int updateUserPasswordByAccountAndPassword(int userId,String originalPassword,String newPassword){
		if(userId <= 0 || originalPassword==null || newPassword == null || originalPassword.trim().equals("") || newPassword.trim().equals(""))
			return 0;
		Map p = new HashMap();
		p.put("id", userId);
		p.put("newPassword", newPassword);
		p.put("password", originalPassword);
		return this.memberDao.updateUser(p);
	}
	
	
	/**
	 * 根据账号和密码取得用户信息，如果不存在返回null，如果存在返回该User
	 * @return User
	 * */
	public User getUserByAccountAndPassword(String account,String password){
		if(account == null || password == null || account.trim().equals("") || password.trim().equals(""))
			return null;
		Map p = new HashMap();
		p.put("account", account);
		p.put("password", password);
		List l = this.getUserList(p);
		if(l.size()!=1)
			return null;
		return (User) l.get(0);
	}
	
	/**
	 * 根据用户id或者或者用户信息
	 * */
	public User getUserByUserId(int userId){
		if(userId <= 0)
			return null;
		Map p = new HashMap();
		p.put("id", userId);
		List l = this.getUserList(p);
		if(l.size()!=1)
			return null;
		return (User)l.get(0);
	}
	/**
	 * 根据用户账号获取用户信息
	 * */
	public User getUserByAccount(String account){
		if(account == null || account.trim().equals("") ){
			return null;
		}
		Map p = new HashMap();
		p.put("account", account);
		List l = this.getUserList(p);
		if(l.size()!=1)
			return null;
		return (User)l.get(0);
	}
	
	
	/**
	 * 修改用户的登录信息
	 * */
	private int setUserLoginInfo(int userId,Date thisLoginTime,int loginTimes){
		if(userId <= 0)
			return 0;
		Map p = new HashMap();
		p.put("loginTimes", loginTimes);
		p.put("id", userId);
		p.put("thisLoginTime", thisLoginTime);
		return this.memberDao.updateUser(p);
	}
	
	public List getUserListOrderBy(String orderBy, int page, int pageSize){
		if(orderBy == null || orderBy.trim().equals(""))
			return new ArrayList();
		Map p = new HashMap();
		p.put("orderBy", orderBy);
		if(page < 1)
			page = 1;
		int begin = (page-1)*pageSize;
		int end = pageSize;
		p.put("begin", begin);
		p.put("end", end);
		return this.memberDao.selectUser(p);
	}
	
	/**
	 * 根据参数取得用户列表
	 * @return ArrayList
	 * */
	private List getUserList(Map p){
		return this.memberDao.selectUser(p);
	}
	public void setMemberDao(MemberDao memberDao) {
		this.memberDao = memberDao;
	}	
	
	
}
