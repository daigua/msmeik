package com.msmeik.member.mvc;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.msmeik.jcaptcha.JcaptchaUtil;
import com.msmeik.member.MemberService;
import com.msmeik.member.User;
import com.octo.captcha.service.multitype.GenericManageableCaptchaService;
import com.opensymphony.xwork2.ActionSupport;
import com.orientor.website.tools.MD5EncodeTool;

public class RegeditUser extends ActionSupport{
	private MemberService memberService;
	private String captchaId;
	private String safeCode;
	
	private User user;
	private int ok;
	
	public String execute(){
		user.setPassword(MD5EncodeTool.encode(user.getPassword()));
		user.setRegeditTime(new Date());
		int userId = this.memberService.saveUser(user);
		user.setId(userId);
		this.ok = 9;
		return "input";
	}
	
	public void validate(){
		if(this.hasErrors())
			return;
		if(!JcaptchaUtil.validate(this.captchaId,this.safeCode)){
			this.addFieldError("safeCode", this.getText("safeCode.inputError"));
			return;
		}
		if(this.memberService.getUserCountByAccount(user.getAccount())!=0){
			this.addFieldError("user.account", this.getText("user.account.existed"));
		}
	}
	public int getOk() {
		return ok;
	}


	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}


	public void setCaptchaId(String captchaId) {
		this.captchaId = captchaId;
	}

	public void setSafeCode(String safeCode) {
		this.safeCode = safeCode;
	}

	public String getSafeCode() {
		return safeCode;
	}
	
	
}
