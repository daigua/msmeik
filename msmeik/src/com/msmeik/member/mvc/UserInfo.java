package com.msmeik.member.mvc;

import com.msmeik.authentication.AuthenticationService;
import com.msmeik.member.MemberService;
import com.msmeik.member.User;

public class UserInfo {
	private MemberService memberService;
	private int userId;
	private String account;
	private User user;
	
	public String execute(){		
		if(userId != 0){			
			this.user = this.memberService.getUserByUserId(userId);
		}else
		if(account!=null){
			this.user = this.memberService.getUserByAccount(account);
		}else{
			this.user = this.memberService.getUserByUserId(AuthenticationService.getLoginUser().getId());
		}
		if(this.user == null){
			this.user = new User();
			return "success";
		}
		this.user.setPassword(null);
		if(userId!=0 || account!=null){
			this.user.setName("");
			this.user.setEmail("");
			this.user.setCellphone("");
			this.user.setTelephone("");
		}
		return "success";
	}

	public User getUser() {
		return user;
	}

	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	
	
	
}
