package com.msmeik.member.mvc;

import com.msmeik.authentication.AuthenticationService;
import com.msmeik.member.MemberService;
import com.msmeik.member.User;
import com.opensymphony.xwork2.ActionSupport;

public class UpdateUser extends ActionSupport{
	
	private MemberService memberService;
	private User user;
	private int ok;
	
	public String execute(){
		User loginUser = AuthenticationService.getLoginUser();
		user.setId(loginUser.getId());
		int i = this.memberService.updateUser(loginUser.getId(), user);
		if(i == 0){//如果不存在，则插入
			user.setAccount(loginUser.getAccount());
			this.memberService.saveUser(user);
		}
		this.ok = 9;
		return "input";
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}

	public int getOk() {
		return ok;
	}
	
	
}
