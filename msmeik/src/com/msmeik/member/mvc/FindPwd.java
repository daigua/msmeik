package com.msmeik.member.mvc;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.msmeik.jcaptcha.JcaptchaUtil;
import com.msmeik.member.MemberService;
import com.msmeik.member.User;
import com.msmeik.member.UserFindPasswordStore;
import com.octo.captcha.service.multitype.GenericManageableCaptchaService;
import com.opensymphony.xwork2.ActionSupport;
import com.orientor.website.tools.FreemarkerTool;
import com.orientor.website.tools.MD5EncodeTool;
import com.orientor.website.tools.SendMailTool;

public class FindPwd extends ActionSupport{
	
	private MemberService memberService;
	private String mailFtl;
	private User user;
	private int ok;
	private String safeCode;
	private String captchaId;
	private String from;
	private String subject;
	
	private String securityCode;
	
	private int step;
	
	public String sendMail(){
		if(this.memberService.getUserCountByAccountAndEmail(this.user.getAccount(), this.user.getEmail())==0){
			this.addFieldError("user.account", this.getText("user.account.email.wrong"));
		}else{
			String securityCode = UserFindPasswordStore.add(user.getAccount());
			Map model = new HashMap();
			model.put("securityCode",securityCode);
			String html = FreemarkerTool.getHtml(model, this.mailFtl);
			SendMailTool.sendMimeMail(this.user.getEmail(), from, subject, html);
			this.ok = 9;
		}
		return "input";
	}
	public void validateSendMail(){
		if(this.hasErrors())
			return;
		if(!JcaptchaUtil.validate(this.captchaId,this.safeCode)){
			this.addFieldError("safeCode", this.getText("safeCode.inputError"));
			return;
		}
	}
	public String validateUrl(){
		this.step = 1;
		user = new User();
		user.setAccount(UserFindPasswordStore.getAccountBySecurityCode(securityCode));
		return "success";
	}
	public String modifyPwd(){
		String account = UserFindPasswordStore.getAccountBySecurityCode(securityCode);
		if(account==null){
			this.addFieldError("user.account", this.getText("user.account.email.wrong"));
			return "input";
		}
		this.memberService.updateUserPasswordByAccount(account, MD5EncodeTool.encode(user.getPassword()));
		UserFindPasswordStore.remove(securityCode);
		this.ok = 9;
		return "input";
	}
	
	public void validateModifyPwd(){
		if(this.hasErrors())
			return;
		if(!JcaptchaUtil.validate(this.captchaId,this.safeCode)){
			this.addFieldError("safeCode", this.getText("safeCode.inputError"));
			return;
		}
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getSafeCode() {
		return safeCode;
	}
	public void setSafeCode(String safeCode) {
		this.safeCode = safeCode;
	}
	public int getOk() {
		return ok;
	}
	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}
	public void setMailFtl(String mailFtl) {
		this.mailFtl = mailFtl;
	}
	public void setCaptchaId(String captchaId) {
		this.captchaId = captchaId;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getSecurityCode() {
		return securityCode;
	}
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
	public int getStep() {
		return step;
	}
	
	
}
