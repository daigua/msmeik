package com.msmeik.member.mvc;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.msmeik.jcaptcha.JcaptchaUtil;
import com.msmeik.member.User;
import com.msmeik.ucenter.UserCenterService;
import com.opensymphony.xwork2.ActionSupport;

public class Login extends ActionSupport{
	private User user;
	private String safeCode;
	private String captchaId;
	
	private int ok;
	private String ucsynlogin;//ucenter ͬ����½
	private String loginToUrl = "";
	
	public String execute(){
		boolean writeToCookie = true;
		Map p = UserCenterService.userLoginFromUcenter(user);
		if((Integer)p.get("status") == 1){
			this.ok = 9;
			HttpServletRequest request = ServletActionContext.getRequest();
			String loginToUrl = request.getRequestURI();
			if(request.getQueryString()!=null)
				loginToUrl += "?"+request.getQueryString();
			this.ucsynlogin = (String)p.get("ucsynlogin");
			return "success";
		}else{
			this.addFieldError("user", this.getText("user.notExist"));
		}
		return "input";
	}
	
	public void validate(){
		if(this.hasErrors())
			return;
		if(!JcaptchaUtil.validate(this.captchaId,this.safeCode)){
			this.addFieldError("safeCode", this.getText("safeCode.inputError"));
			return;
		}
	}
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getSafeCode() {
		return safeCode;
	}

	public void setSafeCode(String safeCode) {
		this.safeCode = safeCode;
	}

	public int getOk() {
		return ok;
	}

	public void setCaptchaId(String captchaId) {
		this.captchaId = captchaId;
	}

	public String getUcsynlogin() {
		return ucsynlogin;
	}

	public String getLoginToUrl() {
		return loginToUrl;
	}
	
	
}
