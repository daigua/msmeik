package com.msmeik.member;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.orientor.website.tools.MD5EncodeTool;

public class UserFindPasswordStore {
	private static Map userFindPwdStore = new HashMap();
	
	/**
	 * 保存用户找回密码的账号，并返回生成的安全码
	 * */
	public static String add(String account){
		String securityCodeStr = account+System.currentTimeMillis()+(new Date());
		String securityCode = MD5EncodeTool.encode(securityCodeStr);
		userFindPwdStore.put(securityCode, account);
		return securityCode;
	}
	public static boolean validate(String securityCode){
		if(!userFindPwdStore.containsKey(securityCode))
			return false;
		return true;
	}
	public static String getAccountBySecurityCode(String securityCode){
		Object account = userFindPwdStore.get(securityCode);
		return account==null?null:account.toString();
	}
	public static void remove(String securityCode){
		userFindPwdStore.remove(securityCode);
	}
}
