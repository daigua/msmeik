package com.msmeik.page.module;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ModuleService {
	private ModuleDao moduleDao;
	
	public int addModuleCategory(Module category){
		return this.moduleDao.insertModule(category);
	}
	
	public List getModuleCategoryList(ModuleCategory category){
		Map p = new HashMap();
		p.put("category", category);
		return this.moduleDao.selectModule(p);
	}

	public void setModuleDao(ModuleDao moduleDao) {
		this.moduleDao = moduleDao;
	}
	
	
}
