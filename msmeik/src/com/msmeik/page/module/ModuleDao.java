package com.msmeik.page.module;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class ModuleDao extends SqlMapClientDaoSupport{
	public int insertModule(Object module){
		return (Integer) this.getSqlMapClientTemplate().insert("insertModule", module);
	}
	public int deleteModule(Object p){
		return this.getSqlMapClientTemplate().delete("deleteModule", p);
	}
	public List selectModule(Object p){
		return this.getSqlMapClientTemplate().queryForList("selectModule", p);
	}
}
