package com.msmeik.page.module;

public class Module {
	private int id;
	private String name;
	private int status;//是否读取 此模块下的数据到缓存
	private int type;
	private String moduleKeys;//数据的key 即 id ；多个用空格隔开
	private String key;
	private ModuleCategory category;
	
	public static int TYPE_STORE = 1;
	public static int TYPE_STORE_PROMOTION = 2;
	public static int TYPE_MEMBER = 3;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public ModuleCategory getCategory() {
		return category;
	}
	public void setCategory(ModuleCategory category) {
		this.category = category;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getModuleKeys() {
		return moduleKeys;
	}
	public void setModuleKeys(String moduleKeys) {
		this.moduleKeys = moduleKeys;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	
	
}
