package com.msmeik.credit;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.msmeik.authentication.AuthenticationService;
import com.msmeik.member.User;
import com.msmeik.ucenter.UserCenterService;
import com.msmeik.ucenter.client.Client;

public class CreditService {
	private CreditDao creditDao;
	
	private static Map creditGetCache = new HashMap();

	public int updateCredit(Credit credit,CreditLog creditLog){
		if(credit.getUser() == null || credit.getUser().getId() <= 0)
			return 0;
		int amount = this.getUserCanGetCredit(credit.getUser(), credit.getCredit());
		if(amount == 0)
			return 0;
		if(amount < 0)
			credit.setDelTimes(1);
		else
			credit.setGetTimes(1);
		credit.setCredit(amount);
		creditLog.setCredit(amount);
		creditLog.setUser(credit.getUser());
		creditLog.setLogTime(new Date());
		creditLog.setOperator(AuthenticationService.getLoginUser());
		if(this.creditDao.updateCredit(credit) == 0){
			this.creditDao.insertCredit(credit);
		}
		
		//兑换成家园积分		
		String status = UserCenterService.exchangeMeikeCreditToHomeCredit(credit.getUser().getId(), credit.getCredit(), 0, credit.getCredit());
		if(status.equals("1")){
			this.creditDao.insertCreditLog(creditLog);
		}
		return amount;
	}
	
	public Credit getCredit(User user){
		if(user.getId() <= 0)
			return null;
		Map p = new HashMap();
		p.put("user", user);
		List l = this.creditDao.selectCredit(p);
		if(l.size() != 1)
			return null;
		return (Credit)l.get(0);
	}
	public List getCreditLog(User user, int page, int pageSize){
		if(user.getId() <= 0)
			return new ArrayList();
		Map p = new HashMap();
		p.put("user", user);
		if(page < 1)
			page = 1;
		int begin = (page-1)*pageSize;
		int end = pageSize;
		p.put("begin", begin);
		p.put("end", end);
		return this.creditDao.selectCreditLog(p);
	}
	
	//根据记录获得用户今天还可获得多少积分
	private int getUserCanGetCredit(User user,int credit){
		if(credit <= 0)
			return credit;
		if(credit > CreditConfig.getMaxGetPerDay())
			credit = CreditConfig.getMaxGetPerDay();
		
		if(!creditGetCache.containsKey(user.getId())){
			creditGetCache.put(user.getId(), credit);
			return credit;
		}else{
			int v = (Integer)creditGetCache.get(user.getId());
			if(v+credit > CreditConfig.getMaxGetPerDay())
				credit = CreditConfig.getMaxGetPerDay() - v;
			creditGetCache.put(user.getId(), credit+v);
			return credit;
		}
	}
	
	//清理积分记录缓存
	public void cleanCreditGetCache(){
		CreditService.creditGetCache.clear();
	}
	
	
	public void setCreditDao(CreditDao creditDao) {
		this.creditDao = creditDao;
	}
	
	
}
