package com.msmeik.credit;

import com.msmeik.member.User;

public class Credit {
	private int id;
	private User user;
	private int getTimes;
	private int delTimes;
	private int credit;
	private int totalCredit;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public int getGetTimes() {
		return getTimes;
	}
	public void setGetTimes(int getTimes) {
		this.getTimes = getTimes;
	}
	
	public int getDelTimes() {
		return delTimes;
	}
	public void setDelTimes(int delTimes) {
		this.delTimes = delTimes;
	}
	public int getCredit() {
		return credit;
	}
	public void setCredit(int credit) {
		this.credit = credit;
	}
	public int getTotalCredit() {
		return totalCredit;
	}
	public void setTotalCredit(int totalCredit) {
		this.totalCredit = totalCredit;
	}
	
	
}
