package com.msmeik.credit;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class CreditDao extends SqlMapClientDaoSupport {
	public int insertCredit(Object credit){
		return (Integer)this.getSqlMapClientTemplate().insert("insertCredit", credit);
	}
	public int updateCredit(Object credit){
		return this.getSqlMapClientTemplate().update("updateCredit", credit);
	}
	public List selectCredit(Object p){
		return this.getSqlMapClientTemplate().queryForList("selectCredit", p);
	}
	
	public int insertCreditLog(Object creditLog){
		return (Integer) this.getSqlMapClientTemplate().insert("insertCreditLog", creditLog);
	}
	public int updateCreditLog(Object creditLog){
		return this.getSqlMapClientTemplate().update("updateCreditLog", creditLog);
	}
	public List selectCreditLog(Object p){
		return this.getSqlMapClientTemplate().queryForList("selectCreditLog", p);
	}
}
