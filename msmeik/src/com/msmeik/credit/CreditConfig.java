package com.msmeik.credit;

import java.util.Map;
import java.util.Set;

public class CreditConfig {
	private static Map<String,Integer> creditRule;
	private static int maxGetPerDay;
	
	public static Map<String,Integer> getCreditRule() {
		return creditRule;
	}
	public void setCreditRule(Map creditRule) {
		Set<String> keySet = creditRule.keySet();
		for(String key:keySet){
			creditRule.put(key, Integer.parseInt(creditRule.get(key).toString()));
		}
		CreditConfig.creditRule = creditRule;
	}
	public static int getMaxGetPerDay() {
		return maxGetPerDay;
	}
	public void setMaxGetPerDay(int maxGetPerDay) {
		CreditConfig.maxGetPerDay = maxGetPerDay;
	}
	
	
}
