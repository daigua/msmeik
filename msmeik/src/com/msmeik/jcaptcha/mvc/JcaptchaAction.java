package com.msmeik.jcaptcha.mvc;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.octo.captcha.service.multitype.GenericManageableCaptchaService;
import com.opensymphony.xwork2.ActionSupport;

public class JcaptchaAction extends ActionSupport{
	private GenericManageableCaptchaService captchaService;
	private String captchaId = "";
	public static final String CAPTCHA_IMAGE_FORMAT = "jpeg";
	public InputStream getImage(){
		ByteArrayOutputStream jpegOutputStream = new ByteArrayOutputStream();  
		HttpServletRequest request=ServletActionContext.getRequest();
		this.captchaId += request.getSession().getId();
		BufferedImage challenge = captchaService.getImageChallengeForID(this.captchaId, request.getLocale());
		try {
			ImageIO.write(challenge, CAPTCHA_IMAGE_FORMAT, jpegOutputStream);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ByteArrayInputStream(jpegOutputStream.toByteArray());
	}
	public GenericManageableCaptchaService getCaptchaService() {
		return captchaService;
	}
	public void setCaptchaService(GenericManageableCaptchaService captchaService) {
		this.captchaService = captchaService;
	}
	public String getCaptchaId() {
		return captchaId;
	}
	public void setCaptchaId(String captchaId) {
		this.captchaId = captchaId;
	}
}
