package com.msmeik.jcaptcha;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.octo.captcha.service.multitype.GenericManageableCaptchaService;

public class JcaptchaUtil {
	private static GenericManageableCaptchaService captchaService;
	
	public static boolean validate(String captchaId,String safeCode){
		HttpServletRequest request=ServletActionContext.getRequest();
		captchaId += request.getSession().getId();		
		try {
			return captchaService.validateResponseForID(captchaId, safeCode);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return false;
		}
	}

	public  void setCaptchaService(
			GenericManageableCaptchaService captchaService) {
		JcaptchaUtil.captchaService = captchaService;
	}
	
	
}
