package com.msmeik.request.mvc;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.msmeik.authentication.AuthenticationService;
import com.msmeik.request.RequestLimit;
import com.msmeik.request.RequestService;
import com.msmeik.store.StoreConfig;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.MethodFilterInterceptor;
import com.orientor.website.tools.Struts2WebUtils;

public class LimitIntercetpor extends MethodFilterInterceptor{

	private String keyParams;
	private String scope;
	private int maxTimes;
	private String timeoutExpression;
	private String limitResult;
	private String resultName = "success";
	private String limitReason;
	@Override
	protected String doIntercept(ActionInvocation action) throws Exception {
		// TODO Auto-generated method stub
		HttpServletRequest request = ServletActionContext.getRequest();
		String key = "uri="+request.getRequestURI()+"?";
		if(keyParams!=null){
			for(String param:keyParams.split("&")){
				key += param+"="+request.getParameter(param)+"&";
			}
		}
		if(scope.equals("user")){
			key = "userId="+AuthenticationService.getLoginUser().getId()+"-"+key;
		}
		if(scope.equals("session")){
			key = "sessionId="+request.getSession().getId()+"-"+key;
		}
		if(scope.equals("ip")){
			key = "ip="+Struts2WebUtils.getRequestIp()+"-"+key;
		}
		if(RequestService.checkLimit(key,maxTimes)!=1){
			action.getInvocationContext().getValueStack().set("limitReason", limitReason);
			return limitResult;
		}
		String result = action.invoke();
		if(result.equals(resultName)){
			RequestLimit limit = new RequestLimit();
			String[] timeoutTypes = this.timeoutExpression.split(":");
			int timeoutValues = Integer.parseInt(timeoutTypes[1]);
			Calendar calendar = Calendar.getInstance();
			if(timeoutTypes[0].equals("Day")){
				calendar.add(Calendar.DAY_OF_MONTH, timeoutValues);
				calendar.set(Calendar.HOUR_OF_DAY, 0);
				calendar.set(Calendar.MINUTE, 0);
				calendar.set(Calendar.SECOND, 0);	
			}
			if(timeoutTypes[0].equals("Min")){
				calendar.add(Calendar.MINUTE, timeoutValues);
			}
			limit.setTimeout(calendar.getTime());
			limit.setTimes(1);
			limit.setKey(key);			
			RequestService.recordLimit(limit, this.maxTimes);
		}
		return result;
	}
	public void setLimitReason(String limitReason) {
		this.limitReason = limitReason;
	}
	public void setKeyParams(String keyParams) {
		this.keyParams = keyParams;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public void setMaxTimes(int maxTimes) {
		this.maxTimes = maxTimes;
	}
	public void setTimeoutExpression(String timeoutExpression) {
		this.timeoutExpression = timeoutExpression;
	}
	public void setLimitResult(String limitResult) {
		this.limitResult = limitResult;
	}
	public void setResultName(String resultName) {
		this.resultName = resultName;
	}
	public static void main(String[] args){
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		System.out.println(calendar.getTime());
	}
}
