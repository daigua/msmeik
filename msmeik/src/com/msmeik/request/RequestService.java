package com.msmeik.request;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class RequestService {
	private static Map<String,RequestLimit> requestLimits;
	
	/**
	 * 验证请求限制
	 * @param -1 返回-1说明 还在限制期内。不能操作
	 * @param 1 可以正常操作
	 * */
	public static int checkLimit(String key,int maxTimes){
		Map<String,RequestLimit> limits = RequestService.getRequestLimits();
		RequestLimit rl = limits.get(key);
		if(rl!=null){
			if(rl.getTimeout().after(new Date()) && rl.getTimes() >= maxTimes){
				return -1;
			}
		}
		return 1;
	}
	/**
	 * 记录请求
	 * */
	public static int recordLimit(RequestLimit limit,int maxTimes){
		Map<String,RequestLimit> limits = RequestService.getRequestLimits();
		RequestLimit rl = limits.get(limit.getKey());
		if(rl == null){
			rl = limit;
		}else{
			if(rl.getTimeout().before(new Date())){
				rl = limit;
			}else{
				if(rl.getTimes()+limit.getTimes() <= maxTimes){
					rl.setTimes(rl.getTimes()+limit.getTimes());
				}else{
					return -1;
				}
			}			
		}
		limits.put(limit.getKey(), rl);
		return 1;
	}
	
	/**
	 * 清理限制记录，删除过时的操作记录
	 * */
	public void clean(){
		if(RequestService.getRequestLimits()==null)
			return;
		Set<String> keys = RequestService.getRequestLimits().keySet();
		for(String key:keys){
			RequestLimit rl = RequestService.getRequestLimits().get(key);
			if(rl.getTimeout().before(new Date()))
				RequestService.getRequestLimits().remove(key);
		}
	}
	private static Map<String,RequestLimit> getRequestLimits(){
		if(RequestService.requestLimits == null){
			RequestService.requestLimits = new HashMap();
		}
		return RequestService.requestLimits;
	}
}
