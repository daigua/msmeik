package com.msmeik.site.mvc;

import com.msmeik.site.SiteConfig;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.MethodFilterInterceptor;

/**
 * 用于初始化一些站点基本信息
 * */
public class SiteConfigInterceptor extends MethodFilterInterceptor{

	@Override
	protected String doIntercept(ActionInvocation action) throws Exception {
		// TODO Auto-generated method stub
		
		action.getInvocationContext().getValueStack().set("SiteConfig", new SiteConfig());		
		return action.invoke();
	}

}
