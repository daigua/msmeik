package com.msmeik.mvc;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.MethodFilterInterceptor;

public class DomainJumpInterceptor extends MethodFilterInterceptor{

	@Override
	protected String doIntercept(ActionInvocation action) throws Exception {
		// TODO Auto-generated method stub
		getSubdomain();
		return action.invoke();
	}
	
	private String getSubdomain(){
		HttpServletRequest request = ServletActionContext.getRequest();
		System.out.println(request.getRequestURI());
		return null;
	}
	
	public static void main(String[] arsg){
		String regex = "^(static)";
		String str = "www";
		System.out.println(str.matches(regex));
	}
}
