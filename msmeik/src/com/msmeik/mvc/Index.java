package com.msmeik.mvc;

import java.sql.Date;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import com.msmeik.store.StoreConfig;
import com.msmeik.store.StoreService;
import com.msmeik.store.promotion.PromotionService;
import com.msmeik.store.search.SearchService;

public class Index {
	private StoreService storeService;
	private PromotionService promotionService;
	private SearchService searchService;
	
	private int cache = 0;//是否重新从数据库里面读取 0 不 1 是
	private StoreConfig storeConfig = new StoreConfig();
	
	private int readed;//是否已经读取过
	
	private List storeListByClickTimes;//人气 排行
	private List storeListByCommentTimes;//点评排行
	private List storeListByScore;//美客评分排行
	private List promotionListByClickTimes;//人气
	private List promotionListByAddTime;//最新发布
	private List promotionListByPrintTimes;//打印排行
	private List storeListByClaimTime;//最新认领的餐厅
	
	private List newMemberList;//最新注册会员列表
	
	public String execute(){
		this.readModle();
		return "success";
	}
	
	private void readModle(){
		if(cache == 1 || readed == 0){
			System.out.println("首页正在更新缓存");
			String orderBy = "click_times desc";
			this.storeListByClickTimes = this.storeService.getStoreListOrderBy(orderBy,1,0,1,1,10);
			orderBy = "co.comment_times desc";
			Map searchParam = new HashMap();
			searchParam.put("orderBy", orderBy);
			this.storeListByCommentTimes = this.searchService.searchStore(searchParam, 1, 10);			
			orderBy = "co.taste_score desc,co.comment_times desc";
			searchParam.put("orderBy", orderBy);
			System.out.println("读取最佳餐厅");
			this.storeListByScore = this.searchService.searchStore(searchParam, 1, 10);
			System.out.println("读取完成");
			orderBy = "claim_time desc";
			this.storeListByClaimTime = this.storeService.getStoreListOrderBy(orderBy,1,0,1,1,6);
			Date now = new Date(System.currentTimeMillis());
			orderBy = "add_time desc";
			this.promotionListByAddTime = this.promotionService.getPromotionList(null, -1, now, orderBy, 1, 10);
			orderBy = "click_times desc";
			this.promotionListByClickTimes = this.promotionService.getPromotionList(null, -1, now, orderBy, 1, 10);
			orderBy = "print_times desc";
			this.promotionListByPrintTimes = this.promotionService.getPromotionList(null, 2, now, orderBy, 1, 10);
			orderBy = "regedit_time desc";
			readed = 1;
			cache = 0;
			System.out.println("首页缓存更新成功");
		}else{
			System.out.println("首页读取缓存");
		}
	}

	public StoreConfig getStoreConfig() {
		return storeConfig;
	}

	public List getStoreListByClickTimes() {
		return storeListByClickTimes;
	}

	public List getStoreListByCommentTimes() {
		return storeListByCommentTimes;
	}

	public List getStoreListByScore() {
		return storeListByScore;
	}

	public List getPromotionListByClickTimes() {
		return promotionListByClickTimes;
	}

	public List getPromotionListByAddTime() {
		return promotionListByAddTime;
	}

	public List getPromotionListByPrintTimes() {
		return promotionListByPrintTimes;
	}

	public List getStoreListByClaimTime() {
		return storeListByClaimTime;
	}

	public List getNewMemberList() {
		return newMemberList;
	}

	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}

	public void setPromotionService(PromotionService promotionService) {
		this.promotionService = promotionService;
	}

	public void setCache(int cache) {
		this.cache = cache;
	}

	public void setSearchService(SearchService searchService) {
		this.searchService = searchService;
	}
	
	
}
