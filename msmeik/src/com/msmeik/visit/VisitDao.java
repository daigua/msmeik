package com.msmeik.visit;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class VisitDao extends SqlMapClientDaoSupport{
	
	public int insertVisitLog(Object visitor){
		return (Integer) this.getSqlMapClientTemplate().insert("insertVisitLog", visitor);
	}
	public int updateVisitLog(Object visitor){
		return this.getSqlMapClientTemplate().update("updateVisitLog", visitor);
	}
	public List selectVisitLog(Object p){
		return this.getSqlMapClientTemplate().queryForList("selectVisitLog", p);
	}
}
