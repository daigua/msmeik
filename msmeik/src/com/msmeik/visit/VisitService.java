package com.msmeik.visit;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.msmeik.member.User;

public class VisitService {
	private VisitDao visitDao;
	
	/**
	 * ����visitor
	 * */
	public int saveVisitLog(User visitor,VisitTarget target){
		if(visitor == null || visitor.getId() <= 0 || target == null || target.getKey() <=0 || target.getType() <= 0)
			return 0;
		Map p = new HashMap();
		p.put("visitor", visitor);
		p.put("target",target);
		p.put("visitTimes", 1);
		p.put("visitTime", new Date());
		if(this.visitDao.updateVisitLog(p) == 0){
			VisitLog visitLog = new VisitLog();
			visitLog.setVisitor(visitor);
			visitLog.setTarget(target);
			visitLog.setVisitTime(new Date());
			visitLog.setVisitTimes(1);
			return this.visitDao.insertVisitLog(visitLog);
		}
		return 1;
	}
	
	/**
	 * ȡ��VisitLog
	 * */
	public List getVisitLogList(VisitTarget target,int page, int pageSize){
		if( target == null || target.getKey() <=0 || target.getType() <= 0)
			return new ArrayList();
		Map p = new HashMap();
		p.put("target", target);
		return this.visitDao.selectVisitLog(p);
	}

	public void setVisitDao(VisitDao visitDao) {
		this.visitDao = visitDao;
	}
	
	
}
