package com.msmeik.visit;

import java.util.Date;

import com.msmeik.member.User;

public class VisitLog {
	private int id;
	private User visitor;
	private VisitTarget target;
	private int visitTimes;
	private Date visitTime;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public User getVisitor() {
		return visitor;
	}
	public void setVisitor(User visitor) {
		this.visitor = visitor;
	}
	public VisitTarget getTarget() {
		return target;
	}
	public void setTarget(VisitTarget target) {
		this.target = target;
	}
	public int getVisitTimes() {
		return visitTimes;
	}
	public void setVisitTimes(int visitTimes) {
		this.visitTimes = visitTimes;
	}
	public Date getVisitTime() {
		return visitTime;
	}
	public void setVisitTime(Date visitTime) {
		this.visitTime = visitTime;
	}
	
	
}
