package com.msmeik.store.vas.domain;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class DomainDao extends SqlMapClientDaoSupport{
	public int insertDomain(Object domain){
		return (Integer) this.getSqlMapClientTemplate().insert("insertDomain", domain);
	}
	public int updateDomain(Object p){
		return this.getSqlMapClientTemplate().update("updateDomain", p);
	}
	public int deleteDomain(Object p){
		return this.getSqlMapClientTemplate().delete("deleteDomain", p);
	}
	public List selectDomain(Object p){
		return this.getSqlMapClientTemplate().queryForList("selectDomain", p);
	}
}
