package com.msmeik.store.vas.domain;

import java.util.Date;

import com.msmeik.store.Store;

public class Domain {
	private int id;
	private Store store;
	private String subdomain;//本站二级域名
	private String customDomain;//自定义域名
	private Date customDomainExpiredTime;//自定义域名过期时间
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Store getStore() {
		return store;
	}
	public void setStore(Store store) {
		this.store = store;
	}
	public String getSubdomain() {
		return subdomain;
	}
	public void setSubdomain(String subdomain) {
		this.subdomain = subdomain;
	}
	public String getCustomDomain() {
		return customDomain;
	}
	public void setCustomDomain(String customDomain) {
		this.customDomain = customDomain;
	}
	public Date getCustomDomainExpiredTime() {
		return customDomainExpiredTime;
	}
	public void setCustomDomainExpiredTime(Date customDomainExpiredTime) {
		this.customDomainExpiredTime = customDomainExpiredTime;
	}
	
	
}
