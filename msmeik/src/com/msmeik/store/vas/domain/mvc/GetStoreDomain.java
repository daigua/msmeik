package com.msmeik.store.vas.domain.mvc;

import com.msmeik.store.Store;
import com.msmeik.store.vas.domain.Domain;
import com.msmeik.store.vas.domain.DomainService;

public class GetStoreDomain {
	private DomainService domainService;
	private int storeId;
	private Domain domain;
	
	public String execute(){
		Store store = new Store();
		store.setId(storeId);
		this.domain = this.domainService.getDomainByStore(store);
		if(domain == null)
			domain = new Domain();
		return "success";
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomainService(DomainService domainService) {
		this.domainService = domainService;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}
	
	
}
