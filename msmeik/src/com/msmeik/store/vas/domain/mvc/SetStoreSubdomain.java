package com.msmeik.store.vas.domain.mvc;

import com.msmeik.store.Store;
import com.msmeik.store.vas.domain.DomainService;
import com.opensymphony.xwork2.ActionSupport;

public class SetStoreSubdomain extends ActionSupport{
	
	private DomainService domainService;
	private int storeId;
	private String subdomain;
	private int ok;
	
	public String execute(){
		Store store = new Store();
		store.setId(storeId);
		this.ok = this.domainService.setStoreSubdomain(store, subdomain);
		return "input";
	}

	public int getStoreId() {
		return storeId;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	
	public String getSubdomain() {
		return subdomain;
	}

	public void setSubdomain(String subdomain) {
		this.subdomain = subdomain;
	}

	public int getOk() {
		return ok;
	}

	public void setDomainService(DomainService domainService) {
		this.domainService = domainService;
	}
	
	
}
