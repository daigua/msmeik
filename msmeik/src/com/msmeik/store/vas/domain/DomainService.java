package com.msmeik.store.vas.domain;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.msmeik.store.Store;
import com.msmeik.store.StoreService;

public class DomainService {
	private DomainDao domainDao;
	private StoreService storeService;
	
	/**
	 * 设置店铺的2级域名
	 * @return -1 表示此域名被阻止注册
	 * @return -2 此域名已经存在
	 * @return >0 插入的id
	 * */
	public int setStoreSubdomain(Store store, String subdomain){
		if(store.getId() <= 0 || subdomain == null || subdomain.trim().equals(""))
			return 0;
		//判断此域名是否被阻止注册
		if(!DomainConfig.checkSubdomain(subdomain))
			return -1;
		if(this.getDomainBySubdomain(subdomain) != null)
			return -2;
		int key = this.insertStoreSubdomain(store, subdomain);
		//更新2级域名到store
		this.storeService.setStoreSubdomain(store,subdomain);
		return key;
	}
	
	/**
	 * 根据2级域名取得域名
	 * */
	public Domain getDomainBySubdomain(String subdomain){
		if(subdomain == null || subdomain.trim().equals(""))
			return null;
		Map p = new HashMap();
		p.put("subdomain", subdomain);
		List l = this.domainDao.selectDomain(p);
		if(l.size() != 1)
			return null;
		return (Domain)l.get(0);
	}
	
	/**
	 * 更新店铺的2级域名
	 * */
	public int updateStoreSubdomain(Store store, String subdomain){
		if(store.getId() <= 0 || subdomain == null || subdomain.trim().equals(""))
			return 0;
		Map p = new HashMap();
		p.put("store", store);
		p.put("subdomain", subdomain);
		int i = this.domainDao.updateDomain(p);
		this.storeService.setStoreSubdomain(store, subdomain);
		return i;
	}
	
	/**
	 * 插入店铺的2级域名
	 * */
	private int insertStoreSubdomain(Store store, String subdomain){
		if(store.getId() <= 0 || subdomain == null || subdomain.trim().equals(""))
			return 0;
		Map p = new HashMap();
		p.put("store", store);
		p.put("subdomain", subdomain);
		return this.domainDao.insertDomain(p);
	}
	
	/**
	 * 根据店铺获取domain
	 * */
	public Domain getDomainByStore(Store store){
		if(store.getId() <= 0)
			return null;
		Map p = new HashMap();
		p.put("store", store);
		List l = this.domainDao.selectDomain(p);
		if(l.size() != 1)
			return null;
		return (Domain)l.get(0);
	}
	
	/**
	 * 根据自定义域名取得Domain
	 * */
	public Domain getDomainByCustomDomain(String customDomain){
		if(customDomain == null || customDomain.trim().equals(""))
			return null;
		Map p = new HashMap();
		p.put("customDomain", customDomain);
		List l = this.domainDao.selectDomain(p);
		if(l.size() != 1)
			return null;
		return (Domain)l.get(0);
	}

	public void setDomainDao(DomainDao domainDao) {
		this.domainDao = domainDao;
	}

	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}
	
	
}
