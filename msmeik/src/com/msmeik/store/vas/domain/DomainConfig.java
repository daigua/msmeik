package com.msmeik.store.vas.domain;

import java.util.ArrayList;
import java.util.List;

public class DomainConfig {
	private static List rejectSubdomains = new ArrayList();//不能注册的本站二级域名
	
	/**
	 * 判断2级域名是否能被注册
	 * @return true 能 
	 * @return false 不能
	 * */
	public static boolean checkSubdomain(String domain){//判断二级域名能不能被注册
		//System.out.println(rejectSubdomains);
		if(rejectSubdomains.contains(domain))
			return false;
		return true;
	}
	
	public void setRejectSubdomains(String domains){
		domains = this.replace(domains);
		for(String domain:domains.split(",")){
			DomainConfig.rejectSubdomains.add(domain);
		}
	}
	
	private String replace(String str){
		return str.trim().replaceAll("\\s{1,}", "");
	}
}
