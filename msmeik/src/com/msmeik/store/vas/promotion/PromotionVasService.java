package com.msmeik.store.vas.promotion;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.msmeik.store.Store;
import com.orientor.website.tools.UtilTools;

public class PromotionVasService {
	private PromotionVasDao promotionVasDao;
	
	/**
	 * 添加推广
	 * 如果此推广已经存在，则返回1,并修改店铺的推广信息
	 * */
	public int addPromotionVas(PromotionVas pv){
		if(pv.getStore().getId() <= 0)
			return 0;
		if(this.updatePromotionVas(pv) != 0){
			return 1;
		}
		return this.promotionVasDao.insertPromotionVas(pv);
	}
	/**
	 * 更新修改推广
	 * */
	public int updatePromotionVas(PromotionVas pv){
		if(pv.getStore().getId() <= 0)
			return 0;
		return this.promotionVasDao.updatePromotionVas(pv);
	}
	/**
	 * 取得推广List
	 * */
	public List getPromotionVasList(Map p,int page, int pageSize){	
		if(page < 1)
			page = 1;
		int begin = (page-1)*pageSize;
		int end = pageSize;
		p.put("begin", begin);
		p.put("end", end);
		return this.promotionVasDao.selectPromotionVas(p);
	}
	/**
	 * 取得推广的数量
	 * */
	public int getPromotionVasCount(Map p){
		return this.promotionVasDao.selectPromotionCount(p);
	}
	
	/**
	 * 根据店铺ID取得店铺推广信息
	 * */
	public PromotionVas getPromotionVasByStore(Store store){
		if(store.getId() <= 0)
			return null;
		Map p = new HashMap();
		p.put("store", store);
		List l = this.promotionVasDao.selectPromotionVas(p);
		if(l.size()!=1)
			return null;
		return (PromotionVas) l.get(0);
	}
	
	/**
	 * 删除推广
	 * */
	public int deletePromotionVas(int id){
		if(id <= 0)
			return 0;
		Map p = new HashMap();
		p.put("id", id);
		return this.promotionVasDao.deletePromotionVas(p);
	}
	/**
	 * 设置推广状态
	 * */
	public int setPromotionVasStatus(int id, int status){
		if(id <= 0)
			return 0;
		Map p = new HashMap();
		p.put("id", id);
		p.put("status", status);
		return this.promotionVasDao.updatePromotionVas(p);
	}

	public void setPromotionVasDao(PromotionVasDao promotionVasDao) {
		this.promotionVasDao = promotionVasDao;
	}
	
	
}
