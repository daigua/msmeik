package com.msmeik.store.vas.promotion;

public class PromotionTarget {
	private int type;
	private int key;
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getKey() {
		return key;
	}
	public void setKey(int key) {
		this.key = key;
	}
	
}
