package com.msmeik.store.vas.promotion.admin.mvc;

import com.msmeik.store.vas.promotion.PromotionVasService;


/**
 * 管理promotionVas 
 * 包括
 * 1、设置推广  关闭 与 正常的状态
 * 2、删除推广
 * */
public class ManagePromotionVas {
	private PromotionVasService promotionVasService;
	private int ok;
	private String p;
	private String v;
	private int pvId;
	
	public String execute(){
		if(p.equals("delete")){
			this.ok = this.promotionVasService.deletePromotionVas(pvId);
		}
		if(p.equals("status")){
			this.ok = this.promotionVasService.setPromotionVasStatus(pvId, Integer.parseInt(v));
		}
		return "success";
	}

	public String getP() {
		return p;
	}

	public void setP(String p) {
		this.p = p;
	}

	public String getV() {
		return v;
	}

	public void setV(String v) {
		this.v = v;
	}

	public int getPvId() {
		return pvId;
	}

	public void setPvId(int pvId) {
		this.pvId = pvId;
	}

	public int getOk() {
		return ok;
	}

	public void setPromotionVasService(PromotionVasService promotionVasService) {
		this.promotionVasService = promotionVasService;
	}
	
	
}
