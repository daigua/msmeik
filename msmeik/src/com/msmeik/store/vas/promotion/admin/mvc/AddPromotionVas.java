package com.msmeik.store.vas.promotion.admin.mvc;

import com.msmeik.store.vas.promotion.PromotionVas;
import com.msmeik.store.vas.promotion.PromotionVasService;
import com.opensymphony.xwork2.ActionSupport;

public class AddPromotionVas extends ActionSupport{
	private PromotionVasService promotionVasService;
	private PromotionVas pv;
	private int ok;
	
	public String execute(){
		
		this.ok = this.promotionVasService.addPromotionVas(pv);
		
		return "input";
	}

	public PromotionVas getPv() {
		return pv;
	}

	public void setPv(PromotionVas pv) {
		this.pv = pv;
	}

	public int getOk() {
		return ok;
	}

	public void setPromotionVasService(PromotionVasService promotionVasService) {
		this.promotionVasService = promotionVasService;
	}
	
	
}
