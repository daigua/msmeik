package com.msmeik.store.vas.promotion.admin.mvc;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.msmeik.store.Store;
import com.msmeik.store.vas.promotion.PromotionTarget;
import com.msmeik.store.vas.promotion.PromotionVasService;

/**
 * 推广列表，即推广查询
 * */
public class PromotionVasList {
	private PromotionVasService promotionVasService;
	private List promotionVasList = new ArrayList();
	
	private Store store = new Store();
	private PromotionTarget target = new PromotionTarget();
	private Date beginTime;
	private Date endTime;
	private String keywords;
	private int status;
	private int page = 1;
	private int pageSize = 20;
	private int totalCount;
	private int searchBack;
	
	public String execute(){
		if(searchBack == 0)
			return "success";
		Map p = new HashMap();
		p.put("store", store);
		p.put("target", target);
		p.put("beginTime", beginTime);
		p.put("endTime",endTime);
		p.put("status", status);
		p.put("keywords", keywords);
		this.promotionVasList = this.promotionVasService.getPromotionVasList(p, page, pageSize);
		this.totalCount = this.promotionVasService.getPromotionVasCount(p);
		return "success";
	}

	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}

	public PromotionTarget getTarget() {
		return target;
	}

	public void setTarget(PromotionTarget target) {
		this.target = target;
	}

	

	

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public Date getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List getPromotionVasList() {
		return promotionVasList;
	}

	public void setPromotionVasService(PromotionVasService promotionVasService) {
		this.promotionVasService = promotionVasService;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setSearchBack(int searchBack) {
		this.searchBack = searchBack;
	}

	
	
}
