package com.msmeik.store.vas.promotion;

import java.util.Date;

import com.msmeik.store.Store;


/**
 * 餐厅推广
 * */
public class PromotionVas {
	private int id;
	private Store store;
	private PromotionTarget target;//推广的目标	
	private String keywords;
	private Date beginTime;
	private Date endTime;
	private int priority;//推广优先级 根据推广付费来设置，一般为付费的金额
	private int status;//状态 1正常 2 关闭
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public PromotionTarget getTarget() {
		return target;
	}
	public void setTarget(PromotionTarget target) {
		this.target = target;
	}	
	public Date getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public Store getStore() {
		return store;
	}
	public void setStore(Store store) {
		this.store = store;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}	
	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	
	
	
}
