package com.msmeik.store.vas.promotion.manage;

import com.msmeik.store.Store;
import com.msmeik.store.vas.promotion.PromotionVas;
import com.msmeik.store.vas.promotion.PromotionVasService;

public class StorePromotionVasInfo {
	private PromotionVasService promotionVasService;
	
	private int storeId;
	
	private PromotionVas promotionVas;
	
	public String execute(){
		Store store = new Store();
		store.setId(storeId);
		this.promotionVas = this.promotionVasService.getPromotionVasByStore(store);
		return "success";
	}

	public int getStoreId() {
		return storeId;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public PromotionVas getPromotionVas() {
		return promotionVas;
	}

	public void setPromotionVasService(PromotionVasService promotionVasService) {
		this.promotionVasService = promotionVasService;
	}
	
	
}
