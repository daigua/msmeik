package com.msmeik.store.vas.promotion.manage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.msmeik.store.Store;
import com.msmeik.store.vas.promotion.PromotionVas;
import com.msmeik.store.vas.promotion.PromotionVasService;

public class StorePromotionVasList {
	private PromotionVasService promotionVasService;
	
	private int storeId;
	
	private List promotionVasList;
	
	public String execute(){
		Store store = new Store();
		store.setId(storeId);
		Map p = new HashMap();
		p.put("store", store);
		this.promotionVasList = this.promotionVasService.getPromotionVasList(p, 1, 20);
		return "success";
	}

	public int getStoreId() {
		return storeId;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	

	public List getPromotionVasList() {
		return promotionVasList;
	}

	public void setPromotionVasService(PromotionVasService promotionVasService) {
		this.promotionVasService = promotionVasService;
	}
	
	
}
