package com.msmeik.store.vas.promotion;

import java.util.HashMap;
import java.util.Map;

import com.orientor.website.tools.UtilTools;

public class PromotionConfig {
	/**
	 * 推广的目标种类
	 * */
	private static Map<String,Integer> targetTypes = new HashMap();
	
	/**
	 * 推广分类的种类
	 * */
	private static Map<String,Integer> categoryTypes = new HashMap();
	
	
	public void setTargetTypes(String targetTypes){
		targetTypes = UtilTools.replaceWhite(targetTypes);
		for(String tts:targetTypes.split(",")){
			String[] args = tts.split(":");
			PromotionConfig.targetTypes.put(args[0], Integer.parseInt(args[1]));
		}
	}
	public void setCategoryTypes(String categoryTypes){
		categoryTypes = UtilTools.replaceWhite(categoryTypes);
		for(String tts:categoryTypes.split(",")){
			String[] args = tts.split(":");
			PromotionConfig.categoryTypes.put(args[0], Integer.parseInt(args[1]));
		}
	}
	
}
