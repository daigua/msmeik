package com.msmeik.store.vas.promotion;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class PromotionVasDao extends SqlMapClientDaoSupport{
	
	public int insertPromotionVas(Object pv){
		return (Integer) this.getSqlMapClientTemplate().insert("insertPromotionVas", pv);
	}
	public int updatePromotionVas(Object pv){
		return this.getSqlMapClientTemplate().update("updatePromotionVas", pv);
	}
	public List selectPromotionVas(Object p){
		return this.getSqlMapClientTemplate().queryForList("selectPromotionVas", p);
	}
	public int selectPromotionCount(Object p){
		return (Integer)this.getSqlMapClientTemplate().queryForObject("selectPromotionVasCount", p);
	}
	public int deletePromotionVas(Object p){
		return this.getSqlMapClientTemplate().delete("deletePromotionVas", p);
	}
}
