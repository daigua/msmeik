package com.msmeik.store;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StoreConfig {
	public static List<Map> diningTypes = new ArrayList();//就餐类型
	public static Map diningTypesMap = new HashMap();
	public static List<Map> foodTypes = new ArrayList();//菜系
	public static Map foodTypesMap = new HashMap();
	public static List<Map> cityAreas = new ArrayList();//城市区域
	public static Map cityAreasMap = new HashMap();
	public static List<Map> cbdAreas = new ArrayList();//商区
	public static Map cbdAreasMap = new HashMap();
	public static List<Map> landmarks = new ArrayList();//地标
	public static Map landmarksMap = new HashMap();
	public static int claimLimit = 10;
	public static List rejectStoreDomains = new ArrayList();
	
	public List getDiningTypeList() {
		return StoreConfig.diningTypes;
	}
	public  void setDiningTypes(String diningTypes) {
		diningTypes = this.replace(diningTypes);		
		for(String exp:diningTypes.split(",")){
			String[] expAry = exp.split("\\|");
			Map p = new HashMap();
			p.put("id", Integer.parseInt(expAry[0]));
			p.put("name", expAry[1]);
			StoreConfig.diningTypes.add(p);
			diningTypesMap.put(expAry[0], expAry[1]);
		}
	}
	public List getFoodTypeList() {
		return StoreConfig.foodTypes;
	}
	public  void setFoodTypes(String foodTypes) {	
		foodTypes = this.replace(foodTypes);
		for(String exp:foodTypes.split(",")){
			String[] expAry = exp.split("\\|");
			Map p = new HashMap();
			p.put("id", Integer.parseInt(expAry[0]));
			p.put("name", expAry[1]);
			StoreConfig.foodTypes.add(p);
			foodTypesMap.put(expAry[0], expAry[1]);
		}
	}	
		
	public List<Map> getCityAreaList() {
		return cityAreas;
	}
	public void setCityAreas(String cityAreas) {
		cityAreas = this.replace(cityAreas);
		for(String exp:cityAreas.split(",")){
			String[] expAry = exp.split("\\|");
			Map p = new HashMap();
			p.put("id", Integer.parseInt(expAry[0]));
			p.put("name", expAry[1]);
			StoreConfig.cityAreas.add(p);
			cityAreasMap.put(expAry[0], expAry[1]);
		}
	}
	public List<Map> getCbdAreaList() {
		return cbdAreas;
	}
	public void setCbdAreas(String cbdAreas) {
		cbdAreas = this.replace(cbdAreas);
		for(String exp:cbdAreas.split(",")){
			String[] expAry = exp.split("\\|");
			Map p = new HashMap();
			p.put("id", Integer.parseInt(expAry[0]));
			p.put("name", expAry[1]);
			StoreConfig.cbdAreas.add(p);
			cbdAreasMap.put(expAry[0], expAry[1]);
		}
	}
	
	public List<Map> getLandmarkList(){
		return landmarks;
	}
	
	public void setLandmarks(String landmarks) {
		landmarks = this.replace(landmarks);
		for(String exp:landmarks.split(",")){
			String[] expAry = exp.split("\\|");
			Map p = new HashMap();
			p.put("id", Integer.parseInt(expAry[0]));
			p.put("name", expAry[1]);
			StoreConfig.landmarks.add(p);
			landmarksMap.put(expAry[0], expAry[1]);
		}
	}
	public int getClaimLimit() {
		return claimLimit;
	}
	public void setClaimLimit(int claimLimit) {
		StoreConfig.claimLimit = claimLimit;
	}
	
		
	
	public List getRejectStoreDomainList() {
		return rejectStoreDomains;
	}
	public void setRejectStoreDomains(String rejectStoreDomains) {
		rejectStoreDomains = rejectStoreDomains.trim().replaceAll("\\s{1,}", " ");
		for(String domain:rejectStoreDomains.split(" ")){			
			StoreConfig.rejectStoreDomains.add(domain.trim());
		}
	}
	
	public static boolean checkDomainIfReject(String domain){
		if(domain==null || domain.equals(""))
			return true;
		if(StoreConfig.rejectStoreDomains.contains(domain))
			return true;
		return false;
	}
	
	public static String getCityAreaNameById(int id){
		return (String)cityAreasMap.get(Integer.toString(id));
	}
	public static String getCbdAreaNameById(int id){
		return (String) cbdAreasMap.get(Integer.toString(id));
	}
	public static String getFoodTypeNameById(int id){
		return (String) foodTypesMap.get(Integer.toString(id));
	}
	public static String getDiningTypeNameById(int id){
		return (String) diningTypesMap.get(Integer.toString(id));
	}
	public static String getLandmarkNameById(int id){
		return (String) landmarksMap.get(Integer.toString(id));
	}
	
	private String replace(String str){
		return str.trim().replaceAll("\\s{1,}", "");
	}
	
}

