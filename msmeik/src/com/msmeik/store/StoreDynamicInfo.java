package com.msmeik.store;

public class StoreDynamicInfo {
	
	private int id;
	private int storeId;
	private int hits;//点击 人气
	private int comeBack;//回头客
	private int goAway;//不会再来
	private int willTry;//想试一试
	private String customerImpression;
	private String customerRecommend;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getStoreId() {
		return storeId;
	}
	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}
	public int getHits() {
		return hits;
	}
	public void setHits(int hits) {
		this.hits = hits;
	}
	public int getComeBack() {
		return comeBack;
	}
	public void setComeBack(int comeBack) {
		this.comeBack = comeBack;
	}
	public int getGoAway() {
		return goAway;
	}
	public void setGoAway(int goAway) {
		this.goAway = goAway;
	}
	public int getWillTry() {
		return willTry;
	}
	public void setWillTry(int willTry) {
		this.willTry = willTry;
	}
	public String getCustomerImpression() {
		return customerImpression;
	}
	public void setCustomerImpression(String customerImpression) {
		this.customerImpression = customerImpression;
	}
	public String getCustomerRecommend() {
		return customerRecommend;
	}
	public void setCustomerRecommend(String customerRecommend) {
		this.customerRecommend = customerRecommend;
	}
	
	
}
