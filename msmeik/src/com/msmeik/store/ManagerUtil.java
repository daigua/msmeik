package com.msmeik.store;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;

import com.msmeik.authentication.AuthenticationRegister;
import com.msmeik.authentication.AuthenticationService;
import com.msmeik.member.User;

public class ManagerUtil {
	
	private static StoreService storeService;
	
	public static boolean checkManager(){
		User user = AuthenticationService.getLoginUser();
		
		if(AuthenticationRegister.checkAuth("admin", user))
			return true;
		String storeIdObj = ServletActionContext.getRequest().getParameter("storeId");
		if(storeIdObj == null)
			storeIdObj = ServletActionContext.getRequest().getParameter("store.id");
		if(storeIdObj == null)
			return false;
		int storeId = Integer.parseInt(storeIdObj);
		Map session = ServletActionContext.getContext().getSession();
		if(session.get("myStoreList")==null){
			List myStoreList = new ArrayList();
			if(ManagerUtil.storeService.checkIfStoreBelongedToUser(storeId, user.getId())){
				myStoreList.add(storeId);
				session.put("myStoreList", myStoreList);
				return true;
			}else{
				return false;
			}
		}else{
			List myStoreList = (ArrayList)session.get("myStoreList");
			if(myStoreList.contains(storeId)){
				return true;
			}else{
				if(ManagerUtil.storeService.checkIfStoreBelongedToUser(storeId, user.getId())){
					myStoreList.add(storeId);
					return true;
				}else{
					return false;
				}
			}
		}
	}

	public void setStoreService(StoreService storeService) {
		ManagerUtil.storeService = storeService;
	}
	
	
}
