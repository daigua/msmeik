package com.msmeik.store.admin.mvc;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.msmeik.member.User;
import com.msmeik.store.Store;
import com.msmeik.store.StoreService;

public class StoreClaimList {
	private StoreService storeService;
	
	private List storeClaimList = new ArrayList();
	private Store store = new Store();
	private User user = new User();
	private String claimResult;
	private Date claimBeginTime;
	private Date claimEndTime;
	private String storeNameLike;
	
	private int totalCount;
	private int page = 1 ;
	private int pageSize = 10;
	
	private int search;

	public String execute(){
		if(search == 0){
			return "success";
		}			
		this.storeClaimList = this.storeService.getStoreClaimList(store, storeNameLike, user, claimBeginTime, claimEndTime, claimResult, page, pageSize, "id asc");
		this.totalCount = this.storeService.getStoreClaimCount(store, storeNameLike, user, claimBeginTime, claimEndTime, claimResult);
		return "success";
	}

	public List getStoreClaimList() {
		return storeClaimList;
	}

	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}

	public void setStore(Store store) {
		this.store = store;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setClaimResult(String claimResult) {
		this.claimResult = claimResult;
	}

	public void setClaimBeginTime(Date claimBeginTime) {
		this.claimBeginTime = claimBeginTime;
	}

	public void setClaimEndTime(Date claimEndTime) {
		this.claimEndTime = claimEndTime;
	}

	public void setStoreNameLike(String storeNameLike) {
		this.storeNameLike = storeNameLike;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getTotalCount() {
		return totalCount;
	}
	public void setSearch(int search) {
		this.search = search;
	}

	public Store getStore() {
		return store;
	}

	public User getUser() {
		return user;
	}

	public String getClaimResult() {
		return claimResult;
	}

	public Date getClaimBeginTime() {
		return claimBeginTime;
	}

	public Date getClaimEndTime() {
		return claimEndTime;
	}

	public String getStoreNameLike() {
		return storeNameLike;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	
}
