package com.msmeik.store.admin.mvc;

import com.msmeik.credit.Credit;
import com.msmeik.credit.CreditConfig;
import com.msmeik.credit.CreditLog;
import com.msmeik.credit.CreditService;
import com.msmeik.member.User;
import com.msmeik.store.StoreService;

public class VerifyStore {
	private StoreService storeService;
	private CreditService creditService;
	private String verify;
	private int storeId;
	private int ok;
	private User user;
	
	public String execute(){
		this.ok = this.storeService.verifyStore(storeId, verify);
		System.out.println(verify.equals("1"));
		if(verify.equals("1")){
			//获得积分
			Credit credit = new Credit();
			int amount = CreditConfig.getCreditRule().get("registerStore");
			credit.setCredit(amount);
			credit.setUser(user);
			CreditLog creditLog = new CreditLog();
			creditLog.setTitle("您登记的店铺通过审核，您获得"+amount+"积分");
			this.creditService.updateCredit(credit, creditLog);
			return "success";
		}
		return "failure";
	}

	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}

	public void setVerify(String verify) {
		this.verify = verify;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public int getOk() {
		return ok;
	}

	public void setCreditService(CreditService creditService) {
		this.creditService = creditService;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
}
