package com.msmeik.store.admin.mvc;

import com.msmeik.store.StoreClaim;
import com.msmeik.store.StoreService;

public class VerifyStoreClaim {
	private StoreService storeService;
	
	private StoreClaim storeClaim;
	private int ok;
	
	public String execute(){
		this.ok = this.storeService.verifyStoreClaim(storeClaim);
		return "success";
	}

	public int getOk() {
		return ok;
	}

	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}

	public void setStoreClaim(StoreClaim storeClaim) {
		this.storeClaim = storeClaim;
	}

	public StoreClaim getStoreClaim() {
		return storeClaim;
	}
	
}
