package com.msmeik.store.admin.mvc;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.msmeik.member.User;
import com.msmeik.store.StoreService;

public class StoreList {
	private StoreService storeService;
	
	private List storeList;
	private String storeName;
	private String nameLike;
	private User registrant = new User();
	private User owner = new User();
	private Date registerBeginTime;
	private Date registerEndTime;
	private Date claimBeginTime;
	private Date claimEndTime;
	private String verify;
	private int search;
	
	private int page=1;
	private int pageSize=10;
	
	private int totalCount;
	
	/**
	 * 有如下几种查找店铺的方式
	 * */
	public String execute(){
		if(search==0){
			this.storeList = new ArrayList();
			return "success";
		}
		this.storeList = this.storeService.getStoreList(storeName, nameLike, registrant, owner, registerBeginTime, registerEndTime, claimBeginTime, claimEndTime, verify,page,pageSize,"id desc");
		this.totalCount = this.storeService.getStoreCount(storeName, nameLike, registrant, owner, registerBeginTime, registerEndTime, claimBeginTime, claimEndTime, verify);
		return "success";
	}

	public List getStoreList() {
		return storeList;
	}

	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public void setNameLike(String nameLike) {
		this.nameLike = nameLike;
	}

	public void setRegistrant(User registrant) {
		this.registrant = registrant;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public void setRegisterBeginTime(Date registerBeginTime) {
		this.registerBeginTime = registerBeginTime;
	}

	public void setRegisterEndTime(Date registerEndTime) {
		this.registerEndTime = registerEndTime;
	}

	public void setClaimBeginTime(Date claimBeginTime) {
		this.claimBeginTime = claimBeginTime;
	}

	public void setClaimEndTime(Date claimEndTime) {
		this.claimEndTime = claimEndTime;
	}

	public void setVerify(String verify) {
		this.verify = verify;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setSearch(int search) {
		this.search = search;
	}

	public StoreService getStoreService() {
		return storeService;
	}

	public String getStoreName() {
		return storeName;
	}

	public String getNameLike() {
		return nameLike;
	}

	public User getRegistrant() {
		return registrant;
	}

	public User getOwner() {
		return owner;
	}

	public Date getRegisterBeginTime() {
		return registerBeginTime;
	}

	public Date getRegisterEndTime() {
		return registerEndTime;
	}

	public Date getClaimBeginTime() {
		return claimBeginTime;
	}

	public Date getClaimEndTime() {
		return claimEndTime;
	}

	public String getVerify() {
		return verify;
	}

	public void setStoreList(List storeList) {
		this.storeList = storeList;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	
	
	
}
