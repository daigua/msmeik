package com.msmeik.store.admin.mvc;

import com.msmeik.store.StoreService;

public class DeleteStore {
	private int storeId;
	private StoreService storeService;
	
	private int ok;
	
	public String execute(){
		this.ok = storeService.deleteStore(storeId);
		return "success";
	}

	public int getOk() {
		return ok;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}
	
	
}
