package com.msmeik.store.relish;

import com.msmeik.store.Store;

public class SetMealCategory {
	private int id;
	private String name;
	private Store store;
	private int orderIndex;
	private int setMealNum;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Store getStore() {
		return store;
	}
	public void setStore(Store store) {
		this.store = store;
	}
	public int getOrderIndex() {
		return orderIndex;
	}
	public void setOrderIndex(int orderIndex) {
		this.orderIndex = orderIndex;
	}
	public int getSetMealNum() {
		return setMealNum;
	}
	public void setSetMealNum(int setMealNum) {
		this.setMealNum = setMealNum;
	}
	
	
}
