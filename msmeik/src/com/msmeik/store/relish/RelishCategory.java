package com.msmeik.store.relish;

import com.msmeik.store.Store;

public class RelishCategory {
	private int id;
	private String name;
	private int relishNum;
	private int orderIndex;
	private Store store;//�ĸ����̵�
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Store getStore() {
		return store;
	}
	public void setStore(Store store) {
		this.store = store;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getRelishNum() {
		return relishNum;
	}
	public void setRelishNum(int relishNum) {
		this.relishNum = relishNum;
	}
	public int getOrderIndex() {
		return orderIndex;
	}
	public void setOrderIndex(int orderIndex) {
		this.orderIndex = orderIndex;
	}
	
	
}
