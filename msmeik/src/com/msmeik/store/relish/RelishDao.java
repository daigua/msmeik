package com.msmeik.store.relish;

import java.util.HashMap;
import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class RelishDao extends SqlMapClientDaoSupport{
	
	/**
	 * 添加美食分类并返回id
	 * */
	public int insertRelishCategory(Object relishCategory){
		return (Integer) this.getSqlMapClientTemplate().insert("insertRelishCategory", relishCategory);
	}
	
	/**
	 * 取得美食分类列表
	 * */
	public List selectRelishCategoryList(Object p){
		return this.getSqlMapClientTemplate().queryForList("selectRelishCategory", p);
	}
	
	/**
	 * 取得分类的数量
	 * */
	public int selectRelishCategoryCount(Object p){
		return (Integer)this.getSqlMapClientTemplate().queryForObject("selectRelishCategoryCount", p);
	}
	
	/**
	 * 删除美食分类
	 * */
	public int deleteRelishCategory(Object p){
		return this.getSqlMapClientTemplate().delete("deleteRelishCategory", p);
	}
	
	/**
	 * 更新美食分类
	 * */
	public int updateRelishCategory(Object relishCategory){
		return this.getSqlMapClientTemplate().update("updateRelishCategory", relishCategory);
	}
	
	/**
	 * 添加美食
	 * */
	public int insertRelish(Object relish){
		return (Integer) this.getSqlMapClientTemplate().insert("insertRelish", relish);
	}
	
	/**
	 * 取得美食
	 * */
	public List selectRelish(Object p){
		return this.getSqlMapClientTemplate().queryForList("selectRelish", p);
	}
	
	/**
	 * 取得美食的数量
	 * */
	public int selectRelishCount(Object p){
		return (Integer) this.getSqlMapClientTemplate().queryForObject("selectRelishCount", p);
	}
	
	/**
	 * 更新美食信息
	 * */
	public int updateRelish(Object relish){
		return this.getSqlMapClientTemplate().update("updateRelish", relish);
	}
	
	/**
	 * 删除美食
	 * */
	public int deleteRelish(Object p){
		return this.getSqlMapClientTemplate().delete("deleteRelish", p);
	}
	
	/**
	 * 统计美味的数量。分别从招牌，推荐，美客推荐，优惠方面
	 * */
	public HashMap countRelishNum(Object p){
		return (HashMap) this.getSqlMapClientTemplate().queryForObject("countRelishNum", p);
	}
	
	/**
	 * 根据分类统计美味数量
	 * */
	public List countRelishGroupByCategory(Object p){
		return this.getSqlMapClientTemplate().queryForList("countRelishGroupByCategory", p);
	}
	
	/**
	 * 套餐
	 * */
	
	/**
	 * 插入套餐分类，并返回id
	 * */
	public int insertSetMealCategory(Object category){
		return (Integer) this.getSqlMapClientTemplate().insert("insertSetMealCategory", category);
	}
	
	/**
	 * 取得套餐分类
	 * */
	public List selectSetMealCategory(Object p){
		return this.getSqlMapClientTemplate().queryForList("selectSetMealCategory", p);
	}
	
	/**
	 * 更新套餐分类
	 * */
	public int updateSetMealCategory(Object p){
		return this.getSqlMapClientTemplate().update("updateSetMealCategory", p);
	}
	
	/**
	 * 删除套餐分类
	 * */
	public int deleteSetMealCategory(Object p){
		return this.getSqlMapClientTemplate().delete("deleteSetMealCategory", p);
	}
	
	/**
	 * 取得套餐分类数量
	 * */
	public int selectCountSetMealCategory(Object p){
		return (Integer) this.getSqlMapClientTemplate().queryForObject("selectCountSetMeal", p);
	}
	
	/**
	 * 添加套餐
	 * */
	public int insertSetMeal(Object setMeal){
		return (Integer) this.getSqlMapClientTemplate().insert("insertSetMeal", setMeal);
	}
	
	/**
	 * 取得套餐
	 * */
	public List selectSetMeal(Object p){
		return this.getSqlMapClientTemplate().queryForList("selectSetMeal", p);
	}
	/**
	 * 取得套餐美味数量
	 * */
	public int selectSetMealCount(Object p){
		return (Integer)this.getSqlMapClientTemplate().queryForObject("selectSetMealCount", p);
	}
	
	/**
	 * 更新套餐
	 * */
	public int updateSetMeal(Object setMeal){
		return this.getSqlMapClientTemplate().update("updateSetMeal", setMeal);
	}
	
	/**
	 * 删除套餐
	 * */
	public int deleteSetMeal(Object p){
		return this.getSqlMapClientTemplate().delete("deleteSetMeal", p);
	}
	
	/**
	 * 保存套餐美味
	 * */
	public int insertSetMealRelish(Object setMealRelish){
		return (Integer) this.getSqlMapClientTemplate().insert("insertSetMealRelish", setMealRelish);
	}
	/**
	 * 删除套餐美味
	 * */
	public int deleteSetMealRelish(Object p){
		return this.getSqlMapClientTemplate().delete("deleteSetMealRelish", p);
	}
	/**
	 * 取得套餐美味
	 * */
	public List selectSetMealRelish(Object p){
		return this.getSqlMapClientTemplate().queryForList("selectSetMealRelish", p);
	}
	/**
	 * 更新套餐美味
	 * */
	public int updateSetMealRelish(Object setMealRelish){
		return this.getSqlMapClientTemplate().update("updateSetMealRelish", setMealRelish);
	}
	
	/**
	 * 统计套餐的数量。分别从招牌，推荐，美客推荐，优惠方面
	 * */
	public HashMap countSetMealNum(Object p){
		return (HashMap) this.getSqlMapClientTemplate().queryForObject("countSetMealNum", p);
	}
}
