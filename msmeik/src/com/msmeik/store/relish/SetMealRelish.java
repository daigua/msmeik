package com.msmeik.store.relish;

import com.msmeik.store.Store;

public class SetMealRelish {
	private int id;
	private Relish relish;
	private SetMeal setMeal;
	private Store store;
	private int orderIndex;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Relish getRelish() {
		return relish;
	}
	public void setRelish(Relish relish) {
		this.relish = relish;
	}
	public SetMeal getSetMeal() {
		return setMeal;
	}
	public void setSetMeal(SetMeal setMeal) {
		this.setMeal = setMeal;
	}
	public int getOrderIndex() {
		return orderIndex;
	}
	public void setOrderIndex(int orderIndex) {
		this.orderIndex = orderIndex;
	}
	public Store getStore() {
		return store;
	}
	public void setStore(Store store) {
		this.store = store;
	}
	
	
}
