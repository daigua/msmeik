package com.msmeik.store.relish;

import java.util.Date;

import com.msmeik.member.User;
import com.msmeik.store.Store;

public class Relish {
	private int id;
	private String name;
	private Store store;//属于哪个店铺的
	private int facia;//是否招牌  1是 2不是
	private int recommend;//是否推荐
	private RelishCategory category;//美食分类	
	private float price;
	private float specialPrice;//优惠价
	private String tags;
	private int collectTimes;//收藏次数
	private int recommendTimes;//推荐次数
	private int totalScore;//总评分
	private int scoreTimes;//评分次数
	private int status;//状态  1 上线  2下线
	private String introduction;
	private int orderIndex;//排序值
	private Date addTime;//收录时间
	private int onTop;
	private Date updateTime;
	private Date orderTime;//排序时间
	
	public int getId() {
		return id;
	}
	public Date getOrderTime() {
		return orderTime;
	}
	public void setOrderTime(Date orderTime) {
		this.orderTime = orderTime;
	}
	
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
	public int getOnTop() {
		return onTop;
	}
	public void setOnTop(int onTop) {
		this.onTop = onTop;
	}
	public int getOrderIndex() {
		return orderIndex;
	}
	public void setOrderIndex(int orderIndex) {
		this.orderIndex = orderIndex;
	}
	public Store getStore() {
		return store;
	}
	public void setStore(Store store) {
		this.store = store;
	}
	public void setId(int id) {
		this.id = id;
	}
	public RelishCategory getCategory() {
		return category;
	}
	public void setCategory(RelishCategory category) {
		this.category = category;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getFacia() {
		return facia;
	}
	public void setFacia(int facia) {
		this.facia = facia;
	}
	public int getRecommend() {
		return recommend;
	}
	public void setRecommend(int recommend) {
		this.recommend = recommend;
	}
	
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public float getSpecialPrice() {
		return specialPrice;
	}
	public void setSpecialPrice(float specialPrice) {
		this.specialPrice = specialPrice;
	}
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	public int getCollectTimes() {
		return collectTimes;
	}
	public void setCollectTimes(int collectTimes) {
		this.collectTimes = collectTimes;
	}
	public int getRecommendTimes() {
		return recommendTimes;
	}
	public void setRecommendTimes(int recommendTimes) {
		this.recommendTimes = recommendTimes;
	}
	public int getTotalScore() {
		return totalScore;
	}
	public void setTotalScore(int totalScore) {
		this.totalScore = totalScore;
	}
	public int getScoreTimes() {
		return scoreTimes;
	}
	public void setScoreTimes(int scoreTimes) {
		this.scoreTimes = scoreTimes;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getIntroduction() {
		return introduction;
	}
	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}
	public Date getAddTime() {
		return addTime;
	}
	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}
	
	
}
