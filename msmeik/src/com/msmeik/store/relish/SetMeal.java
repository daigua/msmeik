package com.msmeik.store.relish;

import java.util.Date;
import java.util.List;

import com.msmeik.store.Store;

public class SetMeal {
	private int id;
	private Store store;
	private String name;
	private float price;
	private float specialPrice;
	private int recommend;
	private int score;
	private int scoredTimes;
	private SetMealCategory category;
	private String tags;
	private int recommendTimes;
	private int status;//״̬ 1���� 2����
	private String introduction;
	private Date addTime;
	private Date updateTime;
	private int onTop;
	private Date orderTime;
	private int relishNum;
	private List<Relish> relishList;
	private String relishIds;
	private int facia;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	
	public Store getStore() {
		return store;
	}
	public void setStore(Store store) {
		this.store = store;
	}
	public float getSpecialPrice() {
		return specialPrice;
	}
	public void setSpecialPrice(float specialPrice) {
		this.specialPrice = specialPrice;
	}
	public int getRecommend() {
		return recommend;
	}
	public void setRecommend(int recommend) {
		this.recommend = recommend;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public int getScoredTimes() {
		return scoredTimes;
	}
	public void setScoredTimes(int scoredTimes) {
		this.scoredTimes = scoredTimes;
	}
	public SetMealCategory getCategory() {
		return category;
	}
	public void setCategory(SetMealCategory category) {
		this.category = category;
	}
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	public int getRecommendTimes() {
		return recommendTimes;
	}
	public void setRecommendTimes(int recommendTimes) {
		this.recommendTimes = recommendTimes;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getIntroduction() {
		return introduction;
	}
	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}
	public Date getAddTime() {
		return addTime;
	}
	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public int getOnTop() {
		return onTop;
	}
	public void setOnTop(int onTop) {
		this.onTop = onTop;
	}
	public Date getOrderTime() {
		return orderTime;
	}
	public void setOrderTime(Date orderTime) {
		this.orderTime = orderTime;
	}
	public int getRelishNum() {
		return relishNum;
	}
	public void setRelishNum(int relishNum) {
		this.relishNum = relishNum;
	}
	public List<Relish> getRelishList() {
		return relishList;
	}

	public String getRelishIds() {
		return relishIds;
	}
	public void setRelishIds(String relishIds) {
		this.relishIds = relishIds;
	}
	public void setRelishList(List<Relish> relishList) {
		this.relishList = relishList;
	}
	public int getFacia() {
		return facia;
	}
	public void setFacia(int facia) {
		this.facia = facia;
	}
	
	
}
