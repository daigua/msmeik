package com.msmeik.store.relish;

import java.util.ArrayList;
import java.util.List;

public class RelishConfig {
	public static List<RelishCategory> relishSystemCategoryList = new ArrayList();
	public static List<SetMealCategory> setMealSystemCategoryList = new ArrayList();
	
	public static RelishCategory defaultRelishCategory = new RelishCategory();
	public static SetMealCategory defaultSetMealCategory = new SetMealCategory();
	
	
}
