package com.msmeik.store.relish.setmeal.mvc;

import com.msmeik.store.Store;
import com.msmeik.store.relish.RelishService;
import com.msmeik.store.relish.SetMealCategory;
import com.opensymphony.xwork2.ActionSupport;

public class AddSetMealCategory extends ActionSupport{

	private RelishService relishService;
	private SetMealCategory category;
	private int ok;
	private int storeId;
	
	public String execute(){
		Store store = new Store();
		store.setId(storeId);
		category.setOrderIndex(1);
		this.ok = this.relishService.addSetMealCategory(store, category);
		return "input";
	}

	public SetMealCategory getCategory() {
		return category;
	}

	public void setCategory(SetMealCategory category) {
		this.category = category;
	}

	public int getStoreId() {
		return storeId;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public int getOk() {
		return ok;
	}

	public void setRelishService(RelishService relishService) {
		this.relishService = relishService;
	}
	
	
}
