package com.msmeik.store.relish.setmeal.mvc;

import java.io.File;

import com.msmeik.store.Store;
import com.msmeik.store.relish.RelishService;
import com.msmeik.store.relish.SetMeal;

public class DeleteSetMeal {
private RelishService relishService;
	
	private int setMealId;
	private int storeId;
	
	private String saveFolder;
	
	private int ok;
	
	public String execute(){
		File file = new File(this.saveFolder+File.separator+this.storeId+File.separator+this.setMealId);
		if(file.exists())
			file.delete();
		Store store = new Store();
		store.setId(storeId);
		SetMeal setMeal = new SetMeal();
		setMeal.setId(setMealId);
		this.ok = this.relishService.deleteSetMeal(store, setMeal);
		return "success";
	}

	public void setSaveFolder(String saveFolder) {
		this.saveFolder = saveFolder;
	}

	public int getOk() {
		return ok;
	}

	public void setRelishService(RelishService relishService) {
		this.relishService = relishService;
	}



	public void setSetMealId(int setMealId) {
		this.setMealId = setMealId;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}
}
