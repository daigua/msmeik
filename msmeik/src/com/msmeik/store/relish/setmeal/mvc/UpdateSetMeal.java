package com.msmeik.store.relish.setmeal.mvc;

import java.util.ArrayList;
import java.util.List;

import com.msmeik.store.Store;
import com.msmeik.store.relish.Relish;
import com.msmeik.store.relish.RelishService;
import com.msmeik.store.relish.SetMeal;

public class UpdateSetMeal {
	private RelishService relishService;
	private int setMealId;
	private SetMeal setMeal;
	private int storeId;
	private List categoryList;
	private List<Integer> deleteRelishIds;//要删除的美味id
	private List<Integer> addRelishIds;//要添加的美味id
	private List<Integer> relishIds;//现在的美味id，用于排序
	private int ok;
	
	public String html(){
		
		Store store = new Store();
		store.setId(storeId);
		this.categoryList = this.relishService.getSetMealCategoryList(store);
		this.setMeal = this.relishService.getSetMeal(store, setMealId);
		return "success";
	}
	
	public String modify(){
		System.out.println("要删除的"+deleteRelishIds);
		System.out.println("要添加的"+addRelishIds);
		System.out.println("删除和添加之后的"+relishIds);
		Store store = new Store();
		store.setId(storeId);
		List relishList = new ArrayList();
		for(int relishId:relishIds){
			Relish relish = new Relish();
			relish.setId(relishId);
			relishList.add(relish);
		}
		setMeal.setRelishList(relishList);
		this.ok = this.relishService.updateSetMeal(store, setMeal, addRelishIds, deleteRelishIds);
		return "input";
	}

	public SetMeal getSetMeal() {
		return setMeal;
	}

	public void setSetMeal(SetMeal setMeal) {
		this.setMeal = setMeal;
	}

	public int getStoreId() {
		return storeId;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public List getCategoryList() {
		return categoryList;
	}

	public void setRelishService(RelishService relishService) {
		this.relishService = relishService;
	}

	public void setSetMealId(int setMealId) {
		this.setMealId = setMealId;
	}

	public int getOk() {
		return ok;
	}

	public void setDeleteRelishIds(List<Integer> deleteRelishIds) {
		this.deleteRelishIds = deleteRelishIds;
	}

	public void setAddRelishIds(List<Integer> addRelishIds) {
		this.addRelishIds = addRelishIds;
	}

	public void setRelishIds(List<Integer> relishIds) {
		this.relishIds = relishIds;
	}

	
	
	
}
