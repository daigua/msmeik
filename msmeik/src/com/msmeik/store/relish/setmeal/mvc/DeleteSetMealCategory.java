package com.msmeik.store.relish.setmeal.mvc;

import com.msmeik.store.Store;
import com.msmeik.store.relish.RelishService;

public class DeleteSetMealCategory {
	private RelishService relishService;
	private int ok;
	private int categoryId;
	private int storeId;
	
	public String execute(){
		Store store = new Store();
		store.setId(storeId);
		this.ok = this.relishService.deleteSetMealCategory(store, categoryId);
		return "success";
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getOk() {
		return ok;
	}

	public void setRelishService(RelishService relishService) {
		this.relishService = relishService;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}
	
	
}
