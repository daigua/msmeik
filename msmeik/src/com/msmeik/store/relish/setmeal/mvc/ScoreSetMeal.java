package com.msmeik.store.relish.setmeal.mvc;

import com.msmeik.store.relish.RelishService;

public class ScoreSetMeal {
	private RelishService relishService;
	private int setMealId;
	private int score;
	private int ok;
	
	public String execute(){
		this.ok = this.relishService.scoreSetMeal(setMealId, score);
		return "success";
	}

	

	public int getSetMealId() {
		return setMealId;
	}



	public void setSetMealId(int setMealId) {
		this.setMealId = setMealId;
	}



	public int getOk() {
		return ok;
	}

	public void setRelishService(RelishService relishService) {
		this.relishService = relishService;
	}

	public void setScore(int score) {
		this.score = score;
	}
	
}
