package com.msmeik.store.relish.setmeal.mvc;

import com.msmeik.store.relish.RelishService;

public class RecommendSetMeal {
	private RelishService relishService;
	private int setMealId;
	private int ok;
	
	public String execute(){
		this.ok = this.relishService.recommendSetMeal(setMealId);
		return "success";
	}	

	public int getSetMealId() {
		return setMealId;
	}

	public void setSetMealId(int setMealId) {
		this.setMealId = setMealId;
	}

	public int getOk() {
		return ok;
	}

	public void setRelishService(RelishService relishService) {
		this.relishService = relishService;
	}
}
