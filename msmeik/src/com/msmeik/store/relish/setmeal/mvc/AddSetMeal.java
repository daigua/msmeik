package com.msmeik.store.relish.setmeal.mvc;

import java.util.ArrayList;
import java.util.List;

import com.msmeik.store.Store;
import com.msmeik.store.relish.Relish;
import com.msmeik.store.relish.RelishService;
import com.msmeik.store.relish.SetMeal;
import com.opensymphony.xwork2.ActionSupport;

public class AddSetMeal extends ActionSupport{
	private RelishService relishService;
	private int ok;
	private SetMeal setMeal;
	private int storeId;
	private List<Integer> relishIds;
	
	public String execute(){
		Store store = new Store();
		store.setId(storeId);
		List relishList = new ArrayList();
		for(int relishId:relishIds){
			Relish relish = new Relish();
			relish.setId(relishId);
			relishList.add(relish);
		}
		setMeal.setRelishList(relishList);
		this.ok = this.relishService.addSetMeal(store, setMeal);
		setMeal = null;
		relishIds = null;
		return "input";
	}

	public SetMeal getSetMeal() {
		return setMeal;
	}

	public void setSetMeal(SetMeal setMeal) {
		this.setMeal = setMeal;
	}

	public int getStoreId() {
		return storeId;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public int getOk() {
		return ok;
	}

	public void setRelishService(RelishService relishService) {
		this.relishService = relishService;
	}

	public void setRelishIds(List<Integer> relishIds) {
		this.relishIds = relishIds;
	}
	
}
