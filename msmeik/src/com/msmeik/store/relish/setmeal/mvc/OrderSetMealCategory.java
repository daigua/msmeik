package com.msmeik.store.relish.setmeal.mvc;

import java.util.List;

import com.msmeik.store.Store;
import com.msmeik.store.relish.RelishService;

public class OrderSetMealCategory {
	private RelishService relishService;
	private List<Integer> categoryIds;
	private List<Integer> orderIndexes;
	private int storeId;
	
	private int ok;
	
	public String execute(){
		Store store = new Store();
		store.setId(storeId);
		//System.out.println(categoryIds);
		ok = relishService.orderSetMealCategory(store, categoryIds, orderIndexes);
		return "success";
	}

	public int getStoreId() {
		return storeId;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public int getOk() {
		return ok;
	}

	public void setRelishService(RelishService relishService) {
		this.relishService = relishService;
	}

	public void setCategoryIds(List<Integer> categoryIds) {
		this.categoryIds = categoryIds;
	}

	public void setOrderIndexes(List<Integer>  orderIndexes) {
		this.orderIndexes = orderIndexes;
	}
	
	
}
