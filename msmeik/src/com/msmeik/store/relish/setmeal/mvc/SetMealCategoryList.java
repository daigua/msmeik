package com.msmeik.store.relish.setmeal.mvc;

import java.util.List;

import com.msmeik.store.Store;
import com.msmeik.store.relish.RelishService;

public class SetMealCategoryList {
	private RelishService relishService;
	private int storeId;
	private List categoryList;
	
	public String execute(){
		Store store = new Store();
		store.setId(storeId);
		this.categoryList = this.relishService.getSetMealCategoryList(store);
		return "success";
	}

	public int getStoreId() {
		return storeId;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public List getCategoryList() {
		return categoryList;
	}

	public void setRelishService(RelishService relishService) {
		this.relishService = relishService;
	}
	
	
}
