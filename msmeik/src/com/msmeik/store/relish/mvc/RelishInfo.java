package com.msmeik.store.relish.mvc;

import com.msmeik.store.relish.Relish;
import com.msmeik.store.relish.RelishService;

public class RelishInfo {
	private RelishService relishService;
	
	private int relishId;
	private int storeId;
	private Relish relish;
	
	public String execute(){
		this.relish = this.relishService.getRelishByStoreIdAndId(storeId, relishId);
		if(relish == null)
			relish = new Relish();
		return "success";
	}

	public Relish getRelish() {
		return relish;
	}

	public void setRelishService(RelishService relishService) {
		this.relishService = relishService;
	}

	public void setRelishId(int relishId) {
		this.relishId = relishId;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}
	
	
}
