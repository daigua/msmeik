package com.msmeik.store.relish.mvc;

import com.msmeik.store.Store;
import com.msmeik.store.relish.RelishCategory;
import com.msmeik.store.relish.RelishService;
import com.opensymphony.xwork2.ActionSupport;

public class UpdateRelishCategory extends ActionSupport{
	private RelishService relishService;
	private int storeId;
	private RelishCategory relishCategory;
	private int ok;
	
	public String execute(){
		Store store = new Store();
		store.setId(storeId);
		relishCategory.setStore(store);
		this.ok = this.relishService.updateRelishCategory(relishCategory);
		return "input";
	}

	public int getStoreId() {
		return storeId;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public RelishCategory getRelishCategory() {
		return relishCategory;
	}

	public void setRelishCategory(RelishCategory relishCategory) {
		this.relishCategory = relishCategory;
	}

	public int getOk() {
		return ok;
	}

	public void setRelishService(RelishService relishService) {
		this.relishService = relishService;
	}
	
	
}
