package com.msmeik.store.relish.mvc;

import java.util.List;

import com.msmeik.store.relish.RelishService;

public class RelishCategoryList {
	private RelishService relishService;
	private int storeId;
	
	private List relishCategoryList;
	
	public String execute(){
		this.relishCategoryList = this.relishService.getRelishCategoryListByStoreId(storeId);
		return "success";
	}

	public List getRelishCategoryList() {
		return relishCategoryList;
	}

	public void setRelishService(RelishService relishService) {
		this.relishService = relishService;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}
	
	
}
