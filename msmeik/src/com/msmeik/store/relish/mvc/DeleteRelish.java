package com.msmeik.store.relish.mvc;

import java.io.File;

import com.msmeik.store.relish.RelishService;

public class DeleteRelish {
	private RelishService relishService;
	
	private int relishId;
	private int storeId;
	
	private String saveFolder;
	
	private int ok;
	
	public String execute(){
		File file = new File(this.saveFolder+File.separator+this.storeId+File.separator+this.relishId);
		if(file.exists())
			file.delete();
		this.ok = this.relishService.deleteRelishByStoreIdAndRelishId(storeId, relishId);
		return "success";
	}

	public void setSaveFolder(String saveFolder) {
		this.saveFolder = saveFolder;
	}

	public int getOk() {
		return ok;
	}

	public void setRelishService(RelishService relishService) {
		this.relishService = relishService;
	}

	public void setRelishId(int relishId) {
		this.relishId = relishId;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}
	
	
}
