package com.msmeik.store.relish.mvc;

import java.util.Date;

import com.msmeik.store.relish.RelishService;

public class SetRelish {
	private RelishService relishService;
	private String p;//����
	private int v;//ֵ
	private int relishId;
	private int storeId;
	private Date orderTime;
	private int ok;
	
	public String execute(){
		if(p.equals("facia")){
			this.ok = this.relishService.setRelishFacia(storeId, relishId, v);
		}
		if(p.equals("onTop")){
			this.ok = this.relishService.setRelishOnTop(storeId, relishId, v);
		}
		if(p.equals("recommend")){
			this.ok = this.relishService.setRelishRecommend(storeId, relishId, v);
		}
		if(p.equals("category")){
			this.ok = this.relishService.setRelishCategory(storeId, relishId, v);
		}
		if(p.equals("orderTime")){
			this.orderTime = new Date();
			this.ok = this.relishService.setRelishOrderTime(storeId, relishId, orderTime);
		}
		if(p.equals("status")){
			this.ok = this.relishService.setRelishStatus(storeId, relishId, v);
		}
		return "success";
	}

	public Date getOrderTime() {
		return orderTime;
	}

	public int getOk() {
		return ok;
	}

	public void setRelishService(RelishService relishService) {
		this.relishService = relishService;
	}

	public void setP(String p) {
		this.p = p;
	}

	public void setV(int v) {
		this.v = v;
	}

	public void setRelishId(int relishId) {
		this.relishId = relishId;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}
	
	
}
