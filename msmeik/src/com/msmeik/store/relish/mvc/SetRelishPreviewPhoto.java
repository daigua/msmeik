package com.msmeik.store.relish.mvc;

import java.io.File;
import java.io.FileOutputStream;

import com.msmeik.io.image.ImageScale;
import com.opensymphony.xwork2.ActionSupport;

public class SetRelishPreviewPhoto extends ActionSupport{
	
	private File previewPhoto;	
	private int maxWidth;
	private int maxHeight;
	private String saveFolder;
	private int ok;
	private int storeId;
	private int relishId;
	
	public String execute(){
		if(storeId <= 0 || relishId <= 0)
			return "input";
		ImageScale imageScale = new ImageScale();
		try {
			File outputFileFolder = new File(this.saveFolder+File.separator+this.storeId);
			if(!outputFileFolder.exists())
				outputFileFolder.mkdirs();
			FileOutputStream out = new FileOutputStream(this.saveFolder+File.separator+this.storeId+File.separator+this.relishId);
			imageScale.compressSave(this.previewPhoto,out,this.maxWidth,this.maxHeight,"jpg");
			out.flush();
			out.close();
			ok = 9;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "input";
	}

	public File getPreviewPhoto() {
		return previewPhoto;
	}

	public void setPreviewPhoto(File previewPhoto) {
		this.previewPhoto = previewPhoto;
	}

	public int getOk() {
		return ok;
	}

	public void setMaxWidth(int maxWidth) {
		this.maxWidth = maxWidth;
	}

	public void setMaxHeight(int maxHeight) {
		this.maxHeight = maxHeight;
	}

	public void setSaveFolder(String saveFolder) {
		this.saveFolder = saveFolder;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public void setRelishId(int relishId) {
		this.relishId = relishId;
	}
	
	
}
