package com.msmeik.store.relish.mvc;

import com.msmeik.store.relish.RelishService;

public class DeleteRelishCategory {
	private RelishService relishService;
	
	private int storeId;
	private int categoryId;
	
	private int ok;
	
	public String execute(){
		this.ok = this.relishService.deleteRelishCategory(categoryId, storeId);
		return "success";
	}

	public int getOk() {
		return ok;
	}

	public void setRelishService(RelishService relishService) {
		this.relishService = relishService;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	
	
	
}
