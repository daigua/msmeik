package com.msmeik.store.relish.mvc;

import java.util.Date;

import com.msmeik.store.Store;
import com.msmeik.store.relish.Relish;
import com.msmeik.store.relish.RelishService;
import com.opensymphony.xwork2.ActionSupport;

public class AddRelish extends ActionSupport{
	private RelishService relishService;
	private Relish relish;
	private int storeId;
	private int ok;
	public String execute(){
		Store store = new Store();
		store.setId(storeId);
		relish.setStore(store);
		relish.setAddTime(new Date());
		relish.setUpdateTime(new Date());
		relish.setOrderTime(new Date());
		this.ok = this.relishService.addRelish(relish);
		return "input";
	}
	public int getStoreId() {
		return storeId;
	}
	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}
	public Relish getRelish() {
		return relish;
	}
	public void setRelish(Relish relish) {
		this.relish = relish;
	}
	public int getOk() {
		return ok;
	}
	public void setRelishService(RelishService relishService) {
		this.relishService = relishService;
	}
	
	
	
}
