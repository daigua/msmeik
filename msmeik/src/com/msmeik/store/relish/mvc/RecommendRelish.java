package com.msmeik.store.relish.mvc;

import com.msmeik.store.relish.RelishService;

public class RecommendRelish {
	private RelishService relishService;
	private int relishId;
	private int ok;
	
	public String execute(){
		this.ok = this.relishService.recommendRelish(relishId);
		return "success";
	}

	public int getRelishId() {
		return relishId;
	}

	public void setRelishId(int relishId) {
		this.relishId = relishId;
	}

	public int getOk() {
		return ok;
	}

	public void setRelishService(RelishService relishService) {
		this.relishService = relishService;
	}
	
	
}
