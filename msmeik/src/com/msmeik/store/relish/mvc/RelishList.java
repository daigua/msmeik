package com.msmeik.store.relish.mvc;

import java.util.List;

import com.msmeik.member.User;
import com.msmeik.store.Store;
import com.msmeik.store.relish.RelishCategory;
import com.msmeik.store.relish.RelishService;

public class RelishList {
	private RelishService relishService;
	private int storeId;
	private int categoryId;
	private String name;
	private List relishList;
	private int page = 1;
	private int pageSize = 10;
	private int totalCount;
	private int recorderId;
	private int status;
	
	public String execute(){
		Store store = new Store();
		store.setId(storeId);
		RelishCategory category = new RelishCategory();
		category.setId(categoryId);
		User recorder = new User();
		recorder.setId(recorderId);
		this.relishList = this.relishService.getRelishList(name, store, category, status , recorder, page, pageSize);
		this.totalCount = this.relishService.getRelishCount(name,store, category, status, recorder);
		return "success";
	}

	public int getStoreId() {
		return storeId;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public int getRecorderId() {
		return recorderId;
	}

	public int getStatus() {
		return status;
	}

	public void setRecorderId(int recorderId) {
		this.recorderId = recorderId;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public List getRelishList() {
		return relishList;
	}

	public void setRelishService(RelishService relishService) {
		this.relishService = relishService;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
