package com.msmeik.store.relish.mvc;

import com.msmeik.store.Store;
import com.msmeik.store.relish.Relish;
import com.msmeik.store.relish.RelishService;
import com.opensymphony.xwork2.ActionSupport;

public class UpdateRelish extends ActionSupport{
	private RelishService relishService;
	private Relish relish;
	private int storeId;
	private int ok;
	
	public String execute(){
		Store store = new Store();
		store.setId(storeId);
		relish.setStore(store);
		this.ok = this.relishService.updateRelish(relish);
		return "input";
	}

	public int getStoreId() {
		return storeId;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public Relish getRelish() {
		return relish;
	}

	public void setRelish(Relish relish) {
		this.relish = relish;
	}

	public int getOk() {
		return ok;
	}

	public void setRelishService(RelishService relishService) {
		this.relishService = relishService;
	}
	
	
}
