package com.msmeik.store.relish;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.msmeik.member.User;
import com.msmeik.store.Store;

public class RelishService {
	private RelishDao relishDao;

	/**
	 * 添加美食分类并返回id
	 * @return -1 此分类名已经存在 
	 * */
	public int addRelishCategory(RelishCategory relishCategory){
		System.out.println(relishCategory.getStore().getId());
		if(relishCategory.getStore().getId() <= 0)
			return 0;
		if(this.checkRelishCategoryExistByNameAndStoreId(relishCategory.getName(), relishCategory.getStore().getId())){
			return -1;
		}
		return this.relishDao.insertRelishCategory(relishCategory);
	}
	
	/**
	 * 根据Store取得美食分类列表
	 * */
	public List getRelishCategoryListByStore(Store store){
		if(store.getId()<=0)
			return new ArrayList();
		Map p = new HashMap();
		p.put("store", store);
		List<RelishCategory> list = this.relishDao.selectRelishCategoryList(p);
		List<Map> countRelishGroupByCategory = this.countRelishGroupByCategory(store,1);
		Map countMap = new HashMap();
		for(Map count:countRelishGroupByCategory){			
			countMap.put(count.get("category"), count.get("relishNum"));
		}
		for(RelishCategory category:list){
			if(countMap.containsKey(category.getId())){
				category.setRelishNum((Integer)countMap.get(category.getId()));
			}
		}
		return list;
	}
	
	/**
	 * 根据美食分类的idlist 取得 分类 list
	 * */
	public List getRelishCategoryList(List ids){
		if(ids.size()==0)
			return new ArrayList();
		Map p = new HashMap();
		p.put("ids", ids);
		return this.relishDao.selectRelishCategoryList(p);
	}
	/**
	 * 根据storeId取得美食分类
	 * */
	public List getRelishCategoryListByStoreId(int storeId){
		if(storeId <= 0)
			return new ArrayList();
		Store store = new Store();
		store.setId(storeId);
		return this.getRelishCategoryListByStore(store);
	}
	/**
	 * 取得store拥有的美味分类数量
	 * */
	public int getRelishCategoryCountByStore(Store store){
		if(store.getId()<0)
			return 0;
		Map p = new HashMap();
		p.put("store", store);
		return this.relishDao.selectRelishCategoryCount(p);
	}
	/**
	 * 取得store拥有的美味分类数量根据store id
	 * */
	public int getRelishCategoryCountByStoreId(int storeId){
		Store store = new Store();
		store.setId(storeId);
		return this.getRelishCategoryCountByStore(store);
	}
	
	/**
	 * 判断relishCategory是否存在
	 * */
	public boolean checkRelishCategoryExistByNameAndStoreId(String name,int storeId){
		if(name == null || name.trim().equals("") || storeId<=0)
			return true;
		Map p = new HashMap();
		p.put("name", name);
		Store store = new Store();
		store.setId(storeId);
		p.put("store", store);
		return this.relishDao.selectRelishCategoryCount(p)>=1?true:false;
	}
	
	/**
	 * 删除美食分类
	 * */
	public int deleteRelishCategory(int categoryId,int storeId){
		if(categoryId<=0 || storeId <= 0)
			return 0;
		this.moveRelishFormCategoryToAnother(storeId, categoryId, 1);
		Map p = new HashMap();
		p.put("id", categoryId);
		p.put("storeId", storeId);
		return this.relishDao.deleteRelishCategory(p);
	}
	
	/**
	 * 更新美食分类
	 * @return -1 所更新的分类名称已经存在  0 更新失败 1更新成功
	 * */
	public int updateRelishCategory(RelishCategory relishCategory){
		if(relishCategory.getStore().getId() <= 0 || relishCategory.getName() == null || relishCategory.getId() <= 0)
			return 0;
		if(this.checkRelishCategoryExistByNameAndStoreId(relishCategory.getName(), relishCategory.getStore().getId())){
			return -1;
		}
		return this.relishDao.updateRelishCategory(relishCategory);
	}
	
	/**
	 * 添加美食
	 * */
	public int addRelish(Relish relish){
		if(relish.getStore().getId() <= 0)
			return 0;
		return this.relishDao.insertRelish(relish);
	}
	/**
	 * 根据storeId和RelishId取得美味
	 * */
	public Relish getRelishByStoreIdAndId(int storeId,int relishId){
		if(storeId <= 0|| relishId <= 0)
			return null;
		Map p = new HashMap();
		Store store = new Store();
		store.setId(storeId);
		p.put("store", store);
		p.put("id", relishId);
		List l = this.relishDao.selectRelish(p);
		if(l.size()!=1)
			return null;
		return (Relish)l.get(0);
	}
	
	/**
	 * 根据storeId和RelishId取得美味
	 * */
	public Relish getRelishById(int relishId){
		if(relishId <= 0)
			return null;
		Map p = new HashMap();
		p.put("id", relishId);
		List l = this.relishDao.selectRelish(p);
		if(l.size()!=1)
			return null;
		return (Relish)l.get(0);
	}
	
	/**
	 * 更新美味信息
	 * */
	public int updateRelish(Relish relish){
		if(relish.getId() <= 0 || relish.getStore().getId() <= 0){
			return 0;
		}
		return this.relishDao.updateRelish(relish);
	}
	
	/**
	 * 根据美食名称，店铺id，美食分类取得美食列表
	 * */
	public List getRelishList(String name,Store store,RelishCategory category, int status,User recorder,int page,int pageSize){
		Map p = new HashMap();
		p.put("name", name);
		if(store != null && store.getId() > 0)
			p.put("store", store);
		if(category != null && category.getId() > 0)	
			p.put("category", category);
		if(status != 0)
			p.put("status", status);
		if(recorder != null && recorder.getId() > 0)
			p.put("recorder", recorder);
		if(page < 1)
			page = 1;
		int begin = (page-1)*pageSize;
		int end = pageSize;
		p.put("begin", begin);
		p.put("end", end);
		List<Relish> relishList = this.relishDao.selectRelish(p);
		List categoryIds = new ArrayList();
		for(Relish relish:relishList){
			if(!categoryIds.contains(relish.getCategory().getId()))
				categoryIds.add(relish.getCategory().getId());
		}
		List<RelishCategory> categoryList = this.getRelishCategoryList(categoryIds);
		Map categoryMap = new HashMap();
		for(RelishCategory cate:categoryList){
			categoryMap.put(cate.getId(), cate);
		}
		for(Relish relish:relishList){
			relish.setCategory((RelishCategory)categoryMap.get(relish.getCategory().getId()));
		}
		return relishList;
	}
	/**
	 * 根据店铺和美食分类取得美食数量
	 * */
	public int getRelishCount(String name,Store store , RelishCategory category, int status, User recorder){
		Map p = new HashMap();
		p.put("name", name);
		if(store != null && store.getId() > 0)
			p.put("store", store);
		if(category != null && category.getId() > 0)	
			p.put("category", category);
		if(status != 0)
			p.put("status", status);
		if(recorder != null && recorder.getId() > 0)
			p.put("recorder", recorder);
		return this.relishDao.selectRelishCount(p);
	}
	
	/**
	 * 取得美味
	 * @param nameLike 
	 * @param store 店铺餐馆
	 * @param category 美食分类
	 * @param facia 招牌  -1 所有 1是招牌
	 * @param recommend 店铺推荐 -1 所有    1 推荐
	 * @param recommendTimes 顾客推荐次数下线
	 * @param status 状态  1 上线  2 下线  -1 所有
	 * @param specialPrice 特价菜 -1 全部 1 是
	 * */
	public List getRelishList(Store store, String nameLike, RelishCategory category, int facia, int recommend, int recommendTimes, int specialPrice, int status, String orderBy, int page, int pageSize){
		Map p = new HashMap();
		p.put("nameLike", nameLike);
		if(store != null && store.getId() > 0)
			p.put("store", store);
		if(category!=null && category.getId() >0)
			p.put("category", category);
		if(facia != -1){
			p.put("facia", facia);
		}
		if(recommend != -1)
			p.put("recommend", recommend);
		p.put("recommendTimes", recommendTimes);
		if(status != -1)
			p.put("status", status);
		if(orderBy != null && !orderBy.trim().equals(""))
			p.put("orderBy", orderBy);
		if(specialPrice != -1)
			p.put("specialPrice", 1);
		int begin = (page-1)*pageSize;
		int end = pageSize;
		p.put("begin", begin);
		p.put("end", end);
		return this.relishDao.selectRelish(p);
	}
	/**
	 * 取得美味数量
	 * @param store 店铺餐馆
	 * @param category 美食分类
	 * @param facia 招牌  -1 所有 1是招牌
	 * @param recommend 店铺推荐 -1 所有    1 推荐
	 * @param recommendTimes 顾客推荐次数下线
	 * @param status 状态  1 上线  2 下线  -1 所有
	 * */
	public int getRelishNum(Store store, String nameLike, RelishCategory category, int facia, int recommend, int recommendTimes, int specialPrice ,int status){
		Map p = new HashMap();
		p.put("nameLike", nameLike);
		if(store != null && store.getId() > 0)
			p.put("store", store);
		if(category!=null && category.getId() >0)
			p.put("category", category);
		if(facia != -1){
			p.put("facia", facia);
		}
		if(recommend != -1)
			p.put("recommend", recommend);
		p.put("recommendTimes", recommendTimes);
		if(status != -1)
			p.put("status", status);
		if(specialPrice != -1)
			p.put("specialPrice", 1);
		return this.relishDao.selectRelishCount(p);
	}
	
	/**
	 * 置顶美食
	 * */
	public int setRelishOnTop(int storeId,int relishId,int onTop){
		if((onTop!=1 && onTop!=2) || storeId <=0 || relishId <= 0)
			return 0;
		Map p = new HashMap();
		Store store = new Store();
		store.setId(storeId);
		p.put("store", store);
		p.put("id", relishId);
		p.put("onTop", onTop);
		return this.relishDao.updateRelish(p);
	}
	/**
	 * 设置招牌
	 * */
	public int setRelishFacia(int storeId,int relishId,int facia){
		if((facia!=1 && facia!=2) || storeId <=0 || relishId <= 0)
			return 0;
		Map p = new HashMap();
		Store store = new Store();
		store.setId(storeId);
		p.put("store", store);
		p.put("id", relishId);
		p.put("facia", facia);
		return this.relishDao.updateRelish(p);
	}
	/**
	 * 设置推荐
	 * */
	public int setRelishRecommend(int storeId,int relishId,int recommend){
		if((recommend!=1 && recommend!=2) || storeId <=0 || relishId <= 0)
			return 0;
		Map p = new HashMap();
		Store store = new Store();
		store.setId(storeId);
		p.put("store", store);
		p.put("id", relishId);
		p.put("recommend", recommend);
		return this.relishDao.updateRelish(p);
	}
	/**
	 * 设置美味所属分类
	 * */
	public int setRelishCategory(int storeId,int relishId,int categoryId){
		if(storeId <= 0 || relishId <= 0 || categoryId <= 0)
			return 0;
		Map p = new HashMap();
		Store store = new Store();
		store.setId(storeId);
		p.put("store", store);
		p.put("id", relishId);
		RelishCategory category = new RelishCategory();
		category.setId(categoryId);
		p.put("category", category);
		return this.relishDao.updateRelish(p);
	}
	
	/**
	 * 设置美味状态
	 * */
	public int setRelishStatus(int storeId,int relishId,int status){
		if(storeId <= 0 || relishId <= 0 || status <= 0)
			return 0;
		Map p = new HashMap();
		Store store = new Store();
		store.setId(storeId);
		p.put("store", store);
		p.put("id", relishId);
		p.put("status", status);
		return this.relishDao.updateRelish(p);
	}
	
	/**
	 * 删除美食
	 * */
	public int deleteRelishByStoreIdAndRelishId(int storeId,int relishId){
		if(storeId <= 0 || relishId <=0)
			return 0;
		Map p = new HashMap();
		p.put("id", relishId);
		Store store = new Store();
		store.setId(storeId);
		p.put("store", store);
		return this.relishDao.deleteRelish(p);
	}
	/**
	 * 把某一分类下的美食设置成为另一个分类
	 * */
	public int moveRelishFormCategoryToAnother(int storeId, int fromCategoryId, int toCategoryId){
		if(storeId <= 0 || fromCategoryId <= 0 || toCategoryId <= 0)
			return 0;
		Map p = new HashMap();
		Store store = new Store();
		store.setId(storeId);
		RelishCategory category = new RelishCategory();
		category.setId(toCategoryId);
		RelishCategory fromCategory = new RelishCategory();
		fromCategory.setId(fromCategoryId);
		p.put("category", category);
		p.put("fromCategory", fromCategory);
		return this.relishDao.updateRelish(p);
	}
	
	/**
	 * 设置美味 排序时间
	 * */
	public int setRelishOrderTime(int storeId,int relishId,Date orderTime){
		if(storeId <= 0 || relishId <=0 || orderTime==null){
			return 0;
		}
		Map p = new HashMap();
		Store store = new Store();
		store.setId(storeId);
		p.put("id", relishId);
		p.put("store", store);
		p.put("orderTime", orderTime);
		return this.relishDao.updateRelish(p);
	}
	/**
	 * 统计美味 从 招牌 推荐 美客推荐 优惠价方面
	 * */
	public HashMap countRelishNum(Store store,int status){
		Map p = new HashMap();
		p.put("store", store);
		if(status != -1)
			p.put("status", status);
		return this.relishDao.countRelishNum(p);
	}
	/**
	 * 统计分类下的美味数量
	 * */
	public List countRelishGroupByCategory(Store store,int status){
		if(store == null || store.getId() <= 0)
			return new ArrayList();
		Map p = new HashMap();
		p.put("store", store);
		if(status != -1)
			p.put("status", status);
		return this.relishDao.countRelishGroupByCategory(p);
	}
	
	/**
	 * 推荐美味
	 * */
	public int recommendRelish(int relishId){
		if(relishId <= 0)
			return 0;
		Map p = new HashMap();
		p.put("id", relishId);
		p.put("recommendTimes", 1);
		return this.relishDao.updateRelish(p);
	}
	
	/**
	 * 给美味打分
	 * */
	public int scoreRelish(int relishId, int score){
		if(relishId <= 0)
			return 0;
		Map p = new HashMap();
		p.put("id", relishId);
		p.put("totalScore", score);
		p.put("scoreTimes", 1);
		return this.relishDao.updateRelish(p);
	}
	
	/**
	 * 根据store id取得套餐分类
	 * */
	public List getSetMealCategoryList(Store store){
		Map p = new HashMap();
		p.put("store", store);
		return this.relishDao.selectSetMealCategory(p);
	}
	/**
	 * 添加套餐分类
	 * */
	public int addSetMealCategory(Store store,SetMealCategory category){
		if(store.getId() <= 0)
			return 0;
		category.setStore(store);
		return this.relishDao.insertSetMealCategory(category);
	}
	
	/**
	 * 修改套餐分类
	 * */
	public int updateSetMealCategory(Store store,SetMealCategory category){
		if(store.getId() <=0 || category.getId() <= 0)
			return 0;
		category.setStore(store);
		return this.relishDao.updateSetMealCategory(category);
	}
	
	/**
	 * 删除套餐分类
	 * */
	public int deleteSetMealCategory(Store store, int categoryId){
		if(categoryId <= 0 || store.getId() <=0)
			return 0;
		Map p = new HashMap();
		p.put("store", store);
		p.put("id", categoryId);
		//将此分类下的美味移动到默认美味分类下
		moveSetMealToCategory(store,categoryId,1);
		return this.relishDao.deleteSetMealCategory(p);			
	}
	
	/**
	 * 套餐分类排序
	 * */
	public int orderSetMealCategory(Store store, List<Integer> categoryIds, List<Integer> orderIndexes){
		int i = 0;
		int j = 0;
		for(int categoryId:categoryIds){
			i += this.orderSetMealCategory(store, categoryId, orderIndexes.get(j++));
		}
		return i;
	}
	/**
	 * 套餐分类排序one by one
	 * */
	public int orderSetMealCategory(Store store, int categoryId, int orderIndex){
		if(store.getId() <=0 || categoryId <= 0 || orderIndex <= 0)
			return 0;
		Map p = new HashMap();
		p.put("store", store);
		p.put("id", categoryId);
		p.put("orderIndex", orderIndex);
		return this.relishDao.updateSetMealCategory(p);
	}
	
	/**
	 * 移动某分类下的套餐到另一个分类
	 * */
	public int moveSetMealToCategory(Store store, int categoryId, int toCategoryId){
		if(store.getId() <= 0 || categoryId <= 0 || toCategoryId <=0)
			return 0;
		Map p = new HashMap();
		p.put("store", store);
		p.put("categoryId", categoryId);
		p.put("toCategoryId", toCategoryId);
		return this.relishDao.updateSetMeal(p);
	}
	
	/**
	 * 添加套餐
	 * */
	public int addSetMeal(Store store, SetMeal setMeal){
		if(store.getId() <= 0 || setMeal.getRelishList() == null || setMeal.getRelishList().size() == 0)
			return 0;
		setMeal.setStore(store);
		Date now = new Date();
		setMeal.setAddTime(now);
		setMeal.setUpdateTime(now);
		setMeal.setOrderTime(now);
		setMeal.setRecommend(0);
		setMeal.setScore(0);
		setMeal.setScoredTimes(0);
		setMeal.setStatus(2);
		int key = this.relishDao.insertSetMeal(setMeal);//保存套餐	
		if(key > 0){//保存套餐美味
			setMeal.setId(key);
			SetMealRelish smr = new SetMealRelish();
			smr.setStore(store);
			smr.setSetMeal(setMeal);
			for(Relish relish:setMeal.getRelishList()){
				smr.setRelish(relish);
				this.relishDao.insertSetMealRelish(smr);
			}
		}
		return key;
	}
	
	/**
	 * 根据店铺 和套餐id取得套餐
	 * */
	public SetMeal getSetMeal(Store store, int setMealId){
		if(store.getId() <= 0 || setMealId <= 0)
			return null;
		Map p = new HashMap();
		p.put("store", store);
		p.put("id", setMealId);
		List l = this.relishDao.selectSetMeal(p);
		if(l.size()!=1)
			return null;
		return (SetMeal) l.get(0);
	}
	
	/**
	 * 取得套餐
	 * */
	public List getSetMealList(Store store, SetMealCategory category, int status, int page, int pageSize, String orderBy){
		if(store.getId() <= 0)
			return new ArrayList();
		Map p = new HashMap();
		p.put("store", store);
		p.put("orderBy", orderBy);
		if(status != -1)
			p.put("status", status);
		if(category.getId() != -1)
			p.put("category", category);
		if(page < 1)
			page = 1;
		int begin = (page-1)*pageSize;
		int end = pageSize;
		p.put("begin", begin);
		p.put("end", end);
		List list = this.relishDao.selectSetMeal(p);
		
		return list;
	}
	
	public int getSetMealNum(Store store,SetMealCategory category, int status){
		if(store.getId() <= 0)
			return 0;
		Map p = new HashMap();
		p.put("store", store);
		if(status != -1)
			p.put("status", status);
		if(category != null && category.getId() != -1)
			p.put("category", category);
		return this.relishDao.selectSetMealCount(p);
	}
	
	public int updateSetMeal(Store store,SetMeal setMeal, List<Integer> addRelishIds, List<Integer> deleteRelishIds){
		if(store.getId() <= 0 || setMeal.getId() <= 0)
			return 0;
		setMeal.setStore(store);
		setMeal.setUpdateTime(new Date());
		//删除要删除的美味
		if(deleteRelishIds != null && deleteRelishIds.size()!=0){
			Map p = new HashMap();
			p.put("relishIds", deleteRelishIds);
			this.relishDao.deleteSetMealRelish(p);
		}
		//添加要添加的美味
		if(addRelishIds != null && addRelishIds.size() != 0){
			SetMealRelish smr = new SetMealRelish();
			smr.setStore(store);
			smr.setSetMeal(setMeal);
			for(int relishId:addRelishIds){
				Relish relish = new Relish();
				relish.setId(relishId);
				smr.setRelish(relish);
				this.relishDao.insertSetMealRelish(smr);
			}
		}
		//美味进行排序
		int index = 0;
		for(Relish relish:setMeal.getRelishList()){
			SetMealRelish smr = new SetMealRelish();
			smr.setStore(store);
			smr.setSetMeal(setMeal);
			smr.setOrderIndex(index++);
			smr.setRelish(relish);
			this.relishDao.updateSetMealRelish(smr);
		}
		return this.relishDao.updateSetMeal(setMeal);
	}
	/**
	 * 设置套餐所属分类
	 * */
	public int setSetMealCategory(Store store, SetMealCategory category, int setMealId){
		if(store.getId() <= 0 || setMealId <=0)
			return 0;
		Map p = new HashMap();
		p.put("store", store);
		p.put("category", category);
		p.put("id", setMealId);
		return this.relishDao.updateSetMeal(p);
	}
	/**
	 * 设置套餐上线下线状态
	 * */
	public int setSetMealStatus(Store store, int setMealId, int status){
		if(store.getId() <=0 || setMealId <= 0 || status <= 0)
			return 0;
		Map p = new HashMap();
		p.put("store", store);
		p.put("id", setMealId);
		p.put("status", status);
		return this.relishDao.updateSetMeal(p);
	}
	/**
	 * 设置套餐是否为招牌
	 * */
	public int setSetMealFacia(Store store, int setMealId, int facia){
		if(store.getId() <=0 || setMealId <= 0 || facia <= 0)
			return 0;
		Map p = new HashMap();
		p.put("store", store);
		p.put("id", setMealId);
		p.put("facia", facia);
		return this.relishDao.updateSetMeal(p);
	}
	/**
	 * 设置套餐是否为推荐
	 * */
	public int setSetMealRecommend(Store store, int setMealId, int recommend){
		if(store.getId() <=0 || setMealId <= 0 || recommend <= 0)
			return 0;
		Map p = new HashMap();
		p.put("store", store);
		p.put("id", setMealId);
		p.put("recommend", recommend);
		return this.relishDao.updateSetMeal(p);
	}
	/**
	 * 设置套餐是否为前置
	 * */
	public int setSetMealOnTop(Store store, int setMealId, int onTop){
		if(store.getId() <=0 || setMealId <= 0 || onTop <= 0)
			return 0;
		Map p = new HashMap();
		p.put("store", store);
		p.put("id", setMealId);
		p.put("onTop", onTop);
		return this.relishDao.updateSetMeal(p);
	}
	/**
	 * 设置套餐的前置时间 排序时间
	 * */
	public int setSetMealOrderTime(Store store, int setMealId, Date orderTime){
		if(store.getId() <=0 || setMealId <= 0)
			return 0;
		Map p = new HashMap();
		p.put("store", store);
		p.put("id", setMealId);
		p.put("orderTime", orderTime);
		return this.relishDao.updateSetMeal(p);
	}
	
	/**
	 * 删除套餐
	 * */
	public int deleteSetMeal(Store store, SetMeal setMeal){
		if(store.getId() <= 0 || setMeal.getId() <= 0)
			return 0;
		//删除套餐美味
		Map p = new HashMap();
		p.put("store", store);
		p.put("setMeal", setMeal);
		this.relishDao.deleteSetMealRelish(p);
		p.put("id", setMeal.getId());
		return this.relishDao.deleteSetMeal(p);
	}
	
	/**
	 * 取得套餐
	 * @param nameLike 
	 * @param store 店铺餐馆
	 * @param category 分类
	 * @param facia 招牌  -1 所有 1是招牌
	 * @param recommend 店铺推荐 -1 所有    1 推荐
	 * @param recommendTimes 顾客推荐次数下线
	 * @param status 状态  1 上线  2 下线  -1 所有
	 * @param specialPrice 特价 -1 全部 1 是
	 * */
	public List getSetMealList(Store store, SetMealCategory category, int facia, int recommend, int recommendTimes, int specialPrice, int status, String orderBy, int page, int pageSize){
		Map p = new HashMap();
		if(store != null && store.getId() > 0)
			p.put("store", store);
		if(category!=null && category.getId() >0)
			p.put("category", category);
		if(facia != -1){
			p.put("facia", facia);
		}
		if(recommend != -1)
			p.put("recommend", recommend);
		p.put("recommendTimes", recommendTimes);
		if(status != -1)
			p.put("status", status);
		if(orderBy != null && !orderBy.trim().equals(""))
			p.put("orderBy", orderBy);
		if(specialPrice != -1)
			p.put("specialPrice", 1);
		int begin = (page-1)*pageSize;
		int end = pageSize;
		p.put("begin", begin);
		p.put("end", end);
		return this.relishDao.selectSetMeal(p);
	}
	/**
	 * 取得美味数量
	 * @param store 店铺餐馆
	 * @param category 美食分类
	 * @param facia 招牌  -1 所有 1是招牌
	 * @param recommend 店铺推荐 -1 所有    1 推荐
	 * @param recommendTimes 顾客推荐次数下线
	 * @param status 状态  1 上线  2 下线  -1 所有
	 * */
	public int getSetMealNum(Store store, SetMealCategory category, int facia, int recommend, int recommendTimes, int specialPrice ,int status){
		Map p = new HashMap();
		if(store != null && store.getId() > 0)
			p.put("store", store);
		if(category!=null && category.getId() >0)
			p.put("category", category);
		if(facia != -1){
			p.put("facia", facia);
		}
		if(recommend != -1)
			p.put("recommend", recommend);
		p.put("recommendTimes", recommendTimes);
		if(status != -1)
			p.put("status", status);
		if(specialPrice != -1)
			p.put("specialPrice", 1);
		return this.relishDao.selectSetMealCount(p);
	}
	
	/**
	 * 统计套餐 从 招牌 推荐 美客推荐 优惠价方面
	 * */
	public HashMap countSetMealNum(Store store,int status){
		Map p = new HashMap();
		p.put("store", store);
		if(status != -1)
			p.put("status", status);
		return this.relishDao.countSetMealNum(p);
	}
	
	/**
	 * 推荐套餐
	 * */
	public int recommendSetMeal(int setMealId){
		if(setMealId <= 0)
			return 0;
		Map p = new HashMap();
		p.put("id", setMealId);
		p.put("recommendTimes", 1);
		return this.relishDao.updateSetMeal(p);
	}
	/**
	 * 给美味打分
	 * */
	public int scoreSetMeal(int setMealId, int score){
		if(setMealId <= 0)
			return 0;
		Map p = new HashMap();
		p.put("id", setMealId);
		p.put("score", score);
		p.put("scoredTimes", 1);
		return this.relishDao.updateSetMeal(p);
	}
	
	
	public void setRelishDao(RelishDao relishDao) {
		this.relishDao = relishDao;
	}
	
}
