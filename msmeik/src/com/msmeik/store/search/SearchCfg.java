package com.msmeik.store.search;

import java.util.HashMap;
import java.util.Map;

import com.msmeik.store.Store;

public class SearchCfg {
	private int id;
	private Store store;
	private String keywords;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Store getStore() {
		return store;
	}
	public void setStore(Store store) {
		this.store = store;
	}
	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	
	
}

