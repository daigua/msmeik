package com.msmeik.store.search;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.msmeik.store.Store;
import com.orientor.website.tools.UtilTools;

public class SearchService {
	private SearchDao searchDao;
	
	/**
	 * insert search cfg
	 * */
	public int insertSearchCfg(SearchCfg searchCfg){
		if(searchCfg.getStore().getId()<=0)
			return 0;
		return this.searchDao.insertSearchCfg(searchCfg);
	}
	

	/**
	 * select search cfg by store id
	 * */
	public SearchCfg getSearchCfgByStoreId(int storeId){
		if(storeId <= 0)
			return null;
		Map p = new HashMap();
		Store store = new Store();
		store.setId(storeId);
		p.put("store", store);
		List<SearchCfg> l = this.searchDao.selectSearchCfg(p);
		if(l.size()!=1)
			return null;
		return l.get(0);
	}
	/**
	 * update SearchCfg 
	 * */
	public int updateSearchCfg(SearchCfg searchCfg){
		if(searchCfg.getStore().getId() <= 0)
			return 0;		
		return this.searchDao.updateSearchCfg(searchCfg);
	}
	
	/**
	 * add search cfg verify
	 * */
	public int applySearchCfgApplication(SearchCfgApplication sca){
		if(sca.getSearchCfg().getStore().getId() <= 0)
			return 0;
		sca.setResult("");
		if(this.updateSearchCfgApplication(sca)==0)
			this.searchDao.insertSearchCfgApplication(sca);
		return 1;
	}
	
	/**
	 * select search cfg verify
	 * */
	public List getSearchCfgApplication(int storeId, int status,int page, int pageSize){
		Map p = new HashMap();
		if(storeId > 0)
			p.put("storeId", storeId);
		if(page < 1)
			page = 1;
		int begin = (page-1)*pageSize;
		int end = pageSize;
		p.put("begin", begin);
		p.put("end", end);
		p.put("status", status);
		return this.searchDao.selectSearchCfgApplication(p);
	}
	
	/**
	 * get searchcfgapp num
	 * */
	public int getSearchCfgApplicationNum(int storeId,int status){
		Map p = new HashMap();
		if(storeId > 0)
			p.put("storeId", storeId);
		p.put("status", status);
		return this.searchDao.selectSearchCfgApplicationCount(p);
		
	}
	/**
	 * get search cfg application by store id
	 * */
	public SearchCfgApplication getSearchCfgApplicationByStoreId(int storeId){
		if(storeId <= 0)
			return null;
		Map p = new HashMap();
		Store store = new Store();
		store.setId(storeId);
		p.put("store", store);
		List l = this.searchDao.selectSearchCfgApplication(p);
		if(l.size()!=1)
			return null;
		return (SearchCfgApplication)l.get(0);
	}
	/**
	 * delete search cfg verify by storeId
	 * */
	public int deleteSearchCfgApplicationByStoreId(int storeId){
		if(storeId <= 0)
			return 0;
		Map p = new HashMap();
		p.put("storeId", storeId);
		return this.searchDao.deleteSearchCfgApplication(p);
	}
	/**
	 * update search cfg verify
	 * */
	public int updateSearchCfgApplication(SearchCfgApplication sca){
		if(sca.getSearchCfg().getStore().getId()<=0)
			return  0;
		return this.searchDao.updateSearchCfgApplication(sca);
	}
	
	/**
	 * just for admin
	 * */
	/**
	 * verify search cfg application
	 * */
	public int verifySearchCfgApplication(int storeId,int status,String result){
		if(storeId <=0)
			return 0;
		Map p = new HashMap();
		p.put("storeId", storeId);
		p.put("status", status);
		p.put("result", result);
		int i = this.searchDao.updateSearchCfgApplication(p);
		
		if(i == 1 && status == 1){//审核通过
			SearchCfgApplication sca = this.getSearchCfgApplicationByStoreId(storeId);
			if(this.updateSearchCfg(sca.getSearchCfg())==0){
				this.insertSearchCfg(sca.getSearchCfg());
			}
		}
		
		return i;
	}
	
	/**
	 * search store
	 * @param keywords 搜索关键字
	 * @param yh 是否有优惠  0 没有 1有
	 * @param perPayMin 人均下线
	 * @param perPayMax 人均上线
	 * @param cityAreaId 城市区域的id
	 * @param cbdAreaId 商区的id
	 * @param foodTypeId 菜系的id
	 * @param landmarkId 地标建筑的id
	 * @param tk 是否有外卖
	 * @param card 是否可刷卡
	 * @param orderBy 排序
	 * */
	public List searchStore(Map p,int page, int pageSize){
//		Map p = new HashMap();		
//		if(keywords!=null){
//			keywords = keywords.replaceAll("\\s{2,}", " ").trim();
//			if(!keywords.equals(""))
//				p.put("keywords", keywords);
//		}
//		if(cityAreaId != 0){
//			Map cityArea = new HashMap();
//			cityArea.put("id", cityAreaId);
//			p.put("cityArea", cityArea);
//		}
//		if(cbdAreaId != 0){
//			Map cbdArea = new HashMap();
//			cbdArea.put("id", cbdAreaId);
//			p.put("cbdArea", cbdArea);
//		}
//		if(foodTypeId != 0){
//			Map foodType = new HashMap();
//			foodType.put("id", foodTypeId);
//			p.put("foodType", foodType);
//		}
//		if(landmarkId != 0){
//			Map landmark = new HashMap();
//			landmark.put("id", landmarkId);
//			p.put("landmark", landmark);
//		}
//		if(tk != 0)
//			p.put("takeAway", 1);
//		if(card != 0)
//			p.put("payCard", 1);
//		if(yh == 1)
//			p.put("yh", 1);
//		if(perPayMin != -1)
//			p.put("perPayMin", perPayMin);
//		if(perPayMax != -1)
//			p.put("perPayMax", perPayMax);
//		if(orderBy != null && !orderBy.equals(""))
//			p.put("orderBy", orderBy);
		p.put("nowTime", UtilTools.formatDate(new Date(System.currentTimeMillis()), "yyyy-MM-dd"));
		if(page < 1)
			page = 1;
		int begin = (page-1)*pageSize;
		int end = pageSize;
		p.put("begin", begin);
		p.put("end", end);
		return this.searchDao.searchStore(p);
	}
	
	/**
	 * @param keywords 搜索关键字
	 * @param yh 是否有优惠  0 没有 1有
	 * @param perPayMin 人均下线
	 * @param perPayMax 人均上线
	 * @param cityAreaId 城市区域的id
	 * @param cbdAreaId 商区的id
	 * @param foodTypeId 菜系的id
	 * @param landmarkId 地标建筑的id
	 * @param tk 是否有外卖
	 * @param card 是否可刷卡
	 * */
	public int countSearchStoreNum(Map p){
//		Map p = new HashMap();		
//		if(keywords!=null){
//			keywords = keywords.replaceAll("\\s{2,}", " ").trim();
//			if(!keywords.equals(""))
//				p.put("keywords", keywords);
//		}
//		if(yh == 1)
//			p.put("yh", 1);
//		if(cityAreaId != 0){
//			Map cityArea = new HashMap();
//			cityArea.put("id", cityAreaId);
//			p.put("cityArea", cityArea);
//		}
//		if(cbdAreaId != 0){
//			Map cbdArea = new HashMap();
//			cbdArea.put("id", cbdAreaId);
//			p.put("cbdArea", cbdArea);
//		}
//		if(foodTypeId != 0){
//			Map foodType = new HashMap();
//			foodType.put("id", foodTypeId);
//			p.put("foodType", foodType);
//		}
//		if(landmarkId != 0){
//			Map landmark = new HashMap();
//			landmark.put("id", landmarkId);
//			p.put("landmark", landmark);
//		}
//		if(perPayMin != -1)
//			p.put("perPayMin", perPayMin);
//		if(perPayMax != -1)
//			p.put("perPayMax", perPayMax);
//		if(tk != 0)
//			p.put("takeAway", 1);
//		if(card != 0)
//			p.put("payCard", 1);
//		if(yh == 1)
//			p.put("yh", 1);
		p.put("nowTime", UtilTools.formatDate(new Date(System.currentTimeMillis()), "yyyy-MM-dd"));
		return this.searchDao.countSearchStoreNum(p);
	}
	
	
	/**
	 * 搜索优惠
	 * @param keywords 关键字
	 * @param cityAreaId 城市区域
	 * @param cbdAreaId 商区
	 * @param foodTypeId 菜系
	 * @param type 优惠类型 0 所有类型 1优惠信息 2优惠券
	 * @param landmarkId 地标建筑的id
	 * @param orderBy 排序
	 * @param page 
	 * @param pageSize
	 * */
	public List searchStorePromotion(Map p, int page, int pageSize){
//		Map p = new HashMap();		
//		if(keywords!=null){
//			keywords = keywords.replaceAll("\\s{2,}", " ").trim();
//			if(!keywords.equals(""))
//				p.put("keywords", keywords);
//		}
//		if(cityAreaId != 0){
//			Map cityArea = new HashMap();
//			cityArea.put("id", cityAreaId);
//			p.put("cityArea", cityArea);
//		}
//		if(cbdAreaId != 0){
//			Map cbdArea = new HashMap();
//			cbdArea.put("id", cbdAreaId);
//			p.put("cbdArea", cbdArea);
//		}
//		if(foodTypeId != 0){
//			Map foodType = new HashMap();
//			foodType.put("id", foodTypeId);
//			p.put("foodType", foodType);
//		}
//		if(landmarkId != 0){
//			Map landmark = new HashMap();
//			landmark.put("id", landmarkId);
//			p.put("landmark", landmark);
//		}
//		if(type != 0)
//			p.put("type", type);
//		if(orderBy != null && !orderBy.equals(""))
//			p.put("orderBy", orderBy);
		p.put("nowTime", UtilTools.formatDate(new Date(System.currentTimeMillis()), "yyyy-MM-dd"));
		if(page < 1)
			page = 1;
		int begin = (page-1)*pageSize;
		int end = pageSize;
		p.put("begin", begin);
		p.put("end", end);
		return this.searchDao.searchStorePromotion(p);
	}
	
	/**
	 * 获取优惠的数量
	 * @param keywords 关键字
	 * @param cityAreaId 城市区域
	 * @param cbdAreaId 商区
	 * @param foodTypeId 菜系
	 * @param landmarkId 地标建筑的id
	 * @param type 优惠类型 0 所有类型 1优惠信息 2优惠券 
	 * */
	public int countStorePromotionNum(Map p){
//		Map p = new HashMap();		
//		if(keywords!=null){
//			keywords = keywords.replaceAll("\\s{2,}", " ").trim();
//			if(!keywords.equals(""))
//				p.put("keywords", keywords);
//		}
//		if(cityAreaId != 0){
//			Map cityArea = new HashMap();
//			cityArea.put("id", cityAreaId);
//			p.put("cityArea", cityArea);
//		}
//		if(cbdAreaId != 0){
//			Map cbdArea = new HashMap();
//			cbdArea.put("id", cbdAreaId);
//			p.put("cbdArea", cbdArea);
//		}
//		if(foodTypeId != 0){
//			Map foodType = new HashMap();
//			foodType.put("id", foodTypeId);
//			p.put("foodType", foodType);
//		}
//		if(landmarkId != 0){
//			Map landmark = new HashMap();
//			landmark.put("id", landmarkId);
//			p.put("landmark", landmark);
//		}
//		if(type != 0)
//			p.put("type", type);
		p.put("nowTime", UtilTools.formatDate(new Date(System.currentTimeMillis()), "yyyy-MM-dd"));
		return this.searchDao.countSearchStorePromotionNum(p);
	}

	public void setSearchDao(SearchDao searchDao) {
		this.searchDao = searchDao;
	}
	
	
}
