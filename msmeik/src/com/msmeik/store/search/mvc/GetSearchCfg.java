package com.msmeik.store.search.mvc;

import com.msmeik.store.search.SearchCfg;
import com.msmeik.store.search.SearchCfgApplication;
import com.msmeik.store.search.SearchService;

public class GetSearchCfg {
	private SearchService searchService;
	private int storeId;
	private int type=0;//0表示读取设置表中的关键字设置，1表示申请表中的关键字
	private SearchCfg searchCfg;
	
	public String execute(){
		if(type == 0){
			this.searchCfg = this.searchService.getSearchCfgByStoreId(storeId);
		}else{
			SearchCfgApplication sca = this.searchService.getSearchCfgApplicationByStoreId(storeId);
			if(sca == null)
				searchCfg = null;
			else
				searchCfg = sca.getSearchCfg();
		}
		if(searchCfg == null)
			searchCfg = new SearchCfg();
		return "success";
	}



	public void setSearchService(SearchService searchService) {
		this.searchService = searchService;
	}



	public SearchCfg getSearchCfg() {
		return searchCfg;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public void setType(int type) {
		this.type = type;
	}
	
}
