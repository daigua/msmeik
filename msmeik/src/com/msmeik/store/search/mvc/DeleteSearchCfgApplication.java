package com.msmeik.store.search.mvc;

import com.msmeik.store.search.SearchService;

public class DeleteSearchCfgApplication {
	private SearchService searchService;
	private int storeId;
	private int ok;
	
	public String execute(){
		this.ok = this.searchService.deleteSearchCfgApplicationByStoreId(storeId);
		return "success";
	}

	public int getOk() {
		return ok;
	}

	public void setSearchService(SearchService searchService) {
		this.searchService = searchService;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}
	
}
