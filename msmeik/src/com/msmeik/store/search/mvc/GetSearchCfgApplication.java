package com.msmeik.store.search.mvc;

import com.msmeik.store.search.SearchCfg;
import com.msmeik.store.search.SearchCfgApplication;
import com.msmeik.store.search.SearchService;

public class GetSearchCfgApplication {
	private SearchService searchService;
	private int storeId;
	private SearchCfgApplication sca;
	
	public String execute(){
		this.sca = this.searchService.getSearchCfgApplicationByStoreId(storeId);
		if(sca == null){
			sca = new SearchCfgApplication();
			sca.setSearchCfg(new SearchCfg());
		}
		return "success";
	}

	public SearchCfgApplication getSca() {
		return sca;
	}

	public void setSearchService(SearchService searchService) {
		this.searchService = searchService;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}
	
}
