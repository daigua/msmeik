package com.msmeik.store.search.mvc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.msmeik.store.StoreConfig;
import com.msmeik.store.search.SearchService;

public class SearchStorePromotion {
	private SearchService searchService;
	
	//�������
	private String kw = "";//keywords
	private int ca;// city area
	private int cba;// cbd area
	private int ft;// food type 
	private int type;//promoiton type
	private String order = "";//order by
	private int lm;
	private int tg = 1;
	private int dt;
	
	private int page =1 ;
	private int pageSize = 10;
	
	private String searchWhat = "yh";
	
	private List promotionSearchResultList;
	private int totalCount;
	
	public String execute(){
		
		String _orderBy = null;
		if(order != null && !order.trim().equals("")){
			_orderBy = order.replaceAll("-", " ");
		}
		String _kw = "";
		_kw = kw;
		Map p = new HashMap();
		StringBuffer kw_buf = new StringBuffer();
		if(_kw!=null)
			kw_buf.append(kw.replaceAll("\\s{2,}", " ").trim());
			
		if(ca != 0){
			kw_buf.append(StoreConfig.getCityAreaNameById(ca)+" ");
		}
		if(cba!=0){
			kw_buf.append(StoreConfig.getCbdAreaNameById(cba)+" ");
		}
		if(ft != 0){
			kw_buf.append(StoreConfig.getFoodTypeNameById(ft)+" ");
		}
		if(lm != 0){
			kw_buf.append(StoreConfig.getLandmarkNameById(lm)+" ");
		}
		if(dt != 0){
			kw_buf.append(StoreConfig.getDiningTypeNameById(dt)+" ");
		}
		_kw = kw_buf.toString();
		if(!_kw.trim().equals("")){
			p.put("keywords", _kw);
		}
		p.put("tg", tg);
		if(type != 0)
			p.put("type", type);
		if(_orderBy != null && !_orderBy.equals(""))
			p.put("orderBy", _orderBy);
		this.promotionSearchResultList = this.searchService.searchStorePromotion(p, page, pageSize);
		this.totalCount = this.searchService.countStorePromotionNum(p);
		return "success";
	}

	public String getKw() {
		return kw;
	}

	public void setKw(String kw) {
		this.kw = kw;
	}

	public int getCa() {
		return ca;
	}

	public void setCa(int ca) {
		this.ca = ca;
	}

	public int getCba() {
		return cba;
	}

	public int getDt() {
		return dt;
	}

	public void setDt(int dt) {
		this.dt = dt;
	}

	public void setCba(int cba) {
		this.cba = cba;
	}

	public int getFt() {
		return ft;
	}

	public void setFt(int ft) {
		this.ft = ft;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	
	public List getPromotionSearchResultList() {
		return promotionSearchResultList;
	}

	public void setSearchService(SearchService searchService) {
		this.searchService = searchService;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public String getSearchWhat() {
		return searchWhat;
	}

	public int getLm() {
		return lm;
	}

	public void setLm(int lm) {
		this.lm = lm;
	}

	public int getTg() {
		return tg;
	}

	public void setTg(int tg) {
		this.tg = tg;
	}


	
	
}
