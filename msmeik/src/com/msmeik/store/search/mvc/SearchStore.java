package com.msmeik.store.search.mvc;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.msmeik.store.StoreConfig;
import com.msmeik.store.promotion.PromotionService;
import com.msmeik.store.search.SearchService;
import com.msmeik.store.search.StoreSearchResult;

public class SearchStore {
	private SearchService searchService;
	private PromotionService promotionService;
	private String kw = "";
	
	private String searchWhat = "store";
	
	private List searchResultList;
	
	//请求参数
	private String perPay="";
	private int yh = 0;//全部 1 有优惠
	private int ca;//城市区域
	private int cba;//商区
	private int ft;//菜系
	private int tk;//外卖
	private int card;//可用卡支付
	private String orderBy="";
	private int lm;//地标
	private int tg = 1;
	private int dt;//就餐类型
	
	private int page = 1;
	private int pageSize = 10;
	
	private int totalCount;
	
	public String execute(){
		int[] perPayRange = this.getPerPayRange();
		String _kw = "";
		_kw = kw;
		Map p = new HashMap();
		StringBuffer kw_buf = new StringBuffer();
		if(_kw!=null)
			kw_buf.append(kw.replaceAll("\\s{2,}", " ").trim());
			
		if(ca != 0){
			kw_buf.append(StoreConfig.getCityAreaNameById(ca)+" ");
		}
		if(cba!=0){
			kw_buf.append(StoreConfig.getCbdAreaNameById(cba)+" ");
		}
		if(ft != 0){
			kw_buf.append(StoreConfig.getFoodTypeNameById(ft)+" ");
		}
		if(lm != 0){
			kw_buf.append(StoreConfig.getLandmarkNameById(lm)+" ");
		}
		if(dt != 0){
			kw_buf.append(StoreConfig.getDiningTypeNameById(dt)+" ");
		}
		_kw = kw_buf.toString();
		if(!_kw.trim().equals("")){
			p.put("keywords", _kw.replaceAll("[/,]", " "));
		}
		if(tk != 0)
			p.put("takeAway", 1);
		if(card != 0)
			p.put("payCard", 1);
		if(yh == 1)
			p.put("yh", 1);
		if(perPayRange[0] != -1)
			p.put("perPayMin", perPayRange[0]);
		if(perPayRange[1] != -1)
			p.put("perPayMax", perPayRange[1]);
		String _orderBy = null;
		if(orderBy != null){
			_orderBy = orderBy.replaceAll("-", " ");
		}
		if(tg == 1){
			p.put("tg", 1);
		}
		p.put("orderBy", _orderBy);
		
		this.searchResultList = this.searchService.searchStore(p, page, pageSize);
		this.totalCount = this.searchService.countSearchStoreNum(p);
		for(StoreSearchResult searchResult:(List<StoreSearchResult>)this.searchResultList){
			Date now = new Date(System.currentTimeMillis());
			if(searchResult.getPromotionOverview().getPromotionNum() > 0){
				searchResult.setPromotionList(this.promotionService.getPromotionList(searchResult.getStore(), -1, now, null, 1, 1));
			}
		}
		return "success";
	}

	

	public String getKw() {
		return kw;
	}



	public void setKw(String kw) {
		this.kw = kw;
	}



	public List getSearchResultList() {
		return searchResultList;
	}
	
	private int[] getPerPayRange(){
		if(this.perPay == null || !this.perPay.matches("^(0|[1-9])\\d{0,2}-(0|[1-9])\\d{0,2}$"))
			return new int[]{-1,-1};
		String[] rangeExp = this.perPay.split("-");
		return new int[]{Integer.parseInt(rangeExp[0]),Integer.parseInt(rangeExp[1])==0?-1:Integer.parseInt(rangeExp[1])};
	}

	public void setSearchService(SearchService searchService) {
		this.searchService = searchService;
	}
	
	
	
	public String getPerPay() {
		return perPay;
	}

	public void setPerPay(String perPay) {
		this.perPay = perPay;
	}

	public int getYh() {
		return yh;
	}

	public void setYh(int yh) {
		this.yh = yh;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getTotalCount() {
		return totalCount;
	}
	
	

	public void setPromotionService(PromotionService promotionService) {
		this.promotionService = promotionService;
	}
	
	

	public int getCa() {
		return ca;
	}

	public void setCa(int ca) {
		this.ca = ca;
	}

	public int getCba() {
		return cba;
	}

	public void setCba(int cba) {
		this.cba = cba;
	}

	public int getFt() {
		return ft;
	}

	public void setFt(int ft) {
		this.ft = ft;
	}
	
	

	public int getTk() {
		return tk;
	}

	public void setTk(int tk) {
		this.tk = tk;
	}

	public int getCard() {
		return card;
	}

	public void setCard(int card) {
		this.card = card;
	}
	
	

	public String getSearchWhat() {
		return searchWhat;
	}



	



	public int getLm() {
		return lm;
	}



	public void setLm(int lm) {
		this.lm = lm;
	}



	public int getTg() {
		return tg;
	}



	public void setTg(int tg) {
		this.tg = tg;
	}



	public int getDt() {
		return dt;
	}



	public void setDt(int dt) {
		this.dt = dt;
	}



	public static void main(String[] args){
		String str = "10-0";
		String regex = "^(0|[1-9])\\d{0,2}-(0|[1-9])\\d{0,2}$";
		System.out.println(str.matches(regex));
	}
}
