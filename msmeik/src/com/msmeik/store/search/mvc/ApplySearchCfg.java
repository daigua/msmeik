package com.msmeik.store.search.mvc;

import com.msmeik.store.Store;
import com.msmeik.store.search.SearchCfg;
import com.msmeik.store.search.SearchCfgApplication;
import com.msmeik.store.search.SearchService;
import com.opensymphony.xwork2.ActionSupport;

public class ApplySearchCfg extends ActionSupport{
	private SearchService searchService;
	private int storeId;
	private int ok;
	private SearchCfg searchCfg;
	
	public String execute(){
		Store store = new Store();
		store.setId(storeId);
		SearchCfgApplication sca = new SearchCfgApplication();
		this.searchCfg.setStore(store);
		sca.setSearchCfg(searchCfg);
		this.ok = this.searchService.applySearchCfgApplication(sca);
		return "input";
	}

	public int getStoreId() {
		return storeId;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	

	public SearchCfg getSearchCfg() {
		return searchCfg;
	}

	public void setSearchCfg(SearchCfg searchCfg) {
		this.searchCfg = searchCfg;
	}

	public int getOk() {
		return ok;
	}

	public void setSearchService(SearchService searchService) {
		this.searchService = searchService;
	}
	
}
