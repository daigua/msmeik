package com.msmeik.store.search;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class SearchDao extends SqlMapClientDaoSupport{
	public int insertSearchCfg(Object searchCfg){
		return (Integer)this.getSqlMapClientTemplate().insert("insertSearchCfg", searchCfg);
	}
	public int updateSearchCfg(Object searchCfg){
		return this.getSqlMapClientTemplate().update("updateSearchCfg", searchCfg);
	}
	public List selectSearchCfg(Object p){
		return this.getSqlMapClientTemplate().queryForList("selectSearchCfg", p);
	}
	
	public int insertSearchCfgApplication(Object searchCfgApp){
		return (Integer)this.getSqlMapClientTemplate().insert("insertSearchCfgApplication", searchCfgApp);
	}
	public int updateSearchCfgApplication(Object searchCfgApp){
		return this.getSqlMapClientTemplate().update("updateSearchCfgApplication", searchCfgApp);
	}
	public List selectSearchCfgApplication(Object p){
		return this.getSqlMapClientTemplate().queryForList("selectSearchCfgApplication", p);
	}
	public int deleteSearchCfgApplication(Object p){
		return this.getSqlMapClientTemplate().delete("deleteSearchCfgApplication", p);
	}
	public int selectSearchCfgApplicationCount(Object p){
		return (Integer)this.getSqlMapClientTemplate().queryForObject("selectSearchCfgApplicationCount", p);
	}
	
	public List searchStore(Object p){
		return this.getSqlMapClientTemplate().queryForList("searchStore", p);
	}
	
	public int countSearchStoreNum(Object p){
		return (Integer) this.getSqlMapClientTemplate().queryForObject("countSearchStoreNum", p);
	}
	
	public List searchStorePromotion(Object p){
		return this.getSqlMapClientTemplate().queryForList("searchStorePromotion", p);
	}
	public int countSearchStorePromotionNum(Object p){
		return (Integer) this.getSqlMapClientTemplate().queryForObject("countSearchStorePromotionNum", p);
	}
}
