package com.msmeik.store.search;

import java.util.ArrayList;
import java.util.List;

import com.msmeik.store.Store;
import com.msmeik.store.comment.CommentOverview;
import com.msmeik.store.promotion.PromotionOverview;
import com.msmeik.store.vas.promotion.PromotionVas;

public class StoreSearchResult {
	private Store store;
	private CommentOverview commentOverview;
	private PromotionOverview promotionOverview;
	private List<com.msmeik.store.promotion.Promotion> promotionList = new ArrayList();
	private PromotionVas promotionVas;
	
	
	public Store getStore() {
		return store;
	}
	public void setStore(Store store) {
		this.store = store;
	}
	public CommentOverview getCommentOverview() {
		return commentOverview;
	}
	public void setCommentOverview(CommentOverview commentOverview) {
		this.commentOverview = commentOverview;
	}
	public PromotionOverview getPromotionOverview() {
		return promotionOverview;
	}
	public void setPromotionOverview(PromotionOverview promotionOverview) {
		this.promotionOverview = promotionOverview;
	}
	public List<com.msmeik.store.promotion.Promotion> getPromotionList() {
		return promotionList;
	}
	public void setPromotionList(
			List<com.msmeik.store.promotion.Promotion> promotionList) {
		this.promotionList = promotionList;
	}
	public PromotionVas getPromotionVas() {
		return promotionVas;
	}
	public void setPromotionVas(PromotionVas promotionVas) {
		this.promotionVas = promotionVas;
	}
	
}
