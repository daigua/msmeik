package com.msmeik.store.search.admin.mvc;

import com.msmeik.store.search.SearchService;

public class VerifySearchCfgApplication {
	private SearchService searchService;
	private int storeId;
	private int ok;
	private int status;
	private String result;
	
	public String execute(){
		this.ok = this.searchService.verifySearchCfgApplication(storeId, status,result);
		return "success";
	}

	public int getOk() {
		return ok;
	}

	public void setSearchService(SearchService searchService) {
		this.searchService = searchService;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}
	
	

	public void setResult(String result) {
		this.result = result;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	
}
