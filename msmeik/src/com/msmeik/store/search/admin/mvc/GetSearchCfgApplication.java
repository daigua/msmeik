package com.msmeik.store.search.admin.mvc;

import java.util.List;

import com.msmeik.store.search.SearchService;

public class GetSearchCfgApplication {
	private SearchService searchService;
	private int storeId;
	private int page = 1;
	private int pageSize=10;
	private int totalCount;
	private int status;
	private List searchCfgApplicationList;
	
	public String execute(){
		this.searchCfgApplicationList = this.searchService.getSearchCfgApplication(storeId, status, page, pageSize);
		this.totalCount = this.searchService.getSearchCfgApplicationNum(storeId, status);
		return "success";
	}

	public int getStoreId() {
		return storeId;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public List getSearchCfgApplicationList() {
		return searchCfgApplicationList;
	}

	public void setSearchService(SearchService searchService) {
		this.searchService = searchService;
	}
	
	
}
