package com.msmeik.store.search;

public class SearchCfgApplication {
	private int id;
	private SearchCfg searchCfg;
	private String result;
	private int status;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public SearchCfg getSearchCfg() {
		return searchCfg;
	}
	public void setSearchCfg(SearchCfg searchCfg) {
		this.searchCfg = searchCfg;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
}
