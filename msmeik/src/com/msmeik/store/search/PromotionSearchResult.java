package com.msmeik.store.search;

import com.msmeik.store.Store;
import com.msmeik.store.promotion.Promotion;
import com.msmeik.store.vas.promotion.PromotionVas;

public class PromotionSearchResult {
	private Promotion promotion;
	private PromotionVas promotionVas;
	private Store store;
	public Promotion getPromotion() {
		return promotion;
	}
	public void setPromotion(Promotion promotion) {
		this.promotion = promotion;
	}
	public PromotionVas getPromotionVas() {
		return promotionVas;
	}
	public void setPromotionVas(PromotionVas promotionVas) {
		this.promotionVas = promotionVas;
	}
	public Store getStore() {
		return store;
	}
	public void setStore(Store store) {
		this.store = store;
	}
	
	
}
