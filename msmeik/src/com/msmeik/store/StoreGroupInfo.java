package com.msmeik.store;

import java.util.Date;

public class StoreGroupInfo {
	private int id;
	private Store store;
	private StoreGroup group;
	private Date addTime;
	private Date expiredTime;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Store getStore() {
		return store;
	}
	public void setStore(Store store) {
		this.store = store;
	}
	public StoreGroup getGroup() {
		return group;
	}
	public void setGroup(StoreGroup group) {
		this.group = group;
	}
	public Date getAddTime() {
		return addTime;
	}
	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}
	public Date getExpiredTime() {
		return expiredTime;
	}
	public void setExpiredTime(Date expiredTime) {
		this.expiredTime = expiredTime;
	}
	
	
}
