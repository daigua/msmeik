package com.msmeik.store.comment;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class CommentDao extends SqlMapClientDaoSupport{
	public int insertComment(Object comment){
		return (Integer)this.getSqlMapClientTemplate().insert("insertComment", comment);
	}
	public int updateComment(Object p){
		return this.getSqlMapClientTemplate().update("updateComment", p);
	}
	public int selectCommentCount(Object p){
		return (Integer)this.getSqlMapClientTemplate().queryForObject("selectCommentCount", p);
	}
	public List selectComment(Object p){
		return this.getSqlMapClientTemplate().queryForList("selectComment", p);
	}
	public int insertCommentOverview(Object commentOverview){
		return (Integer)this.getSqlMapClientTemplate().insert("insertCommentOverview", commentOverview);
	}
	public List selectCommentOveriew(Object p){
		return this.getSqlMapClientTemplate().queryForList("selectCommentOverview", p);
	}
	public int updateCommentOveriew(Object commentOverview){
		return this.getSqlMapClientTemplate().update("updateCommentOverview", commentOverview);
	}
	public int deleteComment(Object p){
		return this.getSqlMapClientTemplate().delete("deleteComment", p);
	}
	
}
