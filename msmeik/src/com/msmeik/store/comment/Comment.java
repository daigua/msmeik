package com.msmeik.store.comment;

import java.util.Date;

import com.msmeik.member.User;
import com.msmeik.store.Store;

public class Comment {
	private int id;
	private Store store;
	private String content;
	private int tasteScore;
	private int evmtScore;//环境评分
	private int serveScore;
	private int worthScore;
	private Date commentTime;
	private int digTimes;//顶的数
	private String diningType;//就餐类型
	private String relishRecommend;//推荐菜
	private float  perPay;//人均
	private User user;
	public int getId() {
		return id;
	}
	public Store getStore() {
		return store;
	}
	public void setStore(Store store) {
		this.store = store;
	}
	public int getTasteScore() {
		return tasteScore;
	}
	public void setTasteScore(int tasteScore) {
		this.tasteScore = tasteScore;
	}
	public int getEvmtScore() {
		return evmtScore;
	}
	public void setEvmtScore(int evmtScore) {
		this.evmtScore = evmtScore;
	}
	public int getServeScore() {
		return serveScore;
	}
	public void setServeScore(int serveScore) {
		this.serveScore = serveScore;
	}
	public int getWorthScore() {
		return worthScore;
	}
	public void setWorthScore(int worthScore) {
		this.worthScore = worthScore;
	}
	public String getDiningType() {
		return diningType;
	}
	public void setDiningType(String diningType) {
		this.diningType = diningType;
	}
	
	public String getRelishRecommend() {
		return relishRecommend;
	}
	public void setRelishRecommend(String relishRecommend) {
		this.relishRecommend = relishRecommend;
	}
	
	public float getPerPay() {
		return perPay;
	}
	public void setPerPay(float perPay) {
		this.perPay = perPay;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getCommentTime() {
		return commentTime;
	}
	public void setCommentTime(Date commentTime) {
		this.commentTime = commentTime;
	}
	public int getDigTimes() {
		return digTimes;
	}
	public void setDigTimes(int digTimes) {
		this.digTimes = digTimes;
	}

	
	
}
