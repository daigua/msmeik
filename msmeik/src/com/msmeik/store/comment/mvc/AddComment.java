package com.msmeik.store.comment.mvc;

import java.util.Date;

import com.msmeik.authentication.AuthenticationService;
import com.msmeik.credit.Credit;
import com.msmeik.credit.CreditConfig;
import com.msmeik.credit.CreditLog;
import com.msmeik.credit.CreditService;
import com.msmeik.member.User;
import com.msmeik.store.comment.Comment;
import com.msmeik.store.comment.CommentService;
import com.opensymphony.xwork2.ActionSupport;

public class AddComment extends ActionSupport{
	private CommentService commentService;
	private CreditService creditService;
	private int ok;
	private Comment comment;
	private int credit;
	public String execute(){
		User user = AuthenticationService.getLoginUser();
		comment.setUser(user);
		this.ok = this.commentService.addComment(comment);
		//获得积分
		Credit credit = new Credit();
		int amount = CreditConfig.getCreditRule().get("comment");
		credit.setCredit(amount);
		credit.setUser(user);
		CreditLog creditLog = new CreditLog();
		creditLog.setTitle("发表点评获得"+amount+"积分");
		this.credit = this.creditService.updateCredit(credit, creditLog);
		return "success";
	}
	public Comment getComment() {
		return comment;
	}
	public void setComment(Comment comment) {
		this.comment = comment;
	}
	public int getOk() {
		return ok;
	}
	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}
	public int getCredit() {
		return credit;
	}
	public void setCreditService(CreditService creditService) {
		this.creditService = creditService;
	}
	
}
