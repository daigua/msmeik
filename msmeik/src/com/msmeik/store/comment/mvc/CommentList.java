package com.msmeik.store.comment.mvc;

import java.util.List;

import com.msmeik.member.User;
import com.msmeik.store.Store;
import com.msmeik.store.comment.CommentService;

public class CommentList {
	private CommentService commentService;
	private int userId;
	private int storeId;
	private List commentList;
	private int lowerScore = -1;
	private int upperScore = -1;
	private int page = 1;
	private int totalCount;
	private int pageSize=10;
	
	public String execute(){
		User user = new User();
		user.setId(userId);
		Store store = new Store();
		store.setId(storeId);
		this.commentList = this.commentService.getCommentList(user, store, lowerScore, upperScore, page,pageSize);
		this.totalCount = this.commentService.getCommentNum(user, store, lowerScore, upperScore);
		return "success";
	}

	
	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}


	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getStoreId() {
		return storeId;
	}


	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public List getCommentList() {
		return commentList;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getUserId() {
		return userId;
	}
	
}
