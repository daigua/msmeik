package com.msmeik.store.comment.mvc;

import com.msmeik.authentication.AuthenticationService;
import com.msmeik.credit.Credit;
import com.msmeik.credit.CreditConfig;
import com.msmeik.credit.CreditLog;
import com.msmeik.credit.CreditService;
import com.msmeik.member.User;
import com.msmeik.store.comment.CommentService;

public class DeleteComment {
	private CommentService commentService;
	private CreditService creditService;
	private int commentId;
	private int ok;
	
	public String execute(){
		
		User user = AuthenticationService.getLoginUser();
		this.ok = this.commentService.deleteComment(user, commentId);
		if(this.ok > 0){
			//删除积分
			Credit credit = new Credit();
			int amount = -CreditConfig.getCreditRule().get("comment");
			credit.setCredit(amount);
			credit.setUser(user);
			CreditLog creditLog = new CreditLog();
			creditLog.setTitle("删除评论并扣除"+(-amount)+"积分");
			this.creditService.updateCredit(credit, creditLog);
		}
		return "success";
	}

	public int getOk() {
		return ok;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}

	public void setCreditService(CreditService creditService) {
		this.creditService = creditService;
	}
	
	
}
