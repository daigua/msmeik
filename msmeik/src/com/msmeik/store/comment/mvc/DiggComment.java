package com.msmeik.store.comment.mvc;

import com.msmeik.store.comment.CommentService;

public class DiggComment {
	private CommentService commentService;
	private int commentId;
	private int ok;
	
	public String execute(){
		this.ok = this.commentService.diggComment(commentId);
		return "success";
	}

	public int getCommentId() {
		return commentId;
	}

	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}

	public int getOk() {
		return ok;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}
	
	
}
