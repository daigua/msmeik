package com.msmeik.store.comment;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.msmeik.member.User;
import com.msmeik.store.Store;

public class CommentService {
	private CommentDao commentDao;

	/**
	 * 添加评论，并返回添加id
	 * */
	public int addComment(Comment comment){
		if(comment.getUser().getId()<=0 || comment.getStore().getId() <=0 )
			return 0;
		comment.setCommentTime(new Date());
		int totalScore = comment.getTasteScore()+comment.getServeScore()+comment.getEvmtScore();
		int score = totalScore/3;
		int id = this.commentDao.insertComment(comment);
		//更新commentOverview
		CommentOverview overview = this.getCommentOverviewByStoreId(comment.getStore().getId());
		if(overview == null){
			overview = new CommentOverview();
			overview.setStore(comment.getStore());
			overview.setCommentTimes(1);
			overview.setLastCommentTime(new Date());
			overview.setScore(score);
			overview.setServeScore(comment.getServeScore());
			overview.setTasteScore(comment.getTasteScore());
			//overview.setWorthScore(comment.getWorthScore());
			overview.setEvmtScore(comment.getEvmtScore());
			this.commentDao.insertCommentOverview(overview);
		}else{			
			overview.setLastCommentTime(new Date());
			overview.setScore((overview.getScore()*overview.getCommentTimes()+score)/(overview.getCommentTimes()+1));
			overview.setServeScore((overview.getServeScore()*overview.getCommentTimes()+comment.getServeScore())/(overview.getCommentTimes()+1));
			overview.setTasteScore((overview.getTasteScore()*overview.getCommentTimes()+comment.getTasteScore())/(overview.getCommentTimes()+1));
			//overview.setWorthScore((overview.getWorthScore()*overview.getCommentTimes()+comment.getWorthScore())/(overview.getCommentTimes()+1));
			overview.setEvmtScore((overview.getEvmtScore()*overview.getCommentTimes()+comment.getEvmtScore())/(overview.getCommentTimes()+1));
			overview.setCommentTimes(1);
			this.commentDao.updateCommentOveriew(overview);
		}
		return id;
	}
	
	/**
	 * get commentOverview by target
	 * */
	public CommentOverview getCommentOverviewByStoreId(int storeId){
		if(storeId <= 0)
			return null;
		Map p = new HashMap();
		Store store = new Store();
		store.setId(storeId);
		p.put("store", store);
		List l = this.commentDao.selectCommentOveriew(p);
		if(l.size()!=1)
			return null;
		return (CommentOverview)l.get(0);
	}
	/**
	 * get comment list
	 * */
	public List getCommentList(User user,Store store,int lowerScore, int upperScore, int page, int pageSize){
		Map p = new HashMap();
		if(user !=null && user.getId()>0)
			p.put("user", user);
		if(lowerScore != -1)
			p.put("lowerScore", lowerScore);
		if(upperScore != -1){
			p.put("uppperScore", upperScore);
		}
		if(page < 1)
			page = 1;
		int begin = (page-1)*pageSize;
		int end = pageSize;
		p.put("begin", begin);
		p.put("end", end);
		if(store!=null && store.getId() > 0){
			p.put("store", store);
		}
		return this.commentDao.selectComment(p);
	}
	/**
	 * get comment number by user and target
	 * */
	public int getCommentNum(User user,Store store, int lowerScore, int upperScore){
		Map p = new HashMap();
		if(user != null && user.getId()>0)
			p.put("user", user);	
		if(store!=null && store.getId() > 0){
			p.put("store", store);
		}
		if(lowerScore != -1)
			p.put("lowerScore", lowerScore);
		if(upperScore != -1){
			p.put("uppperScore", upperScore);
		}
		return this.commentDao.selectCommentCount(p);
	}
	
	/**
	 * delete comment
	 * */
	public int deleteComment(User user,int commentId){
		if(user.getId() <= 0 || commentId <= 0)
			return 0;
		Map p = new HashMap();
		p.put("user", user);
		p.put("id", commentId);
		return this.commentDao.deleteComment(p);
	}
	
	/**
	 * digg comment
	 * */
	public int diggComment(int commentId){
		if(commentId <= 0)
			return 0;
		Map p = new HashMap();
		p.put("id", commentId);
		p.put("digTimes", 1);
		return this.commentDao.updateComment(p);
	}
	
	/**
	 * this method only for admin
	 * delete comment by id
	 * */
	public int deleteComment(int commentId){
		if(commentId <= 0)
			return 0;
		Map p = new HashMap();
		p.put("id", commentId);
		return this.commentDao.deleteComment(p);
	}	
	
	public void setCommentDao(CommentDao commentDao) {
		this.commentDao = commentDao;
	}	
}
