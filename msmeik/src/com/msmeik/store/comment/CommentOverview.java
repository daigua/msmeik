package com.msmeik.store.comment;

import java.util.Date;

import com.msmeik.store.Store;

public class CommentOverview {
	private int id;
	private Store store;
	private int tasteScore;
	private int evmtScore;
	private int serveScore;
	private int worthScore;
	private int commentTimes;//������
	private int score;
	private Date lastCommentTime;
	public int getId() {
		return id;
	}
	public Store getStore() {
		return store;
	}
	public void setStore(Store store) {
		this.store = store;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getTasteScore() {
		return tasteScore;
	}
	public void setTasteScore(int tasteScore) {
		this.tasteScore = tasteScore;
	}
	public int getEvmtScore() {
		return evmtScore;
	}
	public void setEvmtScore(int evmtScore) {
		this.evmtScore = evmtScore;
	}
	public int getServeScore() {
		return serveScore;
	}
	public void setServeScore(int serveScore) {
		this.serveScore = serveScore;
	}
	public int getWorthScore() {
		return worthScore;
	}
	public void setWorthScore(int worthScore) {
		this.worthScore = worthScore;
	}
	public int getCommentTimes() {
		return commentTimes;
	}
	public void setCommentTimes(int commentTimes) {
		this.commentTimes = commentTimes;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public Date getLastCommentTime() {
		return lastCommentTime;
	}
	public void setLastCommentTime(Date lastCommentTime) {
		this.lastCommentTime = lastCommentTime;
	}
	
	
}
