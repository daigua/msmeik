package com.msmeik.store.content;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.msmeik.store.Store;

public class ContentService {
	private ContentDao contentDao;
	
	/**
	 * insert content and result the id
	 * */
	public int addContent(Content content){
		if(content.getStore().getId() <= 0)
			return 0;
		return this.contentDao.insertContent(content);
	}
	
	/**
	 * get content list
	 * */
	public List getContentList(int storeId,int categoryId,int page,int pageSize){
		if(storeId <= 0)
			return new ArrayList();
		Map p = new HashMap();
		Store store = new Store();
		store.setId(storeId);
		p.put("store", store);
		if(categoryId != -1){
			ContentCategory category = new ContentCategory();
			category.setId(categoryId);
			p.put("category", category);
		}
		if(page < 1)
			page = 1;
		int begin = (page-1)*pageSize;
		int end = pageSize;
		p.put("begin", begin);
		p.put("end", end);
		return this.contentDao.selectContentList(p);
	}
	/**
	 * get content number
	 * */
	public int getContentCount(int storeId,int categoryId){
		if(storeId <= 0)
			return 0;
		Map p = new HashMap();
		Store store = new Store();
		store.setId(storeId);
		p.put("store", store);
		if(categoryId != -1){
			ContentCategory category = new ContentCategory();
			category.setId(categoryId);
			p.put("category", category);
		}		
		return this.contentDao.selectContentCount(p);
	}
	/**
	 * delete content by store and content id
	 * */
	public int deleteContentByStoreIdAndContentId(int storeId,int contentId){
		if(storeId <= 0 || contentId <= 0)
			return 0;
		Map p = new HashMap();
		Store store = new Store();
		store.setId(storeId);
		p.put("store", store);
		p.put("id", contentId);
		return this.contentDao.deleteContent(p);
	}
	
	/**
	 * set the content onTop
	 * */
	public int setContentOnTopByStoreIdAndContentId(int storeId,int contentId,int onTop){
		if(storeId <= 0 || contentId <= 0)
			return 0;
		Map p = new HashMap();
		Store store = new Store();
		store.setId(storeId);
		p.put("store", store);
		p.put("id", contentId);
		p.put("onTop", onTop);
		return this.contentDao.updateContent(p);
	}
	
	/**
	 * set the content orderTime
	 * */
	public int setContentOrderTimeByStoreIdAndContentId(int storeId,int contentId,Date orderTime){
		if(storeId <= 0 || contentId <= 0)
			return 0;
		Map p = new HashMap();
		Store store = new Store();
		store.setId(storeId);
		p.put("store", store);
		p.put("id", contentId);
		p.put("orderTime", orderTime);
		return this.contentDao.updateContent(p);
	}
	
	/**
	 * get the content by id
	 * */
	public Content getContentById(int contentId){
		if(contentId <= 0)
			return null;
		Map p = new HashMap();
		p.put("id", contentId);
		List l = this.contentDao.selectContentList(p);
		if(l.size()!=1)
			return null;
		return (Content)l.get(0);
	}
	/**
	 * update content
	 * */
	public int updateContent(Content content){
		if(content.getId()<=0 || content.getStore().getId() <= 0)
			return 0;
		return this.contentDao.updateContent(content);
	}
	
	/**
	 * get content category
	 * */
	public List getCategoryList(Store store,boolean readContentNum){
		if(store == null || store.getId() <= 0)
			return new ArrayList();
		Map p = new HashMap();
		p.put("store", store);
		List<ContentCategory> list = this.contentDao.selectContentCategoryList(p);
		if(readContentNum && list.size() > 0){
			Map categoryContentNumMap = new HashMap();
			List<Map> categoryContentNumList = this.countContentNumGroupByCategory(store);
			for(Map contentNumMap:categoryContentNumList){
				categoryContentNumMap.put(contentNumMap.get("categoryId"), contentNumMap.get("contentNum"));
			}
			for(ContentCategory category:list){
				if(categoryContentNumMap.containsKey(category.getId())){
					category.setContentNum((Integer)categoryContentNumMap.get(category.getId()));
				}
			}
		}
		return list;
	}
	
	/**
	 * count content num group by category
	 * */
	public List countContentNumGroupByCategory(Store store){
		if(store == null || store.getId() <= 0)
			return new ArrayList();
		Map p = new HashMap();
		p.put("store", store);
		return this.contentDao.countContentNumGroupByCategory(p);
	}
	

	public void setContentDao(ContentDao contentDao) {
		this.contentDao = contentDao;
	}
	
	
}
