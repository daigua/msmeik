package com.msmeik.store.content;

import java.util.Date;

import com.msmeik.store.Store;
import com.orientor.website.tools.UtilTools;

public class Content {
	private int id;
	private String title;
	private String content;
	private ContentCategory category;
	private Date addTime;
	private Date updateTime;
	private Store store;
	private Date orderTime;
	private int onTop;
	public int getId() {
		return id;
	}
	public Date getOrderTime() {
		return orderTime;
	}
	public void setOrderTime(Date orderTime) {
		this.orderTime = orderTime;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public ContentCategory getCategory() {
		return category;
	}
	public void setCategory(ContentCategory category) {
		this.category = category;
	}
	public Date getAddTime() {
		return addTime;
	}
	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public Store getStore() {
		return store;
	}
	public void setStore(Store store) {
		this.store = store;
	}
	public int getOnTop() {
		return onTop;
	}
	public void setOnTop(int onTop) {
		this.onTop = onTop;
	}
	
	/**
	 * 获取去除html标签，去除回车换行，去除空格后的内容
	 * */
	public String getContentText(){
		return UtilTools.getHtmlText(this.content);
	}
}
