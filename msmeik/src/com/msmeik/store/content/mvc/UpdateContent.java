package com.msmeik.store.content.mvc;

import java.util.Date;

import com.msmeik.store.Store;
import com.msmeik.store.content.Content;
import com.msmeik.store.content.ContentService;
import com.opensymphony.xwork2.ActionSupport;

public class UpdateContent extends ActionSupport{

	private ContentService contentService;
	private int storeId;
	private Content content;
	private int ok;
	
	public String execute(){
		Store store = new Store();
		store.setId(storeId);
		content.setStore(store);
		content.setUpdateTime(new Date());
		this.ok = this.contentService.updateContent(content);
		return "input";
	}

	public Content getContent() {
		return content;
	}

	public void setContent(Content content) {
		this.content = content;
	}

	public int getOk() {
		return ok;
	}

	public void setContentService(ContentService contentService) {
		this.contentService = contentService;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	
}
