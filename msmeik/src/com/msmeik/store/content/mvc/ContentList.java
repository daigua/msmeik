package com.msmeik.store.content.mvc;

import java.util.List;

import com.msmeik.store.content.ContentService;

public class ContentList {
	private ContentService contentService;
	private int storeId;
	private int categoryId = -1;
	private List contentList;
	private int page = 1;
	private int pageSize=20;
	private int totalCount;
	
	public String execute(){
		this.contentList = this.contentService.getContentList(storeId, categoryId, page, pageSize);
		this.totalCount = this.contentService.getContentCount(storeId, categoryId);
		return "success";
	}

	public int getStoreId() {
		return storeId;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public List getContentList() {
		return contentList;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setContentService(ContentService contentService) {
		this.contentService = contentService;
	}
	
	
}
