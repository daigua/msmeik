package com.msmeik.store.content.mvc;

import com.msmeik.store.content.Content;
import com.msmeik.store.content.ContentService;

public class GetContent {
	private ContentService contentService;
	private int contentId;
	private Content content;
	
	public String execute(){
		this.content = this.contentService.getContentById(contentId);
		if(content == null)
			content = new Content();
		return "success";
	}

	public Content getContent() {
		return content;
	}

	public void setContentService(ContentService contentService) {
		this.contentService = contentService;
	}

	public void setContentId(int contentId) {
		this.contentId = contentId;
	}
	
	
}
