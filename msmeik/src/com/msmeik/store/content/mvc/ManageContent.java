package com.msmeik.store.content.mvc;

import java.util.Date;

import com.msmeik.store.content.ContentService;

public class ManageContent {
	private ContentService contentService;
	private int contentId;
	private int storeId;
	private Date orderTime;
	private int ok;
	private int v;
	
	public String delete(){
		this.ok = this.contentService.deleteContentByStoreIdAndContentId(storeId, contentId);
		return "success";
	}
	public String onTop(){
		this.ok = this.contentService.setContentOnTopByStoreIdAndContentId(storeId, contentId, v);
		return "success";
	}
	public String orderTime(){
		orderTime = new Date();
		this.ok = this.contentService.setContentOrderTimeByStoreIdAndContentId(storeId, contentId, orderTime);
		return "success";
	}
	public int getOk() {
		return ok;
	}
	public void setContentService(ContentService contentService) {
		this.contentService = contentService;
	}
	public void setContentId(int contentId) {
		this.contentId = contentId;
	}
	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}
	public void setV(int v) {
		this.v = v;
	}
	public Date getOrderTime() {
		return orderTime;
	}
	
}
