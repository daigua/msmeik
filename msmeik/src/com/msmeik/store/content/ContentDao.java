package com.msmeik.store.content;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class ContentDao extends SqlMapClientDaoSupport{
	
	public int insertContent(Object content){
		return (Integer)this.getSqlMapClientTemplate().insert("insertContent", content);
	}
	
	public int updateContent(Object content){
		return this.getSqlMapClientTemplate().update("updateContent", content);
	}
	public int selectContentCount(Object p){
		return (Integer)this.getSqlMapClientTemplate().queryForObject("selectContentCount", p);
	}
	public List selectContentList(Object p){
		return this.getSqlMapClientTemplate().queryForList("selectContent", p);
	}
	public int deleteContent(Object p){
		return this.getSqlMapClientTemplate().delete("deleteContent", p);
	}
	
	public List selectContentCategoryList(Object p){
		return this.getSqlMapClientTemplate().queryForList("selectContentCategory", p);
	}
	
	public int insertContentCategory(Object category){
		return (Integer)this.getSqlMapClientTemplate().insert("insertContentCategory", category);
	}
	public int updateContentCategory(Object category){
		return this.getSqlMapClientTemplate().update("updateContentCategory", category);
	}
	public int deleteContentCategory(Object p){
		return this.getSqlMapClientTemplate().delete("deleteContentCategory", p);
	}
	public List countContentNumGroupByCategory(Object p){
		return this.getSqlMapClientTemplate().queryForList("countContentNumGroupByCategory", p);
	}
}
