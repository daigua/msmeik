package com.msmeik.store.mvc;

import java.util.List;

import com.msmeik.store.StoreService;

public class GetStoreListByRegistrant {

	private StoreService storeService;	
	private List storeList;
	private String account;
	private int totalCount;
	private int userId;
	private int page=1;
	private int pageSize=10;
	
	public String execute(){
		if(userId>0){
			this.storeList = this.storeService.getStoreListByRegistrantId(userId, page, pageSize);
			this.totalCount = this.storeService.getStroeCountByRegistrantUserId(userId);
		}else{
			this.storeList = this.storeService.getStoreListByRegistrantAccount(this.account, page, pageSize);
			this.totalCount = this.storeService.getStroeCountByRegistrantAccount(account);
		}
		return "success";
	}

	public List getStoreList() {
		return storeList;
	}

	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}

	

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public int getPage() {
		return page;
	}

	public int getPageSize() {
		return pageSize;
	}
	
	
}
