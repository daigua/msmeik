package com.msmeik.store.mvc;

import com.msmeik.store.StoreConfig;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.MethodFilterInterceptor;

public class StoreConfigInterceptor extends MethodFilterInterceptor{

	@Override
	protected String doIntercept(ActionInvocation action) throws Exception {
		// TODO Auto-generated method stub
		action.getInvocationContext().getValueStack().set("StoreConfig", new StoreConfig());
		return action.invoke();
	}
	
}
