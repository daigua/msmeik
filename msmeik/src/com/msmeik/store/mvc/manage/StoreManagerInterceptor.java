package com.msmeik.store.mvc.manage;

import com.msmeik.store.ManagerUtil;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.MethodFilterInterceptor;

public class StoreManagerInterceptor extends MethodFilterInterceptor{
	
	@Override
	protected String doIntercept(ActionInvocation action) throws Exception {
		// TODO Auto-generated method stub
		if(!ManagerUtil.checkManager())
			return null;
		return action.invoke();
	}

}
