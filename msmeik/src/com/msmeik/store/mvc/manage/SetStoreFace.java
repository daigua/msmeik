package com.msmeik.store.mvc.manage;

import java.io.File;
import java.io.FileOutputStream;

import com.msmeik.io.image.ImageScale;
import com.opensymphony.xwork2.ActionSupport;

public class SetStoreFace extends ActionSupport{
	
	private String saveFolder;
	private File faceFile;
	private int maxWidth;
	private int maxHeight;
	private int ok;
	private int storeId;
	
	public String execute(){
		if(storeId <= 0)
			return "input";
		ImageScale imageScale = new ImageScale();
		try {
			FileOutputStream out = new FileOutputStream(this.saveFolder+File.separator+this.storeId);
			imageScale.compressSave(this.faceFile,out,this.maxWidth,this.maxHeight,"jpg");
			out.flush();
			out.close();
			ok = 9;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "input";
	}

	public int getStoreId() {
		return storeId;
	}

	public File getFaceFile() {
		return faceFile;
	}

	public void setFaceFile(File faceFile) {
		this.faceFile = faceFile;
	}

	public int getOk() {
		return ok;
	}

	public void setSaveFolder(String saveFolder) {
		this.saveFolder = saveFolder;
	}

	public void setMaxWidth(int maxWidth) {
		this.maxWidth = maxWidth;
	}

	public void setMaxHeight(int maxHeight) {
		this.maxHeight = maxHeight;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}
	
	
}
