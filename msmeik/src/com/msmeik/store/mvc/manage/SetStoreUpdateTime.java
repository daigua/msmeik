package com.msmeik.store.mvc.manage;

import java.util.Date;

import com.msmeik.store.StoreService;

public class SetStoreUpdateTime {
	private StoreService storeService;
	private int storeId;
	private int ok;
	public String execute(){
		this.ok = this.storeService.setStoreUpdateTime(storeId);
		return "success";
	}
	public int getStoreId() {
		return storeId;
	}
	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}
	public int getOk() {
		return ok;
	}
	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}
	
}
