package com.msmeik.store.mvc.manage;

import com.msmeik.store.Store;
import com.msmeik.store.StoreService;
import com.opensymphony.xwork2.ActionSupport;

public class UpdateStore extends ActionSupport{
	private StoreService storeService;
	private Store store;
	
	private int ok;
	
	public String execute(){
		store.setName(null);
		this.ok = this.storeService.updateStore(store);
		return "input";
	}

	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}

	public int getOk() {
		return ok;
	}

	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}
	
	
}
