package com.msmeik.store.mvc.manage;

import java.util.List;

import com.msmeik.message.MessageService;
import com.msmeik.message.MessageUser;
import com.msmeik.store.Store;
import com.msmeik.store.StoreService;
import com.msmeik.store.comment.CommentOverview;
import com.msmeik.store.comment.CommentService;
import com.msmeik.store.content.ContentService;
import com.msmeik.store.photo.PhotoService;
import com.msmeik.store.promotion.PromotionService;
import com.msmeik.store.relish.RelishService;

public class StoreIndex {
	private StoreService storeService;
	private PhotoService photoService;
	private MessageService messageService;
	private CommentService commentService;
	private RelishService relishService;
	private ContentService contentService;
	private PromotionService promotionService;
	
	private Store store;
	private List broadcastMessageList;
	private CommentOverview commentOverview;
	private int photoNum;
	private int contentNum;
	private int relishNum;
	private int commentNum;
	private int unreadMessageNum;
	private int totalMessageNum;
	private int promotionNum;
	
	private int storeId;
	
	public String execute(){
		this.store = this.storeService.getStoreById(storeId);
		this.commentOverview = this.commentService.getCommentOverviewByStoreId(storeId);		
		MessageUser receiver = new MessageUser();
		if(this.commentOverview == null)
			this.commentOverview = new CommentOverview();
		receiver.setUserType(MessageUser.STORE_USER_TYPE);
		this.broadcastMessageList = this.messageService.getMessageListByReceiver(receiver, -1, 1, 5);
		this.photoNum = this.photoService.getPhotoCount(store, null, null, null);
		this.contentNum = this.contentService.getContentCount(storeId, -1);
		this.relishNum = this.relishService.getRelishCount(null,store,null, 0,null);
		this.commentNum = this.commentService.getCommentNum(null,store,-1,-1);
		receiver.setUserId(storeId);
		this.unreadMessageNum = this.messageService.getMessageCountByReceiverAndReadStatus(receiver, 0);
		this.totalMessageNum = this.messageService.getMessageCountByReceiverAndReadStatus(receiver, -1);
		this.promotionNum = this.promotionService.getPromotionCount(storeId);
		return "success";
	}

	public int getPromotionNum() {
		return promotionNum;
	}

	public Store getStore() {
		return store;
	}

	public List getBroadcastMessageList() {
		return broadcastMessageList;
	}

	public int getUnreadMessageNum() {
		return unreadMessageNum;
	}

	public int getTotalMessageNum() {
		return totalMessageNum;
	}

	public CommentOverview getCommentOverview() {
		return commentOverview;
	}

	public int getPhotoNum() {
		return photoNum;
	}

	public int getContentNum() {
		return contentNum;
	}

	public int getRelishNum() {
		return relishNum;
	}

	public int getCommentNum() {
		return commentNum;
	}

	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}

	public void setPhotoService(PhotoService photoService) {
		this.photoService = photoService;
	}

	public void setMessageService(MessageService messageService) {
		this.messageService = messageService;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	public void setRelishService(RelishService relishService) {
		this.relishService = relishService;
	}

	public void setContentService(ContentService contentService) {
		this.contentService = contentService;
	}

	public void setPromotionService(PromotionService promotionService) {
		this.promotionService = promotionService;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}
	
	
}
