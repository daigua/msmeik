package com.msmeik.store.mvc;

import java.util.ArrayList;
import java.util.List;

import com.msmeik.store.Store;
import com.msmeik.store.StoreService;

public class GetStoreListByNameOrId {
	private StoreService storeService;
	private String name;
	private String type;
	private int storeId;
	private List storeList;
	private int pageSize=5;
	private int page;
	
	public String execute(){
		this.storeList = new ArrayList();
		if(this.storeId>0){
			Store store = this.storeService.getStoreById(storeId);			
			storeList.add(store);
		}else if(this.name!=null && !this.name.trim().equals("")){
			this.storeList = this.storeService.getStoreListByName(name, this.type, this.page, this.pageSize);
		}
		return "success";
	}

	public List getStoreList() {
		return storeList;
	}

	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}
	
	
}
