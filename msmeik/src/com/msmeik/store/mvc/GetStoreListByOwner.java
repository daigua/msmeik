package com.msmeik.store.mvc;

import java.util.List;

import com.msmeik.authentication.AuthenticationService;
import com.msmeik.store.StoreService;

public class GetStoreListByOwner {
	private StoreService storeService;
	private List storeList;
	private int userId;
	private int page=1;
	private int pageSize=10;
	public String execute(){
		if(userId == 0)
			userId = AuthenticationService.getLoginUser().getId();
		this.storeList = this.storeService.getStoreListByOwnerId(userId, page, pageSize);
		return "success";
	}
	public List getStoreList() {
		return storeList;
	}
	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	
}
