package com.msmeik.store.mvc;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.msmeik.authentication.AuthenticationService;
import com.msmeik.jcaptcha.JcaptchaUtil;
import com.msmeik.member.User;
import com.msmeik.store.Store;
import com.msmeik.store.StoreConfig;
import com.msmeik.store.StoreService;
import com.opensymphony.xwork2.ActionSupport;

public class RegisterStore extends ActionSupport{
	private StoreService storeService;
	private Store store;
	private String captchaId;
	private String safeCode;
	private int ok;
	
	public String execute(){
		User user = AuthenticationService.getLoginUser();
		store.setRegistrant(user);
		store.setRegisterTime(new Date());
		store.setVerify("0");
		store.setClosed("0");
		int storeId = this.storeService.registerStore(store);
		store = new Store();
		store.setId(storeId);
		this.ok = 9;
		return "input";
	}
	
	public void validate(){
		if(this.hasErrors())
			return;
		if(!JcaptchaUtil.validate(captchaId, safeCode)){
			this.addFieldError("safeCode", this.getText("safeCode.inputError"));
			return;
		}
		int storeId = this.storeService.getStoreIdByName(store.getName());
		System.out.println(storeId);
		if(storeId!=0){
			store.setId(storeId);
			this.addFieldError("store.name", this.getText("store.name.existed"));
			return;
		}
		
		
	}
	
	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}

	public String getSafeCode() {
		return safeCode;
	}

	public void setSafeCode(String safeCode) {
		this.safeCode = safeCode;
	}

	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}


	public void setCaptchaId(String captchaId) {
		this.captchaId = captchaId;
	}

	public int getOk() {
		return ok;
	}
	
	
}
