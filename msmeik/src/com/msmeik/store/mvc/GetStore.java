package com.msmeik.store.mvc;

import com.msmeik.store.Store;
import com.msmeik.store.StoreService;

public class GetStore {
	private StoreService storeService;
	private int storeId;
	private String name;
	private Store store;
	
	public String execute(){
		if(storeId>0){
			store = this.storeService.getStoreById(storeId);
		}else
		if(name!=null){
			store = this.storeService.getStoreByName(name);
		}
		return "success";
	}

	public Store getStore() {
		return store;
	}

	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
