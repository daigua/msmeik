package com.msmeik.store.mvc;

import com.msmeik.authentication.AuthenticationService;
import com.msmeik.member.User;
import com.msmeik.store.Store;
import com.msmeik.store.StoreClaim;
import com.msmeik.store.StoreService;

public class ClaimStore {
	private Store store;
	private int ok;
	private StoreService storeService;
	
	public String execute(){
		User user = AuthenticationService.getLoginUser();
		StoreClaim storeClaim = new StoreClaim();
		storeClaim.setStore(store);
		storeClaim.setUser(user);
		storeClaim.setClaimResult("0");
		int id = this.storeService.claimStore(storeClaim);
		if(id > 0)
			this.ok = 9;
		else
			this.ok = id;
		return "success";
	}
	public int getOk() {
		return ok;
	}

	

	public void setStore(Store store) {
		this.store = store;
	}

	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}
	
	
}
