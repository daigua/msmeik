package com.msmeik.store.mvc;

import com.msmeik.store.StoreService;

public class CheckStoreExist {
	private StoreService storeService;
	private String name;
	private int storeId;
	
	public String execute(){
		if(name!=null || !name.trim().equals(""))
			this.storeId = this.storeService.getStoreIdByName(name);
		return "success";
	}


	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}

	public void setName(String name) {
		this.name = name;
	}


	public int getStoreId() {
		return storeId;
	}
	
}
