package com.msmeik.store.mvc;

import java.util.List;

import com.msmeik.authentication.AuthenticationService;
import com.msmeik.member.User;
import com.msmeik.store.StoreService;

public class GetStoreClaimList {
	private StoreService storeService;
	private List storeClaimList;
	
	public String execute(){
		User loginUser = AuthenticationService.getLoginUser();
		System.out.println(loginUser.getId());
		storeClaimList = this.storeService.getStoreClaimByUserAccount(loginUser.getAccount());
		
		return "success";
	}

	public List getStoreClaimList() {
		return storeClaimList;
	}

	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}
	
	
}
