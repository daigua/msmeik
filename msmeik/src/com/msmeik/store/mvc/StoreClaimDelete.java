package com.msmeik.store.mvc;

import com.msmeik.authentication.AuthenticationService;
import com.msmeik.member.User;
import com.msmeik.store.StoreService;

public class StoreClaimDelete {
	private StoreService storeService;
	
	private int claimId;
	private int ok;
	
	public String execute(){
		User user = AuthenticationService.getLoginUser();
		this.storeService.deleteStoreClaimByIdAndUser(user, claimId);
		this.ok = 9;
		return "success";
	}

	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}

	public void setClaimId(int claimId) {
		this.claimId = claimId;
	}

	public int getOk() {
		return ok;
	}
	
	
}
