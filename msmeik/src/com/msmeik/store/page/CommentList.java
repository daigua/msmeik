package com.msmeik.store.page;

import java.util.List;

import com.msmeik.member.User;
import com.msmeik.store.Store;
import com.msmeik.store.comment.CommentService;

public class CommentList {
	private CommentService commentService;
	private int totalCount;
	private List commentList;
	private int page = 1;
	private int pageSize = 5;
	private int lowerScore = -1;
	private int upperScore = -1;
	private int storeId;
	private int userId;
	
	public String execute(){
		Store store = new Store();
		store.setId(storeId);
		User user = new User();
		user.setId(userId);
		this.commentList = this.commentService.getCommentList(user, store, lowerScore, upperScore, page, pageSize);
		this.totalCount = this.commentService.getCommentNum(user, store, lowerScore, upperScore);
		return "success";
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getLowerScore() {
		return lowerScore;
	}

	public void setLowerScore(int lowerScore) {
		this.lowerScore = lowerScore;
	}

	public int getUpperScore() {
		return upperScore;
	}

	public void setUpperScore(int upperScore) {
		this.upperScore = upperScore;
	}

	public int getStoreId() {
		return storeId;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	
	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public List getCommentList() {
		return commentList;
	}
	
	
}
