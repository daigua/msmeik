package com.msmeik.store.page;

import com.msmeik.store.Store;
import com.msmeik.store.StoreService;
import com.msmeik.store.promotion.Promotion;
import com.msmeik.store.promotion.PromotionService;

public class CouponPrint {
	private PromotionService promotionService;
	private StoreService storeService;
	
	private int promotionId;
	private String storeId;
	
	private Promotion promotion;
	private Store store;
	
	public String execute(){
		//this.store = PageUtil.getStore(storeId, storeService);
		this.promotion = this.promotionService.getPromotion(promotionId);
		this.promotionService.addPromotionPrintTimes(promotionId, 1);
		return "success";
	}

	public Promotion getPromotion() {
		return promotion;
	}

	public void setPromotionService(PromotionService promotionService) {
		this.promotionService = promotionService;
	}

	public void setPromotionId(int promotionId) {
		this.promotionId = promotionId;
	}

	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}
	
}
