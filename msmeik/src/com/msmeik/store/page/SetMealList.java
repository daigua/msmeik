package com.msmeik.store.page;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import com.msmeik.store.Store;
import com.msmeik.store.StoreService;
import com.msmeik.store.content.ContentService;
import com.msmeik.store.photo.PhotoService;
import com.msmeik.store.promotion.PromotionService;
import com.msmeik.store.relish.RelishService;
import com.msmeik.store.relish.SetMealCategory;

public class SetMealList {
	private StoreService storeService;
	private RelishService relishService;
	private PhotoService photoService;
	private PromotionService promotionService;
	private ContentService contentService;
	
	private String storeId;
	private Store store;
	private List<SetMealCategory> categoryList;

	
	private int promotionTotalNum;
	private int contentTotalNum;
	private int photoTotalNum;
	
	private List recommendSetMealList;
	private List setMealList;
	
	/**
	 * 读取relish的参数
	 * */
	private int facia = -1;
	private int specialPrice = -1;
	private int recommend = -1;
	private int ctmRecommend = -1;
	private int categoryId = -1;
	
	private int page = 1;
	private int pageSize = 10;
	private int totalCount;
	
	private String orderBy;
	private Map countSetMeal;
	
	private int relishTotalNum;
	
	
	public String execute(){
		this.store = PageUtil.getStore(storeId);
		this.categoryList = this.relishService.getSetMealCategoryList(store);	
		
		this.relishTotalNum = this.relishService.getRelishNum(store, null, null, -1, -1, -1, -1, 1);
		
		this.photoTotalNum = this.photoService.getPhotoCount(store, null, null, null);
		this.promotionTotalNum = this.promotionService.getPromotionNum(store, -1, new Date(System.currentTimeMillis()));
		this.contentTotalNum = this.contentService.getContentCount(store.getId(), -1);
		
		this.recommendSetMealList = this.relishService.getSetMealList(store,null, -1, 1, -1, -1, 1, null, 1, 5);
		//如果推荐美味列表为0的话 则读取美客推荐
		if(this.recommendSetMealList.size() == 0){
			this.recommendSetMealList = this.relishService.getSetMealList(store, null, -1, -1, 1, -1, 1, null, 1, 5);
		}
		
		SetMealCategory category = new SetMealCategory();
		category.setId(categoryId);
		if(orderBy!=null && !orderBy.trim().equals("")){
			orderBy = orderBy.replaceAll("-", " ");
			orderBy += ",order_time desc";
		}
		this.countSetMeal = this.relishService.countSetMealNum(store, 1);
		this.setMealList = this.relishService.getSetMealList(store, category, facia, recommend, ctmRecommend, specialPrice, 1, orderBy, page, pageSize);
		//this.relishList = this.relishService.getRelishList(store, null, category, facia, recommend, ctmRecommend, specialPrice, 1, orderBy, page, pageSize);
		this.totalCount = this.relishService.getSetMealNum(store, category, facia, recommend, ctmRecommend, specialPrice, 1);
		return "success";
	}

	public Store getStore() {
		return store;
	}

	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}

	public void setRelishService(RelishService relishService) {
		this.relishService = relishService;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public List<SetMealCategory> getCategoryList() {
		return categoryList;
	}

	public int getPromotionTotalNum() {
		return promotionTotalNum;
	}

	public int getContentTotalNum() {
		return contentTotalNum;
	}

	public int getPhotoTotalNum() {
		return photoTotalNum;
	}

	
	public List getRecommendSetMealList() {
		return recommendSetMealList;
	}

	public void setPhotoService(PhotoService photoService) {
		this.photoService = photoService;
	}

	public void setPromotionService(PromotionService promotionService) {
		this.promotionService = promotionService;
	}

	public void setContentService(ContentService contentService) {
		this.contentService = contentService;
	}

	public int getFacia() {
		return facia;
	}

	public void setFacia(int facia) {
		this.facia = facia;
	}

	public int getSpecialPrice() {
		return specialPrice;
	}

	public void setSpecialPrice(int specialPrice) {
		this.specialPrice = specialPrice;
	}

	public int getRecommend() {
		return recommend;
	}

	public void setRecommend(int recommend) {
		this.recommend = recommend;
	}

	public int getCtmRecommend() {
		return ctmRecommend;
	}

	public void setCtmRecommend(int ctmRecommend) {
		this.ctmRecommend = ctmRecommend;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	
	public List getSetMealList() {
		return setMealList;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public String getStoreId() {
		return storeId;
	}

	public Map getCountSetMeal() {
		return countSetMeal;
	}

	public int getRelishTotalNum() {
		return relishTotalNum;
	}
	
	
}
