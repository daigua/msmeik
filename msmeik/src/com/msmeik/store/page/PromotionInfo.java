package com.msmeik.store.page;

import com.msmeik.store.Store;
import com.msmeik.store.StoreService;
import com.msmeik.store.content.ContentService;
import com.msmeik.store.photo.PhotoService;
import com.msmeik.store.promotion.Promotion;
import com.msmeik.store.promotion.PromotionService;
import com.msmeik.store.relish.RelishService;

import java.sql.Date;
import java.util.List;
import java.util.Map;

public class PromotionInfo{
	
	private StoreService storeService;
    private PhotoService photoService;
    private RelishService relishService;
    private PromotionService promotionService;
    private ContentService contentService;
    private String storeId;
    private int promotionId;
    private Store store;
    private int relishTotalNum;
    private int contentTotalNum;
    private int photoTotalNum;
    private Map countPromotionByType;
    private Promotion promotion;
    
    private int setMealTotalNum;
    
    private List promotionCouponList;
	private List promotionInfoList;

    public String execute(){
    	this.store = PageUtil.getStore(storeId);
        relishTotalNum = relishService.getRelishNum(store, null, null, -1, -1, -1, -1, 1);
        contentTotalNum = contentService.getContentCount(store.getId(), -1);
        photoTotalNum = photoService.getPhotoCount(store, null, null, null);
        countPromotionByType = promotionService.countPromotionByType(store,new Date(System.currentTimeMillis()));
        promotion = promotionService.getPromotionByStoreIdAndId(store.getId(), promotionId);
        promotionService.addPromotionClickTimes(promotionId, 1);
        
      //��ȡ�ײ���Ϣ
		this.setMealTotalNum = this.relishService.getSetMealNum(store, null, 1);
        
        Date fromTime = new Date(System.currentTimeMillis());
        this.promotionCouponList = this.promotionService.getPromotionList(store, 2, fromTime,null, 1, 5);
		this.promotionInfoList = this.promotionService.getPromotionList(store, 1, fromTime,null, 1, 5);
        
        return "success";
    }

    public String getStoreId(){
        return storeId;
    }

    public void setStoreId(String storeId){
        this.storeId = storeId;
    }

    public int getPromotionId(){
        return promotionId;
    }

    public void setPromotionId(int promotionId){
        this.promotionId = promotionId;
    }

    public Store getStore(){
        return store;
    }

    public int getRelishTotalNum(){
        return relishTotalNum;
    }

    public int getContentTotalNum(){
        return contentTotalNum;
    }

    public int getPhotoTotalNum(){
        return photoTotalNum;
    }

    public Map getCountPromotionByType(){
        return countPromotionByType;
    }

    public Promotion getPromotion(){
        return promotion;
    }

    public void setStoreService(StoreService storeService){
        this.storeService = storeService;
    }

    public void setPhotoService(PhotoService photoService){
        this.photoService = photoService;
    }

    public void setRelishService(RelishService relishService){
        this.relishService = relishService;
    }

    public void setPromotionService(PromotionService promotionService){
        this.promotionService = promotionService;
    }

    public void setContentService(ContentService contentService){
        this.contentService = contentService;
    }

	public List getPromotionCouponList() {
		return promotionCouponList;
	}

	public List getPromotionInfoList() {
		return promotionInfoList;
	}

	public int getSetMealTotalNum() {
		return setMealTotalNum;
	}

    
}
