package com.msmeik.store.page;

import java.sql.Date;
import java.util.List;

import com.msmeik.store.Store;
import com.msmeik.store.StoreService;
import com.msmeik.store.content.Content;
import com.msmeik.store.content.ContentService;
import com.msmeik.store.photo.PhotoService;
import com.msmeik.store.promotion.PromotionService;
import com.msmeik.store.relish.RelishService;

public class NewsInfo {
	private StoreService storeService;
	private PhotoService photoService;
	private RelishService relishService;
	private PromotionService promotionService;
	private ContentService contentService;
	
	private String storeId;
	
	private Store store;
	private int relishTotalNum;
	private int promotionTotalNum;
	private int contentTotalNum;
	private int photoTotalNum;
		
	private List categoryList;
	private List noticeContentList;
	
	private int setMealTotalNum;
	
	private Content news;
	private int contentId;
	
	public String execute(){
		this.store = PageUtil.getStore(storeId);
		
		this.relishTotalNum = this.relishService.getRelishNum(store, null, null, -1, -1, -1, -1, 1);
		this.promotionTotalNum = this.promotionService.getPromotionNum(store, -1, new Date(System.currentTimeMillis()));
		this.contentTotalNum = this.contentService.getContentCount(store.getId(), -1);
		this.photoTotalNum = this.photoService.getPhotoCount(store, null, null, null);
		this.categoryList = this.contentService.getCategoryList(store, true);
		
		//��ȡ�ײ���Ϣ
		this.setMealTotalNum = this.relishService.getSetMealNum(store, null, 1);
		
		this.news = this.contentService.getContentById(contentId);
		this.noticeContentList = this.contentService.getContentList(store.getId(), 1, 1, 5);
		
		return "success";
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public Store getStore() {
		return store;
	}

	public int getRelishTotalNum() {
		return relishTotalNum;
	}

	public int getPromotionTotalNum() {
		return promotionTotalNum;
	}

	public int getContentTotalNum() {
		return contentTotalNum;
	}

	public int getPhotoTotalNum() {
		return photoTotalNum;
	}

	public List getCategoryList() {
		return categoryList;
	}

	public Content getNews() {
		return news;
	}

	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}

	public void setPhotoService(PhotoService photoService) {
		this.photoService = photoService;
	}

	public void setRelishService(RelishService relishService) {
		this.relishService = relishService;
	}

	public void setPromotionService(PromotionService promotionService) {
		this.promotionService = promotionService;
	}

	public void setContentService(ContentService contentService) {
		this.contentService = contentService;
	}

	public void setContentId(int contentId) {
		this.contentId = contentId;
	}

	public List getNoticeContentList() {
		return noticeContentList;
	}

	public int getSetMealTotalNum() {
		return setMealTotalNum;
	}
	
	
}
