package com.msmeik.store.page;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import com.msmeik.store.Store;
import com.msmeik.store.StoreService;
import com.msmeik.store.content.ContentService;
import com.msmeik.store.photo.PhotoService;
import com.msmeik.store.promotion.PromotionService;
import com.msmeik.store.relish.RelishCategory;
import com.msmeik.store.relish.RelishService;

public class RelishList {
	private StoreService storeService;
	private RelishService relishService;
	private PhotoService photoService;
	private PromotionService promotionService;
	private ContentService contentService;
	
	private String storeId;
	private Store store;
	private List<RelishCategory> relishCategoryList;
	private Map countRelish;
	
	private int promotionTotalNum;
	private int contentTotalNum;
	private int photoTotalNum;
	
	private int setMealTotalNum;
	
	private List recommendRelishList;
	private List relishList;
	
	/**
	 * 读取relish的参数
	 * */
	private int facia = -1;
	private int specialPrice = -1;
	private int recommend = -1;
	private int ctmRecommend = -1;
	private int categoryId = -1;
	
	private int page = 1;
	private int pageSize = 10;
	private int totalCount;
	
	private String orderBy;
	
	
	public String execute(){
		this.store = PageUtil.getStore(storeId);
		this.relishCategoryList = this.relishService.getRelishCategoryListByStore(store);
		this.countRelish = this.relishService.countRelishNum(store,1);
		
		this.photoTotalNum = this.photoService.getPhotoCount(store, null, null, null);
		this.promotionTotalNum = this.promotionService.getPromotionNum(store, -1, new Date(System.currentTimeMillis()));
		this.contentTotalNum = this.contentService.getContentCount(store.getId(), -1);
		
		this.recommendRelishList = this.relishService.getRelishList(store,null, null, -1, 1, -1, -1, 1, null, 1, 5);
		//如果推荐美味列表为0的话 则读取美客推荐
		if(this.recommendRelishList.size() == 0){
			this.recommendRelishList = this.relishService.getRelishList(store, null, null, -1, -1, 1, -1, 1, null, 1, 5);
		}
		//读取套餐信息
		this.setMealTotalNum = this.relishService.getSetMealNum(store, null, 1);
		
		RelishCategory category = new RelishCategory();
		category.setId(categoryId);
		if(orderBy!=null && !orderBy.trim().equals("")){
			orderBy = orderBy.replaceAll("-", " ");
			orderBy += ",order_time desc";
		}
		this.relishList = this.relishService.getRelishList(store, null, category, facia, recommend, ctmRecommend, specialPrice, 1, orderBy, page, pageSize);
		this.totalCount = this.relishService.getRelishNum(store, null, category, facia, recommend, ctmRecommend, specialPrice, 1);
		return "success";
	}

	public Store getStore() {
		return store;
	}

	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}

	public void setRelishService(RelishService relishService) {
		this.relishService = relishService;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public Map getCountRelish() {
		return countRelish;
	}

	public List<RelishCategory> getRelishCategoryList() {
		return relishCategoryList;
	}

	public int getPromotionTotalNum() {
		return promotionTotalNum;
	}

	public int getContentTotalNum() {
		return contentTotalNum;
	}

	public int getPhotoTotalNum() {
		return photoTotalNum;
	}

	public List getRecommendRelishList() {
		return recommendRelishList;
	}

	public void setPhotoService(PhotoService photoService) {
		this.photoService = photoService;
	}

	public void setPromotionService(PromotionService promotionService) {
		this.promotionService = promotionService;
	}

	public void setContentService(ContentService contentService) {
		this.contentService = contentService;
	}

	public int getFacia() {
		return facia;
	}

	public void setFacia(int facia) {
		this.facia = facia;
	}

	public int getSpecialPrice() {
		return specialPrice;
	}



	public void setSpecialPrice(int specialPrice) {
		this.specialPrice = specialPrice;
	}

	public int getRecommend() {
		return recommend;
	}

	public void setRecommend(int recommend) {
		this.recommend = recommend;
	}

	public int getCtmRecommend() {
		return ctmRecommend;
	}

	public void setCtmRecommend(int ctmRecommend) {
		this.ctmRecommend = ctmRecommend;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public List getRelishList() {
		return relishList;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public String getStoreId() {
		return storeId;
	}

	public int getSetMealTotalNum() {
		return setMealTotalNum;
	}
	
	
}
