package com.msmeik.store.page;

import java.sql.Date;
import java.util.List;

import com.msmeik.authentication.AuthenticationService;
import com.msmeik.member.User;
import com.msmeik.store.Store;
import com.msmeik.store.StoreService;
import com.msmeik.store.comment.CommentOverview;
import com.msmeik.store.comment.CommentService;
import com.msmeik.store.content.ContentService;
import com.msmeik.store.photo.PhotoCategory;
import com.msmeik.store.photo.PhotoService;
import com.msmeik.store.promotion.PromotionService;
import com.msmeik.store.relish.RelishService;
import com.msmeik.visit.VisitService;
import com.msmeik.visit.VisitTarget;

public class Index {
	private StoreService storeService;
	private CommentService commentService;
	private PhotoService photoService;
	private RelishService relishService;
	private PromotionService promotionService;
	private ContentService contentService;
	private String storeId;
	
	private Store store;
	private List storeShowPhotoList;
	private List storePhotoList;
	private List relishPhotoList;
	
	private List faciaRelishList;
	private List recommendRelishList;
	private List ctmRecommendRelishList;
	private List specialPriceRelishList;
	private int relishTotalNum;
	
	private int promotionTotalNum;
	private List promotionCouponList;
	private List promotionInfoList;
	
	private int contentTotalNum;
	private List noticeContentList;
	private List newsContentList;
	
	private int commentTotalNum;
	private List commentList;
	
	private int setMealTotalNum;
	
	private List visitLogList;
	
	//套餐
	//private List recommendSetMealList;
	
	
	private int photoTotalNum;
	
	private CommentOverview commentOverview;
	private VisitService visitService;
	
	
	public String execute(){
		
		this.store = PageUtil.getStore(storeId);
		this.commentOverview = this.commentService.getCommentOverviewByStoreId(store.getId());
		if(commentOverview == null)
			commentOverview = new CommentOverview();
		PhotoCategory category = new PhotoCategory();
		category.setId(2);//餐厅展示图片
		this.storeShowPhotoList = this.photoService.getPhotoList(store, null, category, null, 1, 10, null);
		if(this.storeShowPhotoList.size() == 0){
			this.storeShowPhotoList = this.photoService.getPhotoList(store, null, null, null, 1, 10, null);
		}
		category.setId(1);//餐厅图片
		this.storePhotoList = this.photoService.getPhotoList(store, null, category, null, 1, 6, null);
		category.setId(3);//美味图片
		this.relishPhotoList = this.photoService.getPhotoList(store, null, category, null, 1, 6, null);
		this.photoTotalNum = this.photoService.getPhotoCount(store, null, null, null);
		
		//读取美味
		this.faciaRelishList = this.relishService.getRelishList(store,null, null, 1, -1, -1, -1, 1, null, 1, 5);
		this.recommendRelishList = this.relishService.getRelishList(store,null, null, -1, 1, -1, -1, 1, null, 1, 5);
		this.ctmRecommendRelishList = this.relishService.getRelishList(store,null, null, -1, -1, 1, -1, 1, "recommend_times desc,on_top asc,order_time desc ", 1, 5);
		this.relishTotalNum = this.relishService.getRelishNum(store, null, null, -1, -1, -1, -1, 1);
		this.specialPriceRelishList = this.relishService.getRelishList(store,null, null, -1, -1, -1, 1, 1, null, 1, 5);
		
		//读取优惠信息
		Date now = new Date(System.currentTimeMillis());
		this.promotionTotalNum = this.promotionService.getPromotionNum(store, -1, now);
		this.promotionCouponList = this.promotionService.getPromotionList(store, 2, now, null, 1, 5);
		this.promotionInfoList = this.promotionService.getPromotionList(store, 1, now, null, 1, 5);
		
		//读取套餐信息
		this.setMealTotalNum = this.relishService.getSetMealNum(store, null, 1);
		
		//内容
		this.contentTotalNum = this.contentService.getContentCount(store.getId(), -1);
		this.noticeContentList = this.contentService.getContentList(store.getId(), 1, 1, 1);
		this.newsContentList = this.contentService.getContentList(store.getId(), 2, 1, 5);
		if(AuthenticationService.checkLogin()!=null){
			User visitor = AuthenticationService.getLoginUser();
			VisitTarget target = new VisitTarget();
			target.setType(VisitTarget.TYPE_STORE);
			target.setKey(store.getId());
			target.setName(store.getName());
			this.visitService.saveVisitLog(visitor, target);
		}	
		VisitTarget target = new VisitTarget();
		target.setType(VisitTarget.TYPE_STORE);
		target.setKey(store.getId());
		this.visitLogList = this.visitService.getVisitLogList(target, 1, 6);
		
		//读取套餐信息
		//this.recommendSetMealList = this.relishService.getSetMealList(store, null, -1, 1, -1, -1, 1, null, 1, 5);
		
		//更新店铺的访问次数 点击率
		this.storeService.addStoreClickTimes(store.getId(), 1);
		return "success";
	}


	public List getSpecialPriceRelishList() {
		return specialPriceRelishList;
	}


	public String getStoreId() {
		return storeId;
	}


	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}


	public Store getStore() {
		return store;
	}


	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}


	public CommentOverview getCommentOverview() {
		return commentOverview;
	}


	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}


	public List getStoreShowPhotoList() {
		return storeShowPhotoList;
	}


	public List getStorePhotoList() {
		return storePhotoList;
	}


	public List getRelishPhotoList() {
		return relishPhotoList;
	}


	public int getPhotoTotalNum() {
		return photoTotalNum;
	}


	public void setPhotoService(PhotoService photoService) {
		this.photoService = photoService;
	}


	public List getFaciaRelishList() {
		return faciaRelishList;
	}


	public List getRecommendRelishList() {
		return recommendRelishList;
	}


	public List getCtmRecommendRelishList() {
		return ctmRecommendRelishList;
	}


	

	public int getRelishTotalNum() {
		return relishTotalNum;
	}


	public void setRelishService(RelishService relishService) {
		this.relishService = relishService;
	}


	public int getPromotionTotalNum() {
		return promotionTotalNum;
	}


	public List getPromotionCouponList() {
		return promotionCouponList;
	}


	public List getPromotionInfoList() {
		return promotionInfoList;
	}


	public void setPromotionService(PromotionService promotionService) {
		this.promotionService = promotionService;
	}


	public int getContentTotalNum() {
		return contentTotalNum;
	}


	public List getNoticeContentList() {
		return noticeContentList;
	}


	public List getNewsContentList() {
		return newsContentList;
	}


	public void setContentService(ContentService contentService) {
		this.contentService = contentService;
	}


	public int getCommentTotalNum() {
		return commentTotalNum;
	}


	public List getCommentList() {
		return commentList;
	}


	public void setVisitService(VisitService visitService) {
		this.visitService = visitService;
	}


	public List getVisitLogList() {
		return visitLogList;
	}


	public int getSetMealTotalNum() {
		return setMealTotalNum;
	}
	
	
}
