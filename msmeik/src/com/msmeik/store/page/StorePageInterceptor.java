package com.msmeik.store.page;


import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.MethodFilterInterceptor;

public class StorePageInterceptor extends MethodFilterInterceptor{

	@Override
	protected String doIntercept(ActionInvocation action) throws Exception {
		// TODO Auto-generated method stub
		String storeBaseUrl = "";
		String requestURI = ServletActionContext.getRequest().getRequestURI();
		String[] args = requestURI.split(",");
		System.out.println(args[0]+"."+args[1]+"."+args[2]);
		action.getInvocationContext().getValueStack().set("StoreBaseUrl", null);
		return action.invoke();
	}

}
