package com.msmeik.store.page;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import com.msmeik.store.Store;
import com.msmeik.store.StoreService;
import com.msmeik.store.content.ContentService;
import com.msmeik.store.photo.PhotoService;
import com.msmeik.store.photo.PhotoTarget;
import com.msmeik.store.promotion.PromotionService;
import com.msmeik.store.relish.Relish;
import com.msmeik.store.relish.RelishCategory;
import com.msmeik.store.relish.RelishService;

public class RelishInfo {
	private StoreService storeService;
	private RelishService relishService;
	private PhotoService photoService;
	private PromotionService promotionService;
	private ContentService contentService;
	
	private String storeId;
	private Store store;
	private List<RelishCategory> relishCategoryList;
	private Map countRelish;
	
	private int promotionTotalNum;
	private int contentTotalNum;
	private int photoTotalNum;
	
	private List recommendRelishList;
	
	private int setMealTotalNum;
	
	private Relish relish;
	private int relishId;
	
	private List photoList;
	
	public String execute(){
		this.store = PageUtil.getStore(storeId);
		this.relishCategoryList = this.relishService.getRelishCategoryListByStore(store);
		this.countRelish = this.relishService.countRelishNum(store,1);
		
		this.photoTotalNum = this.photoService.getPhotoCount(store, null, null, null);
		this.promotionTotalNum = this.promotionService.getPromotionNum(store, -1, new Date(System.currentTimeMillis()));
		this.contentTotalNum = this.contentService.getContentCount(store.getId(), -1);
		
		this.recommendRelishList = this.relishService.getRelishList(store,null, null, -1, 1, -1, -1, 1, null, 1, 5);
		this.relish = this.relishService.getRelishById(relishId);
		if(relish == null)
			relish = new Relish();
		PhotoTarget target = new PhotoTarget();
		target.setType(PhotoTarget.TYPE_RELISH);
		target.setKey(relishId);
		this.photoList = this.photoService.getPhotoList(store, null, null, target, 1, 30, null);
		//��ȡ�ײ���Ϣ
		this.setMealTotalNum = this.relishService.getSetMealNum(store, null, 1);
		return "success";
	}

	public Store getStore() {
		return store;
	}

	public Map getCountRelish() {
		return countRelish;
	}

	public int getPromotionTotalNum() {
		return promotionTotalNum;
	}

	public int getContentTotalNum() {
		return contentTotalNum;
	}

	public int getPhotoTotalNum() {
		return photoTotalNum;
	}

	public List getRecommendRelishList() {
		return recommendRelishList;
	}

	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}

	public void setRelishService(RelishService relishService) {
		this.relishService = relishService;
	}

	public void setPhotoService(PhotoService photoService) {
		this.photoService = photoService;
	}

	public void setPromotionService(PromotionService promotionService) {
		this.promotionService = promotionService;
	}

	public void setContentService(ContentService contentService) {
		this.contentService = contentService;
	}

	public int getSetMealTotalNum() {
		return setMealTotalNum;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public List<RelishCategory> getRelishCategoryList() {
		return relishCategoryList;
	}

	public Relish getRelish() {
		return relish;
	}

	public void setRelishId(int relishId) {
		this.relishId = relishId;
	}

	public List getPhotoList() {
		return photoList;
	}

	public String getStoreId() {
		return storeId;
	}
	
	
}
