package com.msmeik.store.page;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import com.msmeik.store.Store;
import com.msmeik.store.StoreService;
import com.msmeik.store.content.ContentService;
import com.msmeik.store.photo.PhotoService;
import com.msmeik.store.promotion.PromotionService;
import com.msmeik.store.relish.RelishService;

public class PromotionList {
	private StoreService storeService;
	private PhotoService photoService;
	private RelishService relishService;
	private PromotionService promotionService;
	private ContentService contentService;
	
	private String storeId;
	
	private Store store;
	private int relishTotalNum;
	private int contentTotalNum;
	private int photoTotalNum;
	
	private int setMealTotalNum;
	
	private Map countPromotionByType;
	private List promotionList;
	private int type = -1;
	
	private List promotionCouponList;
	private List promotionInfoList;
	
	private int page = 1;
	private int pageSize = 20;
	private int totalCount;
	
	public String execute(){
		
		this.store = PageUtil.getStore(storeId);
		
		this.relishTotalNum = this.relishService.getRelishNum(store, null, null, -1, -1, -1, -1, 1);
		this.contentTotalNum = this.contentService.getContentCount(store.getId(), -1);
		this.photoTotalNum = this.photoService.getPhotoCount(store, null, null, null);
		this.countPromotionByType = this.promotionService.countPromotionByType(store,new Date(System.currentTimeMillis()));
		
		Date fromTime = new Date(System.currentTimeMillis());
		this.promotionCouponList = this.promotionService.getPromotionList(store, 2, fromTime, null, 1, 5);
		this.promotionInfoList = this.promotionService.getPromotionList(store, 1, fromTime,null, 1, 5);
		//��ȡ�ײ���Ϣ
		this.setMealTotalNum = this.relishService.getSetMealNum(store, null, 1);
		
		this.promotionList = this.promotionService.getPromotionList(store, type, fromTime, null, page, pageSize);
		this.totalCount = this.promotionService.getPromotionNum(store, type, fromTime);
		return "success";
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public Store getStore() {
		return store;
	}

	public int getRelishTotalNum() {
		return relishTotalNum;
	}

	public int getContentTotalNum() {
		return contentTotalNum;
	}

	public int getPhotoTotalNum() {
		return photoTotalNum;
	}

	public Map getCountPromotionByType() {
		return countPromotionByType;
	}

	public List getPromotionList() {
		return promotionList;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}

	public void setPhotoService(PhotoService photoService) {
		this.photoService = photoService;
	}

	public void setRelishService(RelishService relishService) {
		this.relishService = relishService;
	}

	public void setPromotionService(PromotionService promotionService) {
		this.promotionService = promotionService;
	}

	public void setContentService(ContentService contentService) {
		this.contentService = contentService;
	}

	public List getPromotionCouponList() {
		return promotionCouponList;
	}

	public List getPromotionInfoList() {
		return promotionInfoList;
	}

	public int getSetMealTotalNum() {
		return setMealTotalNum;
	}
	
	
}
