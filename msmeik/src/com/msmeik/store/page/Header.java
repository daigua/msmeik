package com.msmeik.store.page;

import java.sql.Date;

import com.msmeik.store.Store;
import com.msmeik.store.StoreService;
import com.msmeik.store.comment.CommentService;
import com.msmeik.store.content.ContentService;
import com.msmeik.store.photo.PhotoService;
import com.msmeik.store.promotion.PromotionService;
import com.msmeik.store.relish.RelishService;

public class Header {
	private StoreService storeService;
	private CommentService commentService;
	private PhotoService photoService;
	private RelishService relishService;
	private PromotionService promotionService;
	private ContentService contentService;
	private String storeId;
	
	private int relishTotalNum;
	private int promotionTotalNum;
	private int contentTotalNum;
	private int setMealTotalNum;
	private int photoTotalNum;
	
	private Store store;
	private String navMenu="index";
	
	public String execute(){
		this.store = PageUtil.getStore(storeId);
		this.relishTotalNum = this.relishService.getRelishNum(store, null, null, -1, -1, -1, -1, 1);
		this.photoTotalNum = this.photoService.getPhotoCount(store, null, null, null);
		Date now = new Date(System.currentTimeMillis());
		this.promotionTotalNum = this.promotionService.getPromotionNum(store, -1, now);
		this.setMealTotalNum = this.relishService.getSetMealNum(store, null, 1);
		this.contentTotalNum = this.contentService.getContentCount(store.getId(), -1);
		return "success";
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public int getRelishTotalNum() {
		return relishTotalNum;
	}

	public int getPromotionTotalNum() {
		return promotionTotalNum;
	}

	public int getContentTotalNum() {
		return contentTotalNum;
	}

	public int getSetMealTotalNum() {
		return setMealTotalNum;
	}

	public int getPhotoTotalNum() {
		return photoTotalNum;
	}

	public Store getStore() {
		return store;
	}

	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	public void setPhotoService(PhotoService photoService) {
		this.photoService = photoService;
	}

	public void setRelishService(RelishService relishService) {
		this.relishService = relishService;
	}

	public void setPromotionService(PromotionService promotionService) {
		this.promotionService = promotionService;
	}

	public void setContentService(ContentService contentService) {
		this.contentService = contentService;
	}

	public String getNavMenu() {
		return navMenu;
	}

	public void setNavMenu(String navMenu) {
		this.navMenu = navMenu;
	}
	
	
}
