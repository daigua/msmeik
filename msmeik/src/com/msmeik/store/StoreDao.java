package com.msmeik.store;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class StoreDao extends SqlMapClientDaoSupport{
	
	/**
	 * 插入store 并返回插入后生成的id
	 * */
	public Object insertStore(Object store){
		return this.getSqlMapClientTemplate().insert("insertStore", store);
	}
	
	/**
	 * 取得店铺数量
	 * */
	public int selectStoreCount(Object p){
		return (Integer)this.getSqlMapClientTemplate().queryForObject("selectStoreCount", p);
	}
	
	/**
	 * 根据参数取得店铺的id
	 * */
	public List selectStoreIdList(Object p){
		return this.getSqlMapClientTemplate().queryForList("selectStoreId", p);
	}
	/**
	 * 去得店铺List
	 * */
	public List selectStoreList(Object p){
		return this.getSqlMapClientTemplate().queryForList("selectStore", p);
	}
	
	/**
	 * 认领店铺
	 * */
	public Object insertStoreClaim(Object storeClaim){
		return this.getSqlMapClientTemplate().insert("insertStoreClaim", storeClaim);
	}
	/**
	 * 取得店铺认领的数量
	 * */
	public int selectStoreClaimCount(Object p){
		return (Integer)this.getSqlMapClientTemplate().queryForObject("selectStoreClaimCount", p);
	}
	
	/**
	 * 取得店铺认领列表
	 * */
	public List selectStoreClaimList(Object p){
		return this.getSqlMapClientTemplate().queryForList("selectStoreClaim", p);
	}
	
	/**
	 * 删除店铺认领
	 * */
	public int deleteStoreClaim(Object p){
		return this.getSqlMapClientTemplate().delete("deleteStoreClaim", p);
	}
	/**
	 * 更新店铺认领
	 * */
	public int updateStoreClaim(Object storeClaim){
		return this.getSqlMapClientTemplate().update("updateStoreClaim", storeClaim);
	}
	
	/**
	 * 更新店铺
	 * */
	public int updateStore(Object store){
		return this.getSqlMapClientTemplate().update("updateStore", store);
	}
	/**
	 * 更新店铺动态信息
	 * */
	public int updateStoreDynamicInfo(Object store){
		return this.getSqlMapClientTemplate().update("updateStoreDynamicInfo", store);
	}
	
	/**
	 * 删除店铺
	 * */
	public int deleteStore(Object p){
		return this.getSqlMapClientTemplate().delete("deleteStore", p);
	}
}
