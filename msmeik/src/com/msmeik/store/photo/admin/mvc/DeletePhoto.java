package com.msmeik.store.photo.admin.mvc;

import java.util.Date;
import java.util.List;

import com.msmeik.credit.Credit;
import com.msmeik.credit.CreditConfig;
import com.msmeik.credit.CreditLog;
import com.msmeik.credit.CreditService;
import com.msmeik.member.User;
import com.msmeik.store.photo.PhotoService;

public class DeletePhoto {
	private PhotoService photoService;
	private CreditService creditService;
	private List<Integer> photoIds;
	private int userId;
	private int ok;
	
	public String execute(){
		this.ok = this.photoService.deletePhotos(photoIds);
		if(this.ok == 0)
			return "failure";	
		if(this.ok > 0){
			//删除积分
			Credit credit = new Credit();
			int amount = -CreditConfig.getCreditRule().get("photo")*this.ok;
			credit.setCredit(amount);
			User user = new User();
			user.setId(userId);
			credit.setUser(user);
			CreditLog creditLog = new CreditLog();
			creditLog.setTitle("管理员删除图片"+this.ok+"张并扣除"+(-amount)+"积分");
			this.creditService.updateCredit(credit, creditLog);
		}
		return "success";
	}

	public int getOk() {
		return ok;
	}

	public void setPhotoService(PhotoService photoService) {
		this.photoService = photoService;
	}

	public void setPhotoIds(List<Integer> photoIds) {
		this.photoIds = photoIds;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public void setCreditService(CreditService creditService) {
		this.creditService = creditService;
	}
	
	
}
