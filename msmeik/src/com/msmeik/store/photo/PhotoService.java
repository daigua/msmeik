package com.msmeik.store.photo;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.msmeik.member.User;
import com.msmeik.store.Store;

public class PhotoService {
	private PhotoDao photoDao;
	
	private String photoSaveFolder;

	/**
	 * 添加图片分类，并返回添加生成的id
	 * */
	public int addPhotoCategory(PhotoCategory photoCategory){
		if(photoCategory.getStore().getId() <= 0)
			return 0;
		return this.photoDao.insertPhotoCategory(photoCategory);
	}
	/**
	 * 删除图片分类
	 * */
	public int deletePhotoCategoryByStoreIdAndCategoryId(int storeId,int categoryId,int moveToCategoryId){
		if(storeId <= 0 || categoryId <= 0)
			return 0;
		Store store = new Store();
		store.setId(storeId);
		Map p = new HashMap();
		p.put("store", store);
		p.put("id", categoryId);
		int i = this.photoDao.deletePhotoCategory(p);
		if(i > 0)
			this.movePhotoToCategoryFromCategory(storeId, categoryId, moveToCategoryId);
		return i;
	}
	/**
	 * 更新图片分类
	 * */
	public int updatePhotoCategory(PhotoCategory category){
		if(category == null || category.getStore() == null || category.getStore().getId() <= 0 || category.getId() <= 0)
			return 0;
		return this.photoDao.updatePhotoCategory(category);
	}
	/**
	 * 取得图片分类列表
	 * */
	public List getPhotoCategoryListByStoreId(int storeId){
		Store store = new Store();
		store.setId(storeId);
		Map p = new HashMap();
		p.put("store", store);		
		return this.photoDao.selectPhotoCategory(p);
	}
	/**
	 * 取得photo分类
	 * */
	public List getPhotoCategoryList(Store store,boolean countPhotoNum){
		Map p = new HashMap();
		p.put("store", store);
		List<PhotoCategory> list = this.photoDao.selectPhotoCategory(p);
		if(countPhotoNum && list.size() > 0){
			List<Map> countPhotoNumMapList = this.countPhotoGroupByCategory(store);
			Map photoNumMap = new HashMap();
			for(Map count:countPhotoNumMapList){
				photoNumMap.put(count.get("categoryId"), count.get("photoNum"));
			}
			for(PhotoCategory category:list){
				if(photoNumMap.containsKey(category.getId())){
					category.setPhotoNum((Integer)photoNumMap.get(category.getId()));
				}
			}
		}
		return list;
	}
	
	/**
	 * 添加photo，并返回添加后的id
	 * */
	public int addPhoto(Photo photo){
		if(photo.getStore().getId() <= 0 || photo.getTarget().getKey() <= 0 || photo.getTarget().getType() <= 0)
			return 0;
		return this.photoDao.insertPhoto(photo);
	}
	
	/**
	 * 取得photoList
	 * */
	public List getPhotoList(Store store,User uploader,PhotoCategory category,PhotoTarget target,int page,int pageSize,String orderBy){
		Map p = new HashMap();
		if( store!=null && store.getId() > 0)
			p.put("store", store);
		if( uploader!=null && uploader.getId() > 0)
			p.put("uploader", uploader);
		if( category!=null && category.getId() > 0)
			p.put("category", category);
		if( target!=null && target.getKey() > 0 && target.getType() > 0)
			p.put("target", target);
		if(orderBy != null)
			p.put("orderBy", orderBy);
		if(page < 1)
			page = 1;
		int begin = (page-1)*pageSize;
		int end = pageSize;
		p.put("begin", begin);
		p.put("end", end);
		return this.photoDao.selectPhotoList(p);
	}
	/**
	 * 取得photo的数量
	 * */
	public int getPhotoCount(Store store,User uploader,PhotoCategory category,PhotoTarget target){
		Map p = new HashMap();
		if( store!=null && store.getId() > 0)
			p.put("store", store);
		if( uploader!=null && uploader.getId() > 0)
			p.put("uploader", uploader);
		if( category!=null && category.getId() > 0)
			p.put("category", category);
		if( target!=null && target.getKey() > 0 && target.getType() > 0)
			p.put("target", target);
		return this.photoDao.selectPhotoCount(p);
	}
	
	
	/**
	 * 设置图片所属分类
	 * */
	public int setPhotoCategory(int storeId,List photoIds,int categoryId){
		if(storeId <= 0 || photoIds.size() == 0 || categoryId < 0)
			return 0;
		Map p = new HashMap();
		p.put("photoIds", photoIds);
		PhotoCategory category = new PhotoCategory();
		category.setId(categoryId);
		p.put("category", category);
		Store store = new Store();
		store.setId(storeId);
		p.put("store", store);
		return this.photoDao.updatePhoto(p);
	}
	
	/**
	 * 移动图片所属分类
	 * */
	public int movePhotoToCategoryFromCategory(int storeId,int fromCategoryId, int toCategoryId){
		if(storeId <= 0 || fromCategoryId < 0 || toCategoryId < 0 || (fromCategoryId == toCategoryId)){
			return 0;
		}
		Map p = new HashMap();
		Store store = new Store();
		store.setId(storeId);
		p.put("store", store);
		PhotoCategory category = new PhotoCategory();
		category.setId(fromCategoryId);
		p.put("fromCategory", category);
		category.setId(toCategoryId);
		p.put("toCategory", category);
		return this.photoDao.updatePhoto(p);
	}
	
	/**
	 * 删除photo
	 * */
	public int deletePhotoByUploader(User uploader,List<Integer> photoIds){
		if(photoIds.size() == 0 || uploader.getId() <= 0)
			return 0;
		Map p = new HashMap();
		p.put("uploader", uploader);
		p.put("photoIds", photoIds);
		List<Photo> deletePhotos = this.photoDao.selectPhotoList(p);
		if(deletePhotos.size() == 0)
			return 0;
		for(Photo photo:deletePhotos){
			File file = new File(this.photoSaveFolder+File.separator+photo.getStore().getId()+File.separator+photo.getId());
			if(file.exists())
				file.delete();
			File files = new File(this.photoSaveFolder+File.separator+photo.getStore().getId()+File.separator+photo.getId()+"_s");
			if(files.exists())
				files.delete();
		}
		return this.photoDao.deletePhoto(p);
	}
	/**
	 * 删除photo
	 * 保留 方法，不建议用
	 * 店主用此方法可以删除自己店铺的图片，包括美客上传的图片
	 * */
	public int deletePhoto(int storeId,List<Integer> photoIds){
		if(storeId <= 0 || photoIds.size() == 0 )
			return 0;
		Map p = new HashMap();
		Store store = new Store();
		store.setId(storeId);
		p.put("store", store);
		p.put("photoIds", photoIds);
		for(int photoId:photoIds){
			File file = new File(this.photoSaveFolder+File.separator+storeId+File.separator+photoId);
			if(file.exists())
				file.delete();
			File files = new File(this.photoSaveFolder+File.separator+storeId+File.separator+photoId+"_s");
			if(files.exists())
				files.delete();
		}
		return this.photoDao.deletePhoto(p);
	}
	
	
	/**
	 * 根据用户id取得photo数量
	 * */
	public int getPhotoNumByUploaderId(int userId){
		if(userId <= 0)
			return 0;
		Map  p = new HashMap();
		User user = new User();
		user.setId(userId);
		p.put("uploader", user);
		return this.photoDao.selectPhotoCount(p);
	}
	
	/**
	 * this method just for admin
	 * delete photos by ids
	 * */
	public int deletePhotos(List photoIds){
		if(photoIds == null || photoIds.size()==0)
			return 0;
		Map p = new HashMap();
		p.put("photoIds", photoIds);
		List<Photo> deletePhotos = this.photoDao.selectPhotoList(p);
		if(deletePhotos.size() == 0)
			return 0;
		for(Photo photo:deletePhotos){
			File file = new File(this.photoSaveFolder+File.separator+photo.getStore().getId()+File.separator+photo.getId());
			if(file.exists())
				file.delete();
			File files = new File(this.photoSaveFolder+File.separator+photo.getStore().getId()+File.separator+photo.getId()+"_s");
			if(files.exists())
				files.delete();
		}
		return this.photoDao.deletePhoto(p);
	}
	
	/**
	 * 取得分类下photo num
	 * */
	public List countPhotoGroupByCategory(Store store){
		if(store == null || store.getId() <= 0)
			return new ArrayList();
		Map p = new HashMap();
		p.put("store", store);
		return this.photoDao.countPhotoGroupByCategory(p);
	}
	
	public void setPhotoSaveFolder(String photoSaveFolder) {
		this.photoSaveFolder = photoSaveFolder;
	}
	public void setPhotoDao(PhotoDao photoDao) {
		this.photoDao = photoDao;
	}
	
	
}
