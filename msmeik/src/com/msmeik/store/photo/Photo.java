package com.msmeik.store.photo;

import java.util.Date;

import com.msmeik.member.User;
import com.msmeik.store.Store;

public class Photo {
	private int id;
	private String name;
	private User uploader;
	private PhotoCategory category; 
	private Store store;
	private PhotoTarget target;
	private int size;
	private Date uploadTime;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public PhotoTarget getTarget() {
		return target;
	}
	public void setTarget(PhotoTarget target) {
		this.target = target;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Store getStore() {
		return store;
	}
	public void setStore(Store store) {
		this.store = store;
	}
	public User getUploader() {
		return uploader;
	}
	public void setUploader(User uploader) {
		this.uploader = uploader;
	}
	
	public PhotoCategory getCategory() {
		return category;
	}
	public void setCategory(PhotoCategory category) {
		this.category = category;
	}
	
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public Date getUploadTime() {
		return uploadTime;
	}
	public void setUploadTime(Date uploadTime) {
		this.uploadTime = uploadTime;
	}
	
	
}
