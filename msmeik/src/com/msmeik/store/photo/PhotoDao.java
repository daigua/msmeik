package com.msmeik.store.photo;

import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class PhotoDao extends SqlMapClientDaoSupport{
	
	/**
	 * 添加PHOTO，并返回添加后的id
	 * */
	public int insertPhoto(Object photo){
		return (Integer) this.getSqlMapClientTemplate().insert("insertPhoto", photo);
	}
	/**
	 * 更新photo，并返回更新数量
	 * */
	public int updatePhoto(Object photo){
		return this.getSqlMapClientTemplate().update("updatePhoto", photo);
	}
	/**
	 * 取得PHOTO List
	 * */
	public List selectPhotoList(Object p){
		return this.getSqlMapClientTemplate().queryForList("selectPhoto", p);
	}
	/**
	 * 取得PHOTO数量
	 * */
	public int selectPhotoCount(Object p){
		return (Integer)this.getSqlMapClientTemplate().queryForObject("selectPhotoCount", p);
	}
	/**
	 * 删除photo，并返回删除数量
	 * */
	public int deletePhoto(Object p){
		return this.getSqlMapClientTemplate().delete("deletePhoto", p);
	}
	
	/**
	 * 添加photo分类
	 * */
	public int insertPhotoCategory(Object category){
		return (Integer) this.getSqlMapClientTemplate().insert("insertPhotoCategory", category);
	}
	/**
	 * 更新photoCategory
	 * */
	public int updatePhotoCategory(Object category){
		return this.getSqlMapClientTemplate().update("updatePhotoCategory", category);
	}
	/**
	 * 删除PhotoCategory
	 * */
	public int deletePhotoCategory(Object p){
		return this.getSqlMapClientTemplate().delete("deletePhotoCategory", p);
	}
	/**
	 * 取得PHotoCategory
	 * */
	public List selectPhotoCategory(Object p){
		return this.getSqlMapClientTemplate().queryForList("selectPhotoCategory", p);
	}
	
	/**
	 * 统计分类下的photo数量
	 * */
	public List countPhotoGroupByCategory(Object p){
		return this.getSqlMapClientTemplate().queryForList("countPhotoGroupByCategory", p);
	}
}
