package com.msmeik.store.photo.mvc;

import java.util.List;

import com.msmeik.member.User;
import com.msmeik.store.Store;
import com.msmeik.store.photo.PhotoCategory;
import com.msmeik.store.photo.PhotoService;
import com.msmeik.store.photo.PhotoTarget;

public class GetPhotoList {
	private PhotoService photoService;
	private int storeId;
	private int uploaderId;
	private List photoList;
	private int page = 1;
	private int pageSize = 30;
	private int targetKey;
	private int targetType;
	private int totalCount;
	private int categoryId ;
	private String orderBy;
	
	public String execute(){
		Store store = new Store();
		store.setId(storeId);
		User uploader = new User();
		uploader.setId(uploaderId);
		PhotoCategory category = new PhotoCategory();
		category.setId(categoryId);
		PhotoTarget target = new PhotoTarget();
		target.setKey(targetKey);
		target.setType(targetType);
		this.photoList = this.photoService.getPhotoList(store, uploader, category, target, page, pageSize, orderBy);
		this.totalCount = this.photoService.getPhotoCount(store, uploader, category, target);
		return "success";
	}

	public void setTargetKey(int targetKey) {
		this.targetKey = targetKey;
	}

	public void setTargetType(int targetType) {
		this.targetType = targetType;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public int getStoreId() {
		return storeId;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public List getPhotoList() {
		return photoList;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setUploaderId(int uploaderId) {
		this.uploaderId = uploaderId;
	}

	public int getUploaderId() {
		return uploaderId;
	}

	public void setPhotoService(PhotoService photoService) {
		this.photoService = photoService;
	}
	
	
}
