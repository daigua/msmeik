package com.msmeik.store.photo.mvc;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;

import com.msmeik.authentication.AuthenticationService;
import com.msmeik.credit.Credit;
import com.msmeik.credit.CreditConfig;
import com.msmeik.credit.CreditLog;
import com.msmeik.credit.CreditService;
import com.msmeik.io.image.ImageScale;
import com.msmeik.member.User;
import com.msmeik.store.Store;
import com.msmeik.store.photo.Photo;
import com.msmeik.store.photo.PhotoCategory;
import com.msmeik.store.photo.PhotoService;
import com.msmeik.store.photo.PhotoTarget;
import com.opensymphony.xwork2.ActionSupport;

public class PhotoUpload extends ActionSupport{
	private PhotoService photoService;
	private CreditService creditService;
	private File photoFile;
	private String photoFileFileName;
	private int storeId;
	private int categoryId;
	private int targetKey;
	private int targetType;
	private String targetName;
	private int maxWidth;
	private int maxHeight;
	private int smallWidth = 100;
	
	private int credit;
	
	private int smallHeight = 100;
	private String saveFolder;
	
	private int ok;
	
	public String execute(){
		if(this.storeId <= 0 || photoFile == null)
			return "input";
		Photo photo = new Photo();
		photo.setUploadTime(new Date());
		User user = AuthenticationService.getLoginUser();
		photo.setUploader(user);
		Store store = new Store();
		store.setId(storeId);
		photo.setStore(store);
		photo.setName(this.photoFileFileName.replaceAll("\\..*$", ""));
		photo.setSize((int)this.photoFile.length());
		PhotoTarget target = new PhotoTarget();
		target.setKey(targetKey);
		target.setName(targetName);
		target.setType(targetType);
		photo.setTarget(target);
		PhotoCategory photoCategory = new PhotoCategory();
		photoCategory.setId(categoryId);
		photo.setCategory(photoCategory);
		int photoId = this.photoService.addPhoto(photo);
		ImageScale imageScale = new ImageScale();
		try {
			File file = new File(this.saveFolder+File.separator+this.storeId);
			if(!file.exists())
				file.mkdirs();
			FileOutputStream out = new FileOutputStream(this.saveFolder+File.separator+this.storeId+File.separator+photoId);
			imageScale.compressSave(this.photoFile,out,this.maxWidth,this.maxHeight,"jpg");
			out.flush();
			out.close();
			FileOutputStream outSmall = new FileOutputStream(this.saveFolder+File.separator+this.storeId+File.separator+photoId+"_s");
			imageScale.compressSave(this.photoFile,outSmall,this.smallWidth,this.smallHeight,"jpg");
			outSmall.flush();
			outSmall.close();
			//获得积分
			Credit credit = new Credit();
			int amount = CreditConfig.getCreditRule().get("photo");
			credit.setCredit(amount);
			credit.setUser(user);
			CreditLog creditLog = new CreditLog();
			creditLog.setTitle(user.getAccount()+"上传图片获得"+amount+"积分");
			this.credit = this.creditService.updateCredit(credit, creditLog);
			ok = photoId;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "success";
	}

	public File getPhotoFile() {
		return photoFile;
	}

	public void setPhotoFile(File photoFile) {
		this.photoFile = photoFile;
	}

	public String getPhotoFileFileName() {
		return photoFileFileName;
	}

	public void setPhotoFileFileName(String photoFileFileName) {
		this.photoFileFileName = photoFileFileName;
	}

	public int getOk() {
		return ok;
	}

	public void setPhotoService(PhotoService photoService) {
		this.photoService = photoService;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public void setMaxWidth(int maxWidth) {
		this.maxWidth = maxWidth;
	}

	public void setMaxHeight(int maxHeight) {
		this.maxHeight = maxHeight;
	}

	public void setSmallWidth(int smallWidth) {
		this.smallWidth = smallWidth;
	}

	public void setSmallHeight(int smallHeight) {
		this.smallHeight = smallHeight;
	}

	public void setSaveFolder(String saveFolder) {
		this.saveFolder = saveFolder;
	}
	
	public int getTargetKey() {
		return targetKey;
	}

	public void setTargetKey(int targetKey) {
		this.targetKey = targetKey;
	}

	public int getTargetType() {
		return targetType;
	}

	public void setTargetType(int targetType) {
		this.targetType = targetType;
	}

	public String getTargetName() {
		return targetName;
	}

	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}

	public int getStoreId() {
		return storeId;
	}

	public int getCredit() {
		return credit;
	}

	public void setCreditService(CreditService creditService) {
		this.creditService = creditService;
	}
	
	
}
