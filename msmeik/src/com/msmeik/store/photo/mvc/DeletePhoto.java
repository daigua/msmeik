package com.msmeik.store.photo.mvc;

import java.util.List;

import com.msmeik.authentication.AuthenticationService;
import com.msmeik.credit.Credit;
import com.msmeik.credit.CreditConfig;
import com.msmeik.credit.CreditLog;
import com.msmeik.credit.CreditService;
import com.msmeik.member.User;
import com.msmeik.store.photo.PhotoService;

public class DeletePhoto {
	private PhotoService photoService;
	private CreditService creditService;
	private List<Integer> photoIds;
	private int ok;
	
	public String execute(){
		User uploader = AuthenticationService.getLoginUser();
		this.ok = this.photoService.deletePhotoByUploader(uploader, photoIds);
		if(this.ok > 0){
			//ɾ������
			Credit credit = new Credit();
			int amount = -CreditConfig.getCreditRule().get("photo")*this.ok;
			credit.setCredit(amount);
			credit.setUser(uploader);
			CreditLog creditLog = new CreditLog();
			creditLog.setTitle("ɾ��ͼƬ"+this.ok+"�Ų��۳�"+(-amount)+"����");
			this.creditService.updateCredit(credit, creditLog);
		}
		return "success";
	}

	public int getOk() {
		return ok;
	}

	public void setPhotoService(PhotoService photoService) {
		this.photoService = photoService;
	}

	public void setPhotoIds(List<Integer> photoIds) {
		this.photoIds = photoIds;
	}

	public void setCreditService(CreditService creditService) {
		this.creditService = creditService;
	}
	
	
}
