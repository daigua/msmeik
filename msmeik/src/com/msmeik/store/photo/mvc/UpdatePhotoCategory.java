package com.msmeik.store.photo.mvc;

import com.msmeik.store.Store;
import com.msmeik.store.photo.PhotoCategory;
import com.msmeik.store.photo.PhotoService;
import com.opensymphony.xwork2.ActionSupport;

public class UpdatePhotoCategory extends ActionSupport{
	private PhotoService photoService;
	private PhotoCategory photoCategory;
	private int storeId;
	private int ok;
	
	public String execute(){
		Store store = new Store();
		store.setId(storeId);
		this.photoCategory.setStore(store);
		this.ok = this.photoService.updatePhotoCategory(photoCategory);
		return "input";
	}

	public int getStoreId() {
		return storeId;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public PhotoCategory getPhotoCategory() {
		return photoCategory;
	}

	public void setPhotoCategory(PhotoCategory photoCategory) {
		this.photoCategory = photoCategory;
	}

	public int getOk() {
		return ok;
	}

	public void setPhotoService(PhotoService photoService) {
		this.photoService = photoService;
	}
	
}
