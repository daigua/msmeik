package com.msmeik.store.photo.mvc;

import com.msmeik.store.photo.PhotoService;

public class DeletePhotoCategory {
	private PhotoService photoService;
	
	private int storeId;
	private int categoryId;
	private int toCategoryId = 1;
	
	private int ok;
	
	public String execute(){
		this.ok = this.photoService.deletePhotoCategoryByStoreIdAndCategoryId(storeId, categoryId,toCategoryId);
		return "success";
	}

	public int getOk() {
		return ok;
	}

	public void setPhotoService(PhotoService photoService) {
		this.photoService = photoService;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public void setToCategoryId(int toCategoryId) {
		this.toCategoryId = toCategoryId;
	}
	
	
}
