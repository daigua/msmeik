package com.msmeik.store.photo.mvc;

import java.util.List;

import org.apache.struts2.ServletActionContext;

import com.msmeik.store.photo.PhotoService;

public class ManagePhoto {
	private PhotoService photoService;
	private List<Integer> photoIds;
	private String p;
	private int v;
	private int storeId;
	private int ok;
	
	public String execute(){
		System.out.println(ServletActionContext.getRequest().getQueryString());
		if(photoIds == null || photoIds.size() ==0)
			return "success";
		if(this.p.equals("category")){
			this.ok = this.photoService.setPhotoCategory(storeId, photoIds, v);
		}
		return "success";
	}

	public int getOk() {
		return ok;
	}

	public void setPhotoService(PhotoService photoService) {
		this.photoService = photoService;
	}

	public void setPhotoIds(List photoIds) {
		this.photoIds = photoIds;
	}

	public void setP(String p) {
		this.p = p;
	}

	public void setV(int v) {
		this.v = v;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}
	
	
}
