package com.msmeik.store.photo.mvc;

import java.util.List;

import com.msmeik.store.photo.PhotoService;

public class GetPhotoCategory {
	private PhotoService photoService;
	private int storeId;
	private List categoryList;
	public String execute(){
		this.categoryList = this.photoService.getPhotoCategoryListByStoreId(storeId);
		return "success";
	}
	public List getCategoryList() {
		return categoryList;
	}
	public void setPhotoService(PhotoService photoService) {
		this.photoService = photoService;
	}
	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}
	
	
}
