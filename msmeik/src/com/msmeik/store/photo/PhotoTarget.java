package com.msmeik.store.photo;

public class PhotoTarget {
	private int key;
	private int type;
	private String name;
	
	public static int TYPE_STORE = 1;
	public static int TYPE_RELISH = 2;
	public static int TYPE_SETMEAL = 3;
	
	public int getKey() {
		return key;
	}
	public void setKey(int key) {
		this.key = key;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
