package com.msmeik.store.promotion.mvc;

import java.io.File;
import java.io.FileOutputStream;

import com.msmeik.io.image.ImageScale;
import com.opensymphony.xwork2.ActionSupport;

public class UploadPromotionImage extends ActionSupport{
	private int maxWidth;
	private int maxHeight;
	private int smallWidth = 200;
	private int smallHeight = 200;
	private String saveFolder;
	private int ok;
	private int storeId;
	private int promotionId;
	private File promotionImage;
	
	public String execute(){
		if(storeId <= 0 || promotionId <= 0)
			return "input";
		ImageScale imageScale = new ImageScale();
		try {
			File outputFileFolder = new File(this.saveFolder+File.separator+this.storeId);
			if(!outputFileFolder.exists())
				outputFileFolder.mkdirs();
			FileOutputStream out = new FileOutputStream(this.saveFolder+File.separator+this.storeId+File.separator+this.promotionId);
			imageScale.compressSave(this.promotionImage,out,this.maxWidth,this.maxHeight,"jpg");
			FileOutputStream out1 = new FileOutputStream(this.saveFolder+File.separator+this.storeId+File.separator+this.promotionId+"_s");
			imageScale.compressSave(this.promotionImage,out1,this.smallWidth,this.smallHeight,"jpg");
			out.flush();
			out.close();
			ok = 9;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "input";
	}

	public File getPromotionImage() {
		return promotionImage;
	}

	public void setSmallWidth(int smallWidth) {
		this.smallWidth = smallWidth;
	}

	public void setSmallHeight(int smallHeight) {
		this.smallHeight = smallHeight;
	}

	public void setPromotionImage(File promotionImage) {
		this.promotionImage = promotionImage;
	}

	public int getOk() {
		return ok;
	}

	public void setMaxWidth(int maxWidth) {
		this.maxWidth = maxWidth;
	}

	public void setMaxHeight(int maxHeight) {
		this.maxHeight = maxHeight;
	}

	public void setSaveFolder(String saveFolder) {
		this.saveFolder = saveFolder;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public void setPromotionId(int promotionId) {
		this.promotionId = promotionId;
	}
	
	
}
