package com.msmeik.store.promotion.mvc;

import java.util.Date;

import com.msmeik.store.promotion.Promotion;
import com.msmeik.store.promotion.PromotionService;
import com.msmeik.store.Store;
import com.opensymphony.xwork2.ActionSupport;

public class AddPromotion extends ActionSupport{
	private PromotionService promotionService;
	private int storeId;
	private Promotion promotion;
	private int ok;
	
	public String execute(){
		this.promotion.setAddTime(new Date());
		this.promotion.setUpdateTime(new Date());
		this.promotion.setOrderTime(new Date());
		Store store = new Store();
		store.setId(storeId);
		this.promotion.setStore(store);
		this.ok = this.promotionService.addPromotion(promotion);
		return "input";
	}

	public Promotion getPromotion() {
		return promotion;
	}

	public void setPromotion(Promotion promotion) {
		this.promotion = promotion;
	}

	public int getOk() {
		return ok;
	}

	public void setPromotionService(PromotionService promotionService) {
		this.promotionService = promotionService;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}
	
	
}
