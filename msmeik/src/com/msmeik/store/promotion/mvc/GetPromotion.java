package com.msmeik.store.promotion.mvc;

import com.msmeik.store.promotion.Promotion;
import com.msmeik.store.promotion.PromotionService;

public class GetPromotion {
	private PromotionService promotionService;
	
	private int storeId;
	private int promotionId;
	private Promotion promotion;
	
	public String execute(){
		this.promotion = this.promotionService.getPromotionByStoreIdAndId(storeId, promotionId);
		if(this.promotion==null)
			this.promotion=new Promotion();
		return "success";
	}

	public Promotion getPromotion() {
		return promotion;
	}

	public void setPromotionService(PromotionService promotionService) {
		this.promotionService = promotionService;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public void setPromotionId(int promotionId) {
		this.promotionId = promotionId;
	}
	
	
}
