package com.msmeik.store.promotion.mvc;

import java.util.Date;

import com.msmeik.store.promotion.Promotion;
import com.msmeik.store.promotion.PromotionService;
import com.msmeik.store.Store;
import com.opensymphony.xwork2.ActionSupport;

public class UpdatePromotion extends ActionSupport{

	private PromotionService promotionService;
	private Promotion promotion;
	
	private int storeId;
	private int ok;
	
	public String execute(){
		Store store = new Store();
		store.setId(storeId);
		this.promotion.setStore(store);
		this.promotion.setUpdateTime(new Date());
		this.ok = this.promotionService.updatePromotion(promotion);
		return "input";
	}

	public Promotion getPromotion() {
		return promotion;
	}

	public void setPromotion(Promotion promotion) {
		this.promotion = promotion;
	}

	public int getOk() {
		return ok;
	}

	public void setPromotionService(PromotionService promotionService) {
		this.promotionService = promotionService;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}
	
	
}
