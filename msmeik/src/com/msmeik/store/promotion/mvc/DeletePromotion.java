package com.msmeik.store.promotion.mvc;

import java.io.File;

import com.msmeik.store.promotion.PromotionService;

public class DeletePromotion {
	private PromotionService promotionService;
	private int storeId;
	private int promotionId;
	private String saveFolder;
	private int ok;
	
	public String execute(){
		this.ok = this.promotionService.deletePromotionByStoreIdAndPromotionId(storeId, promotionId);
		if(this.ok > 0){
			File file = new File(this.saveFolder+File.separator+this.storeId+File.separator+this.promotionId);
			if(file.exists())
				file.delete();
			File file1 = new File(this.saveFolder+File.separator+this.storeId+File.separator+this.promotionId+"_s");
			if(file1.exists())
				file1.delete();
		}
		return "success";
	}

	public int getOk() {
		return ok;
	}

	public void setPromotionService(PromotionService promotionService) {
		this.promotionService = promotionService;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public void setPromotionId(int promotionId) {
		this.promotionId = promotionId;
	}

	public void setSaveFolder(String saveFolder) {
		this.saveFolder = saveFolder;
	}
	
	
}
