package com.msmeik.store.promotion.mvc;

import java.util.Date;

import com.msmeik.store.promotion.PromotionService;

public class SetPromotion {
	private PromotionService promotionService;
	private int storeId;
	private String p;
	private int v;
	private int ok;
	private int promotionId;
	
	private Date orderTime;
	public String execute(){
		if(p.equals("orderTime")){
			orderTime = new Date();
			this.ok = this.promotionService.setPromotionOrderTime(storeId, promotionId, orderTime);
		}
		return "success";
	}
	public Date getOrderTime() {
		return orderTime;
	}
	public int getOk() {
		return ok;
	}
	public void setPromotionService(PromotionService promotionService) {
		this.promotionService = promotionService;
	}
	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}
	public void setP(String p) {
		this.p = p;
	}
	public void setV(int v) {
		this.v = v;
	}
	public void setPromotionId(int promotionId) {
		this.promotionId = promotionId;
	}
	
}
