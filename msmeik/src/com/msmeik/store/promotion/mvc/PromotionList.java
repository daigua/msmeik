package com.msmeik.store.promotion.mvc;

import java.util.List;

import com.msmeik.store.promotion.PromotionService;

public class PromotionList {
	private PromotionService promotionService;
	private int storeId;
	private List promotionList;
	private int page = 1;
	private int pageSize = 10;
	private int totalCount;
	

	public String execute(){
		this.promotionList = this.promotionService.getPromotionListByStoreId(storeId,page,pageSize);
		this.totalCount = this.promotionService.getPromotionCount(storeId);
		return "success";
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setPromotionList(List promotionList) {
		this.promotionList = promotionList;
	}

	public List getPromotionList() {
		return promotionList;
	}

	public void setPromotionService(PromotionService promotionService) {
		this.promotionService = promotionService;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}
	
	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

}
