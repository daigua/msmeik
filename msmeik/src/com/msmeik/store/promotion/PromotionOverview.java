package com.msmeik.store.promotion;

import java.util.Date;

import com.msmeik.store.Store;

public class PromotionOverview {
	private int id;
	private Store store;
	private int promotionNum;
	private int couponNum;
	private int infoNum;
	private Date lastPromotionTime;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Store getStore() {
		return store;
	}
	public void setStore(Store store) {
		this.store = store;
	}
	public int getPromotionNum() {
		return promotionNum;
	}
	public void setPromotionNum(int promotionNum) {
		this.promotionNum = promotionNum;
	}
	public Date getLastPromotionTime() {
		return lastPromotionTime;
	}
	public void setLastPromotionTime(Date lastPromotionTime) {
		this.lastPromotionTime = lastPromotionTime;
	}
	
	
}
