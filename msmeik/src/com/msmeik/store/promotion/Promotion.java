package com.msmeik.store.promotion;

import java.util.Date;

import com.msmeik.store.Store;

public class Promotion {
	private int id;
	private String name;
	private Store store;
	private int type;//1为优惠信息 2为凭券消费
	private Date beginTime;
	private Date endTime;
	private String tags;
	private String introduction;
	private Date addTime;
	private Date orderTime;
	private Date updateTime;
	private int clickTimes;//点击次数
	private int printTimes;//打印次数
	private boolean expired;
	private int remainingDays;//剩下几天
	
	public static int TYPE_INFO = 1;
	public static int TYPE_COUPON = 2;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Store getStore() {
		return store;
	}
	public void setStore(Store store) {
		this.store = store;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public Date getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	public String getIntroduction() {
		return introduction;
	}
	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}
	public Date getAddTime() {
		return addTime;
	}
	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}
	public Date getOrderTime() {
		return orderTime;
	}
	public void setOrderTime(Date orderTime) {
		this.orderTime = orderTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public int getClickTimes() {
		return clickTimes;
	}
	public void setClickTimes(int clickTimes) {
		this.clickTimes = clickTimes;
	}
	public int getPrintTimes() {
		return printTimes;
	}
	public void setPrintTimes(int printTimes) {
		this.printTimes = printTimes;
	}	
	
	public int getRemainingDays() {
		long now = new Date().getTime();
		long endTime = this.endTime.getTime();
		return (int) ((endTime - now + 24 * 60 * 60 * 1000)/(24 * 60 * 60 * 1000))+1;
	}
	public boolean isExpired() {
		if(this.endTime.before(new Date()))
			return true;
		return false;
	}
	
}
