package com.msmeik.store.promotion;

import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class PromotionDao extends SqlMapClientDaoSupport{
	
	/**
	 * 添加促销信息
	 * */
	public int insertPromotion(Object promotion){
		return (Integer) this.getSqlMapClientTemplate().insert("insertPromotion", promotion);
	}
	
	/**
	 * 读取促销信息
	 * */
	public List selectPromotion(Object p){
		return this.getSqlMapClientTemplate().queryForList("selectPromotion", p);
	}
	
	/**
	 * 更新促销信息
	 * */
	public int updatePromotion(Object p){
		return this.getSqlMapClientTemplate().update("updatePromotion", p);
	}
	
	/**
	 * 删除促销信息
	 * */
	public int deletePromotion(Object p){
		return this.getSqlMapClientTemplate().delete("deletePromotion", p);
	}
	/**
	 * 取得促销信息的数量
	 * */
	public int selectPromotionCount(Object p){
		return (Integer) this.getSqlMapClientTemplate().queryForObject("selectPromotionCount", p);
	}
	
	/**
	 * 根据促销类型统计数量
	 * */
	public Map countPromotionByType(Object p){
		return (Map) this.getSqlMapClientTemplate().queryForObject("countPromotionByType", p);
	}
	
	/***/
	public int insertPromotionOverview(Object overview){
		return (Integer) this.getSqlMapClientTemplate().insert("insertPromotionOverview", overview);
	}
	public int updatePromotionOverview(Object overview){
		return this.getSqlMapClientTemplate().update("updatePromotionOverview", overview);
	}
	public List selectPromotionOverview(Object p){
		return this.getSqlMapClientTemplate().queryForList("selectPromotionOverview", p);
	}
	public int updatePromotionDynamicInfo(Object p){
		return this.getSqlMapClientTemplate().update("updatePromotionDynamicInfo", p);
	}
}
