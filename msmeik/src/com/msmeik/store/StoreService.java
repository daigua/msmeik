package com.msmeik.store;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.msmeik.member.User;

public class StoreService {
	private StoreDao storeDao;
	
	/**
	 * 登记店铺，并返回登记的id
	 * */
	public int registerStore(Store store){
		return (Integer)this.storeDao.insertStore(store);
	}
	
	/**
	 * 根据店铺名称取得店铺数量
	 * */
	
	public int getStoreCountByName(String name){
		if(name==null || name.equals(""))
			return 0;
		Map p = new HashMap();
		p.put("name", name);
		return this.storeDao.selectStoreCount(p);
	}
	
	/**
	 * 根据store id 判断店铺是否存在
	 * */
	public boolean checkStoreExistById(int id){
		if(id<=0)
			return false;
		Map p = new HashMap();
		p.put("id", id);
		if(this.storeDao.selectStoreCount(p)!=1)
			return false;
		return true;
	}
	
	/**
	 * 判断店铺是否已被认领
	 * */
	public boolean checkStoreHasBeenClaimed(int id){
		Map p = new HashMap();
		p.put("id", id);
		p.put("claimed", true);
		if(this.storeDao.selectStoreCount(p)!=1)
			return false;
		return true;
	}
	
	/**
	 * 判断店铺是否有人正在认领
	 * */
	public boolean checkStoreIsBeenClaimingByStoreId(int id){
		Map p = new HashMap();
		p.put("storeId", id);
		if(this.storeDao.selectStoreClaimCount(p)!=1)
			return false;
		return true;
	}
	
	/**
	 * 根据店铺名取得店铺ID
	 * */
	public int getStoreIdByName(String name){
		if(name==null || name.equals(""))
			return 0;
		Map p = new HashMap();
		p.put("name", name);
		List l = this.storeDao.selectStoreIdList(p);
		if(l.size()>1)
			return -1;
		if(l.size()==1)
			return (Integer)l.get(0);
		return 0;
	}
	
	/**
	 * 根据storeId取得store
	 * */
	public Store getStoreById(int storeId){
		if(storeId<=0)
			return null;
		Map p = new HashMap();
		p.put("id", storeId);
		List l = this.storeDao.selectStoreList(p);
		if(l.size()>1 || l.size()==0)
			return null;
		return (Store)l.get(0);
	}
	
	/**
	 * 根据自定义域名取得店铺
	 * */
	public Store getStoreByCustomDomain(String domain){
		if(domain == null)
			return null;
		Map p = new HashMap();
		p.put("customDomain", domain);
		List l = this.storeDao.selectStoreList(p);
		if(l.size()!=1)
			return null;
		return (Store)l.get(0);
	}
	
	/**
	 * 根据store name id 取得store
	 * */
	public Store getStoreBySubdomain(String subdomain){
		if(subdomain == null)
			return null;
		Map p = new HashMap();
		p.put("subdomain", subdomain);
		List l = this.storeDao.selectStoreList(p);
		if(l.size()>1 || l.size()==0)
			return null;
		return (Store)l.get(0);
	}
	/**
	 * 根据店铺名称来取得店铺
	 * */
	public Store getStoreByName(String name){
		if(name == null || name.equals(""))
			return null;
		Map p = new HashMap();
		p.put("name", name);
		List l = this.storeDao.selectStoreList(p);
		if(l.size()!=1)
			return null;
		return (Store) l.get(0);
	}
	
	/**
	 * 根据store id 和 user id判断店铺是否属于这个用户
	 * */
	public boolean checkIfStoreBelongedToUser(int storeId,int userId){
		if(storeId <=0 || userId <= 0)
			return false;
		Map p = new HashMap();
		p.put("id", storeId);
		User user = new User();
		user.setId(userId);
		p.put("owner", user);
		int i = this.storeDao.selectStoreCount(p);
		if(i==0)
			return false;
		return true;
	}
	
	/**
	 * 设置店铺的2级域名
	 * */
	public int setStoreSubdomain(Store store, String subdomain){
		if(store.getId() <= 0 || subdomain.trim().equals(""))
			return 0;
		Map p = new HashMap();
		p.put("id", store.getId());
		p.put("subdomain", subdomain);
		return this.storeDao.updateStore(p);
	}
	/**
	 * 设置店铺的自定义域名
	 * */
	public int setStoreCustomDomain(Store store,String customDomain){
		if(store.getId() <= 0 || customDomain.trim().equals(""))
			return 0;
		Map p = new HashMap();
		p.put("id", store.getId());
		p.put("customDomain", customDomain);
		return this.storeDao.updateStore(p);
	}
	
	/**
	 * 根据store name 取得store 列表
	 * @param name:store的名称
	 * @param type:查找的类型，  默认为=查找
	 * */
	public List getStoreListByName(String name,String type,int page,int pageSize){
		if(name==null || name.equals(""))
			return new ArrayList();
		
		Map p = new HashMap();
		if(page < 1)
			page = 1;
		int begin = (page-1)*pageSize;
		int end = pageSize;
		p.put("begin", begin);
		p.put("end", end);
		if(type!=null && type.equals("like")){
			p.put("nameLike", "like");
		}
		p.put("name", name);
		return this.storeDao.selectStoreList(p);
	}
	
	/**
	 * 取得用户的昵称拥有的店铺
	 * */
	public List getStoreListByOwnerAccount(String account,int page,int pageSize){
		if(account==null || account.equals(""))
			return new ArrayList();
		Map p = new HashMap();
		if(page < 1)
			page = 1;
		int begin = (page-1)*pageSize;
		int end = pageSize;
		p.put("begin", begin);
		p.put("end", end);
		User user = new User();
		user.setAccount(account);
		p.put("owner", user);
		return this.storeDao.selectStoreList(p);
	}
	
	/**
	 * 取得用户的昵称登记的店铺
	 * */
	public List getStoreListByRegistrantAccount(String account,int page,int pageSize){
		if(account==null || account.equals(""))
			return new ArrayList();
		Map p = new HashMap();
		if(page < 1)
			page = 1;
		int begin = (page-1)*pageSize;
		int end = pageSize;
		p.put("begin", begin);
		p.put("end", end);
		User user = new User();
		user.setAccount(account);
		p.put("registrant", user);
		return this.storeDao.selectStoreList(p);
	}
	
	/**
	 * 取得用户的昵称拥有的店铺
	 * */
	public List getStoreListByOwnerId(int userId,int page,int pageSize){
		if(userId<=0)
			return new ArrayList();
		Map p = new HashMap();
		if(page < 1)
			page = 1;
		int begin = (page-1)*pageSize;
		int end = pageSize;
		p.put("begin", begin);
		p.put("end", end);
		User user = new User();
		user.setId(userId);
		p.put("owner", user);
		return this.storeDao.selectStoreList(p);
	}
	
	/**
	 * 取得用户的昵称登记的店铺
	 * */
	public List getStoreListByRegistrantId(int userId,int page,int pageSize){
		if(userId <= 0)
			return new ArrayList();
		Map p = new HashMap();
		if(page < 1)
			page = 1;
		int begin = (page-1)*pageSize;
		int end = pageSize;
		p.put("begin", begin);
		p.put("end", end);
		User user = new User();
		user.setId(userId);
		p.put("registrant", user);
		return this.storeDao.selectStoreList(p);
	}
	
	/**
	 * 根据用户id取得该用户登记的店铺的数量
	 * */
	public int getStroeCountByRegistrantUserId(int userId){
		if(userId <= 0 )
			return 0;
		Map p = new HashMap();
		User user = new User();
		user.setId(userId);
		p.put("registrant", user);
		return this.storeDao.selectStoreCount(p);
	}
	/**
	 * 根据用户昵称取得该用户登记的店铺数量
	 * */
	public int getStroeCountByRegistrantAccount(String account){
		if(account == null || account.equals(""))
			return 0;
		Map p = new HashMap();
		User user = new User();
		user.setAccount(account);
		p.put("registrant", user);
		return this.storeDao.selectStoreCount(p);
	}
	
	
		
	/**
	 * 更新店铺
	 * */
	public int updateStore(Store store){
		if(store.getId()<=0)
			return 0;
		store.setUpdateTime(new Date());
		return this.storeDao.updateStore(store);
	}
	
	/**
	 * 更新店铺的访问次数 点击次数
	 * */
	public int addStoreClickTimes(int storeId, int clickTimes){
		if(storeId <= 0 || clickTimes <= 0)
			return 0;
		Map p = new HashMap();
		p.put("id", storeId);
		p.put("clickTimes", clickTimes);
		return this.storeDao.updateStoreDynamicInfo(p);
	}
	/**
	 * 设置店铺更新时间
	 * */
	public int setStoreUpdateTime(int storeId){
		if(storeId <= 0)
			return 0;
		Map p = new HashMap();
		p.put("id", storeId);
		p.put("updateTime", new Date());
		return this.storeDao.updateStoreDynamicInfo(p);
	}
	
	/**
	 * 认领店铺
	 * @return -1 店铺不存在
	 * @return -2 店铺已经被认领
	 * @return -3 有人正在认领该店
	 * @return -4 认领店的个数达到上限
	 * @return >0 认领成功
	 * */
	public int claimStore(StoreClaim storeClaim){
		if(storeClaim.getUser()==null || storeClaim.getStore() == null)
			return -1;
		if(storeClaim.getUser().getAccount()==null || storeClaim.getStore().getId()<=0){
			return -1;
		}
		if(this.getClaimStoreCountByUserAccount(storeClaim.getUser().getAccount(),1)>=StoreConfig.claimLimit)
			return -4;
		if(!this.checkStoreExistById(storeClaim.getStore().getId()))
			return -1;
		if(this.checkStoreHasBeenClaimed(storeClaim.getStore().getId()))
			return -2;
		if(this.checkStoreIsBeenClaimingByStoreId(storeClaim.getStore().getId()))
			return -3;			
		storeClaim.setClaimTime(new Date());
		return (Integer)this.storeDao.insertStoreClaim(storeClaim);
	}
	
	/**
	 * 根据用户昵称取得用户认领商店的个数
	 * */
	public int getClaimStoreCountByUserAccount(String account,int notStatus){
		Map p = new HashMap();
		User user = new User();
		user.setAccount(account);
		p.put("user", user);
		if(notStatus != -1)
			p.put("notStatus", notStatus);
		return this.storeDao.selectStoreClaimCount(p);
	}
	
	/**
	 * 取得某个用户的店铺认领
	 * */
	public List getStoreClaimByUserAccount(String account){
		if(account==null || account.trim().equals(""))
			return new ArrayList();
		Map p = new HashMap();
		User user = new User();
		user.setAccount(account);
		p.put("user", user);
		return this.storeDao.selectStoreClaimList(p);
	}
	
	/**
	 * 取得认领id取得店铺认领信息
	 * */
	public StoreClaim getStoreClaimById(int claimId){
		if(claimId<=0)
			return null;
		Map p = new HashMap();
		p.put("id", claimId);
		List l = this.storeDao.selectStoreClaimList(p);
		if(l.size()!=1)
			return null;
		return (StoreClaim) l.get(0);
	}
	
	/**
	 * 删除某个用户的哦店铺认领
	 * */
	public int deleteStoreClaimByIdAndUser(User user,int claimId){
		if(user.getAccount()==null || claimId<=0)
			return 0;
		Map p = new HashMap();
		p.put("user", user);
		p.put("id", claimId);
		return this.storeDao.deleteStoreClaim(p);
	}
	
	public List getStoreListOrderBy(String orderBy, int claimed, int closed, int verify, int page, int pageSize){
		if(orderBy==null || orderBy.trim().equals(""))
			return new ArrayList();
		Map p = new HashMap();
		p.put("orderBy", orderBy);
		if(closed != -1)
			p.put("closed", closed);
		if(verify != -1)
			p.put("verify",verify);
		if(page < 1)
			page = 1;
		int begin = (page-1)*pageSize;
		int end = pageSize;
		p.put("begin", begin);
		p.put("end", end);
		if(claimed == 1)
			p.put("claimed", 1);
		return this.storeDao.selectStoreList(p);
	}
	
	/**
	 * 以下方法主要用于管理员。
	 * */
	
	/**
	 * 根据店铺名称，模糊查找，审核状态，登记人，认领人，登记开始日期，登记结束日期，认领开始日期，认领结束日期取得店铺列表
	 * */
	public List getStoreList(String storeName,String nameLike,User registrant,User owner,Date registerBeginTime,Date registerEndTime,Date claimBeginTime, Date claimEndTime,String verify,int page, int pageSize,String orderBy){
		Map p = new HashMap();
		p.put("name", storeName);
		if(nameLike!=null && nameLike.equals("like")){
			p.put("nameLike", "like");
		}
		p.put("registrant", registrant);
		p.put("owner", owner);
		p.put("registerBeginTime", registerBeginTime);
		p.put("registerEndTime", registerEndTime);
		p.put("claimBeginTime", claimBeginTime);
		p.put("claimEndTime", claimEndTime);
		p.put("verify", verify);
		if(page < 1)
			page = 1;
		int begin = (page-1)*pageSize;
		int end = pageSize;
		p.put("begin", begin);
		p.put("end", end);
		p.put("orderBy", orderBy);
		return this.storeDao.selectStoreList(p);
	}
	
	/**
	 * 删除店铺
	 * */
	public int deleteStore(int storeId){
		if(storeId <= 0)
			return 0;
		Map p = new HashMap();
		p.put("id", storeId);
		return this.storeDao.deleteStore(p);
	}
	
	/**
	 * 根据店铺名称，模糊查找，审核状态，登记人，认领人，登记开始日期，登记结束日期，认领开始日期，认领结束日期取得店铺数量
	 * */
	public int getStoreCount(String storeName,String nameLike,User registrant,User owner,Date registerBeginTime,Date registerEndTime,Date claimBeginTime, Date claimEndTime,String verify){
		Map p = new HashMap();
		p.put("name", storeName);
		if(nameLike!=null && nameLike.equals("like")){
			p.put("nameLike", "like");
		}
		p.put("registrant", registrant);
		p.put("owner", owner);
		p.put("registerBeginTime", registerBeginTime);
		p.put("registerEndTime", registerEndTime);
		p.put("claimBeginTime", claimBeginTime);
		p.put("claimEndTime", claimEndTime);
		p.put("verify", verify);
		return this.storeDao.selectStoreCount(p);
	}
	
	/**
	 * 审核店铺
	 * */
	public int verifyStore(int storeId,String verify){
		if(!verify.matches("^[012]$"))
			return 0;
		Map p = new HashMap();
		p.put("id", storeId);
		p.put("verify", verify);
		p.put("updateTime", new Date());
		return this.storeDao.updateStore(p);
	}
	
	/**
	 * 根据店铺名称， 模糊查找， 审核状态， 认领人， 认领开始日期，认领结束日期， 第几页 ， 页数 ， 排序方式 取得店铺认领记录
	 * */
	public List getStoreClaimList(Store store,String storeNameLike, User user, Date claimBeginTime, Date claimEndTime,String claimResult,int page, int pageSize, String orderBy){
		
		Map p = new HashMap();
		p.put("store", store);
		if(storeNameLike!=null && storeNameLike.equals("like")){
			p.put("storeNameLike", "like");
		}
		p.put("user", user);
		p.put("claimBeginTime", claimBeginTime);
		p.put("claimEndTime", claimEndTime);
		p.put("claimResult", claimResult);
		if(page < 1)
			page = 1;
		int begin = (page-1)*pageSize;
		int end = pageSize;
		p.put("begin", begin);
		p.put("end", end);
		p.put("orderBy", orderBy);
		return this.storeDao.selectStoreClaimList(p);
	}
	
	/**
	 * 根据店铺名称， 模糊查找， 审核状态， 认领人， 认领开始日期，认领结束日期， 第几页 ， 页数 ， 排序方式 取得店铺认领记录个数
	 * */
	public int getStoreClaimCount(Store store,String storeNameLike, User user, Date claimBeginTime, Date claimEndTime,String claimResult){
		
		Map p = new HashMap();
		p.put("store", store);
		if(storeNameLike!=null && storeNameLike.equals("like")){
			p.put("storeNameLike", "like");
		}
		p.put("user", user);
		p.put("claimBeginTime", claimBeginTime);
		p.put("claimEndTime", claimEndTime);
		p.put("claimResult", claimResult);
		return this.storeDao.selectStoreClaimCount(p);
	}
	
	/**
	 * 设置store的拥有者
	 * */
	public int setStoreOwner(User owner,int storeId){
		if(storeId <= 0 || owner.getId() <= 0 || owner.getAccount()==null || owner.getAccount().trim().equals(""))
			return 0;
		Map p = new HashMap();
		p.put("owner", owner);
		p.put("id", storeId);
		return this.storeDao.updateStore(p);
	}
	
	/**
	 * 检查store是否属于某个用户
	 * */
	public boolean checkStoreIfBelongsOwner(Store store, User owner){
		if(store.getId() <= 0 || owner.getId() <= 0)
			return false;
		Map p = new HashMap();
		p.put("id", store.getId());
		p.put("owner", owner);
		int i = this.storeDao.selectStoreCount(p);
		if(i == 0)
			return false;
		return true;
	}
	
	/**
	 * 审核店铺认领
	 * */
	public int verifyStoreClaim(StoreClaim storeClaim){
		if(storeClaim == null || storeClaim.getId()<=0)
			return 0;
		StoreClaim sc = this.getStoreClaimById(storeClaim.getId());
		if(sc == null)
			return 0;
		if(storeClaim.getClaimResult().equals("1")){
			Map p = new HashMap();
			p.put("owner", sc.getUser());
			p.put("id", sc.getStore().getId());
			p.put("claimTime", new Date());
			this.storeDao.updateStore(p);
		}
		return this.storeDao.updateStoreClaim(storeClaim);
	}
	
	/**
	 * 根据用户id取得该用户认领成功的店铺数量
	 * */
	public int getStoreNumByOwnerId(int userId){
		if(userId <=0)
			return 0;
		Map p = new HashMap();
		User user = new User();
		user.setId(userId);
		p.put("owner", user);
		return this.storeDao.selectStoreCount(p);
	}
	
	/**
	 * 根据store 的 domain 来获取store的信息
	 * */

	public void setStoreDao(StoreDao storeDao) {
		this.storeDao = storeDao;
	}
	
}
