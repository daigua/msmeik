package com.msmeik.store;

import java.util.Date;

import com.msmeik.member.User;

public class Store {
	private int id;
	private String subdomain;
	private String name;
	private String address;
	private String businessTime;
	private double mapLongitude;//地理位置的经度
	private double mapLatitude;//地理位置的维度
	private String diningType;
	private String closed;
	private String parking;
	private String box;
	private String payCard;
	private String takeaway;
	private String tags;
	private String website;
	private String customDomain;
	private String busLine;
	private String intersection;
	private String telephone;
	private String introduction;
	private String foodType;//食物类型。菜系
	private Date registerTime;//登记时间
	private User registrant;//登记者
	private Date claimTime;//认领时间
	private User owner;//拥有者
	private String verify;//是否审核通过 0 正在审核，1通过,2未通过
	private Date updateTime;

	
	private int perPay;//人均消费
	private int clickTimes;
	
	public int getId() {
		return id;
	}
	
	public int getPerPay() {
		return perPay;
	}

	public void setPerPay(int perPay) {
		this.perPay = perPay;
	}

	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getBusinessTime() {
		return businessTime;
	}
	public void setBusinessTime(String businessTime) {
		this.businessTime = businessTime;
	}
	
	public String getParking() {
		return parking;
	}
	public void setParking(String parking) {
		this.parking = parking;
	}
	public String getBox() {
		return box;
	}
	public void setBox(String box) {
		this.box = box;
	}
	public String getPayCard() {
		return payCard;
	}
	public void setPayCard(String payCard) {
		this.payCard = payCard;
	}
	public String getTakeaway() {
		return takeaway;
	}
	public void setTakeaway(String takeaway) {
		this.takeaway = takeaway;
	}
	public void setClosed(String closed) {
		this.closed = closed;
	}
	public void setVerify(String verify) {
		this.verify = verify;
	}
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getBusLine() {
		return busLine;
	}
	public void setBusLine(String busLine) {
		this.busLine = busLine;
	}
	
	public String getIntersection() {
		return intersection;
	}
	public void setIntersection(String intersection) {
		this.intersection = intersection;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getIntroduction() {
		return introduction;
	}
	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	
	public String getFoodType() {
		return foodType;
	}
	public void setFoodType(String foodType) {
		this.foodType = foodType;
	}
	public Date getRegisterTime() {
		return registerTime;
	}
	public void setRegisterTime(Date registerTime) {
		this.registerTime = registerTime;
	}
	
	public User getRegistrant() {
		return registrant;
	}
	public void setRegistrant(User registrant) {
		this.registrant = registrant;
	}
	public Date getClaimTime() {
		return claimTime;
	}
	public void setClaimTime(Date claimTime) {
		this.claimTime = claimTime;
	}
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}
	
	public String getVerify() {
		return verify;
	}


	public double getMapLongitude() {
		return mapLongitude;
	}

	public void setMapLongitude(double mapLongitude) {
		this.mapLongitude = mapLongitude;
	}

	public double getMapLatitude() {
		return mapLatitude;
	}

	public void setMapLatitude(double mapLatitude) {
		this.mapLatitude = mapLatitude;
	}

	public String getDiningType() {
		return diningType;
	}
	public void setDiningType(String diningType) {
		this.diningType = diningType;
	}
	public String getClosed() {
		return closed;
	}
	

	public String getSubdomain() {
		if(subdomain != null && subdomain.trim().equals(""))
			return null;
		return subdomain;
	}

	public void setSubdomain(String subdomain) {
		this.subdomain = subdomain;
	}

	public String getCustomDomain() {
		if(customDomain != null && customDomain.trim().equals(""))
			return null;
		return customDomain;
	}

	public void setCustomDomain(String customDomain) {
		this.customDomain = customDomain;
	}

	public int getClickTimes() {
		return clickTimes;
	}

	public void setClickTimes(int clickTimes) {
		this.clickTimes = clickTimes;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}	
	
}
