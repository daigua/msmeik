package com.msmeik.message;

import java.util.Date;

public class Inbox {
	private int id;
	private MessageUser sender;
	private MessageUser receiver;
	private Date sendTime;
	private Message message;
	private int readStatus;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public MessageUser getSender() {
		return sender;
	}
	public void setSender(MessageUser sender) {
		this.sender = sender;
	}
	public MessageUser getReceiver() {
		return receiver;
	}
	public void setReceiver(MessageUser receiver) {
		this.receiver = receiver;
	}
	public Date getSendTime() {
		return sendTime;
	}
	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}
	public Message getMessage() {
		return message;
	}
	public void setMessage(Message message) {
		this.message = message;
	}
	public int getReadStatus() {
		return readStatus;
	}
	public void setReadStatus(int readStatus) {
		this.readStatus = readStatus;
	}
	
	
}
