package com.msmeik.message;

import java.util.Date;

public class Outbox {
	private int id;
	private Message message;
	private MessageUser receiver;
	private MessageUser sender;
	private Date saveTime;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Message getMessage() {
		return message;
	}
	public void setMessage(Message message) {
		this.message = message;
	}
	
	public MessageUser getReceiver() {
		return receiver;
	}
	public void setReceiver(MessageUser receiver) {
		this.receiver = receiver;
	}
	public MessageUser getSender() {
		return sender;
	}
	public void setSender(MessageUser sender) {
		this.sender = sender;
	}
	public Date getSaveTime() {
		return saveTime;
	}
	public void setSaveTime(Date saveTime) {
		this.saveTime = saveTime;
	}
	
	
}
