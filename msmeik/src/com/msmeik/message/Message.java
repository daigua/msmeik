package com.msmeik.message;

import java.util.Date;

public class Message {
	private int id;
	private MessageUser sender;
	private MessageUser receiver;
	private Date sendTime;
	private String title;
	private String content;
	private int readStatus;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public MessageUser getSender() {
		return sender;
	}
	public void setSender(MessageUser sender) {
		this.sender = sender;
	}
	public MessageUser getReceiver() {
		return receiver;
	}
	public void setReceiver(MessageUser receiver) {
		this.receiver = receiver;
	}
	public Date getSendTime() {
		return sendTime;
	}
	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}
	public int getReadStatus() {
		return readStatus;
	}
	public void setReadStatus(int readStatus) {
		this.readStatus = readStatus;
	}
	
	
}
