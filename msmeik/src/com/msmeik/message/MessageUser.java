package com.msmeik.message;

public class MessageUser {
	private int userType;//使用者的类型
	private int userId;
	private String userName;
	
	public static int ADMIN_USER_TYPE = 1;
	public static int MEMBER_USER_TYPE = 2;
	public static int STORE_USER_TYPE = 3;
	
	public int getUserType() {
		return userType;
	}
	public void setUserType(int userType) {
		this.userType = userType;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}
