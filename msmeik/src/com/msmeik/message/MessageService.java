package com.msmeik.message;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessageService {
	private MessageDao messageDao;
	
	/**
	 * send message and return the message id
	 * */
	public int sendMessage(Message message){
		if(message.getSender().getUserId() <= 0)
			return 0;
		return this.messageDao.insertMessage(message);
	}
	/**
	 * Set message read status
	 * @param 0 unread status 1 read status
	 * */
	private int setMessageReadStatus(int messageId,int status){
		if(messageId <= 0)
			return 0;
		Map p = new HashMap();
		p.put("id", messageId);
		p.put("readStatus", status);
		return this.messageDao.updateMessage(p);
	}
	/**
	 * delete message
	 * */
	public int deleteMessage(MessageUser receiver,int messageId){
		Map p = new HashMap();
		p.put("receiver", receiver);
		p.put("id", messageId);
		return this.messageDao.deleteMessage(p);
	}
	/**
	 * only for admin delete
	 * */
	public int deleteMessage(int messageId){
		if(messageId <= 0)
			return 0;
		Map p = new HashMap();
		p.put("id", messageId);
		return this.messageDao.deleteMessage(p);
	}
	/**
	 * get message list by receiver
	 * */
	public List getMessageListByReceiver(MessageUser receiver,int readStatus,int page, int pageSize){
		Map p = new HashMap();
		if(readStatus != -1){
			p.put("readStatus", readStatus);
		}
		p.put("receiver", receiver);
		if(page < 1)
			page = 1;
		int begin = (page-1)*pageSize;
		int end = pageSize;
		p.put("begin", begin);
		p.put("end", end);
		return this.messageDao.selectMessageList(p);
	}
	/**
	 * get message info by receiver and message id
	 * */
	public Message getMessage(MessageUser receiver,int messageId){
		if(messageId <= 0)
			return null;
		Map p = new HashMap();
		p.put("receiver", receiver);
		p.put("id", messageId);
		List l = this.messageDao.selectMessageList(p);
		if(l.size()!=1)
			return null;
		return (Message)l.get(0);
	}
	/**
	 * get message number by receiver and read status
	 * */
	public int getMessageCountByReceiverAndReadStatus(MessageUser receiver,int readStatus){
		Map p = new HashMap();
		if(readStatus != -1){
			p.put("readStatus", readStatus);
		}
		p.put("receiver", receiver);
		return this.messageDao.selectMessageCount(p);
	}
	public void setMessageDao(MessageDao messageDao) {
		this.messageDao = messageDao;
	}
	
	/**
	 * read the message and set message readstatus
	 * */
	public Message readMessage(MessageUser receiver,int messageId,int readStatus){
		Message msg = this.getMessage(receiver, messageId);
		if(msg != null && receiver.getUserId()!=0)
			this.setMessageReadStatus(messageId, readStatus);
		return msg;
	}
	
	
}
