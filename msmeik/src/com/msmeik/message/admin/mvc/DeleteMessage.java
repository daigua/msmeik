package com.msmeik.message.admin.mvc;

import com.msmeik.message.MessageService;

public class DeleteMessage {
	private MessageService messageService;
	private int messageId;
	private int ok;
	
	public String execute(){
		this.ok = messageService.deleteMessage(messageId);
		return "success";
	}

	public int getOk() {
		return ok;
	}

	public void setMessageService(MessageService messageService) {
		this.messageService = messageService;
	}

	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}
	
	
}
