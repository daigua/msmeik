package com.msmeik.message.admin.mvc;

import java.util.Date;

import com.msmeik.authentication.AuthenticationService;
import com.msmeik.member.User;
import com.msmeik.message.Message;
import com.msmeik.message.MessageService;
import com.opensymphony.xwork2.ActionSupport;

public class SendMessage extends ActionSupport{
	
	private MessageService messageService;
	private String receivers;//收件人 多个直接用空格隔开，都是收件人id
	private Message message;
	private int ok;
	
	public String execute(){
		
		User user = AuthenticationService.getLoginUser();
		message.getSender().setUserType(1);
		message.getSender().setUserId(user.getId());
		message.setSendTime(new Date());
		for(String receiver:receivers.split(" ")){
			this.message.getReceiver().setUserId(Integer.parseInt(receiver));
			this.messageService.sendMessage(message);
		}
		this.ok = 9;
		return "input";
	}

	
	
	public Message getMessage() {
		return message;
	}



	public void setMessage(Message message) {
		this.message = message;
	}



	public int getOk() {
		return ok;
	}



	public void setMessageService(MessageService messageService) {
		this.messageService = messageService;
	}



	public String getReceivers() {
		return receivers;
	}

	public void setReceivers(String receivers) {
		receivers = receivers.trim().replace("\\s{2,}", " ");
		this.receivers = receivers;
	}
	
	
}
