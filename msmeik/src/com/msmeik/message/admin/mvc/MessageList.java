package com.msmeik.message.admin.mvc;

import java.util.List;

import com.msmeik.message.MessageService;
import com.msmeik.message.MessageUser;

public class MessageList {
	private MessageService messageService;
	private int receiverType;
	private int receiverId;
	private List messageList;
	private int page = 1;
	private int pageSize=20;
	
	public String execute(){
		MessageUser receiver = new MessageUser();
		receiver.setUserId(receiverId);
		receiver.setUserType(receiverType);
		this.messageList = this.messageService.getMessageListByReceiver(receiver, -1, page, pageSize);
		return "success";
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public List getMessageList() {
		return messageList;
	}

	public void setMessageService(MessageService messageService) {
		this.messageService = messageService;
	}

	public void setReceiverType(int receiverType) {
		this.receiverType = receiverType;
	}

	public void setReceiverId(int receiverId) {
		this.receiverId = receiverId;
	}

	public int getReceiverType() {
		return receiverType;
	}

	public int getReceiverId() {
		return receiverId;
	}
	
}
