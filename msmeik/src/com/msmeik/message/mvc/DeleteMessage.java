package com.msmeik.message.mvc;

import com.msmeik.message.MessageService;
import com.msmeik.message.MessageUser;

public class DeleteMessage {
	private MessageService messageService;
	private int messageId;
	private int receiverType;//接受消息者的类型 2是会员 3是店铺
	private int receiverId;//接受消息者的id
	private int ok;
	
	public String execute(){
		MessageUser receiver = new MessageUser();
		receiver.setUserId(receiverId);
		receiver.setUserType(receiverType);
		this.ok = this.messageService.deleteMessage(receiver, messageId);
		
		return "success";
	}

	public int getOk() {
		return ok;
	}

	public void setMessageService(MessageService messageService) {
		this.messageService = messageService;
	}

	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}

	public void setReceiverType(int receiverType) {
		this.receiverType = receiverType;
	}

	public void setReceiverId(int receiverId) {
		this.receiverId = receiverId;
	}
	
	
}
