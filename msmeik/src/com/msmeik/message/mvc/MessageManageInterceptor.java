package com.msmeik.message.mvc;

import org.apache.struts2.ServletActionContext;

import com.msmeik.authentication.AuthenticationRegister;
import com.msmeik.authentication.AuthenticationService;
import com.msmeik.member.User;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.MethodFilterInterceptor;

public class MessageManageInterceptor extends MethodFilterInterceptor{

	private int receiverType;
	@Override
	protected String doIntercept(ActionInvocation action) throws Exception {
		// TODO Auto-generated method stub
		int receiverId = Integer.parseInt(ServletActionContext.getRequest().getParameter("receiverId").toString());
		if(receiverId == 0){//此是管理广播信息，只有管理员有此权限
			User user = AuthenticationService.getLoginUser();
			if(!AuthenticationRegister.checkAuth("admin", user));
				return null;
		}
		int checkId = 0;
		if(receiverType == 3){
			checkId = Integer.parseInt(ServletActionContext.getRequest().getParameter("storeId").toString());			
		}else
		if(receiverType==2){
			checkId = AuthenticationService.getLoginUser().getId();
		}else{
			return null;
		}
		if(checkId != receiverId)
			return null;
		return action.invoke();
	}
	public void setReceiverType(int receiverType) {
		this.receiverType = receiverType;
	}
	
}
