package com.msmeik.message.mvc;

import com.msmeik.message.MessageService;
import com.msmeik.message.MessageUser;

public class CountMessage {
	private MessageService messageService;
	private int totalMessageNum;
	private int unreadMessageNum;
	private int broadcastMessageNum;
	private int userType;
	private int userId;
	
	public String execute(){	
		MessageUser receiver = new MessageUser();
		receiver.setUserId(userId);
		receiver.setUserType(userType);
		this.totalMessageNum = this.messageService.getMessageCountByReceiverAndReadStatus(receiver, -1);
		this.unreadMessageNum = this.messageService.getMessageCountByReceiverAndReadStatus(receiver, 0);
		receiver.setUserId(0);
 		this.broadcastMessageNum = this.messageService.getMessageCountByReceiverAndReadStatus(receiver, 0);
		return "success";
	}

	public int getTotalMessageNum() {
		return totalMessageNum;
	}

	public int getUnreadMessageNum() {
		return unreadMessageNum;
	}

	public int getBroadcastMessageNum() {
		return broadcastMessageNum;
	}

	public void setMessageService(MessageService messageService) {
		this.messageService = messageService;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	
	
	
}
