package com.msmeik.message.mvc;

import com.msmeik.message.Message;
import com.msmeik.message.MessageService;
import com.msmeik.message.MessageUser;

public class ReadMessage {
	private MessageService messageService;
	private int receiverType;
	private int receiverId;
	private Message message;
	private int messageId;
	
	public String execute(){
		MessageUser receiver = new MessageUser();
		receiver.setUserId(receiverId);
		receiver.setUserType(receiverType);
		this.message = this.messageService.readMessage(receiver, messageId, 1);
		if(message == null)
			message = new Message();
		return "success";
	}

	public Message getMessage() {
		return message;
	}

	public void setMessageService(MessageService messageService) {
		this.messageService = messageService;
	}

	public void setReceiverType(int receiverType) {
		this.receiverType = receiverType;
	}

	public void setReceiverId(int receiverId) {
		this.receiverId = receiverId;
	}

	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}
	
	
}
