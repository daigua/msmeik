package com.msmeik.message.mvc;

import java.util.List;

import com.msmeik.message.MessageService;
import com.msmeik.message.MessageUser;

public class MessageList {
	private MessageService messageService;
	private List messageList;
	private int type;//message Type 1是广播消息 0是非广播消息
	private int receiverType;//接受消息者的类型 2是会员 3是店铺
	private int receiverId;//接受消息者的id
	private int status=-1;
	private int page=1;
	private int pageSize=20;
	private int totalCount;
	
	public String execute(){
		MessageUser receiver = new MessageUser();
		receiver.setUserType(receiverType);
		if(type!=1){			
			receiver.setUserId(receiverId);
		}
		this.messageList = this.messageService.getMessageListByReceiver(receiver, status, page, pageSize);
		this.totalCount = this.messageService.getMessageCountByReceiverAndReadStatus(receiver, status);
		return "success";
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getReceiverType() {
		return receiverType;
	}

	public void setReceiverType(int receiverType) {
		this.receiverType = receiverType;
	}

	public int getReceiverId() {
		return receiverId;
	}

	public void setReceiverId(int receiverId) {
		this.receiverId = receiverId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public List getMessageList() {
		return messageList;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setMessageService(MessageService messageService) {
		this.messageService = messageService;
	}
	
	
}
