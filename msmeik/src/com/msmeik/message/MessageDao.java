package com.msmeik.message;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class MessageDao extends SqlMapClientDaoSupport{
	
	/**
	 * 添加消息，并返回添加后的id
	 * */
	public int insertMessage(Object message){
		return (Integer) this.getSqlMapClientTemplate().insert("insertMessage", message);
	}
	/**
	 * 修改message
	 * */
	public int updateMessage(Object message){
		return this.getSqlMapClientTemplate().update("updateMessage", message);
	}
	/**
	 * 删除
	 * */
	public int deleteMessage(Object p){
		return this.getSqlMapClientTemplate().delete("deleteMessage", p);
	}
	
	/**
	 * select message number
	 * */
	public int selectMessageCount(Object p){
		return (Integer)this.getSqlMapClientTemplate().queryForObject("selectMessageCount", p);
	}
	/**
	 * select message list
	 * */
	public List selectMessageList(Object p){
		return this.getSqlMapClientTemplate().queryForList("selectMessage", p);
	}
}
