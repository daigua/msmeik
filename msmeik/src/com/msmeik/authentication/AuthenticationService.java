package com.msmeik.authentication;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.springframework.core.io.Resource;

import com.msmeik.member.User;
import com.msmeik.ucenter.UserCenterService;
import com.msmeik.ucenter.client.Client;
import com.orientor.security.DesEncrypt;
import com.orientor.website.tools.MD5EncodeTool;

public class AuthenticationService {
	
	private static String AUTH_KEY = "com.msmeik.luling.auth_key.201004151312";
	
	private static int LOGIN_SAVE_MAX_AGE = 3153600;
	private static String COOKIE_DOMAIN = "msmeik.com";
	private static String COOKIE_PATH = "/";
	private static DesEncrypt des = new DesEncrypt(AUTH_KEY);
	/**
	 * 用户保存单点登录的用户登录信息
	 * key为登录token，value为LoginInfo
	 * */	
	private static Map<String,LoginInfo> singlepointerLoginRegister	= new HashMap();//登录寄存器，	
	
	private Resource configFile;
	
	public void init(){		
	    try {
	    	InputStream in = new FileInputStream(configFile.getFile());
		    Properties properties = new Properties();
	    	System.out.println("正则初始化Authentication配置数据");
			properties.load(in);
			AUTH_KEY = properties.getProperty("AUTH_KEY",AUTH_KEY);
			LOGIN_SAVE_MAX_AGE = Integer.parseInt(properties.getProperty("LOGIN_SAVE_MAX_AGE",Integer.toString(LOGIN_SAVE_MAX_AGE)));
			COOKIE_DOMAIN = properties.getProperty("COOKIE_DOMAIN",COOKIE_DOMAIN);
			COOKIE_PATH = properties.getProperty("COOKIE_PATH",COOKIE_PATH);
			AuthenticationRegister.initRegister(properties.getProperty("LOGIN_AUTH_EXPRESSION","user:all,admin:1|2"));
			System.out.println("初始化Authentication配置数据成功");
		} catch (Exception e) {
			System.out.println("没有找到Authentication配置文件");
		}
	}
	
	/**
	 * 从ucenter 登录 ，并返回center返回的信息
	 * */
	public static Map loginFromUcenter(User user){
		Map p = UserCenterService.userLoginFromUcenter(user);
		if((Integer)p.get("status")==1){			
			p.put("token", setUserLoginStatus((User)p.get("user")));
		}
		return p;
	}
	/**
	 * 从ucenter登出，并返回ucenter返回的信息
	 * */
	public static String logoutFromUcenter(){
		System.out.println("登出美食美客，开始同步登出");
		String result = UserCenterService.userLogoutFromUcenter();
		logout();
		System.out.println("取得同步登出代码"+result);
		return result;
	}
	
	/**
	 * 用户登出,
	 * 步骤，
	 * 1、根据token，从loginRegister中删除相应的登录信息
	 * 2、注销cookie，
	 * 3、注销session，
	 * */
	public static void logout(){
		System.out.println("开始登出，并从cookie获取token");
		removeLoginStatusFromCookie();	
		clearLoginStatusFromSession();
	}
	
	/**
	 * 清楚用户的登录session
	 * */
	private static void clearLoginStatusFromSession(){
		Map session = ServletActionContext.getContext().getSession();
		session.clear();
	}
	
	/**
	 * 判断用户是否登录，如果登录返回登录的token信息,否则返回null
	 * 步骤
	 * 1、判断session是否有用户的登录信息，如果有直接返回null。
	 * 2、如果没有，则从cookie中读取，如果有返回user，并且将登录信息写入到session中，如果没有返回null；
	 * 3、如果用的session存在，而loginRegister中不存在，则删除session,并返回null；
	 * */
	public static String checkLogin(){
		//System.out.println("当前登录的用户信息"+loginRegister);
		LoginInfo loginInfo = getLoginInfoFromCookie();
		Map session = ServletActionContext.getContext().getSession();
		if(loginInfo == null){
			clearLoginStatusFromSession();
			return null;
		}		
		String token = loginInfo.getToken();
		if(session == null || session.get("token") == null){			
			//如果cookie 登录成功，则将登录信息写入session
			User user = new User();
			user.setAccount(loginInfo.getAccount());
			user.setId(loginInfo.getUserId());
			saveLoginStatusToSession(token, user);
		}		
		return token;
	}
	
	/**
	 * 取得登录的用户
	 * 此方法从session中读取User， 不能用此判断用户是否登录，只是用此取出登录的user
	 * */
	public static User getLoginUser(){
		Map session = ServletActionContext.getContext().getSession();
		User user = new User();
		user.setAccount(session.get("account").toString());
		user.setId((Integer)session.get("userId"));
		return user;
	}
	
	/**
	 * 设置user的登录信息,并返回生成的token
	 * 登录之前check下当前是否有用户登录，如果登录，则删除此用户的登录信息。
	 * 步骤：
	 * 1、生成登录token，并以token为key LoginInfo为value的方式存入loginResiter中，
	 * 2、写入登陆信息到cookie。
	 * 3、写入登陆信息到session。
	 * */
	public static String setUserLoginStatus(User user){
		String token = getToken(user);		
		saveLoginStatusToCookie(token,user.getAccount(),user.getId());
		saveLoginStatusToSession(token,user);
		return token;
	}
	
	
	/**
	 * 根据用户的信息和AUTH_KEY生成MD5加密的token
	 * */
	private static String getToken(User user){
		String key = user.getAccount()+AUTH_KEY+user.getId()+System.currentTimeMillis();
		return MD5EncodeTool.encode(key);
	}
	
	
	/**
	 * 保存用户信息到session，
	 * 保存userId， Account，和 登录token，
	 * 验证的时候，先判断token是否存在与loginRegister中。
	 * */
	private static void saveLoginStatusToSession(String token,User user){
		System.out.println("写入登陆信息到session");
		Map session = ServletActionContext.getContext().getSession();
		session.put("token", token);
		session.put("userId", user.getId());
		session.put("account", user.getAccount());
		System.out.println("写入登陆信息到session成功");
		
	}
	
	/**
	 * 将登陆信息写入到cookie，只写入登陆的token
	 * */
	private static void saveLoginStatusToCookie(String token,String account,int userId){
		System.out.println("写入登陆信息到cookie");
		HttpServletResponse response = ServletActionContext.getResponse();
		response.addHeader("P3P", "CP=\"CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR\"");
		try {
			Cookie tokenCookie = new Cookie("msmeik_auth",getCookieAuthStr(token,account,userId));			
			tokenCookie.setMaxAge(LOGIN_SAVE_MAX_AGE);
			tokenCookie.setPath(COOKIE_PATH);
			tokenCookie.setDomain(COOKIE_DOMAIN);
			
			response.addCookie(tokenCookie);
			System.out.println("写入登陆信息到cookie成功");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("写入登陆信息到cookie失败");
		}
		//写入cookie加密验证信息
	}
	
	/**
	 * 生成cookie加密字符串
	 * */
	private static String getCookieAuthStr(String token,String account,int userId) throws Exception{
		String tokenStr = token+","+account+","+userId;
		return des.encrypt(tokenStr);
	}
	/**
	 * 从cookie中删除用户的登录token
	 * */
	private static void removeLoginStatusFromCookie(){
		System.out.println("开始删除登录cookie");
		HttpServletResponse response = ServletActionContext.getResponse();
		response.addHeader("P3P", "CP=\"CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR\"");
		Cookie tokenCookie = new Cookie("msmeik_auth","");			
		tokenCookie.setMaxAge(0);
		tokenCookie.setPath(COOKIE_PATH);
		tokenCookie.setDomain(COOKIE_DOMAIN); 
		response.addCookie(tokenCookie);
		System.out.println("删除登录cookie成功");
	}	
	
	/**
	 * 从cookie中读取用户的登录信息
	 * */
	private static LoginInfo getLoginInfoFromCookie(){
		HttpServletRequest request = ServletActionContext.getRequest();
		Cookie[] cookies = request.getCookies();
		if(cookies == null){
			return null;
		}
		String token = null;
		try {
			for(Cookie cookie:cookies){
				if(cookie.getName().equals("msmeik_auth")){
					token = cookie.getValue();
				}
			}
			if(token==null)
				return null;
			String tokenStr = des.decrypt(token);
			LoginInfo loginInfo = new LoginInfo();
			String[] args = tokenStr.split(",");
			loginInfo.setToken(args[0]);
			loginInfo.setAccount(args[1]);
			loginInfo.setUserId(Integer.parseInt(args[2]));
			return loginInfo;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 单点登录判断,如果登录，则返回用户的登录token,并将登录信息保存到单点登录寄存器中
	 * */
	public static String singlePointCheckLogin(){
		String token = AuthenticationService.checkLogin();
		if(token != null){
			LoginInfo loginInfo = getLoginInfoFromCookie();
			singlepointerLoginRegister.put(token, loginInfo);
		}
		return token;
	}
	
	
	/**
	 * 单点登录
	 *如果本站已经登录，则返回。
	 * */
	public static LoginInfo loginWithSinglePoint(String token,String domain){
		LoginInfo loginInfo = singlepointerLoginRegister.get(token);
		singlepointerLoginRegister.remove(token);
		if(loginInfo != null){
			User user = new User();
			user.setId(loginInfo.getUserId());
			user.setAccount(loginInfo.getAccount());
			saveLoginStatusToSinglePointCookie(domain,token,user);
		}
		return loginInfo;
	}
	/**
	 * 保存单点登录信息到单点cookie
	 * */
	public static void saveLoginStatusToSinglePointCookie(String domain,String token,User user){
		System.out.println("写入登陆信息到cookie");
		HttpServletResponse response = ServletActionContext.getResponse();
		response.addHeader("P3P", "CP=\"CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR\"");
		try {
			Cookie tokenCookie = new Cookie("msmeik_auth",getCookieAuthStr(token,user.getAccount(),user.getId()));			
			tokenCookie.setMaxAge(-1);
			tokenCookie.setPath("/");
			tokenCookie.setDomain(domain);			
			response.addCookie(tokenCookie);
			System.out.println("写入登陆信息到cookie成功");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("写入登陆信息到cookie失败");
		}
	}

	public void setConfigFile(Resource configFile) {
		this.configFile = configFile;
	}
	
	
}
