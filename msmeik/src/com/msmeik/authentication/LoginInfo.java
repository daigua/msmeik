package com.msmeik.authentication;

import java.util.Date;

/**
 * 用户的登录信息，将保存到登录的寄存器当中
 * */
public class LoginInfo {
	private int userId;
	private String account;
	private Date loginTime;
	private String token;
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public Date getLoginTime() {
		return loginTime;
	}
	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	
	public String toString(){
		return "account="+account+" userId="+userId+" token="+token;
	}
}
