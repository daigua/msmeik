package com.msmeik.authentication.mvc;

import com.msmeik.authentication.AuthenticationService;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.MethodFilterInterceptor;

public class GlobalCheckLoginInterceptor extends MethodFilterInterceptor{

	@Override
	protected String doIntercept(ActionInvocation action) throws Exception {
		// TODO Auto-generated method stub
		AuthenticationService.checkLogin();
		return action.invoke();
	}
	
}
