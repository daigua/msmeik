package com.msmeik.authentication.mvc;

import java.util.Map;

import com.msmeik.authentication.AuthenticationService;
import com.msmeik.jcaptcha.JcaptchaUtil;
import com.msmeik.member.User;
import com.opensymphony.xwork2.ActionSupport;

public class Login extends ActionSupport{
	private User user;
	private String safeCode;
	private String captchaId;
	
	private int ok;
	private String ucsynlogin;
	
	private String token;//��¼���ɵ�token
	
	public String execute(){
		Map p = AuthenticationService.loginFromUcenter(user);
		if((Integer)p.get("status") == 1){
			this.ok = 9;
			this.ucsynlogin = (String)p.get("ucsynlogin");
			this.token = p.get("token").toString();
			return "success";
		}else{
			this.addFieldError("user", this.getText("user.notExist"));
		}
		return "input";
	}
	
	public void validate(){
		if(this.hasErrors())
			return;
		if(!JcaptchaUtil.validate(this.captchaId,this.safeCode)){
			this.addFieldError("safeCode", this.getText("safeCode.inputError"));
			return;
		}
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getSafeCode() {
		return safeCode;
	}

	public void setSafeCode(String safeCode) {
		this.safeCode = safeCode;
	}

	public int getOk() {
		return ok;
	}

	public String getUcsynlogin() {
		return ucsynlogin;
	}

	public void setCaptchaId(String captchaId) {
		this.captchaId = captchaId;
	}

	public String getToken() {
		return token;
	}
	
	
}
