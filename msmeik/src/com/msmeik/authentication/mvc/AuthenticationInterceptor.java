package com.msmeik.authentication.mvc;

import com.msmeik.authentication.AuthenticationRegister;
import com.msmeik.authentication.AuthenticationService;
import com.msmeik.member.User;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.MethodFilterInterceptor;

public class AuthenticationInterceptor extends MethodFilterInterceptor{

	private String authName;
	private String loginResult;
	
	/**
	 * 首先要判断是否登录，如果没有的登录则转到登录页面
	 * 如果登录了，判断。用户是否拥有此操作权限，如果有，则继续操作。如果没有，返回用户null；
	 * */
	@Override
	protected String doIntercept(ActionInvocation action) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("正在登录权限验证");
		String token = AuthenticationService.checkLogin();
		if(token == null){
			System.out.println("登录权限验证失败，返回登录页面");
			return loginResult;
		}
		System.out.println("登录权限验证成功");
		User user = AuthenticationService.getLoginUser();
		System.out.println("正在进行操作权限验证 userId"+user.getId()+"是否属于权限组"+authName);
		if(!AuthenticationRegister.checkAuth(authName, user)){//您没有此权限进行操作
			System.out.println("操作权限验证失败");
			return null;
		}
		System.out.println("验证成功");
		return action.invoke();
	}
	public void setAuthName(String authName) {
		this.authName = authName;
	}
	public void setLoginResult(String loginResult) {
		this.loginResult = loginResult;
	}

	
}
