package com.msmeik.authentication.mvc.singlepoint;

import com.msmeik.authentication.AuthenticationService;

public class CheckLogin {
	
	private String token;
	public String execute(){		
		token = AuthenticationService.singlePointCheckLogin();		
		System.out.println("单点登录，读取到用户的登录token为"+token);
		return "success";
	}
	public String getToken() {
		return token;
	}	
}
