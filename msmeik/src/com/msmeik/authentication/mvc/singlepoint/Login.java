package com.msmeik.authentication.mvc.singlepoint;

import java.util.HashMap;
import java.util.Map;

import com.msmeik.authentication.AuthenticationService;
import com.msmeik.authentication.LoginInfo;

public class Login {
	
	private String token;
	private String domain;
	private LoginInfo loginInfo;
	
	public String execute(){
		System.out.println("开始单点登录");
		loginInfo = AuthenticationService.loginWithSinglePoint(token,domain);
		if(loginInfo != null)
			System.out.println("单点登录成功");
		return "success";
	}



	public void setToken(String token) {
		this.token = token;
	}



	public String getDomain() {
		return domain;
	}



	public void setDomain(String domain) {
		this.domain = domain;
	}



	public LoginInfo getLoginInfo() {
		return loginInfo;
	}



}
