package com.msmeik.authentication.mvc;

import com.msmeik.authentication.AuthenticationService;


public class Logout {

private String ucsynlogout;
	
	public String execute(){
		this.ucsynlogout = AuthenticationService.logoutFromUcenter();
		return "success";
	}

	public String getUcsynlogout() {
		return ucsynlogout;
	}
}
