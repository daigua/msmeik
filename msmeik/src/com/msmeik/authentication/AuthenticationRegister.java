package com.msmeik.authentication;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.msmeik.member.User;


/**
 * 权限寄存器
 * */
public class AuthenticationRegister {
	
	private static Map<String,List<String>> register = new HashMap();
	
	
	/**
	 * 根据auth名称 和用户id判断用户是否拥有此权限
	 * */
	public static boolean checkAuth(String name,User user){
		if(name == null || user.getId() <= 0)
			return false;
		if(!register.containsKey(name)){
			System.out.println("不存在这样的权限");
			return false;
		}
		if(register.get(name) == null){//如果权限list为空的话，则说明所有的登录用户都拥有该权限
			
			return true;
		}
		if(register.get(name).contains(user.getId()+"")){
			System.out.println(user.getId()+"属于"+name+"权限组");
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * 根据auth名称和用户id来判断用户是否拥有此权限
	 * */
	public static boolean checkAuth(String name, int userId){
		User user = new User();
		user.setId(userId);
		return checkAuth(name,user);
	}
	
	
	/**
	 * 初始化register
	 * exp 格式 如下
	 * login:all,admin:1|2|3
	 * */
	public static void initRegister(String exp){
		exp = replace(exp);
		String[] authExps = exp.split(",");
		for(String authExp:authExps){
			String[] auth = authExp.split(":");
			if(auth[1].equals("all")){
				register.put(auth[0], null);
			}else{
				List l = new ArrayList();
				for(String uid:auth[1].split("\\|")){
					l.add(uid);
				}
				register.put(auth[0], l);
			}
		}
		System.out.println("权限寄存器"+register);
	}
	
	private static String replace(String str){
		return str.trim().replaceAll("\\s{1,}", "");
	}
}
