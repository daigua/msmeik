// JavaScript Document

var LoginPanel = {};

//�û���½Panel
LoginPanel = {};
LoginPanel.init = function(){
	if(this.isInit){
		return;
	}
	this.isInit = true;
	this.loginPanel = new HtmlUi.IframePanel({width:300,height:215});
}
LoginPanel.show = function(){
	HtmlUi.OverlayTip.close();
	this.init();
	this.loginPanel.show("/member/loginPanel.html?"+(new Date()));
}


LoginPanel.checkLogin = function(data,callback){
	if(data.loginRequired){
		this.show();
		this.loginCallback = callback;
		return false;
	}
	return true;
}
LoginPanel.close = function(){
	this.loginPanel.close();
}
LoginPanel.callback = function(){
	if($.isFunction(this.loginCallback)){
		this.loginCallback();
	}
	this.close();
}
