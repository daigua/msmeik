// JavaScript Document
$(function(){
	initLoginFieldEvent();	
})
function initLoginFieldEvent(){
	var fieldSelector = "input[name^='user'],.regedit_form input[name='safeCode']";	
	$(fieldSelector).focus(function(){
								$(this).select();							
							});
	$(fieldSelector).blur(function(){
								validateMemberField($(this).attr("name"));								
							});
}
function validateMemberField(field){
	Member.validate(field,$("*[name='"+field+"']").val(),showMemberValidateResult);
	
}
function showMemberValidateResult(field,ok,result){
	if(!ok){
		$(".login_result_show").html(result).show();
	}else{
		$(".login_result_show").hide();
	}
}

function login(){
	var fields = ["user.account","user.password","safeCode"];
	var user = {};
	$.each(fields,function(i,n){
			user[n]=$("input[name='"+n+"']").val();				   
		});
	Member.login(user,loginUserCallback,showMemberValidateResult,function(){
																		$(".login_result_show").html("正在登陆，请稍后……").show();		  
																	});
}
function loginUserCallback(data){
	if(data.ok==9){
		if(data.ucsynlogin)
			window.parent.$("head").append(data.ucsynlogin);
		//这里如果是从子站登录的。则要登录到主站，使用单点登录到主站
		var pageUrl = window.location.href;
		if(pageUrl.indexOf("ctospace.com") == -1){
			//如果不是从ctospace.com中登录的话，则登录到ctospace中，方式是，动态生成一个iframe 并添加到parent中
			var iframe = "<iframe style='display:none' src='"+siteDomain+"//member/singlepoint/login.html?loginType=iframe&token="+data.token+"'></iframe>";
			window.parent.$("body").append(iframe);
		}
		parent.LoginPanel.callback();
	}else{
		if(data.safeCode!=9){
			showMemberValidateResult("safeCode",false,"验证码输入不正确");							
		}
		if(data.user != 9){
			showMemberValidateResult("user.account",false,"账号不存在或密码不正确");	
		}
		$("*[name='safeCode']").val("");
		refreshJcaptcha('#loginJcaptcha','login');
	}
}
