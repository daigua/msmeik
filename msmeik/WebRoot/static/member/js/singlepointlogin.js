// JavaScript Document

var SinglePointLog = {};
SinglePointLog.checkLoginUrl = "http://ms.ctospace.com/member/singlepoint/checklogin?jsoncallback=?";
SinglePointLog.loginUrl = "/member/singlepoint/login";
SinglePointLog.checkLogin = function(){
	var url = window.location.href;
	if(!loginStatus && url.indexOf("ctospace.com")==-1)
		$.getJSON(this.checkLoginUrl,this.checkLoginCallback);
}
SinglePointLog.checkLoginCallback = function(data){
	if(data.token == ""){
		return;
	}
	SinglePointLog.login(data.token);
}
SinglePointLog.login = function(token){
	var param = {};
	param.token = token;
	var url = window.location.href;	
	param.domain = url.split("/")[2];	
	$.get(this.loginUrl,param,function(data){
							$("#loginAccount").html(data.loginInfo.account);
							$(".login_panel").show();
							$(".logout_panel").hide();	   
						},"json");
}
$(function(){
SinglePointLog.checkLogin();		   
})