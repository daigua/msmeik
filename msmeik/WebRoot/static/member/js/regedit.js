// JavaScript Document
$(function(){
	initFieldEvent();	
})
function initFieldEvent(){
	var fieldSelector = ".regedit_form input[name^='user'],.regedit_form input[name='password1'],.regedit_form input[name='safeCode']";
	$(fieldSelector).each(function(){
							$(this).attr("initTip",$(this).parent().find(".field_tip").html());	
						});
	$(fieldSelector).focus(function(){
								$(this).parent().parent().addClass("row_selected");	
								if($.trim($(this).attr("initTip"))!="")
									$(this).parent().find(".field_tip").removeClass("field_tip_right").removeClass("field_tip_error").html($(this).attr("initTip"));
								$(this).select();
							});
	$(fieldSelector).blur(function(){
								$(this).parent().parent().removeClass("row_selected");	
								validateMemberField($(this).attr("name"));
								if($(this).attr("name")=="user.account"){
									Member.checkUserExist("account",$(this).val(),function(data){
										if(data.checkResult!=0){
												showMemberValidateResult("user.account",false,"帐号已经存在");
											}
									});
								}
								if($(this).attr("name")=="user.account"){
									Member.checkUserExist("account",$(this).val(),function(data){
										if(data.checkResult!=0){
											showMemberValidateResult("user.account",false,"昵称已经存在");
										}
									});
								}
							});
}
function validateMemberField(field){
	Member.validate(field,$("*[name='"+field+"']").val(),showMemberValidateResult);
	
}
function showMemberValidateResult(field,ok,result){
	if(!ok){
		$("input[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_right").addClass("field_tip_error").html(result);
	}else{
		$("input[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_error").addClass("field_tip_right").html("&nbsp;");
	}
}

function regeditUser(){
	var fields = ["user.account","user.password","password1","user.account","user.email","safeCode"];
	var user = {};
	$.each(fields,function(i,n){
			user[n]=$("input[name='"+n+"']").val();				   
		});
	Member.regedit(user,regeditUserCallback,showMemberValidateResult,function(){
																		HtmlUi.OverlayTip.show("正在注册，请稍后……");		  
																	});
}
function regeditUserCallback(data){
	if(data.ok==9){
		HtmlUi.OverlayTip.show("注册成功，<a href='/member/login.html'>点击登录</a>");
	}else{
		HtmlUi.OverlayTip.close();
		if(data.safeCode!=9){
			showMemberValidateResult("safeCode",false,"验证码输入不正确");							
		}	
		$("*[name='safeCode']").val("");
		refreshJcaptcha('#regeditJcaptcha','regedit');
	}
}