// JavaScript Document
$(function(){
	Member.getUser({},showUserInfo);
})
function showUserInfo(data){
	var user = data.user;
	//alert(user.account);
	$("*[name='user.account']").html(user.account);
	if(user.name != null){
		$("*[name='user.name']").val(user.name);
	}
	$("*[name='user.gender'][value='"+user.gender+"']").attr("checked",true);	
	if(user.telephone != null){
		$("*[name='user.telephone']").val(user.telephone);
	}
	if(user.email != null){
		$("*[name='user.email']").val(user.email);
	}
	if(user.cellphone != null){
		$("*[name='user.cellphone']").val(user.cellphone);
	}
}

//修改用户信息
$(function(){
	initUpdateUserFieldEvent();	
})
function initUpdateUserFieldEvent(){
	var fieldSelector = "input[name='user.name'],input[name='user.gender'],input[name='user.email'],input[name='user.cellphone'],input[name='user.telephone']";
	$(fieldSelector).each(function(){
							$(this).attr("initTip",$(this).parent().find(".field_tip").html());	
						});
	$(fieldSelector).focus(function(){
								$(this).parent().parent().addClass("row_selected");		
								if($.trim($(this).attr("initTip"))!="")
									$(this).parent().find(".field_tip").removeClass("field_tip_right").removeClass("field_tip_error").html($(this).attr("initTip"));
								$(this).select();
							});
	$(fieldSelector).blur(function(){
								$(this).parent().parent().removeClass("row_selected");	
								validateMemberField($(this).attr("name"));								
							});
}
function validateMemberField(field){
	Member.validate(field,$("*[name='"+field+"']").val(),showMemberValidateResult);
	
}
function showMemberValidateResult(field,ok,result){
	if(!ok){
		$("input[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_right").addClass("field_tip_error").html(result);
	}else{
		$("input[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_error").addClass("field_tip_right").html("&nbsp;");
	}
}

function updateUser(callback){
	var fields = ["user.name","user.gender","user.email","user.cellphone","user.telephone"];
	var user = {};
	$.each(fields,function(i,n){
			var val;
			if(n=="user.gender")
				val = $("input[name='"+n+"']:checked").val();
			else
				val = $("input[name='"+n+"']").val();
			user[n]=val;				   
	});
	if(!$.isFunction(callback)){
		callback = updateUserCallback;
	}
	Member.updateUser(user,callback,showMemberValidateResult,function(){
																		HtmlUi.OverlayTip.show("正在更新个人资料，请稍后……");		  
																	});
}
function updateUserCallback(data){
	if(data.ok==9){
		HtmlUi.OverlayTip.show("个人资料更新成功",true);
	}else{
		HtmlUi.OverlayTip.close();
	}
}
