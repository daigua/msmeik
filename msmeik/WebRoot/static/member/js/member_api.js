// JavaScript Document
var Member = {};
Member.validate = function(field,value,callback){
	var value = $.trim(value);
	var result = "正确";
	var flag = true;
	if(field == "user.account"){
		if(value == ""){
			result="帐号不能为空！";
			flag=false;
		}else
		if(value.byteLength()>10){
			result="帐号不能超过10个字符或5个汉字";
			flag = false;
		}else
		if(/[<>]/.test(value)){
			result="帐号中包含非法字符";
			flag=false;
		}
	}
	if(field=="originalPassword"){
		if(value.length <6){
			result = "密码长度不能小于6";
			flag=false;
		}else 
		if(value.length > 16){
			result = "密码太长不能大于16";
			flag=false;
		}
	}
	if(field=="user.password"){
		if(value.length <6){
			result = "密码长度不能小于6";
			flag=false;
		}else 
		if(value.length > 16){
			result = "密码太长不能大于16";
			flag=false;
		}
	}
	if(field=="user.email"){
		if(value == ""){
			result = "Email不能为空";
			flag=false;
		}else if(!/^\w{1,}@\w{1,20}.[A-Za-z]{2,6}$/.test(value)){
			result = "Email输入错误";
			flag=false;
		}
	}
	if(field=="user.name"){
		if(value == ""){
			result = "姓名不能为空";
			flag=false;
		}else if(value.length > 10){
			result = "姓名输入错误";
			flag=false;
		}
	}
	if(field=="user.cellphone"){
		if(value == ""){
			result = "手机不能为空";
			flag=false;
		}
	}
	if(field == "password1"){
		var val = $.trim($("input[name='password1']").val());
		if(val == ""){
			result = "确认密码不能为空";
			flag = false;
		}else 
		if(val != $.trim($("input[name='user.password']").val())){
			result = "两次输入的密码不一致";
			flag = false;
		}
	}
	if(field == "safeCode"){
		var val = $.trim($("input[name='safeCode']").val());
		if(val == ""){
			result = "验证码不能为空";
			flag = false;
		}else
		if(!/^[0-9A-Za-z]{4}$/.test(val)){
			result = "验证码格式输入错误";
			flag = false;
		}
	}
	if($.isFunction(callback)){
		callback(field,flag,result);
	}
	return {ok:flag,result:result};
}
//验证账号或者昵称是否存在
Member.checkUserExist = function(field,value,callback){
	if($.trim(value)=="")
		return;
	var data = {};
	if(field == "account"){
		data["account"] = value;
	}
	if(field=="account"){
		data["account"] = value;
	}
	var url = "/member/checkUserExist";
	$.ajaxSetup({cache:false});
	$.post(url,data,callback,"json");
}

//注册用户
Member.regedit = function(user,callback,validateCallback,beforeRegeditFunction){
	var self = this;
	if(this.regediting == 1){
		return;
	}
	var flag = true;
	$.each(user,function(i,n){
				flag = Member.validate(i,n,validateCallback).ok && flag;			 
			});
	if(!flag)
		return;
	if($.isFunction(beforeRegeditFunction)){
		beforeRegeditFunction();
	}
	this.regediting =1;	
	var url = "/member/regedit";
	$.ajaxSetup({cache:false});
	$.post(url,user,function(data){
				self.regediting = 0;
				callback(data);
			},"json");
}

//用户登陆
Member.login = function(user,callback,validateCallback,beforeLoginFunction){
	var self = this;
	if(self.logining == 1){
		return;
	}
	var flag = true;
	$.each(user,function(i,n){
				flag = Member.validate(i,n,validateCallback).ok && flag;			 
			});
	if(!flag)
		return;
	if($.isFunction(beforeLoginFunction)){
		beforeLoginFunction();
	}
	this.logining =1;
	var url  = "/member/login";
	$.ajaxSetup({cache:false});
	$.post(url,user,function(data){
				self.logining = 0;
				callback(data);
			},"json");
}

//找回密码
Member.findPwd = {};
Member.findPwd.sendMail = function(user,callback,validateCallback,beforeLoginFunction){
	var self = this;
	if(self.findPwding == 1){
		return;
	}
	var flag = true;
	$.each(user,function(i,n){
				flag = Member.validate(i,n,validateCallback).ok && flag;			 
			});
	if(!flag)
		return;
	if($.isFunction(beforeLoginFunction)){
		beforeLoginFunction();
	}
	this.findPwding = 1;
	var url  = "/member/findPwd_sendMail";
	$.ajaxSetup({cache:false});
	$.post(url,user,function(data){
				self.findPwding = 0;
				callback(data);
			},"json");
}

//找回密码修改密码
Member.findPwd.modifyPwd = function(user,callback,validateCallback,beforeDoFunction){
	var self = this;
	if(self.modifyPwding == 1){
		return;
	}
	var flag = true;
	$.each(user,function(i,n){
				flag = Member.validate(i,n,validateCallback).ok && flag;			 
			});
	if(!flag)
		return;
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.modifyPwding = 1;
	var url  = "/member/findPwd_modifyPwd";
	$.ajaxSetup({cache:false});
	$.post(url,user,function(data){
				self.modifyPwding = 0;
				callback(data);
			},"json");
}

//获得用户信息
Member.getUser = function(user,callback,beforeDoFunction){
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	var url = "/member/userInfo";
	$.ajaxSetup({cache:false});
	$.post(url,user,function(data){
				callback(data);
			},"json");
}

//更新用户信息
Member.updateUser = function(user,callback,validateCallback,beforeDoFunction){
	var self = this;
	if(self.updateUsering == 1){
		return;
	}
	var flag = true;
	$.each(user,function(i,n){
				flag = Member.validate(i,n,validateCallback).ok && flag;			 
			});
	if(!flag)
		return;
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.updateUsering = 1;
	var url  = "/member/updateUser";
	$.ajaxSetup({cache:false});
	$.post(url,user,function(data){
				self.updateUsering = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Member.updateUser(user,callback,validateCallback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}

//修改用户头像
Member.uploadUserHead = function(form,callback,validateCallback,beforeDoFunction){
	var self = this;
	if(self.uploadUserHeading == 1){
		return ;
	}	
	var fileVal = $("#userHeadFile").val();
	if($.trim(fileVal)=="")
		return;
	if(!/\.(jpg)|(jpeg)|(bmp)|(gif)|(png)$/i.test(fileVal)){
		validateCallback("您选择的图片格式不正确");
		return;
	}
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.uploadUserHeading=1;
	var iframe = $("<iframe id='uploadUserHeadIframe' name='uploadUserHeadIframe' style='display:none'></iframe>");
	$("body").append(iframe);
	iframe.load(function(){
					var data = eval("("+iframe.contents().find("body").html()+")");
					iframe.remove();
					self.uploadUserHeading = 0;
					if(!LoginPanel.checkLogin(data,function(){
																	Member.uploadUserHead(form,callback,validateCallback,beforeDoFunction);		
																}))
						return;
					callback(data);		 
				});
	document.getElementById(form).target = "uploadUserHeadIframe";
	document.getElementById(form).method = "post";
	document.getElementById(form).enctype  = "multipart/form-data";
	document.getElementById(form).action = "/member/setUserHead.do";
	document.getElementById(form).submit();
}

//修改密码
Member.modifyPwd = function(user,callback,validateCallback,beforeDoFunction){
	var self = this;
	if(self.modifyPwding == 1){
		return;
	}
	var flag = true;
	$.each(user,function(i,n){
				flag = Member.validate(i,n,validateCallback).ok && flag;			 
			});
	if(!flag)
		return;
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.modifyPwding = 1;
	var url  = "/member/modifyPwd";
	$.ajaxSetup({cache:false});
	$.post(url,user,function(data){
				self.modifyPwding = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Member.modifyPwd(user,callback,validateCallback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}