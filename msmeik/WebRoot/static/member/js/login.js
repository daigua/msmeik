// JavaScript Document
$(function(){
	initLoginFieldEvent();	
})
function initLoginFieldEvent(){
	var fieldSelector = ".regedit_form input[name^='user'],.regedit_form input[name='safeCode']";
	$(fieldSelector).each(function(){
							$(this).attr("initTip",$(this).parent().find(".field_tip").html());	
						});
	$(fieldSelector).focus(function(){
								$(this).parent().parent().addClass("row_selected");	
								if($.trim($(this).attr("initTip"))!="")
									$(this).parent().find(".field_tip").removeClass("field_tip_right").removeClass("field_tip_error").html($(this).attr("initTip"));
								$(this).select();
							});
	$(fieldSelector).blur(function(){
								$(this).parent().parent().removeClass("row_selected");	
								validateMemberField($(this).attr("name"));								
							});
}
function validateMemberField(field){
	Member.validate(field,$("*[name='"+field+"']").val(),showMemberValidateResult);
	
}
function showMemberValidateResult(field,ok,result){
	if(!ok){
		$("input[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_right").addClass("field_tip_error").html(result);
	}else{
		$("input[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_error").addClass("field_tip_right").html("&nbsp;");
	}
}

function login(){
	var fields = ["user.account","user.password","safeCode"];
	var user = {};
	$.each(fields,function(i,n){
			user[n]=$("input[name='"+n+"']").val();				   
		});
	Member.login(user,loginUserCallback,showMemberValidateResult,function(){
																		HtmlUi.OverlayTip.show("正在登陆，请稍后……");		  
																	});
}
function loginUserCallback(data){
	if(data.ok==9){
		if(data.ucsynlogin)
			$("head").append(data.ucsynlogin);
		var loginStr = "登陆成功,选择进入<br/><a href='/'>首页</a> <a href='/manage/center/store/'>商户中心</a>  <a href='http://home.ctospace.com'>美客家园</a>";
		if(data.loginToUrl!=""){
			loginStr += "<br /><a href='"+data.loginToUrl+"'>"+data.loginToUrl.replace(/\?.*$/,"")+"</a>";
		}
		HtmlUi.OverlayTip.show(loginStr);		
	}else{
		HtmlUi.OverlayTip.close();
		if(data.safeCode!=9){
			showMemberValidateResult("safeCode",false,"验证码输入不正确");							
		}
		if(data.user != 9){
			showMemberValidateResult("user.account",false,"账号不存在或密码不正确");	
		}
		$("*[name='safeCode']").val("");
		refreshJcaptcha('#loginJcaptcha','login');
	}
}
