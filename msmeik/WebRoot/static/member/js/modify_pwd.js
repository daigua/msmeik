// JavaScript Document
$(function(){
	initFieldEvent();	
})
function initFieldEvent(){
	var fieldSelector = "input[name='user.password'],input[name='password1'],input[name='originalPassword']";
	$(fieldSelector).each(function(){
							$(this).attr("initTip",$(this).parent().find(".field_tip").html());	
						});
	$(fieldSelector).focus(function(){
								$(this).parent().parent().addClass("row_selected");
								if($.trim($(this).attr("initTip"))!="")
									$(this).parent().find(".field_tip").removeClass("field_tip_right").removeClass("field_tip_error").html($(this).attr("initTip"));
								$(this).select();
							});
	$(fieldSelector).blur(function(){
								$(this).parent().parent().removeClass("row_selected");	
								validateMemberField($(this).attr("name"));
							});
}
function validateMemberField(field){
	Member.validate(field,$("*[name='"+field+"']").val(),showMemberValidateResult);
	
}
function showMemberValidateResult(field,ok,result){
	if(!ok){
		$("input[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_right").addClass("field_tip_error").html(result);
	}else{
		$("input[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_error").addClass("field_tip_right").html("&nbsp;");
	}
}

function modifyPwd(){
	var fields = ["user.password","password1","originalPassword"];
	var user = {};
	$.each(fields,function(i,n){
			user[n]=$("input[name='"+n+"']").val();				   
		});
	Member.modifyPwd(user,modifyPwdCallback,showMemberValidateResult,function(){
																		HtmlUi.OverlayTip.show("正在修改，请稍后……");		  
																	});
}
function modifyPwdCallback(data){
	if(data.ok==9){
		HtmlUi.OverlayTip.show("密码修改成功",true);
		$("input[name='user.password'],input[name='password1'],input[name='originalPassword']").val("");
	}else{
		HtmlUi.OverlayTip.close();
		showMemberValidateResult("originalPassword",false,"原密码输入不正确");							
	}
}