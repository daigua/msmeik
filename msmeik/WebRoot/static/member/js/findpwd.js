// JavaScript Document
$(function(){
	initFieldEvent();	
})
function initFieldEvent(){
	var fieldSelector = "input[name^='user'],input[name='safeCode'],input[name='password1']";
	$(fieldSelector).each(function(){
							$(this).attr("initTip",$(this).parent().find(".field_tip").html());	
						});
	$(fieldSelector).focus(function(){
								$(this).parent().parent().addClass("row_selected");	
								if($.trim($(this).attr("initTip"))!="")
									$(this).parent().find(".field_tip").removeClass("field_tip_right").removeClass("field_tip_error").html($(this).attr("initTip"));
								$(this).select();
							});
	$(fieldSelector).blur(function(){
								$(this).parent().parent().removeClass("row_selected");	
								validateMemberField($(this).attr("name"));
							});
}
function validateMemberField(field){
	Member.validate(field,$("*[name='"+field+"']").val(),showMemberValidateResult);
	
}
function showMemberValidateResult(field,ok,result){
	if(!ok){
		$("input[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_right").addClass("field_tip_error").html(result);
	}else{
		$("input[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_error").addClass("field_tip_right").html(" ");
	}
}

function findPwdSendMail(){
	var fields = ["user.account","user.email","safeCode"];
	var user = {};
	$.each(fields,function(i,n){
			user[n]=$("input[name='"+n+"']").val();				   
		});
	Member.findPwd.sendMail(user,findPwdSendMailCallback,showMemberValidateResult,function(){
																		HtmlUi.OverlayTip.show("正在操作，请稍后……");		  
																	});
}
function findPwdSendMailCallback(data){
	if(data.ok==9){
		HtmlUi.OverlayTip.show("电子邮件发送成功，请尽快收取邮件修改密码",true);
	}else{
		HtmlUi.OverlayTip.close();
		if(data.safeCode!=9){
			showMemberValidateResult("safeCode",false,"验证码输入不正确");							
		}
		if(data.user != 9){
			showMemberValidateResult("user.account",false,"账号与邮件不匹配");	
		}
		$("*[name='safeCode']").val("");
		refreshJcaptcha('#jcaptcha','findPwd');
	}
}

function findPwdModifyPwd(){
	var fields = ["user.password","securityCode","safeCode"];
	var user = {};
	$.each(fields,function(i,n){
			user[n]=$("input[name='"+n+"']").val();				   
		});
	Member.findPwd.modifyPwd(user,findPwdModifyPwdCallback,showMemberValidateResult,function(){
																		HtmlUi.OverlayTip.show("正在操作，请稍后……");		  
																	});
}
function findPwdModifyPwdCallback(data){
	if(data.ok==9){
		HtmlUi.OverlayTip.show("密码修改成功，<a href='/member/login.html'>点击登录</a>");
	}else{
		HtmlUi.OverlayTip.close();
		if(data.safeCode!=9){
			showMemberValidateResult("safeCode",false,"验证码输入不正确");							
		}else{
			HtmlUi.OverlayTip.show("密码修改链接过期，请重新<a href='/member/findpwd.html'>找回密码</a>",true);
		}
		$("*[name='safeCode']").val("");
		refreshJcaptcha('#jcaptcha','findPwd');
	}
}
