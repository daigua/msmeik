// JavaScript Document
var Search = {};

Search.SearchCfg = {};

Search.SearchCfg.validate = function(field,value,callback){
	var value = $.trim(value.replaceHtmlTags());
	var result = "正确";
	var flag = true;
	var call = false;
	if(field == "searchCfg.keywords"){
		if(value == ""){
			result="搜索关键字不能为空";
			flag=false;
		}else
		if(value.byteLength()>400){
			result="搜索关键字最多400个字母或200个汉字";
			flag = false;
		}
		call = true;
	}
	if($.isFunction(callback) && call){
		callback(field,flag,result);
	}
	return {ok:flag,result:result};
}

Search.SearchCfg.apply = function(searchCfg,callback,validateCallback,beforeDoFunction){
	var self = this;
	if(this.applying == 1){
		return;
	}
	var flag = true;
	$.each(searchCfg,function(i,n){
				flag = Search.SearchCfg.validate(i,n,validateCallback).ok && flag;			 
			});
	if(!flag)
		return;
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.applying =1;	
	var url = "/manage/store/search/apply";
	$.ajaxSetup({cache:false});
	$.post(url,searchCfg,function(data){
				self.applying = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Search.SearchCfg.apply(searchCfg,callback,validateCallback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}

Search.SearchCfgApplication = {};
Search.SearchCfgApplication.del = function(storeId,callback,beforeDoFunction){
	var self = this;
	if(this.deleting == 1){
		return;
	}
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.deleting = 1;
	var url = "/manage/store/search/apply/delete";
	$.ajaxSetup({cache:false});
	$.post(url,{storeId:storeId},function(data){
				self.deleting = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Search.SearchCfgApplication.del(storeId,callback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}
