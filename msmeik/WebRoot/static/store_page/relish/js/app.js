// JavaScript Document
var RelishAddPanel = {};
RelishAddPanel.init = function(){
	if(this.isInit){
		return;
	}
	this.isInit = true;
	this.panel = new HtmlUi.IframePanel({width:550,height:520});
}
RelishAddPanel.show = function(storeName,storeId){
	this.init();
	this.panel.show("/relish/add.html?storeName="+encodeURI(storeName)+"&storeId="+storeId);
}

RelishAddPanel.close = function(){
	this.panel.close();
}