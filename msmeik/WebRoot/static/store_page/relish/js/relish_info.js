// JavaScript Document
$(function(){
	$(".star_score a").mouseover(function(){
									$(this).nextAll().removeClass("selected");	
									$(this).prevAll().addClass("selected");
									$(this).addClass("selected");
									showScoreTip($(this).parent(),$(this).attr("percent"));
								});	
	$(".star_score a").click(function(){									
									$(this).parent().attr("percent",$(this).attr("percent"));
									scoreRelish(relishId,$(this).attr("percent"));
							})
	$(".star_score").mouseout(function(){
									if($(this).attr("percent")== 0){
										$(this).children().removeClass("selected");
										showScoreTip($(this),0);
										return;
									}
									$(this).find("[percent='"+$(this).attr("percent")+"']").prevAll().addClass("selected");
									$(this).find("[percent='"+$(this).attr("percent")+"']").addClass("selected");
									$(this).find("[percent='"+$(this).attr("percent")+"']").nextAll().removeClass("selected");
									showScoreTip($(this),$(this).attr("percent"));
								});
	
	//图片overlay显示
	$(".photo_list .photo a").zoomimage({centered:true});
	
})

function showScoreTip(id,score){
	var html = "";
	if(score==20)
		html = "差";
	if(score==40)
		html = "中";	
	if(score==60)
		html = "好";
	if(score==80)
		html = "很好";
	if(score==100)
		html = "非常好";
	$($(id).attr("scoreTip")).html(html);
}

function recommendRelish(id,relishId,num){
	var param = {relishId:relishId};
	RelishApi1.recommend(param,function(data){recommendRelishCallback(data,id,relishId,num)});
}
function recommendRelishCallback(data,id,relishId,num){
	if(data.ok == 1){
		$(id).children("span").html("("+(num+1)+")");
		$(id).removeAttr("onclick");
	}else{
		
	}
}
function scoreRelish(relishId,score){
	var param = {relishId:relishId,score:score};
	RelishApi1.score(param,scoreRelishCallback);
}
function scoreRelishCallback(data){
	$(".star_score a").unbind();
	if(data.ok == 1){		
	}else{
		$(".star_score a").removeClass("selected");	
	}
}