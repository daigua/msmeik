// JavaScript Document
// JavaScript Document
$(function(){
	initRelishFieldEvent();	
	getRelishCategory();
})
function initRelishFieldEvent(){
	var fieldSelector = "input[name^='relish'],textarea[name^='relish']";
	$(fieldSelector).each(function(){
							$(this).attr("initTip",$(this).parent().find(".field_tip").html());
						});
	$(fieldSelector).focus(function(){
								$(this).parent().parent().addClass("row_selected");	
								if($.trim($(this).attr("initTip"))!="")
									$(this).parent().find(".field_tip").removeClass("field_tip_right").removeClass("field_tip_error").html($(this).attr("initTip"));
								$(this).select();
								
							});
	$(fieldSelector).blur(function(){
								$(this).parent().parent().removeClass("row_selected");	
								validateRelishField($(this).attr("name"));
							});
}
function validateRelishField(field){
	Relish.validate(field,$("*[name='"+field+"']").val(),showRelishValidateResult);
	
}
function showRelishValidateResult(field,ok,result){
	if(!ok){
		$("*[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_right").addClass("field_tip_error").html(result);
	}else{
		$("*[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_error").addClass("field_tip_right").html("&nbsp;");
	}
}

function addRelish(){
	var fields = $("*[name^='relish']");
	var relish = {};
	$.each(fields,function(i,n){
						   
			relish[$(n).attr("name")]=$(n).val();				   
		});		
	relish.storeId = storeId+"";
	Relish.add(relish,addRelishCallback,showRelishValidateResult,function(){
																		HtmlUi.OverlayTip.show("正在保存，请稍后……");		  
																	});
}
function addRelishCallback(data){
	if(data.ok>0){
		HtmlUi.OverlayTip.show("美味记录成功，请等待审核。<br /> <a href='javascript:' onclick='HtmlUi.OverlayTip.close()'>继续添加</a> <a href='javascript:' onclick='window.parent.RelishAddPanel.close()'>关闭</a>",true);
		resetRelishField();
	}else{
		HtmlUi.OverlayTip.show("添加失败，请刷新页面重试",true);
	}
}
function resetRelishField(){
	var fieldSelector = "input[name^='relish'],textarea[name^='relish']";
	$(fieldSelector).val("");
}

//读取美食分类
function getRelishCategory(){
	Relish.Category.get(storeId,getRelishCategoryCallback);
}
function getRelishCategoryCallback(data){
	var categoryList = data.relishCategoryList;
	$.each(categoryList,function(i,n){
							$("select[name='relish.category.id']").append("<option value='"+n.id+"'>"+n.name+"</option>");		 
						})
}