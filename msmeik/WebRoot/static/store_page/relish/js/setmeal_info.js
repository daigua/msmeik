// JavaScript Document
$(function(){
	$(".star_score a").mouseover(function(){
									$(this).nextAll().removeClass("selected");	
									$(this).prevAll().addClass("selected");
									$(this).addClass("selected");
									showScoreTip($(this).parent(),$(this).attr("percent"));
								});	
	$(".star_score a").click(function(){									
									$(this).parent().attr("percent",$(this).attr("percent"));
									scoreSetMeal(setMealId,$(this).attr("percent"));
							})
	$(".star_score").mouseout(function(){
									if($(this).attr("percent")== 0){
										$(this).children().removeClass("selected");
										showScoreTip($(this),0);
										return;
									}
									$(this).find("[percent='"+$(this).attr("percent")+"']").prevAll().addClass("selected");
									$(this).find("[percent='"+$(this).attr("percent")+"']").addClass("selected");
									$(this).find("[percent='"+$(this).attr("percent")+"']").nextAll().removeClass("selected");
									showScoreTip($(this),$(this).attr("percent"));
								});
	
	//图片overlay显示
	$(".photo_list .photo a").zoomimage({centered:true});
	
})

function showScoreTip(id,score){
	var html = "";
	if(score==20)
		html = "差";
	if(score==40)
		html = "中";	
	if(score==60)
		html = "好";
	if(score==80)
		html = "很好";
	if(score==100)
		html = "非常好";
	$($(id).attr("scoreTip")).html(html);
}

function recommendSetMeal(id,setMealId,num){
	var param = {setMealId:setMealId};
	SetMealApi1.recommend(param,function(data){recommendSetMealCallback(data,id,setMealId,num)});
}
function recommendSetMealCallback(data,id,setMealId,num){
	if(data.ok == 1){
		$(id).children("span").html("("+(num+1)+")");
		$(id).removeAttr("onclick");
	}else{
		
	}
}
function scoreSetMeal(setMealId,score){
	var param = {setMealId:setMealId,score:score};
	SetMealApi1.score(param,scoreSetMealCallback);
}
function scoreSetMealCallback(data){
	$(".star_score a").unbind();
	if(data.ok == 1){		
	}else{
		$(".star_score a").removeClass("selected");	
	}
}