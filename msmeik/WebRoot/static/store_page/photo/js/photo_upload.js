// JavaScript Document
$(function(){
	Photo.Category.get(storeId,initPhotoCategoryCallback);	  
});

function initPhotoCategoryCallback(data){
	var categoryList = data.categoryList;
	$.each(categoryList,function(i,n){
							if(n.status == 1)
								return;
							var str = "";
							if(n.store.id == 0)
								str = "(系统)";
							$("#categoryListSelect").append("<option value='"+n.id+"'>"+n.name+str+"</option>");	
						});
	$("#categoryListSelect").val(categorySelectId);
}

var photoForms = [];
var uploadIndex = 0;
function uploadPhotos(){
	photoForms = [];
	uploadIndex = 0;
	for(var i=1;i<6;i++){
		if($("#uploadPhotoForm"+i+" input[type='file']").val()!="" && !$("#uploadPhotoForm"+i+" input[type='file']").attr("disabled")){
			photoForms.push("uploadPhotoForm"+i);
		}
	}
	if(photoForms.length == 0){
		HtmlUi.OverlayTip.show("您还没有选择要上传的图片",true);
		return;
	}
	uploadPhoto(photoForms[0]);
}

function uploadPhoto(form){
	var param = {storeId:storeId,targetKey:targetKey,targetType:targetType,targetName:targetName,categoryId:$("#categoryListSelect").val()};
	Photo.upload(form,
				 param,
				 function(data){
					uploadPhotoCallback(form,data);	 
				 },
				 function(field,ok,result){
				 	showValidateInfo(form,ok,result); 
				 },
				  function(){
					HtmlUi.OverlayTip.show("正在上传，请稍后");																														 				 });
}
var totalCredit = 0;
function uploadPhotoCallback(form,data){
	if(data.ok > 0){		
		$("#"+form+" .column_1").removeClass("error").html("上传成功");
		$("#"+form+" input[type='file']").attr("disabled","disabled");
		totalCredit += data.credit;
		uploadIndex++;
		if(uploadIndex<photoForms.length){
			uploadPhoto(photoForms[uploadIndex]);
		}else{
			HtmlUi.OverlayTip.show("上传成功<br />并获得"+totalCredit+"积分",true);
			totalCredit = 0;
		}
	}else{
		HtmlUi.OverlayTip.close();
		if(data.photoFile != 9){
			$("#"+form+" .column_1").addClass("error").html(data.photoFile);
		}else{
			$("#"+form+" .column_1").addClass("error").html("图片太大或者格式不正确");
		}
	}
}
function showValidateInfo(form,flag,result){
	if(!flag){
		HtmlUi.OverlayTip.close();
		$("#"+form+" .column_1").addClass("error").html(result);
	}else{
		$("#"+form+" .column_1").removeClass("error").html("等待上传");
	}
}

function resetPhotoForms(){
	$("input[type='file']").attr("disabled",false);
	$("form .column_1").removeClass("error").html("选择图片");
	for(var i=1;i<6;i++){
		document.getElementById("uploadPhotoForm"+i).reset();
	}
	totalCredit = 0;
}