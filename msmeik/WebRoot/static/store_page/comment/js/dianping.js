// JavaScript Document
$(function(){
	$(".star_score a").mouseover(function(){
									$(this).nextAll().removeClass("selected");	
									$(this).prevAll().addClass("selected");
									$(this).addClass("selected");
									showScoreTip($(this).parent(),$(this).attr("percent"));
								});	
	$(".star_score a").click(function(){									
									$(this).parent().attr("percent",$(this).attr("percent"));		  
							})
	$(".star_score").mouseout(function(){
									if($(this).attr("percent")== 0){
										$(this).children().removeClass("selected");
										showScoreTip($(this),0);
										return;
									}
									$(this).find("[percent='"+$(this).attr("percent")+"']").prevAll().addClass("selected");
									$(this).find("[percent='"+$(this).attr("percent")+"']").addClass("selected");
									$(this).find("[percent='"+$(this).attr("percent")+"']").nextAll().removeClass("selected");
									showScoreTip($(this),$(this).attr("percent"));
								});
})

function showScoreTip(id,score){
	var html = "";
	if(score==20)
		html = "(差)";
	if(score==40)
		html = "(中)";	
	if(score==60)
		html = "(好)";
	if(score==80)
		html = "(很好)";
	if(score==100)
		html = "(非常好)";
	$($(id).attr("scoreTip")).html(html);
}

function showCommentValidateResult(field,ok,result){
	if(!ok){
		result = "<span class='highlight'>"+result+"</span>";
		if(field == "comment.evmtScore"){
			$(".score_evmt_tip").html(result);
		}
		if(field == "comment.tasteScore"){
			$(".score_taste_tip").html(result);
		}
		if(field == "comment.serveScore"){
			$(".score_serve_tip").html(result);
		}
		if(field == "comment.content"){
			$(".content_tip").html(result);
		}
		if(field =="comment.perPay"){
			$(".perPay_tip").html(result);
		}
	}
}

function addComment(){
	var fields = $("input[name^='comment'],textarea[name^='comment']");
	var param = {};
	param["comment.tasteScore"]=getScore(".score_taste");
	param["comment.evmtScore"]=getScore(".score_evmt");
	param["comment.serveScore"]=getScore(".score_serve");
	var diningType = "";
	$.each($("input[name='diningType']:checked"),function(i,n){
													diningType += $(n).val()+" ";		  
					});
	param["comment.diningType"]=diningType;
	$.each(fields,function(i,n){	
			param[$(n).attr("name")]=$(n).val();				   
		});		
	Comment.add(param,addCommentCallback,showCommentValidateResult,function(){
																		HtmlUi.OverlayTip.show("正在保存，请稍后……");		  
																	});
}
function addCommentCallback(data){
	if(data.ok>0){
		HtmlUi.OverlayTip.show("点评添加成功<br />并获得"+data.credit+"积分",true);
		callback();
		resetCommentField();
	}else{
		HtmlUi.OverlayTip.show("添加失败，请刷新页面重试",true);
	}
}
function getScore(field){
	return $(field).attr("percent");
}
function resetCommentField(){
	$(".score_taste").attr("percent",0);
	$(".star_score a").removeClass("selected");
	showScoreTip($(".score_taste"),0);
	$(".score_evmt").attr("percent",0);
	$(".score_evmt a").removeClass("selected");
	showScoreTip($(".score_evmt"),0);
	$(".score_serve").attr("percent",0);
	$(".score_serve a").removeClass("selected");
	showScoreTip($(".score_serve"),0);
	$("input[name='diningType']").attr("checked",false);
	$("input[name='comment.perPay']").val("");
	$("input[name='comment.relishRecommend']").val("");
	$("textarea[name^='comment']").val("");
}