// JavaScript Document
$(function(){
	initCommentFieldEvent();	
})
function initCommentFieldEvent(){
	var fieldSelector = "input[name^='comment'],textarea[name^='comment']";
	$(fieldSelector).each(function(){
							$(this).attr("initTip",$(this).parent().find(".field_tip").html());
						});
	$(fieldSelector).focus(function(){
								$(this).parent().parent().addClass("row_selected");	
								if($.trim($(this).attr("initTip"))!="")
									$(this).parent().find(".field_tip").removeClass("field_tip_right").removeClass("field_tip_error").html($(this).attr("initTip"));
								$(this).select();
								
							});
	$(fieldSelector).blur(function(){
								$(this).parent().parent().removeClass("row_selected");	
								validateCommentField($(this).attr("name"));
							});
}
function validateCommentField(field){
	var commentType = $("input[name='comment.target.type']").val();
	Comment.validate(parseInt(commentType),field,$("*[name='"+field+"']").val(),showCommentValidateResult);
	
}
function showCommentValidateResult(field,ok,result){
	if(!ok){
		$("*[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_right").addClass("field_tip_error").html(result);
	}else{
		$("*[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_error").addClass("field_tip_right").html("&nbsp;");
	}
}

function addComment(){
	var fields = $("input[name^='comment'],textarea[name^='comment']");
	var comment = {};
	$.each(fields,function(i,n){	
			comment[$(n).attr("name")]=$(n).val();				   
		});		
	Comment.add(comment["comment.target.type"],comment,addCommentCallback,showCommentValidateResult,function(){
																		HtmlUi.OverlayTip.show("正在保存，请稍后……");		  
																	});
}
function addCommentCallback(data){
	if(data.ok>0){
		HtmlUi.OverlayTip.show("点评添加成功",true);
		callback();
		resetCommentField();
	}else{
		HtmlUi.OverlayTip.show("添加失败，请刷新页面重试",true);
	}
}
function resetCommentField(){
	var fieldSelector = "input[name^='comment'],textarea[name^='comment']";
	$(fieldSelector).val("");
}