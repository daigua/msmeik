// JavaScript Document
String.prototype.byteLength = function(){
	return this.replace(/[^\x00-\xff]/gi,'xx').length;
}
String.prototype.replaceHtmlTags = function(){
	var tags = [["<","&lt;"],[">","&gt;"]];
	var str = this;
	for(var i=0;i<tags.length;i++){
		str = str.replace(eval("/"+tags[i][0]+"/g"),tags[i][1]);
	}
	return str;
}
//垂直居中
function middle(el){
	var e = $(el);
	var parent = e.parent();
	var height = e.height();
	e.css({marginTop:(parent.innerHeight()-height)/2});
}
//更新验证码
function refreshJcaptcha(img,captchaId){
	$(img).attr("src","/jcaptcha.do?captchaId="+captchaId+"&"+new Date());
}

