(function($) {
	$.fn.calendar=function(options){
		var me=$(this);
		options=$.extend({
			yearBegin:1985,
			yearEnd:new Date().getUTCFullYear()+2,
			format:"yyyy-MM-dd",
			show:"",
			hide:"",
			close:"auto",
			initDateRead:"val",
			position:"bottom left",
			offset:{top:5,left:0},
			handle:".calendar_handle",
			callback:function(date){me.val(date);me.blur()},
			draggalbe:true
		}, options || {});
		var date = new Date();
		var curYear=date.getUTCFullYear();
		var curMonth=date.getUTCMonth()+1;
		var curDay=date.getUTCDate();
		
		var template = '<div class="calendar">'
					   +'<div class="calendar_top"></div>'
						+'<div class="date_show"><a href="#" class="show_pre"></a>'
						+'<div class="date_show_on"><a href="javascript:" class="date_year menu_show">2009</a> 年 <a href="javascript:)" class="date_month menu_show" >12</a> 月</div><a href="#" class="show_next"></a></div>'
						+'  <div class="week_list">'
						+'	 <div>日</div>'
						+'	 <div>一</div>'
						+'	 <div>二</div>'
						+'	 <div>三</div>'
						+'	 <div>四</div>'
						+'	 <div>五</div>'
						+'	 <div>六</div>'
						+'  </div>'
						+'<div class="day_list">'
						+'  <a href="#" class="active">5</a>'
						+'</div>'
						+'<div class="button_list"></div>'
						+'<div class="calendar_bottom"></div>'
					 +' <div class="year_list"><a href="#">2000</a></div>'
					 +' <div class="month_list"><a href="#">1</a> <a href="#">2</a> <a href="#">3</a> <a href="#">4</a> <a href="#">5</a> <a href="#">6</a> <a href="#">7</a> <a href="#">8</a> <a href="#">9</a> <a href="#">10</a> <a href="#">11</a> <a href="#">12</a></div>'
					+'</div>';

		
		bindEvent();
		if($(".calendar").html() == null){
			$("body").append(template);
		}
		function initCalendar(){			
			initCurDate();
			initYearList();
			initMonthList();			
		}
		
		function initYearList(){
			var yearId=$(".calendar > .year_list");
			yearId.empty();
			for(var i=options.yearEnd;i>=options.yearBegin;i--){
				var a=$("<a href='javascript:'></a>");
				a.data("year",i);
				a.click(function(){
							$(".calendar").find(".date_year").html($(this).data("year"));	
							curYear = $(this).data("year");
							$(yearId).hide();
							initDayList();
							return false;
						});
				a.html(i);
				yearId.append(a);
			}
			$(".calendar").find(".date_year").html(curYear);
		}
		
		function initMonthList(){
			var monthId=$(".calendar > .month_list");
			monthId.empty();
			for(var i=1;i<=12;i++){
				var a=$("<a href='javascript:'></a>");
				a.href="javascript:";
				a.data("month",i);
				a.bind("click",function(event){
					$(".calendar").find(".date_month").html($(this).data("month")>9?$(this).data("month"):"0"+$(this).data("month"));
					curMonth = $(this).data("month");
					$(monthId).hide();
					initDayList();
					return false;
				});
				a.html(i);
				monthId.append(a);
			}
			$(".calendar").find(".date_month").html(curMonth>9?curMonth:"0"+curMonth);		
		}
		
		function initDayList(){
			var dayId=$(".calendar > .day_list");
			dayId.empty();
			var beginWeek=new Date(curYear,curMonth-1,1).getUTCDay();
			var daynum=getDayNum(curYear,curMonth);
			if(beginWeek%6!=0){
				for(var i=0;i<=beginWeek;i++){
					var a=document.createElement("a");
					a.href="javascript:";
					a.onclick="return false";
					$(a).css("visibility","hidden");
					dayId.append(a);
				}
			}
			for(var i=1;i<=daynum;i++){
				var a=$("<a href='javascript:'></a>");
				a.html(i);
				a.data("day",i);
				if(i==curDay){
					a.addClass("active");
				}
				a.click(function(){click_do($(this).data("day"));return false;});
				dayId.append(a);
			}
		}
		
		function getDayNum(year,month){
			var num=0;
			var leap=false;
			if(year%4==0&&year%100!=0 || year%400==0){
				leap=true;
			}
			if(month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12){
				num = 31;
			}
			if(month == 4 || month == 6 || month == 9 || month == 11 )
				num = 30;
			if(month == 2){
				if(!leap){
					num=28; 
				}else{
					num=29;
				}
			}
			return num;
		}
		
		function initCurDate(){
			var date = me[options.initDateRead]();
			if(date != ""){
				var dateAry = date.split("-");
				curYear = dateAry[0];
				curMonth = dateAry[1];
				curDay = dateAry[2];
				curMonth = curMonth.replace(/^0/,"");
				curDay = curDay.replace(/^0/,"");
			}
		}		
		
		//下一个月
		function nextMonth(){
			curMonth++;
			if(curMonth == 13){
				curYear++;
				curMonth = 1;
			}
			initDayList();
			$(".calendar").find(".date_month").html(curMonth>9?curMonth:"0"+curMonth);	
			$(".calendar").find(".date_year").html(curYear);
		}
		
		//上一个月
		function preMonth(){
			curMonth--;
			if(curMonth == 0){
				curMonth = 12;
				curYear--;
			}
			initDayList();
			$(".calendar").find(".date_month").html(curMonth>9?curMonth:"0"+curMonth);	
			$(".calendar").find(".date_year").html(curYear);
		}
		
		
		//定义日期的回调函数
		function click_do(day){		
			var dateString=options.format.replace("yyyy",curYear);
			dateString=dateString.replace("MM",curMonth<10?"0"+curMonth:curMonth);
			dateString=dateString.replace("dd",day<10?"0"+day:day);
			$(".calendar").hide();
			options.callback(dateString);			
		}
		//显示日历
		function showCalendar(){	
			initCurDate();
			initDayList();
			var offset = getCalendarOffset();
			$(".calendar").css({top:offset.top+"px",left:offset.left+"px"}).show();
			
		}
		function getCalendarOffset(){
			var meOffset = me.offset();
			var top = 0;
			var left = 0;
			if(options.position.indexOf("left") != -1){
				left = meOffset.left;
			}
			if(options.position.indexOf("right") != -1){
				left = meOffset.left + me.width();
			}
			if(options.position.indexOf("top") != -1){
				top = meOffset.top - $(".calendar").height();
			}
			if(options.position.indexOf("bottom") != -1){
				top = meOffset.top + me.height();
			}
			return {top:top+options.offset.top,left:left+options.offset.left};
		}
		//绑定相应的时间
		function bindEvent(){
			$(".calendar").click(function(event){$.cancelBubble(event)});
			$(".calendar").find(".date_month").click(function(){$(".calendar > .month_list").show();return false;});
			$(".calendar").find(".date_year").click(function(){$(".calendar > .year_list").show();return false;});
			$(".calendar").find(".month_list").mouseleave(function(){$(this).hide(200)});
			$(".calendar").find(".year_list").mouseleave(function(){$(this).hide(200)});
			$(".calendar").find(".show_next").click(function(){nextMonth();return false;});
			$(".calendar").find(".show_pre").click(function(){preMonth();return false;});
			//$(".calendar").mouseleave(function(){$(this).hide(200)});
			$(document).click(function(){$(".calendar").hide()});
			me.bind("click",function(event){				
				$.cancelBubble(event);
				$(document).click();
				initCalendar();
				showCalendar();
			});
			$(options.handle).bind("click",function(event){
				$.cancelBubble(event);
				showCalendar();
			});
			if(options.draggalbe)
				$(".calendar").draggable({handle:".calendar > .date_show"});
		}		
				
	}
})(jQuery);
