// JavaScript Document
var HtmlUi = {};


HtmlUi.Panel = function(opt){
	this.options = 	$.extend({
					width:400,
					height:400,
					overlay:{}
			   }, opt || {});
}
HtmlUi.Panel.prototype.template =  '<div class="html_ui html_ui_panel"><div class="panel_bg"></div><div class="panel_main"></div><div class="panel_handle"></div><div class="panel_close"></div></div>';
HtmlUi.Panel.prototype.init = function(){
	if(this.isInit){
		return;
	}
	this.isInit = true;
	this.panel =  $(this.template);
	this.panel.find(".panel_main").css({width:this.options.width,height:this.options.height});
	var self = this;
	this.panel.find(".panel_close").click(function(){
										self.close();			  
									});	
	this.overlay = new HtmlUi.Overlay(this.options.overlay).init();
	$("body").append(this.panel);
	this.initEvent();
}
HtmlUi.Panel.prototype.show = function(floatOpt){
	this.init();
	$.floatCenter(floatOpt || {who:this.panel});
	this.panel.show();
	this.overlay.show();
	return this;
}
HtmlUi.Panel.prototype.setHtml = function(html,reset){
	this.init();
	this.showHtml = html;
	if(reset){
		this.showHtmlParent = html.parent();
	}
	this.panel.find(".panel_main").append(html);
	return this;
}
HtmlUi.Panel.prototype.close = function(){
	if(this.showHtmlParent){
		this.showHtmlParent.append(this.showHtml);
	}
	this.panel.hide();
	this.overlay.hide();
	return this;
}
HtmlUi.Panel.prototype.initEvent = function(){
	this.panel.draggable();
}

//使用iframePanel显示内容
HtmlUi.IframePanel = function(opt){
	this.options = $.extend({
						width:300,
						height:200,
						overlay:{}
					},opt || {});
}
HtmlUi.IframePanel.prototype.template = '<iframe  frameborder="no" border="0" marginwidth="0"marginheight="0" scrolling="no" allowtransparency="yes"></iframe>';
HtmlUi.IframePanel.prototype.init = function(){
	if(this.isInit)
		return;
	this.isInit = true;
	this.panel = new HtmlUi.Panel({width:this.options.width,height:this.options.height,overlay:this.options.overlay});
	this.iframe = $(this.template).css({width:this.options.width,height:this.options.height});
	this.panel.setHtml(this.iframe);
}
HtmlUi.getIframe = function(){return this.iframe;}
HtmlUi.IframePanel.prototype.show = function(src){
	this.init();
	this.panel.show();
	this.iframe.attr("src",src);	
}
HtmlUi.IframePanel.prototype.hide = function(){
	this.panel.hide();
}
HtmlUi.IframePanel.prototype.close = function(){
	this.panel.close();
}

//背景覆盖

HtmlUi.Overlay = function(opt){
	this.options = $.extend({
						bgColor:"#f8f3ce",
						opacity:"0.2",
						zIndex:100
					},opt ||{});	
};
HtmlUi.Overlay.prototype.template = '<div></div>';
HtmlUi.Overlay.prototype.init = function(){
	if(this.isInit)
		return;
	this.isInit = true;
	this.overlay = $(this.template).css({opacity:this.options.opacity,width:$(document).width(),height:$(document).height(),backgroundColor:this.options.bgColor,zIndex:this.options.zIndex,position:"absolute",top:0,left:0}).hide();
	$("body").append(this.overlay);
	return this;
}
HtmlUi.Overlay.prototype.show = function(){
	this.init();
	this.overlay.show();
	return this;
}
HtmlUi.Overlay.prototype.hide = function(){
	this.overlay.hide();
	return this;
}
HtmlUi.Overlay.prototype.destory = function(){
	this.overlay.remove();
	return this;
}

//浮动层提示
HtmlUi.OverlayTip = {};
HtmlUi.OverlayTip.template = '<div class="html_ui overlay_tip"><div class="overlay_tip_body"></div><div class="close"></div></div>';
HtmlUi.OverlayTip.init = function(){
	if(this.isInit)
		return;
	this.isInit = true;
	this.overlay = new HtmlUi.Overlay().init();
	this.tipArea = $(this.template);
	this.tipArea.find(".close").click(function(){HtmlUi.OverlayTip.close();});
	$("body").append(this.tipArea);
}
HtmlUi.OverlayTip.options = {overlayShow:true};
HtmlUi.OverlayTip.show = function(tip,showClose,options){
	this.init();
	this.tipArea.find(".overlay_tip_body").empty().append(tip);
	$.floatCenter({who:this.tipArea});
	options = $.extend(this.options,options);
	if(options.overlayShow)
	    this.overlay.show();
	this.tipArea.show();
	if(!showClose){
		this.tipArea.find(".close").hide();
	}else{
		this.tipArea.find(".close").show();
	}
	return this;
}
HtmlUi.OverlayTip.close = function(){
	if(!this.isInit)
		return;
	this.overlay.hide();
	this.tipArea.hide();
	return this;
}

//html confirm提示
HtmlUi.Confirm = {};
HtmlUi.Confirm.template = '<div class="html_ui html_ui_confirm"><div class="confirm_body"></div><div class="confirm_btn_list clearfix"></div><div class="close"></div></div>';
HtmlUi.Confirm.init = function(){
	if(this.isInit)
		return;
	this.isInit = true;
	this.overlay = new HtmlUi.Overlay().init();
	this.tipArea = $(this.template);
	this.tipArea.find(".close").click(function(){HtmlUi.Confirm.close();});
	$("body").append(this.tipArea);
}
HtmlUi.Confirm.deafultButtons = {"确定":function(){HtmlUi.Confirm.close();},"取消":function(){HtmlUi.Confirm.close();}};
HtmlUi.Confirm.show = function(tip,btns){
	this.init();
	this.setButtons(btns);
	this.tipArea.find(".confirm_body").empty().append(tip);
	$.floatCenter({who:this.tipArea});
	this.overlay.show();
	this.tipArea.show();
	return this;
}
HtmlUi.Confirm.setButtons = function(btns){
	if(!btns){
		btns = this.deafultButtons;
	}
	this.tipArea.find(".confirm_btn_list").empty();
	var self = this;
	$.each(btns,function(i,n){
					self.tipArea.find(".confirm_btn_list").prepend($('<input type="button" class="ui_btn" value="'+i+'" />').click(n).click(function(){HtmlUi.Confirm.close();})); 
				})
}
HtmlUi.Confirm.close = function(){
	this.overlay.hide();
	this.tipArea.hide();
	return this;
}

HtmlUi.AltTip = {};
HtmlUi.AltTip.template = "<div class='html_ui html_ui_alt_tip'><div class='html_ui_alt_tip_bg'></div><div class='html_ui_alt_tip_show'></div></div>";
HtmlUi.AltTip.init = function(){
	if(this.isInit)
		return;
	this.isInit = true;	
	this.tipObj = $(this.template);
	$("body").append(this.tipObj);
}
HtmlUi.AltTip.show = function(tip,options){
	this.init();
	
	if(this.isShown){
		this.position(options.top,options.left);
		return;
	}
	this.isShown = true;
	if(tip.indexOf("$:") == 0){
		tip = $(tip.replace("$:","")).clone().show();
	}

	this.tipObj.children(".html_ui_alt_tip_show").append(tip);
	this.position(options.top,options.left);
	this.tipObj.show();
	return this;
}
HtmlUi.AltTip.position = function(top,left){
	if(left + this.tipObj.outerWidth() > $(window).width()+$(window).scrollLeft()){
		left = $(window).width()+$(window).scrollLeft()-this.tipObj.outerWidth();
	}
	if(top + this.tipObj.outerHeight() > $(window).height()+$(window).scrollTop()){
		top = $(window).height()+$(window).scrollTop()-this.tipObj.outerHeight();
	}
	this.tipObj.css({top:top,left:left});
}
HtmlUi.AltTip.close = function(){
	this.isShown = false;
	if(!this.isInit)
		return;
	this.tipObj.children(".html_ui_alt_tip_show").empty("");
	this.tipObj.hide();
	return this;
}
HtmlUi.AltTip.create = function(){
	$.each($("*[altTip]"),function(i,n){
							if($(n).attr("altTip")!=""){
								$(n).mousemove(function(e){
										HtmlUi.AltTip.show($(this).attr("altTip"),{top:e.pageY+13,left:e.pageX+13});			
								});
								$(n).mouseout(function(e){
										HtmlUi.AltTip.close();			
								});
							}
						});
}
$(function(){
	HtmlUi.AltTip.create();		   
})

