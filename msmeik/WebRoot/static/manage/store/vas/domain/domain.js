// JavaScript Document
// JavaScript Document
$(function(){
	initStoreFieldEvent();	
})
function initStoreFieldEvent(){
	var fieldSelector = "input[name^='subdomain']";
	$(fieldSelector).each(function(){
							$(this).attr("initTip",$(this).parent().find(".field_tip").html());
						});
	$(fieldSelector).focus(function(){
								$(this).parent().parent().addClass("row_selected");	
								if($.trim($(this).attr("initTip"))!="")
									$(this).parent().find(".field_tip").removeClass("field_tip_right").removeClass("field_tip_error").html($(this).attr("initTip"));
								$(this).select();
								
							});
	$(fieldSelector).blur(function(){
								$(this).parent().parent().removeClass("row_selected");	
								validateStoreField($(this).attr("name"));								
							});
}
function validateStoreField(field){
	Domain.validate(field,$("*[name='"+field+"']").val(),showStoreValidateResult);
	
}
function showStoreValidateResult(field,ok,result){
	if(!ok){
		$("*[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_right").addClass("field_tip_error").html(result);
	}else{
		$("*[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_error").addClass("field_tip_right").html("&nbsp;");
	}
}

function setStoreSubdomain(){
	HtmlUi.Confirm.show("您确定使用此域名吗？注册之后不可更改，请谨慎操作！",{
							"确定":function(){
									var param = {};
									param.storeId = storeId;
									param.subdomain = $("input[name='subdomain']").val();
									Domain.setSubdomain(param,setStoreDomainCallback,showStoreValidateResult,function(){
																										HtmlUi.OverlayTip.show("正在保存，请稍后……");		  
																									});
								},
							"取消":function(){}
						});		
}
function setStoreDomainCallback(data){
	if(data.ok > 0){
		HtmlUi.OverlayTip.show("域名设置成功！",true);
	}else
	if(data.ok == -1 || data.ok == -2){
		HtmlUi.OverlayTip.show("域名已经存在！",true);
	}else{
		HtmlUi.OverlayTip.show("设置失败！",true);
	}
}