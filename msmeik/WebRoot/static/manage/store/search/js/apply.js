// JavaScript Document
$(function(){
	initSearchCfgFieldEvent();	
	$(".radio_list label").click(function(){
								$(this).siblings().removeClass("selected");
								$(this).addClass("selected");
							});	
})
function initSearchCfgFieldEvent(){
	var fieldSelector = "*[name^='searchCfg']";
	$(fieldSelector).each(function(){
							$(this).attr("initTip",$(this).parent().find(".field_tip").html());
						});
	$(".table_row").mouseover(function(){
								$(this).addClass("row_selected");				
							});
	$(".table_row").mouseout(function(){
								$(this).removeClass("row_selected");				
							});
	$(fieldSelector).focus(function(){
								//$(this).parent().parent().addClass("row_selected");	
								if($.trim($(this).attr("initTip"))!="")
									$(this).parent().find(".field_tip").removeClass("field_tip_right").removeClass("field_tip_error").html($(this).attr("initTip"));
								$(this).select();
								
							});
	$(fieldSelector).blur(function(){
								//$(this).parent().parent().removeClass("row_selected");	
								validateSearchCfgField($(this).attr("name"));
							});
}
function validateSearchCfgField(field){
	Search.SearchCfg.validate(field,$("*[name='"+field+"']").val(),showSearchCfgValidateResult);
	
}
function showSearchCfgValidateResult(field,ok,result){
	if(!ok){
		$("*[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_right").addClass("field_tip_error").html(result);
	}else{
		$("*[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_error").addClass("field_tip_right").html("&nbsp;");
	}
}

function apply(){
	var searchCfg = {};
	searchCfg["storeId"] = storeId+"";
	searchCfg["searchCfg.keywords"] = $("textarea[name='searchCfg.keywords']").val();
	Search.SearchCfg.apply(searchCfg,applyCallback,showSearchCfgValidateResult,function(){
																		HtmlUi.OverlayTip.show("正在保存，请稍后……");		  
																	});
}
function applyCallback(data){
	if(data.ok>0){
		HtmlUi.OverlayTip.show("保存成功，我们会在2个工作日内对您的关键字进行审核。",true);
	}else{
		HtmlUi.OverlayTip.show("保存失败，请刷新页面重试",true);
	}
}