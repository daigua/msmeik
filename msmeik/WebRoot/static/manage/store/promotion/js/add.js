// JavaScript Document
$(function(){
	initPromotionFieldEvent();		
	$("#promotionBeginTime").calendar();
	$("#promotionEndTime").calendar();
})
function initPromotionFieldEvent(){
	var fieldSelector = "input[name^='promotion'],textarea[name^='promotion']";
	$(fieldSelector).each(function(){
							$(this).attr("initTip",$(this).parent().find(".field_tip").html());
						});
	$(fieldSelector).focus(function(){
								$(this).parent().parent().addClass("row_selected");	
								if($.trim($(this).attr("initTip"))!="")
									$(this).parent().find(".field_tip").removeClass("field_tip_right").removeClass("field_tip_error").html($(this).attr("initTip"));
								$(this).select();
								
							});
	$(fieldSelector).blur(function(){
								$(this).parent().parent().removeClass("row_selected");	
								validatePromotionField($(this).attr("name"));
							});
}
function validatePromotionField(field){
	Promotion.validate(field,$("*[name='"+field+"']").val(),showPromotionValidateResult);
	
}
function showPromotionValidateResult(field,ok,result){
	if(!ok){
		$("*[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_right").addClass("field_tip_error").html(result);
	}else{
		$("*[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_error").addClass("field_tip_right").html("&nbsp;");
	}
}

function addPromotion(){
	var fields = $("*[name^='promotion']");
	var promotion = {};
	$.each(fields,function(i,n){						   
			promotion[$(n).attr("name")]=$(n).val();				   
		});	
	promotion.storeId =  storeId+"";
	Promotion.add(promotion,addPromotionCallback,showPromotionValidateResult,function(){
																		HtmlUi.OverlayTip.show("正在保存，请稍后……");		  
																	});
}
function addPromotionCallback(data){
	if(data.ok>0){
		HtmlUi.OverlayTip.show("优惠添加成功,你可以进行如下操作：<br /> <a href='javascript:' onclick='HtmlUi.OverlayTip.close()'>继续添加</a> <a href='/manage/store/promotion/modify.html?storeId="+storeId+"&promotionId="+data.ok+"'>修改优惠</a> <a href='/manage/store/promotion/modify.html?storeId="+storeId+"&promotionId="+data.ok+"'>设置优惠图片</a>",true);
		resetPromotionField();
	}else{
		HtmlUi.OverlayTip.show("添加失败，请刷新页面重试",true);
	}
}
function resetPromotionField(){
	var fieldSelector = "input[name^='promotion'],textarea[name^='promotion']";
	$(fieldSelector).val("");
}