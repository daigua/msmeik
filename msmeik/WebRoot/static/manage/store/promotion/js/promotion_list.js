// JavaScript Document
//设置美食
function setPromotion(promotionId,p,v){
	HtmlUi.Confirm.show("您确定进行此操作吗？",{
							"确定":function(){
									Promotion.set(promotionId,storeId,p,v,function(data){
										setPromotionCallback(data,promotionId,p,v);		
									},
									function(){
										HtmlUi.OverlayTip.show("正在操作……");		
									});
								},
							"取消":function(){}
						});
	
}
function setPromotionCallback(data,promotionId,p,v){
	if(data.ok > 0){
		HtmlUi.OverlayTip.show("操作成功",true);
		setTimeout(function(){HtmlUi.OverlayTip.close();},300);	
		if(p == "orderTime"){
			$("#promotionRow"+promotionId+" .column_5").html(data.orderTime.replace("T"," "));
		}
	}else{
		HtmlUi.OverlayTip.show("操作失败",true);	
	}
}

// 删除美食
function deletePromotion(promotionId){	
	HtmlUi.Confirm.show("您确定删除吗？",{
							"确定":function(){
									Promotion.del(storeId,promotionId,function(data){
										deletePromotionCallback(data,promotionId);	 
								},function(){
									HtmlUi.OverlayTip.show("正在删除，请稍后……");		  
								});
								},
							"取消":function(){}
						});	
}
function deletePromotionCallback(data,promotionId){
	if(data.ok > 0){
		HtmlUi.OverlayTip.close();	
		$("#promotionRow"+promotionId).addClass("has_deleted");
		$("#promotionRow"+promotionId+" *").unbind("click");
		$("#promotionRow"+promotionId).click(function(){ return false;});
	}else{
		HtmlUi.OverlayTip.show("删除失败",true);
	}
}