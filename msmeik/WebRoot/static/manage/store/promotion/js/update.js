// JavaScript Document
$(function(){
	initPromotionFieldEvent();		
	$("#promotionBeginTime").calendar();
	$("#promotionEndTime").calendar();
})
function initPromotionFieldEvent(){
	var fieldSelector = "input[name^='promotion'],textarea[name^='promotion']";
	$(fieldSelector).each(function(){
							$(this).attr("initTip",$(this).parent().find(".field_tip").html());
						});
	$(fieldSelector).focus(function(){
								$(this).parent().parent().addClass("row_selected");	
								if($.trim($(this).attr("initTip"))!="")
									$(this).parent().find(".field_tip").removeClass("field_tip_right").removeClass("field_tip_error").html($(this).attr("initTip"));
								$(this).select();
								
							});
	$(fieldSelector).blur(function(){
								$(this).parent().parent().removeClass("row_selected");	
								validatePromotionField($(this).attr("name"));
							});
}
function validatePromotionField(field){
	Promotion.validate(field,$("*[name='"+field+"']").val(),showPromotionValidateResult);
	
}
function showPromotionValidateResult(field,ok,result){
	if(!ok){
		$("*[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_right").addClass("field_tip_error").html(result);
	}else{
		$("*[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_error").addClass("field_tip_right").html("&nbsp;");
	}
}

function updatePromotion(){
	var fields = $("*[name^='promotion']");
	var promotion = {};
	$.each(fields,function(i,n){						   
			promotion[$(n).attr("name")]=$(n).val();				   
		});	
	promotion.storeId =  storeId+"";
	Promotion.update(promotion,updatePromotionCallback,showPromotionValidateResult,function(){
																		HtmlUi.OverlayTip.show("正在保存，请稍后……");		  
																	});
}
function updatePromotionCallback(data){
	if(data.ok>0){
		HtmlUi.OverlayTip.show("修改成功",true);
	}else{
		HtmlUi.OverlayTip.show("修改失败，请刷新页面重试",true);
	}
}

//设置美食图片
function uploadPromotionImage(){
	Promotion.uploadImage("setPromotionImgForm",uploadPromotionImageCallback,showPromotionValidateResult,function(){
																		HtmlUi.OverlayTip.show("正在上传图片，请稍后……");		  
																	});
}
function uploadPromotionImageCallback(data){
	if(data.ok == 9){
		HtmlUi.OverlayTip.show("设置成功",true);	
		$(".promotion_image").attr("src","http://file.ctospace.com/promotion/"+storeId+"/"+promotionId+"?"+(new Date()));
	}else{		
		HtmlUi.OverlayTip.show(data.promotionImage,true);
	}
}