$(function(){
	initContentFieldEvent();	
})
function initContentFieldEvent(){
	var fieldSelector = "input[name^='content'],textarea[name^='content']";
	$(fieldSelector).each(function(){
							$(this).attr("initTip",$(this).parent().find(".field_tip").html());
						});
	$(fieldSelector).focus(function(){
								$(this).parent().parent().addClass("row_selected");	
								if($.trim($(this).attr("initTip"))!="")
									$(this).parent().find(".field_tip").removeClass("field_tip_right").removeClass("field_tip_error").html($(this).attr("initTip"));
								$(this).select();
								
							});
	$(fieldSelector).blur(function(){
								$(this).parent().parent().removeClass("row_selected");	
								validateContentField($(this).attr("name"));
							});
}
function validateContentField(field){
	Content.validate(field,$("*[name='"+field+"']").val(),showContentValidateResult);
	
}
function showContentValidateResult(field,ok,result){
	if(!ok){
		$("*[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_right").addClass("field_tip_error").html(result);
	}else{
		$("*[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_error").addClass("field_tip_right").html("&nbsp;");
	}
}

function addContent(){
	$("#content").val(KE.util.getData("content"));
	var fields = $("*[name^='content']");
	var content = {};
	$.each(fields,function(i,n){		
			if($(n).attr("type")=="radio" && !$(n).attr("checked"))
				return;
			content[$(n).attr("name")]=$(n).val();				   
		});		
	content.storeId = storeId+"";
	Content.add(content,addContentCallback,showContentValidateResult,function(){
																		HtmlUi.OverlayTip.show("正在保存，请稍后……");		  
																	});
}
function addContentCallback(data){
	if(data.ok>0){
		HtmlUi.OverlayTip.show("添加,你可以进行如下操作：<br /> <a href='javascript:' onclick='HtmlUi.OverlayTip.close()'>继续添加</a> <a href='/manage/store/content/modify.html?storeId="+storeId+"&contentId="+data.ok+"'>修改</a>",true);
		resetContentField();
	}else{
		HtmlUi.OverlayTip.show("添加失败，请刷新页面重试",true);
	}
}
function resetContentField(){
	var fieldSelector = "input[name^='content'][type!='radio'],textarea[name^='content']";
	$(fieldSelector).val("");
	clearEditor("content");
}
function clearEditor(id) {
  KE.g[id].iframeDoc.open();
  KE.g[id].iframeDoc.write(KE.util.getFullHtml(id));
  KE.g[id].iframeDoc.close();
  KE.g[id].newTextarea.value = '';
}
