// JavaScript Document

//设置美食
function manageContent(contentId,p,v){
	if(p=="orderTime" || p=="delete"){
		var tip = "您确定前置吗?";
		if(p =="delete")
			tip = "您确定删除吗？";
		HtmlUi.Confirm.show(tip,{
							"确定":function(){
									Content.manage(storeId,contentId,p,v,function(data){
																					manageContentCallback(data,contentId,p,v);		 
																				},function(){
																					HtmlUi.OverlayTip.show("正在操作，请稍后……");		  
																				});
								},
							"取消":function(){}
						});	
		return;
	}
	Content.manage(storeId,contentId,p,v,function(data){
						manageContentCallback(data,contentId,p,v);		 
					},function(){
						HtmlUi.OverlayTip.show("正在操作，请稍后……");		  
					});
}
function manageContentCallback(data,contentId,p,v){
	if(data.ok > 0){
		HtmlUi.OverlayTip.show("操作成功",true);
		setTimeout(function(){HtmlUi.OverlayTip.close();},300);	
		if(p == "onTop"){
			var html = "";
			if(v==1)
				html = "<a href='javascript:' class=\"highlight\" onclick=\"manageContent("+contentId+",'onTop',0)\">取消</a>";
			if(v == 0)
				html = "<a href='javascript:' onclick=\"manageContent("+contentId+",'onTop',1)\">置顶</a>";
			$("#content"+contentId+"OnTop").html(html);
		}
		if(p=="orderTime"){
			$("#content"+contentId+"OrderTime").html(data.orderTime.replace("T"," "));
		}
		if(p=="delete"){
			HtmlUi.OverlayTip.close();	
			$("#contentRow"+contentId).addClass("has_deleted");
			$("#contentRow"+contentId+" *").unbind("click");
			$("#contentRow"+contentId).click(function(){ return false;});
		}
	}else{
		HtmlUi.OverlayTip.show("操作失败",true);	
	}
}