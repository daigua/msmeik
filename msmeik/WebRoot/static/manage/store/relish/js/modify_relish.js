// JavaScript Document
// JavaScript Document
$(function(){
	initRelishFieldEvent();	
	getRelishCategory();
})
function initRelishFieldEvent(){
	var fieldSelector = "input[name^='relish'],textarea[name^='relish']";
	$(fieldSelector).each(function(){
							$(this).attr("initTip",$(this).parent().find(".field_tip").html());
						});
	$(fieldSelector).focus(function(){
								$(this).parent().parent().addClass("row_selected");	
								if($.trim($(this).attr("initTip"))!="")
									$(this).parent().find(".field_tip").removeClass("field_tip_right").removeClass("field_tip_error").html($(this).attr("initTip"));
								$(this).select();
								
							});
	$(fieldSelector).blur(function(){
								$(this).parent().parent().removeClass("row_selected");	
								validateRelishField($(this).attr("name"));
							});
}
function validateRelishField(field){
	Relish.validate(field,$("*[name='"+field+"']").val(),showRelishValidateResult);
	
}
function showRelishValidateResult(field,ok,result){
	if(!ok){
		$("*[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_right").addClass("field_tip_error").html(result);
	}else{
		$("*[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_error").addClass("field_tip_right").html("&nbsp;");
	}
}

function modifyRelish(){
	var fields = $("*[name^='relish']");
	var relish = {};
	$.each(fields,function(i,n){
						   
			relish[$(n).attr("name")]=$(n).val();				   
		});		
	relish.storeId = storeId+"";
	Relish.update(relish,addRelishCallback,showRelishValidateResult,function(){
																		HtmlUi.OverlayTip.show("正在保存，请稍后……");		  
																	});
}
function addRelishCallback(data){
	if(data.ok>0){
		HtmlUi.OverlayTip.show("修改成功",true);
	}else{
		HtmlUi.OverlayTip.show("修改失败，请刷新页面重试",true);
	}
}

//读取美食分类
function getRelishCategory(){
	Relish.Category.get(storeId,getRelishCategoryCallback);
}
function getRelishCategoryCallback(data){
	var categoryList = data.relishCategoryList;
	$.each(categoryList,function(i,n){
							$("select[name='relish.category.id']").append("<option value='"+n.id+"'>"+n.name+"</option>");		 
						})
	$("select[name='relish.category.id']").val($("select[name='relish.category.id']").attr("defaultValue"));
}

//设置美食图片
function setPreviewPhoto(){
	Relish.setPreviewPhoto("setPreviewPhotoForm",setPreviewPhotoCallback,showRelishValidateResult,function(){
																		HtmlUi.OverlayTip.show("正在上传图片，请稍后……");		  
																	});
}
function setPreviewPhotoCallback(data){
	if(data.ok == 9){
		HtmlUi.OverlayTip.show("设置成功",true);	
		$(".relish_preview_photo").attr("src","http://file.ctospace.com/relish/"+storeId+"/"+relishId+"?1=1&"+(new Date()));
	}else{
		HtmlUi.OverlayTip.show("上传失败，请检查您上传的图片是否符合要求",true);	
	}
}