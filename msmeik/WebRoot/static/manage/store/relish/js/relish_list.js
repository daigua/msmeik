// JavaScript Document

$(function(){
	getRelishCategory();
	$(".relish_category_show").each(function(){$(this).popmenu({menuSelector:".relish_category_menu"})});
})

//读取美食分类
function getRelishCategory(){
	Relish.Category.get(storeId,getRelishCategoryCallback);
}
function getRelishCategoryCallback(data){
	var categoryList = data.relishCategoryList;
	$.each(categoryList,function(i,n){
							$("#relishCategoryList").append("<option value='"+n.id+"'>"+n.name+"</option>");								
							$(".relish_category_menu").append("<a href='javascript:' categoryId='"+n.id+"'>"+n.name+"</a>");
						});
	$("#relishCategoryList").val($("#relishCategoryList").attr("defaultValue"));
}

//选择分类
function showRelishByCategory(id){
	document.location = "/manage/store/relish/list?storeId="+storeId+"&categoryId="+$(id).val()+"&page="+page+"&pageSize="+pageSize;
}

//设置美食
function setRelish(id,relishId,p,v){
	if(!v)
		v = $(id).attr("value");
	if(p=="orderTime"){
		HtmlUi.Confirm.show("您确定前置吗？",{
							"确定":function(){
									Relish.set(relishId,storeId,p,v,function(data){
										setRelishCallback(data,id,relishId,p,v);		
									},
									function(){
										HtmlUi.OverlayTip.show("正在操作……");		
									});
								},
							"取消":function(){}
						});	
		return;
	}
	Relish.set(relishId,storeId,p,v,function(data){
						setRelishCallback(data,id,relishId,p,v);		
					},
					function(){
						HtmlUi.OverlayTip.show("正在操作……");		
					});
}
function setRelishCallback(data,id,relishId,p,v){
	if(data.ok > 0){
		HtmlUi.OverlayTip.show("操作成功",true);
		setTimeout(function(){HtmlUi.OverlayTip.close();},300);	
		if(p=="category"){
			$("#relish"+relishId+"Category").attr("categoryId",v);
			$("#relish"+relishId+"Category").html($(".relish_category_menu a[categoryId='"+v+"']").html());
			return;
		}		
		if(p=="orderTime"){
			if(p == "orderTime"){
			$("#relishRow"+relishId+" .column_4").html(data.orderTime.replace("T"," "));
		}
			return;
		}
		if($(id).hasClass("mark")){
			$(id).removeClass("mark");
			$(id).addClass("mark_gray");
			$(id).attr("value",1);
			$(id).attr("title",$(id).attr("title").replace("取消","设为"));
			if(p == "status"){
				$(id).html("上线");
			}
		}else{
			$(id).removeClass("mark_gray");
			$(id).addClass("mark");
			$(id).attr("value",2);
			$(id).attr("title",$(id).attr("title").replace("设为","取消"));
			if(p == "status"){
				$(id).html("下线");
			}
		}			
	}else{
		HtmlUi.OverlayTip.show("操作失败",true);	
	}
}

//设置美味分类点击事件
function setChangeCategoryEvent(relishId,id){
	$(".relish_category_menu  a").show();
	$(".relish_category_menu  a[categoryId='"+$(id).attr("categoryId")+"']").hide();
	$(".relish_category_menu  a").unbind("click");
	$(".relish_category_menu  a").click(function(){setRelish(id,relishId,"category",$(this).attr("categoryId"))});
}
	
// 删除美食
function deleteRelish(relishId){	
	HtmlUi.Confirm.show("您确定删除吗？",{
							"确定":function(){
									Relish.del(storeId,relishId,function(data){
										deleteRelishCallback(data,relishId);	 
								},function(){
									HtmlUi.OverlayTip.show("正在删除，请稍后……");		  
								});
								},
							"取消":function(){}
						});	
}
function deleteRelishCallback(data,relishId){
	if(data.ok > 0){
		HtmlUi.OverlayTip.close();	
		$("#relishRow"+relishId).addClass("has_deleted");
		$("#relishRow"+relishId+" *").unbind("click");
		$("#relishRow"+relishId).click(function(){ return false;});
	}else{
		HtmlUi.OverlayTip.show("删除失败",true);
	}
}