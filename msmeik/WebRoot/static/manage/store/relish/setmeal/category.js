// JavaScript Document
$(function(){
			   
})
function addCategory(){
	var fields = ["input[name='category.name']"];
	var category = {};
	$.each(fields,function(i,n){
			category[$(n).attr("name")]=$(n).val();				   
		});
	category.storeId = storeId+"";
	SetMeal.Category.add(category,addCategoryCallback,showCategoryValidateResult,function(){
																										   	HtmlUi.OverlayTip.show("正在添加分类，请稍后……");
																										   });
}

function showCategoryValidateResult(field,ok,result){
	if(!ok){
		HtmlUi.OverlayTip.show(result,true);
	}
}
function addCategoryCallback(data){
	if(data.ok > 0){
		HtmlUi.OverlayTip.close();
		addCategoryToHtml(data.ok);
		categoryIds.push(data.ok);
	}else
	if(data.ok == -1){
		HtmlUi.OverlayTip.show("分类名已经存在",true);
	}else{
		HtmlUi.OverlayTip.show("填写的信息不正确",true);
	}
}
function addCategoryToHtml(id){
	var tmp = '<div class="table_row ">'
              +'<div class="row_column column_0"><span class="category_name">{name}</span><span class="category_editor"><input type="text" class="input_txt" value="{name}"/> <input type="button" value="修改保存" onclick="updateCategory(this,\'{cateId}\')" class="btn_style_2" /></span></div>'
			  +'<div class="row_column column_2" altTip="值越大，越靠前。降序排列。最小值为1"><input type="text" name="orderIndex" value="1" style="width:20px; height:12px; line-height:12px; font-size:12px;"/></div>'
			  +'<div class="row_column column_1 lowlight"><a href="javascript:" onclick="showCategoryEditor(this)">编辑</a> <a href="javascript:" onclick="deleteCategory({cateId},{storeId},this)">删除</a></div>'
              +'</div>';
	tmp = tmp.replace(/{name}/g,$("input[name='category.name']").val());
	tmp = tmp.replace(/{cateId}/g,id);
	tmp = tmp.replace(/{storeId}/g,storeId);
	var html = $(tmp);
	if($(".table_row_list > div").length%2 == 0){
		html.addClass("row_even");
	}
	$("input[name='relishCategory.name']").val("");
	html.insertBefore($(".table_row_list .table_row:last"));
}
function deleteCategory(cateId,storeId,row){
	HtmlUi.Confirm.show("您确定删除此分类吗？删除后此分类下的套餐将被移动到 本店套餐 分类下。",{
							"确定":function(){
									SetMeal.Category.del({categoryId:cateId,storeId:storeId},
															function(data){
																if(data.ok>0){
																	HtmlUi.OverlayTip.close();
																	$(row).parent().parent().remove();
																}
																if(data.ok<=0){
																	HtmlUi.OverlayTip.show("删除失败",true);	
																}
															},
															function(){
																HtmlUi.OverlayTip.show("正在删除请稍后……");	
															}
														)
								},
							"取消":function(){}
						});	
}

function showCategoryEditor(id){
	if($(id).html() == "编辑"){
		$(id).parent().parent().find(".category_name").hide();
		$(id).parent().parent().find(".category_editor input[type='text']").val($(id).parent().parent().find(".category_name").html());
		$(id).parent().parent().find(".category_editor").show();
		$(id).html("取消编辑");
	}else{
		$(id).parent().parent().find(".category_name").show();
		$(id).parent().parent().find(".category_editor").hide();
		$(id).html("编辑");
	}
}

function updateCategory(id,cateId){
	var fields = ["input[name='category.name']"];
	var category = {};
	category["storeId"] = storeId+"";
	category["category.name"] = $(id).parent().find("input[type='text']").val();	
	category["category.id"] = cateId;
	SetMeal.Category.modify(category,
						   function(data){
							   updateCategoryCallback(data,id,category["category.name"])
							},
							showCategoryValidateResult,
							function(){
									HtmlUi.OverlayTip.show("正在修改分类，请稍后……");
							});
}

function updateCategoryCallback(data,id,categoryName){
	if(data.ok > 0){
		HtmlUi.OverlayTip.close();
		$(id).parent().parent().find(".category_name").html(categoryName);
		$(id).parent().parent().next("div").next("div").find("a:first").click();
	}else
	if(data.ok == -1){
		HtmlUi.OverlayTip.show("分类名已经存在",true);
	}else{
		HtmlUi.OverlayTip.show("分类名修改失败，请检查您的输入",true);
	}
}

function order(){
	var orderIndexes = [];
	var flag = true;
	$.each($("input[name='orderIndex']"),function(i,n){
												  if($(n).val() < 1){
												  	flag = false;
													return;
												  }
												  orderIndexes.push($(n).val());		  
										});
	if(!flag){
		HtmlUi.OverlayTip.show("排序值最小为1，请正确输入",true);
		return;
	}
	if(categoryIds.length == 0)
		return;
	HtmlUi.OverlayTip.show("正在保存，请稍后");
	SetMeal.Category.order({categoryIds:categoryIds,orderIndexes:orderIndexes,storeId:storeId},orderCallback);
}

function orderCallback(data){
	if(data.ok > 0){
		HtmlUi.OverlayTip.show("排序已保存",true);
	}else{
		HtmlUi.OverlayTip.show("未知错误",true);
	}
}
