// JavaScript Document

$(function(){
	$(".relish_category_show").each(function(){$(this).popmenu({menuSelector:".relish_category_menu"})});
})


//选择分类
function showSetMealByCategory(id){
	document.location = "/manage/store/relish/setmeal/list?storeId="+storeId+"&categoryId="+$(id).val()+"&page="+page;
}

//设置美食
function setSetMeal(id,setMealId,p,v){
	if(!v)
		v = $(id).attr("value");
	var param = {setMealId:setMealId,storeId:storeId,p:p,v:v};
	if(p=="orderTime"){
		HtmlUi.Confirm.show("您确定前置吗？",{
							"确定":function(){
									SetMeal.set(param,function(data){
										setSetMealCallback(data,id,setMealId,p,v);		
									},
									function(){
										HtmlUi.OverlayTip.show("正在操作……");		
									});
								},
							"取消":function(){}
						});	
		return;
	}
	SetMeal.set(param,function(data){
						setSetMealCallback(data,id,setMealId,p,v);		
					},
					function(){
						HtmlUi.OverlayTip.show("正在操作……");		
					});
}
function setSetMealCallback(data,id,setMealId,p,v){
	if(data.ok > 0){
		HtmlUi.OverlayTip.show("操作成功",true);
		setTimeout(function(){HtmlUi.OverlayTip.close();},300);	
		if(p=="category"){
			$("#relish"+setMealId+"Category").attr("categoryId",v);
			$("#relish"+setMealId+"Category").html($(".relish_category_menu a[categoryId='"+v+"']").html());
			return;
		}		
		if(p=="orderTime"){

			$("#relishRow"+setMealId+" .column_4").html(data.orderTime.replace("T"," "));
			return;
		}
		if($(id).hasClass("mark")){
			$(id).removeClass("mark");
			$(id).addClass("mark_gray");
			$(id).attr("value",1);
			$(id).attr("title",$(id).attr("title").replace("取消","设为"));
			if(p == "status"){
				$(id).html("上线");
			}
		}else{
			$(id).removeClass("mark_gray");
			$(id).addClass("mark");
			$(id).attr("value",2);
			$(id).attr("title",$(id).attr("title").replace("设为","取消"));
			if(p == "status"){
				$(id).html("下线");
			}
		}			
	}else{
		HtmlUi.OverlayTip.show("操作失败",true);	
	}
}

//设置美味分类点击事件
function setChangeCategoryEvent(setMealId,id){
	$(".relish_category_menu  a").show();
	$(".relish_category_menu  a[categoryId='"+$(id).attr("categoryId")+"']").hide();
	$(".relish_category_menu  a").unbind("click");
	$(".relish_category_menu  a").click(function(){setSetMeal(id,setMealId,"category",$(this).attr("categoryId"))});
}
	
// 删除美食
function deleteSetMeal(setMealId){	
	HtmlUi.Confirm.show("您确定删除吗？",{
							"确定":function(){
									SetMeal.del({storeId:storeId,setMealId:setMealId},function(data){
										deleteSetMealCallback(data,setMealId);	 
								},function(){
									HtmlUi.OverlayTip.show("正在删除，请稍后……");		  
								});
								},
							"取消":function(){}
						});	
}
function deleteSetMealCallback(data,setMealId){
	if(data.ok > 0){
		HtmlUi.OverlayTip.close();	
		$("#relishRow"+setMealId).addClass("has_deleted");
		$("#relishRow"+setMealId+" *").unbind("click");
		$("#relishRow"+setMealId).click(function(){ return false;});
	}else{
		HtmlUi.OverlayTip.show("删除失败",true);
	}
}