// JavaScript Document
// JavaScript Document
$(function(){
	initSetMealFieldEvent();
	getRelishCategory();
	$(".setmeal_relish_list").sortable();
})
function initSetMealFieldEvent(){
	var fieldSelector = "input[name^='setMeal'],textarea[name^='setMeal']";
	$(fieldSelector).each(function(){
							$(this).attr("initTip",$(this).parent().find(".field_tip").html());
						});
	$(fieldSelector).focus(function(){
								$(this).parent().parent().addClass("row_selected");	
								if($.trim($(this).attr("initTip"))!="")
									$(this).parent().find(".field_tip").removeClass("field_tip_right").removeClass("field_tip_error").html($(this).attr("initTip"));
								$(this).select();
								
							});
	$(fieldSelector).blur(function(){
								$(this).parent().parent().removeClass("row_selected");	
								validateSetMealField($(this).attr("name"));
							});
}
function validateSetMealField(field){
	SetMeal.validate(field,$("*[name='"+field+"']").val(),showSetMealValidateResult);
	
}
function showSetMealValidateResult(field,ok,result){
	if(!ok){
		$("*[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_right").addClass("field_tip_error").html(result);
	}else{
		$("*[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_error").addClass("field_tip_right").html("&nbsp;");
	}
}

function modifySetMeal(){
	var fields = $("*[name^='setMeal']");
	var param = {};
	$.each(fields,function(i,n){						   
			param[$(n).attr("name")]=$(n).val();				   
		});		
	param.storeId = storeId+"";
	param["relishIds"] = getRelishIds();
	param["deleteRelishIds"] = getDeleteRelishIds(param["relishIds"]);
	param["addRelishIds"] = getAddRelishIds(param["relishIds"]);
	//alert(param["deleteRelishIds"]+" "+param["addRelishIds"]);
	if(param["relishIds"].length == 0){
		HtmlUi.OverlayTip.show("您还没有选择美味，请选择",true);
		return;
	}

	SetMeal.modify(param,modifySetMealCallback,showSetMealValidateResult,function(){
																		HtmlUi.OverlayTip.show("正在保存，请稍后……");		  
																	});
}
function modifySetMealCallback(data){
	if(data.ok>0){
		HtmlUi.OverlayTip.show("修改成功",true);
	}else{
		HtmlUi.OverlayTip.show("添加失败，请刷新页面重试",true);
	}
}
function getRelishIds(){
	var ids = [];
	$.each($(".setmeal_relish_list li"),function(i,n){
													ids.push($(n).attr("relishId"));
												 });
	return ids;
}
function getDeleteRelishIds(nowRelishIds){//原来里面有的 但是现在却没有的，则是要删除的
	return $.map(relishIds,function(n){
							if($.inArray(n+"",nowRelishIds) == -1){
								return n;
							}else
								return null;
						});
}
//取得要添加的美味id数组
function getAddRelishIds(nowRelishIds){//现在里面有的而原来里面没有的 则是要添加的
	return $.map(nowRelishIds,function(n){
								if($.inArray(parseInt(n),relishIds)==-1){
									return n;
								}else{
									return null;
								}
							});
}
function resetSetMealField(){
	var fieldSelector = "input[name^='setMeal'],textarea[name^='setMeal']";
	$(fieldSelector).val("");
	clearRelish();
}

function getRelish(categoryId,page){
	if(categoryId == -99){
		$(".relish_list").html("请选择分类获取美味列表");
		return;
	}
	Relish.list({storeId:storeId,categoryId:categoryId,page:page,pageSize:20},getRelishCallback);	
}
function getRelishCallback(data){
	$(".relish_list").html("正在读取，请稍后");
	if(data.relishList.length == 0){
		$(".relish_list").html("本分类下没有美味");
		return;
	}
	$(".relish_list").empty();
	$.each(data.relishList,function(i,n){
									var selected = "";
									if($(".setmeal_relish_list li[relishId='"+n.id+"']").html())
										selected = "selected";
						$(".relish_list").append("<a class='"+selected+"' href='javascript:;' onclick=selectRelish('"+n.name+"',"+n.id+",this) relishId='"+n.id+"'>"+n.name+"</a>");				
					})
}

function selectRelish(name,relishId,id){
	if($(id).hasClass("selected")){
		removeRelish(relishId);
		return;
	}
	var li = "<li relishId='"+relishId+"'>"+name+"<span onclick=removeRelish("+relishId+")>&times;</span></li>";

	if($.trim($(".setmeal_relish_list").html()) == "还没有选择美味"){
		$(".setmeal_relish_list").empty();
	}
	$(".setmeal_relish_list").append(li);
	$(id).addClass("selected");
	$(".setmeal_relish_list").sortable();
}
function removeRelish(relishId){
	$(".setmeal_relish_list li[relishId='"+relishId+"']").remove();
	$(".relish_list a[relishId='"+relishId+"']").removeClass("selected");
}
function clearRelish(){
	$(".setmeal_relish_list").empty();
	$(".relish_list a").removeClass("selected");
}

//读取美食分类
function getRelishCategory(){
	Relish.Category.get(storeId,getRelishCategoryCallback);
}

function getRelishCategoryCallback(data){
	var categoryList = data.relishCategoryList;
	$.each(categoryList,function(i,n){
							$("#relishCategorySelect").append("<option value='"+n.id+"'>"+n.name+"</option>");		 
						})
}

//设置美食图片
function setPreviewPhoto(){
	SetMeal.setPreviewPhoto("setPreviewPhotoForm",setPreviewPhotoCallback,showSetMealValidateResult,function(){
																		HtmlUi.OverlayTip.show("正在上传图片，请稍后……");		  
																	});
}
function setPreviewPhotoCallback(data){
	if(data.ok == 9){
		HtmlUi.OverlayTip.show("设置成功",true);	
		$(".relish_preview_photo").attr("src","http://file.ctospace.com/setmeal/"+storeId+"/"+setMealId+"?1=1&"+(new Date()));
	}else{
		HtmlUi.OverlayTip.show("上传失败，请检查您上传的图片是否符合要求",true);	
	}
}