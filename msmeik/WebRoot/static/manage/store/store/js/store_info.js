// JavaScript Document
// JavaScript Document
$(function(){
	initStoreFieldEvent();	
	initSelectStoreItemEvent();
})
function initStoreFieldEvent(){
	var fieldSelector = "input[name^='store'],textarea[name^='store'],input[name='safeCode']";
	$(fieldSelector).each(function(){
							$(this).attr("initTip",$(this).parent().find(".field_tip").html());
						});
	$(fieldSelector).focus(function(){
								$(this).parent().parent().addClass("row_selected");	
								if($.trim($(this).attr("initTip"))!="")
									$(this).parent().find(".field_tip").removeClass("field_tip_right").removeClass("field_tip_error").html($(this).attr("initTip"));
								$(this).select();
								
							});
	$(fieldSelector).blur(function(){
								$(this).parent().parent().removeClass("row_selected");	
								validateStoreField($(this).attr("name"));								
							});
}
function validateStoreField(field){
	Store.validate(field,$("*[name='"+field+"']").val(),showStoreValidateResult);
	
}
function showStoreValidateResult(field,ok,result){
	if(!ok){
		$("*[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_right").addClass("field_tip_error").html(result);
	}else{
		$("*[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_error").addClass("field_tip_right").html("&nbsp;");
	}
}

function updateStore(){
	var fields = $("*[name^='store']");
	var store = {};
	$.each(fields,function(i,n){
			store[$(n).attr("name")]=$(n).val();				   
		});
	Store.updateStore(store,updateStoreCallback,showStoreValidateResult,function(){
																		HtmlUi.OverlayTip.show("正在保存，请稍后……");		  
																	});
}
function updateStoreCallback(data){
	if(data.ok>0){
		HtmlUi.OverlayTip.show("店铺资料更新成功",true);
	}else{
		HtmlUi.OverlayTip.show("发生未知错误，请刷新页面重新再试",true);
	}
}

function initSelectStoreItemEvent(){
	$("#foodTypeList > a").click(function(){
								var flag = selectStoreItem("input[name='store.foodType']",$(this).html(),5);	
								if(flag){
									if($(this).hasClass("selected"))
										$(this).removeClass("selected");
									else
										$(this).addClass("selected");
								}
								return false;
							});
	
	$("#diningTypeList > a").click(function(){
								var flag = selectStoreItem("input[name='store.diningType']",$(this).html(),5);	
								if(flag){
									if($(this).hasClass("selected"))
										$(this).removeClass("selected");
									else
										$(this).addClass("selected");
								}
								return false;
							});
	$.each($("#foodTypeList > a"),function(i,n){
									$.each($("#foodTypeList").attr("initSelectedItems").split(" "),function(j,m){
																									if($(n).html() == m){
																										$(n).addClass("selected");
																									}
																								})		   
								});
	$.each($("#diningTypeList > a"),function(i,n){
									$.each($("#diningTypeList").attr("initSelectedItems").split(" "),function(j,m){
																									if($(n).html() == m){
																										$(n).addClass("selected");
																									}
																								})		   
								});
}

function selectStoreItem(field,value,maxNum){
	var fieldVal = $.trim($(field).val());
	if(fieldVal.indexOf(value)!=-1){
		fieldVal = fieldVal.replace(value,"").replace(/\s{1,}/g," ");
		$(field).val($.trim(fieldVal));
		return true;
	}else{
		if(maxNum==1){
			$(field).val($.trim(value));			
		}else
		if(fieldVal.split(" ").length >= maxNum){
			return false;
		}else{
			$(field).val($.trim(fieldVal+" "+value));
		}
	}
	return true;
}
function showTable(id){
	if($(id).html() == "[显示]"){
		$(id).parent().next("div").show();
		$(id).html("[隐藏]");
	}else{
		$(id).parent().next("div").hide();
		$(id).html("[显示]");
	}
}

//地图标注
function showMarkMap(){
	MapMarkPanel.show($("input[name='store.mapLongitude']").val(),$("input[name='store.mapLatitude']").val(),markStorePoint);
}
function markStorePoint(point){
	$("input[name='store.mapLatitude']").val(point.lat());
	$("input[name='store.mapLongitude']").val(point.lng());
	MapMarkPanel.close();
}