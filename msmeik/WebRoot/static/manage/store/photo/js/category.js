// JavaScript Document
function addPhotoCategory(){
	var fields = ["input[name='photoCategory.name']"];
	var photoCategory = {};
	$.each(fields,function(i,n){
			photoCategory[$(n).attr("name")]=$(n).val();				   
		});
	photoCategory.storeId = storeId+"";
	Photo.Category.add(photoCategory,addPhotoCategoryCallback,showPhotoCategoryValidateResult,function(){
																										   	HtmlUi.OverlayTip.show("正在添加分类，请稍后……");
																										   });
}

function showPhotoCategoryValidateResult(field,ok,result){
	if(!ok){
		HtmlUi.OverlayTip.show(result,true);
	}
}
function addPhotoCategoryCallback(data){
	if(data.ok > 0){
		HtmlUi.OverlayTip.close();
		addCategoryToHtml(data.ok);
	}else
	if(data.ok == -1){
		HtmlUi.OverlayTip.show("分类名已经存在",true);
	}else{
		HtmlUi.OverlayTip.show("填写的信息不正确",true);
	}
}
function addCategoryToHtml(id){
	var tmp = '<div class="table_row ">'
              +'<div class="row_column column_0"><span class="category_name">{name}</span><span class="category_editor"><input type="text" class="input_txt" value="{name}"/> <input type="button" value="修改保存" onclick="updateCategory(this,\'{cateId}\')" class="btn_style_2" /></span></div>'
			  +'<div class="row_column column_1 lowlight"><a href="javascript:" onclick="showCategoryEditor(this)">编辑</a> <a href="javascript:" onclick="deleteCategory({cateId},{storeId},this)">删除</a></div>'
              +'</div>';
	tmp = tmp.replace(/{name}/g,$("input[name='photoCategory.name']").val());
	tmp = tmp.replace(/{cateId}/g,id);
	tmp = tmp.replace(/{storeId}/g,$("input[name='photoCategory.store.id']").val());
	var html = $(tmp);
	if($(".relish_cate_list_table > div").length%2 == 0){
		html.addClass("row_even");
	}
	$("input[name='photoCategory.name']").val("");
	$(".relish_cate_list_table").append(html);
}
function deleteCategory(cateId,storeId,row){
	HtmlUi.Confirm.show("您确定删除此分类吗？删除后此分类下的图片将会被移动到 餐厅图片 分类下。",{
							"确定":function(){
									Photo.Category.del(cateId,
															storeId,
															function(data){
																if(data.ok>0){
																	HtmlUi.OverlayTip.close();
																	$(row).parent().parent().remove();
																}
																if(data.ok<=0){
																	HtmlUi.OverlayTip.show("删除失败",true);	
																}
															},
															function(){
																HtmlUi.OverlayTip.show("正在删除请稍后……");	
															}
														)
								},
							"取消":function(){}
						});	
}

function showCategoryEditor(id){
	if($(id).html() == "编辑"){
		$(id).parent().parent().find(".category_name").hide();
		$(id).parent().parent().find(".category_editor input[type='text']").val($(id).parent().parent().find(".category_name").html());
		$(id).parent().parent().find(".category_editor").show();
		$(id).html("取消编辑");
	}else{
		$(id).parent().parent().find(".category_name").show();
		$(id).parent().parent().find(".category_editor").hide();
		$(id).html("编辑");
	}
}

function updateCategory(id,cateId){
	var fields = ["input[name='photoCategory.name']"];
	var photoCategory = {};
	photoCategory["photoCategory.name"] = $(id).parent().find("input[type='text']").val();	
	photoCategory["photoCategory.id"] = cateId;
	photoCategory.storeId = storeId+"";
	Photo.Category.modify(photoCategory,
						   function(data){
							   updateCategoryCallback(data,id,photoCategory["photoCategory.name"])
							},
							showPhotoCategoryValidateResult,
							function(){
									HtmlUi.OverlayTip.show("正在修改分类，请稍后……");
							});
}

function updateCategoryCallback(data,id,categoryName){
	if(data.ok > 0){
		HtmlUi.OverlayTip.close();
		$(id).parent().parent().find(".category_name").html(categoryName);
		$(id).parent().parent().next("div").find("a:first").click();
	}else
	{
		HtmlUi.OverlayTip.show("分类名修改失败，请检查您的输入",true);
	}
}