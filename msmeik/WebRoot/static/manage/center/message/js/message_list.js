// JavaScript Document
function deleteMessage(messageId){
	HtmlUi.Confirm.show("您确定删除吗？",{
							"确定":function(){
									var param = {receiverType:receiverType,receiverId:receiverId,messageId:messageId};
									if(receiverType==3){
										param.storeId = storeId;
									}
									Message.del(messageId,param,function(data){
														deleteMessageCallback(data,messageId);  
												},function(){
													HtmlUi.OverlayTip.show("正在删除");	
												});
								},
							"取消":function(){}
						});		
}
function deleteMessageCallback(data,messageId){
	if(data.ok > 0){
		HtmlUi.OverlayTip.close();
		$("#message"+messageId).remove();
	}else{
		HtmlUi.OverlayTip.show("删除失败",true);	
	}
}