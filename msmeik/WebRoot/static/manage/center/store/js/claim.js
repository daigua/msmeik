// JavaScript Document

function showClaimStep(step){
	$(".claim_step").hide();
	$($(".claim_step")[step]).show();
	$(".claim_step_title span").removeClass("step_active");
	$($(".claim_step_title span").get(step)).addClass("step_active");
}

//保存查找店铺正处于第几页中
var SearchStore = {page:1,pageSize:5};
function searchStore(){
	var name = $.trim($("#storeName").val());
	var type = $("#nameType").val();
	var param = {};
	param.name = name;
	param.page = SearchStore.page;
	param.pageSize=SearchStore.pageSize;
	if(type==1){
		param.type = "like";
	}
	Store.getStoreListByNameOrId(param,searchStoreCallback);
}

function searchStoreCallback(data){
	showStoreList(data.storeList);
}

//显示查找出来的店铺
function showStoreList(storeList){
	
	$(".claim_store_list ul").empty();	
	$.each(storeList,function(i,n){
		var li = '<li>'
		   li+='<p><span  class="list_title hightlight">店&nbsp;&nbsp;名：</span><span  class="store_name hightlight">'+n.name+'</span></p>'
		   li+=' <p><span  class="list_title">地&nbsp;&nbsp;址：</span>'+n.address+'</p>'
		   li+=' <p><span  class="list_title">认领状态：</span>'
		   if(n.owner==null || n.owner.account==""){
		   	  li+='未认领 [<a href="javascript:" onclick=selectStoreToClaim('+n.id+',"'+n.name+'")>认领</a>]</p>';
		   }else{
		   	li+='已认领';
		   }
	       li+=' </li>';
		   $(".claim_store_list ul").append(li);
	});
	if(storeList.length < SearchStore.pageSize)
		$(".claim_store_list ul").append("<li>您要找的店铺还没有登记吗？点击这里<a href='register.html'>登记</a>您的店铺</li>");
	if(SearchStore.page == 1){
		$(".claim_store_list .split_page a").show();
		if(storeList.length<SearchStore.pageSize)
			$(".claim_store_list .split_page").hide();
		else{
			$($(".claim_store_list .split_page a").get(0)).hide();
			$(".claim_store_list .split_page").show();	
		}			
	}else{
		$(".claim_store_list .split_page a").show();
		if(SearchStore.pageSize > storeList.length){
			$($(".claim_store_list .split_page a").get(1)).hide();
		}
	}
	$(".claim_store_list").show();
}

function showStoreNextPage(){
	SearchStore.page ++;
	searchStore()
}
function showStorePrePage(){
	if(SearchStore.page <=1)
		return;
	SearchStore.page --;
	searchStore();
}

//选择要认领的美食店
function selectStoreToClaim(id,name){
	store.id = id;
	$(".store_to_claim_name").html(name);
	$.getScript("/static/member/js/member_api.js",
				function(){
					$.getScript("/static/member/js/user.js")														  
				});
	showClaimStep(1);
}

//认领店铺
//步骤 首先更新用户联系方式信息，然后再认领店铺
function claimStore(){
	updateUser(function(){
		HtmlUi.OverlayTip.show("正在保存认领信息，请稍后……");	
		Store.claimStore({"store.id":store.id},claimStoreCallback)				
	});
}

function claimStoreCallback(data){
	if(data.ok == 9){
		HtmlUi.OverlayTip.close();
		showClaimStep(2);	
	}else{
		var result = "";
		if(data.ok == -1)
			result = "您所认领的店铺不存在";
		if(data.ok == -2)
			result = "您所认领的店铺已经被其他人认领，如有问题，请联系美食美客客服。";
		if(data.ok == -3)
			result = "您所认领的店铺正在被其他人认领，如有问题，请联系美食美客客服。";
		if(data.ok == -4)
			result = "您所认领的店铺数量达到2个的上限，不能再认领店铺，如果有其他店铺需要认领，请等待现有认领审核完毕再进行认领申请。";
		HtmlUi.OverlayTip.show(result,true);	
	}
}