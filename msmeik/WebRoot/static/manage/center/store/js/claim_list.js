// JavaScript Document
function deleteStoreClaim(claimId){
	HtmlUi.Confirm.show("您确定删除此店铺认领吗？",{
							"确定":function(){
									Store.deleteStoreClaim(claimId,function(){deleteStoreClaimCallback(claimId)});
								},
							"取消":function(){}
						});	
}
function deleteStoreClaimCallback(claimId){
	$("#storeClaimRow"+claimId).fadeOut(200);
}