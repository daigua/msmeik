// JavaScript Document
function deleteComment(param){	
	HtmlUi.Confirm.show("您确定删除吗？",{
							"确定":function(){
									CommentAdmin.del(param,function(data){
										deleteCommentCallback(data,param);	 
								},function(){
									HtmlUi.OverlayTip.show("正在删除，请稍后……");		  
								});
								},
							"取消":function(){}
						});	
}
function deleteCommentCallback(data,param){
	if(data.ok > 0){
		HtmlUi.OverlayTip.close();	
		$("#commentRow"+param.commentId).css("opacity",0.2);
		$("#commentRow"+param.commentId).click(function(){ return false;});
	}else{
		HtmlUi.OverlayTip.show("删除失败",true);
	}
}
function showContentMore(id){
	if($(id).html()=="...更多"){
		$(id).html("隐藏");
		$(id).prev().show();
	}else{
		$(id).html("...更多");
		$(id).prev().hide();
	}
}