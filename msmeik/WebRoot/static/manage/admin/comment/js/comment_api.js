// JavaScript Document
var CommentAdmin = {};
CommentAdmin.del = function(param,callback,beforeDoFunction){
	var self = this;
	if(this.deling == 1){
		return;
	}
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.deling =1;	
	var url = "/manage/admin/comment/delete";	
	$.ajaxSetup({cache:false});
	$.post(url,param,function(data){
				self.deling = 0;
				if(!LoginPanel.checkLogin(data,function(){
																CommentAdmin.del(param,callback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}