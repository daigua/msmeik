// JavaScript Document
$(function(){  
	$(".photo_manage_list .img_view a").zoomimage({centered:true});
});
function photoManageMode(id){
	if($(id).html()=="图片管理"){
		$(id).html("取消管理");
		$(".photo_manage_menu").show();
		$(".photo_manage_list .img_view a").unbind("click");
		$(".photo_manage_list .photo").click(function(){selectPhoto($(this))});
	}else{
		$(id).html("图片管理");
		$(".photo_manage_menu").hide();
		$(".photo_manage_list .img_view a").zoomimage({centered:true});
	}
}

//全选 取消全选图片
function selectPhotoAll(id){
	if($(id).html()=="全选"){
		$(id).html("取消全选");
		$(".photo_manage_list .photo").addClass("selected");
	}else{
		$(id).html("全选");
		$(".photo_manage_list .photo").removeClass("selected");
	}
}
//选择图片
function selectPhoto(id){
	if($(id).hasClass("selected")){
		$(id).removeClass("selected");
	}else{
		$(id).addClass("selected");
	}
}

//移动图片
function movePhoto(){
	if($(".photo_manage_list .selected").size()==0){
		HtmlUi.OverlayTip.show("您还没有选择图片",true);
		return;
	}
	$(".photo_category_menu a").show();
	$(".photo_category_menu a[categoryId="+categoryId+"]").hide();
	$(".photo_category_menu a").click(function(){
						Photo.manage({photoIds:getSelectedPhotos(),userId:uploaderId,userAccount:uploaderAccount},function(data){
																				HtmlUi.OverlayTip.show("移动成功",true);	
																				$(".photo_manage_list .selected").remove();
																				setInterval(function(){HtmlUi.OverlayTip.close()},300);
																			},function(){
																							HtmlUi.OverlayTip.show("正在移动，请稍后……");		  
																						});						   
					});
}

//删除图片
function deletePhoto(){
	if($(".photo_manage_list .selected").size()==0){
		HtmlUi.OverlayTip.show("您还没有选择图片",true);
		return;
	}
	HtmlUi.Confirm.show("您确定删除所选图片吗？",{
							"确定":function(){
									PhotoAdmin.del({photoIds:getSelectedPhotos(),userId:uploaderId,userAccount:uploaderAccount},function(data){
																				HtmlUi.OverlayTip.show("删除成功",true);																					
																				$(".photo_manage_list .selected").remove();
																				$(".photo_manage_list .photo").removeClass("selected");
																				setInterval(function(){HtmlUi.OverlayTip.close()},500);
																			},function(){
																							HtmlUi.OverlayTip.show("正在删除，请稍后……");		  
																						});
								},
							"取消":function(){}
						});			
}

function getSelectedPhotos(me){
	var photoIds = [];
	var selector = $(".photo_manage_list .selected");
	if(me!= undefined)
		selector = $(".photo_manage_list .selected[uploaderId='"+userId+"']");
	$.each(selector,function(i,n){
												photoIds.push($(n).attr("photoId"));		  
											});
	return photoIds;
}