// JavaScript Document
var PhotoAdmin = {};
PhotoAdmin.del = function(param,callback,beforeDoFunction){
	if(param.photoIds.length == 0)
		return;
	var self = this;
	if(this.manageing == 1){
		return;
	}
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.manageing =1;	
	var url = "/manage/admin/photo/delete";	
	$.ajaxSetup({cache:false});
	$.post(url,param,function(data){
				self.manageing = 0;
				if(!LoginPanel.checkLogin(data,function(){
																PhotoAdmin.del(param,callback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}