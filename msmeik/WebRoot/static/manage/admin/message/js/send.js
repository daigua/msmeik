// JavaScript Document
$(function(){
	initMessageFieldEvent();
})
function initMessageFieldEvent(){
	var fieldSelector = "input[name^='message'],input[name='receivers'],textarea[name^='message']";
	$(fieldSelector).each(function(){
							$(this).attr("initTip",$(this).parent().find(".field_tip").html());
						});
	$(fieldSelector).focus(function(){
								$(this).parent().parent().addClass("row_selected");	
								if($.trim($(this).attr("initTip"))!="")
									$(this).parent().find(".field_tip").removeClass("field_tip_right").removeClass("field_tip_error").html($(this).attr("initTip"));
								$(this).select();
								
							});
	$(fieldSelector).blur(function(){
								$(this).parent().parent().removeClass("row_selected");	
								validateMessageField($(this).attr("name"));
							});
}
function validateMessageField(field){
	Message.validate(field,$("*[name='"+field+"']").val(),showMessageValidateResult);
	
}
function showMessageValidateResult(field,ok,result){
	if(!ok){
		$("*[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_right").addClass("field_tip_error").html(result);
	}else{
		$("*[name='"+field+"']").parent().find(".field_tip").removeClass("field_tip_hide").removeClass("field_tip_error").addClass("field_tip_right").html("&nbsp;");
	}
}

function sendMessage(){
	var fields = $("*[name^='message'],input[name='receivers']");
	var message = {};
	$.each(fields,function(i,n){						   
			message[$(n).attr("name")]=$(n).val();				   
		});		
	MessageAdmin.sendMessage(message,sendMessageCallback,showMessageValidateResult,function(){
																		HtmlUi.OverlayTip.show("正在发送，请稍后……");		  
																	});
}
function sendMessageCallback(data){
	if(data.ok>0){
		HtmlUi.OverlayTip.show("发送成功",true);
	}else{
		HtmlUi.OverlayTip.show("添加失败，请刷新页面重试",true);
	}
}