// JavaScript Document
var MessageAdmin = {};

//发送短消息
MessageAdmin.sendMessage = function(message,callback,validateCallback,beforeDoFunction){
	var self = this;
	if(this.sending == 1){
		return;
	}
	var flag = true;
	$.each(message,function(i,n){
				flag = Message.validate(i,n,validateCallback).ok && flag;			 
			});
	if(!flag)
		return;
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.sending =1;	
	var url = "/manage/admin/message/send";
	$.ajaxSetup({cache:false});
	$.post(url,message,function(data){
				self.sending = 0;
				if(!LoginPanel.checkLogin(data,function(){
																MessageAdmin.sendMessage(message,callback,validateCallback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}

//删除短消息
MessageAdmin.del = function(messageId,callback,beforeDoFunction){
	var self = this;
	if(this.sending == 1){
		return;
	}
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.sending =1;	
	var url = "/manage/admin/message/delete";
	$.ajaxSetup({cache:false});
	$.post(url,{messageId:messageId},function(data){
				self.sending = 0;
				if(!LoginPanel.checkLogin(data,function(){
																MessageAdmin.del(messageId,callback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}