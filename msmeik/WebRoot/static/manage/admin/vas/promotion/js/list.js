// JavaScript Document
function del(id){
	HtmlUi.Confirm.show("您确定删除此推广吗？",
						{
							"确定":function(){
									PromotionVas.manage({p:"delete",pvId:id},function(data){delCallback(data,id)},function(){HtmlUi.OverlayTip.show("正在删除，稍后……")});
								},
							"取消":function(){}
						});
}
function delCallback(data,id){
	if(data.ok > 0){
		HtmlUi.OverlayTip.show("删除成功",true);
		$("#promotionVas"+id).remove();
	}else{
		HtmlUi.OverlayTip.show("删除失败",true);
	}
}
function setStatus(pvId,status,id){
	PromotionVas.manage({p:"status",pvId:pvId,v:status},function(data){setStatusCallback(data,id)},function(){HtmlUi.OverlayTip.show("正在操作，稍后……")});
}
function setStatusCallback(data,id){
	if(data.ok > 0){
		HtmlUi.OverlayTip.show("设置成功");
		setTimeout(function(){HtmlUi.OverlayTip.close()},300);
		$(id).siblings().removeClass("mark");
		$(id).addClass("mark");
		$(id).removeClass("mark_gray");
		$(id).siblings().addClass("mark_gray");
	}else{
		HtmlUi.OverlayTip.show("设置失败",true);
	}
}