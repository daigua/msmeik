// JavaScript Document
var PromotionVas = {};

PromotionVas.add = function(param,callback,beforeDoFunction){
	var self = this;
	if(this.adding == 1){
		return;
	}
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.adding =1;	
	var url = "/manage/admin/vas/promotion/add";
	$.ajaxSetup({cache:false});
	$.post(url,param,function(data){
				self.adding = 0;
				if(!LoginPanel.checkLogin(data,function(){
																PromotionVas.add(param,callback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}

PromotionVas.manage = function(param,callback,beforeDoFunction){
	var self = this;
	if(this.manageing == 1){
		return;
	}
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.manageing =1;	
	var url = "/manage/admin/vas/promotion/manage";
	$.ajaxSetup({cache:false});
	$.post(url,param,function(data){
				self.manageing = 0;
				if(!LoginPanel.checkLogin(data,function(){
																PromotionVas.manage(param,callback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}