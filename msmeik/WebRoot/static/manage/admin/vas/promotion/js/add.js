// JavaScript Document
function addToServer(){
	var param = {};
	var fields = $("*[name^='pv']");
	$.each(fields,function(i,n){						   
			param[$(n).attr("name")]=$(n).val();				   
		});	
	//$.each(param,function(i,n){alert(i+" =  "+n)});
	PromotionVas.add(param,addCallback,function(){HtmlUi.OverlayTip.show("正在添加，稍后……")});
}
function add(){
	HtmlUi.Confirm.show("确定添加吗？请在添加之前认真检查填写是否正确！",
						{
							"确定":function(){addToServer();},
							"取消":function(){}
						});
}
function addCallback(data){
	if(data.ok > 0){
		HtmlUi.OverlayTip.show("添加成功",true);
	}else
	if(data.ok == -1){
		HtmlUi.OverlayTip.show("此分类推广已经存在",true);
	}
	else{
		HtmlUi.OverlayTip.show("添加失败，请检查添加数据是否正确！",true);
	}
}