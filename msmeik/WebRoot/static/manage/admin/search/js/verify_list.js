// JavaScript Document
var resultInfoPanel = new HtmlUi.Panel({width:300,height:200});
function verifyApp(storeId,status,id){
	HtmlUi.Confirm.show("您确定进行此操作吗？",{
							"确定":function(){
									SearchAdmin.verify(storeId,status,function(data){
										verifyAppCallback(data,id);	 
								},function(){
									HtmlUi.OverlayTip.show("正在操作，请稍后……");		  
								});
								},
							"取消":function(){}
						});	
	HtmlUi.Confirm.show("您确定进行此操作码？",{
							"确定":function(){
									$(".verify_result_info_textarea textarea").val("");
									if(status==1){			
										verifyAppToServer(storeId,status,"",id);
									}
									if(status==2){			
										$(".verify_result_info_textarea input[type='button']").unbind("click");
										$(".verify_result_info_textarea input[type='button']").click(
														function(){
																resultInfoPanel.close();
																verifyAppToServer(storeId,status,$(".verify_result_info_textarea textarea").val(),id);
														});
										resultInfoPanel.setHtml($(".verify_result_info_textarea").show()).show();
									}
								},
							"取消":function(){}
						});	
}
function verifyAppToServer(storeId,status,result,id){
	SearchAdmin.verify(storeId,status,result,function(data){
										verifyAppCallback(data,id);	 
								},function(){
									HtmlUi.OverlayTip.show("正在操作，请稍后……");		  
								});
}
function verifyAppCallback(data,id){
	if(data.ok > 0){
		HtmlUi.OverlayTip.close();
		if($(id).html()=="通过"){
			$(id).attr("onclick","");
			$(id).html("已通过");
		}
		if($(id).html()=="失败"){
			$(id).attr("onclick","");
			$(id).html("已失败");
		}
	}else{
		HtmlUi.OverlayTip.show("操作失败，稍后重试",true);	
	}
}