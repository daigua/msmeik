// JavaScript Document
var SearchAdmin = {};

SearchAdmin.verify = function(storeId,status,result,callback,beforeDoFunction){
	var self = this;
	if(this.verifying == 1){
		return;
	}
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.verifying = 1;
	var url = "/manage/admin/search/app/verify";
	$.ajaxSetup({cache:false});
	$.post(url,{storeId:storeId,status:status,result:result},function(data){
				self.verifying = 0;
				if(!LoginPanel.checkLogin(data,function(){
																SearchAdmin.verify(storeId,status,callback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}