// JavaScript Document
var Management = {};
Management.Member = {};

//查看用户信息
Management.Member.UserInfo = {};
Management.Member.UserInfo.init = function(){
	if(this.isInit)
		return;
	this.isInit = true;
	this.userInfoPanel = new HtmlUi.IframePanel({width:340,height:360});
}
Management.Member.UserInfo.show = function(p,v){
	this.init();
	this.userInfoPanel.show("/manage/admin/member/userInfo?"+p+"="+v);
}
Management.Member.UserInfo.close = function(){
	this.userInfoPanel.close();
}
