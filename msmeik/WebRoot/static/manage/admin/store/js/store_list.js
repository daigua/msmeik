// JavaScript Document
function showSearchForm(id){
	if($(id).html()=="[显示]"){
		$(id).html("[隐藏]");
		$("#searchForm").show();
	}else{
		$(id).html("[显示]");
		$("#searchForm").hide();
	}
}

function verifyStore(storeId,verify,param){
	HtmlUi.Confirm.show("您确定此操作吗",{
							"确定":function(){
									HtmlUi.OverlayTip.show("正在操作，请稍后……");
									StoreManager.verifyStore(storeId,verify,param,function(data){
												verifyStoreCallback(data,storeId,verify,param);		 
											});
								},
							"取消":function(){}
						});	
	
}
function delStore(storeId){
	HtmlUi.Confirm.show("您确定删除吗",{
							"确定":function(){
									HtmlUi.OverlayTip.show("正在操作，请稍后……");
									StoreManager.del({storeId:storeId},function(data){
															delStoreCallback(data,storeId);					
														});
								},
							"取消":function(){}
						});
}
function verifyStoreCallback(data,storeId,verify,param){
	HtmlUi.OverlayTip.show("操作成功",true);
	if(data.ok == 1){
		var html ;
		if(verify == 0)
			html = '未 <a href="javascript:" onclick="verifyStore('+storeId+',1,{userId:$userId,userAccount:$userAccount})">通过</a> <a href="javascript:" onclick="verifyStore('+storeId+',2,{userId:$userId,userAccount:$userAccount})">失败</a>';
		if(verify == 1)
			html = '<a href="javascript:" onclick="verifyStore('+storeId+',0,{userId:$userId,userAccount:$userAccount})">未</a> 通过 <a href="javascript:" onclick="verifyStore('+storeId+',2,{userId:$userId,userAccount:$userAccount})">失败</a>';
		if(verify == 2)
			html = '<a href="javascript:" onclick="verifyStore('+storeId+',0,{userId:$userId,userAccount:$userAccount})">未</a> <a href="javascript:" onclick="verifyStore('+storeId+',1,{userId:$userId,userAccount:$userAccount})">通过</a> 失败';
		html = html.replace(/\$userId/g,param.userId).replace(/\$userAccount/g,"'"+param.userAccount+"'");
		$("#store"+storeId+"VerifyStatus").html(html);
	}
}

function delStoreCallback(data,storeId){
	if(data.ok >0){
		HtmlUi.OverlayTip.show("删除成功",true);
		$("#storeRow"+storeId).remove();
	}else{
		HtmlUi.OverlayTip.show("删除失败",true);
	}
}