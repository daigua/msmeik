// JavaScript Document
function showSearchForm(id){
	if($(id).html()=="[显示]"){
		$(id).html("[隐藏]");
		$("#searchForm").show();
	}else{
		$(id).html("[显示]");
		$("#searchForm").hide();
	}
}
var resultInfoPanel = new HtmlUi.Panel({width:300,height:200});
function verifyStoreClaim(claimId,verify){	
	HtmlUi.Confirm.show("您确定进行此操作码？",{
							"确定":function(){
									$(".verify_result_info_textarea textarea").val("");
									if(verify==1 || verify == 0){			
										verifyStoreClaimToServer(claimId,verify,"");
									}
									if(verify==2){			
										$(".verify_result_info_textarea input[type='button']").unbind("click");
										$(".verify_result_info_textarea input[type='button']").click(function(){
																									resultInfoPanel.close();
																									verifyStoreClaimToServer(claimId,verify,$(".verify_result_info_textarea textarea").val());			  
																								});
										resultInfoPanel.setHtml($(".verify_result_info_textarea").show()).show();
									}
								},
							"取消":function(){}
						});	
}
function verifyStoreClaimToServer(claimId,verify,resultInfo){
	HtmlUi.OverlayTip.show("正在操作，请稍后……");
	var storeClaim = {};
	storeClaim["storeClaim.id"] = claimId;
	storeClaim["storeClaim.claimResult"] = verify;
	storeClaim["storeClaim.resultInfo"] = resultInfo;
	StoreManager.verifyStoreClaim(storeClaim,function(data){
												verifyStoreClaimCallback(data,claimId,verify);		 
											});
}
function verifyStoreClaimCallback(data,claimId,verify){
	HtmlUi.OverlayTip.show("操作成功",true);
	if(data.ok == 1){
		var html ;
		if(verify == 0)
			html = '未 <a href="javascript:" onclick="verifyStoreClaim('+claimId+',1)">通过</a> <a href="javascript:" onclick="verifyStoreClaim('+claimId+',2)">失败</a>';
		if(verify == 1)
			html = '<a href="javascript:" onclick="verifyStoreClaim('+claimId+',0)">未</a> 通过 <a href="javascript:" onclick="verifyStoreClaim('+claimId+',2)">失败</a>';
		if(verify == 2)
			html = '<a href="javascript:" onclick="verifyStoreClaim('+claimId+',0)">未</a> <a href="javascript:" onclick="verifyStoreClaim('+claimId+',1)">通过</a> 失败';
		$("#storeClaim"+claimId+"VerifyStatus").html(html);
	}else{
		HtmlUi.OverlayTip.show("发生未知错误，请刷新页面重新再试",true);
	}
}