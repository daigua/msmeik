// JavaScript Document
var StoreManager = {};

StoreManager.verifyStore = function(storeId, verify, param,callback){
	if(!/^[012]$/.test(verify))
		return;
	if(this.verifyStoreing == 1)
		return;
	this.verifyStoreing = 1;
	var self = this;
	var url = "/manage/admin/store/verifyStore";
	var data = {storeId:storeId,verify:verify};
	data = $.extend(param || {},data);
	alert(data["user.id"]);
	$.ajaxSetup({cache:false});
	$.post(url,data,function(data){
						self.verifyStoreing = 0;
						if(!LoginPanel.checkLogin(data,function(){
																StoreManager.verifyStore(storeId, verify, callback);		
															}))
							return;	
						callback(data);
					},"json");
}
StoreManager.updateStore = function(store,callback,validateCallback,beforeDoFunction){
	var self = this;
	if(self.updateStoreing == 1){
		return;
	}
	var flag = true;
	$.each(store,function(i,n){
				flag = Store.validate(i,n,validateCallback).ok && flag;			 
			});
	if(!flag)
		return;
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.updateStoreing = 1;
	var url  = "/manage/admin/store/updateStore";
	$.ajaxSetup({cache:false});
	$.post(url,store,function(data){
				self.updateStoreing = 0;
				if(!LoginPanel.checkLogin(data,function(){
																StoreManager.updateStore(store,callback,validateCallback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}
StoreManager.verifyStoreClaim = function(storeClaim, callback){
	if(!/^[012]$/.test(storeClaim["storeClaim.claimResult"]))
		return;
	if(this.verifyStoreClaiming == 1)
		return;
	this.verifyStoreClaiming = 1;
	var self = this;
	var url = "/manage/admin/store/verifyStoreClaim";
	$.ajaxSetup({cache:false});
	$.post(url,storeClaim,function(data){
						self.verifyStoreClaiming = 0;
						if(!LoginPanel.checkLogin(data,function(){
																StoreManager.verifyStoreClaim(storeClaim,callback);		
															}))
							return;	
						callback(data);
					},"json");
}
StoreManager.del = function(parm,callback){
	var url = "/manage/admin/store/delete";
	var self = this;
	if(this.deling == 1)
		return;
	this.deling = 1;
	$.ajaxSetup({cache:false});
	$.get(url,parm,function(data){
						self.deling = 0;
						if(!LoginPanel.checkLogin(data,function(){
																StoreManager.del(parm,callback);		
															}))
							return;	
						callback(data);
					},"json");
}
