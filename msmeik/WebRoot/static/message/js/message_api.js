// JavaScript Document
var Message = {};

Message.validate = function(field,value,callback){
	var value = $.trim(value.replaceHtmlTags());
	var result = "正确";
	var flag = true;
	if(field == "receivers"){
		if(value == ""){
			result="收件人不能为空";
			flag=false;
		}else
		if(!/^0|([1-9][\s\d]{0,})$/.test(value)){
			result = "收件人只能是数字ID";
			flag = false;
		}
	}
	if(field=="message.title"){
		if(value == ""){
			result="信息标题不能为空";
			flag=false;
		}else
		if(value.byteLength()>40){
			result="信息标题最多20个汉字或者40个字母";
			flag = false;
		}
	}
	if(field=="message.content"){
		if(value == ""){
			result="信息内容不能为空";
			flag=false;
		}
	}
		
	if($.isFunction(callback)){
		callback(field,flag,result);
	}
	return {ok:flag,result:result};
}

Message.count = function(userId,userType,callback,beforeDoFunction){
	var self = this;
	if(this.counting == 1){
		return;
	}
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.counting =1;	
	var url = "/message/count";
	$.ajaxSetup({cache:false});
	$.post(url,{userId:userId,userType:userType},function(data){
				self.counting = 0;
				callback(data);
			},"json");
}

Message.del = function(messageId,param,callback,beforeDoFunction){
	var self = this;
	if(this.deling == 1){
		return;
	}
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.deling =1;	
	var url = "/message/store/delete";
	if(param.receiverType==2)
		url = "/message/user/delete";
	$.ajaxSetup({cache:false});
	$.post(url,param,function(data){
				self.deling = 0;
				callback(data);
			},"json");
}
Message.read = function(messageId,param,callback,beforeDoFunction){
	var self = this;
	if(this.reading == 1){
		return;
	}
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.reading =1;	
	var url = "/message/store/read";
	if(param.receiverType==2)
		url = "/message/user/read";
	$.ajaxSetup({cache:false});
	$.post(url,param,function(data){
				self.reading = 0;
				callback(data);
			},"json");
}

Message.showPanel = {};
Message.showPanel.init = function(){
	if(this.isInit){
		return;
	}
	this.isInit = true;
	this.panel = new HtmlUi.IframePanel({width:300,height:215});
}
Message.showPanel.show = function(param){
	this.init();
	var url = "/message/user/read";
	if(param.receiverType==3)
		url = "/message/store/read";
	this.panel.show(url+"?"+$.param(param));
}


Message.showPanel.checkLogin = function(data,callback){
	if(data.loginRequired){
		this.show();
		this.loginCallback = callback;
		return false;
	}
	return true;
}
Message.showPanel.close = function(){
	this.panel.close();
}
