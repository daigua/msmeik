// JavaScript Document
//地图标记panel
var MapMarkPanel = {};
MapMarkPanel.template = '<div id="markMapDiv" style="border:1px solid #ccc;"></div>';
MapMarkPanel.init = function(lng,lat,callback){
	if(this.isInit){
		return;
	}
	this.isInit = true;
	this.mapDiv = $(this.template).height(500).width(700).css("cursor","default !important");
	this.panel = new HtmlUi.Panel({width:702,height:502});
	this.panel.show();
	this.panel.setHtml(this.mapDiv);
	if (GBrowserIsCompatible()) {
		this.map = new GMap2(document.getElementById("markMapDiv"),{draggableCursor:"crosshair"});			
		this.map.enableScrollWheelZoom()
		this.map.addControl(new GSmallZoomControl());
		GEvent.addListener(this.map,"click", function(overlay, latlng) {     
		  if (latlng) {
			MapMarkPanel.setGMarker(latlng);
		  	callback(latlng);
		  }
		});
		if(lat != 0 && lat != "" && lng !=0 && lng != ""){
			this.map.setCenter(new GLatLng(lat, lng),16);
			this.setGMarker(new GLatLng(lat,lng));
		}else{
			this.map.setCenter(new GLatLng(37.86, 112.54), 12);
		}
	 }		
}
MapMarkPanel.show = function(lng,lat,callback){
	this.init(lng,lat,callback);
	this.panel.show();	
}
MapMarkPanel.setGMarker = function(latlng){
	if(this.marker){
		this.marker.setLatLng(latlng);
	}else{
		var flagIcon = new GIcon(G_DEFAULT_ICON);
		flagIcon.iconSize = new GSize(32,32);
		flagIcon.image = "/static/images/flag_green.png";
		this.marker = new GMarker(latlng,{icon:flagIcon});
		this.map.addOverlay(this.marker);
	}	
}
MapMarkPanel.close = function(){
	this.panel.close();
}

var MapTools = {};
MapTools.addInfoWindow = function(map,latlng,div,offset){
	map.addOverlay(new MapInfoWindow(latlng,div,offset));
}

//地图InfoWindow提示
function MapInfoWindow(latlng,div,offset){
	this.latlng = latlng;
	this.offset = offset || {x:0,y:0};
	this.div = div;
}
MapInfoWindow.prototype = new GOverlay();
MapInfoWindow.prototype.initialize = function(map) {
    var pos = map.fromLatLngToDivPixel(this.latlng);
	this.div.style.left = (pos.x+this.offset.x) + "px";
    this.div.style.top = (pos.y+this.offset.y) + "px";
    map.getPane(G_MAP_MAP_PANE).appendChild(this.div);
   this.map = map;
}

// Remove the main DIV from the map pane
MapInfoWindow.prototype.remove = function() {
  this.div.parentNode.removeChild(this.div);
}

// Copy our data to a new Rectangle
MapInfoWindow.prototype.copy = function() {
  return new MapInfoWindow(this.div);
}

// Redraw the rectangle based on the current projection and zoom level
MapInfoWindow.prototype.redraw = function(force) {
	if(!force)
		return;
	var pos = map.fromLatLngToDivPixel(this.latlng);
	this.div.style.left = (pos.x+this.offset.x) + "px";
    this.div.style.top = (pos.y+this.offset.y) + "px";
}