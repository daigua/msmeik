// JavaScript Document
var MapPanel = {};
//用户登陆Panel
MapPanel.init = function(){
	if(this.isInit){
		return;
	}
	this.isInit = true;
	this.panel = new HtmlUi.IframePanel({width:702,height:502});
}
MapPanel.show = function(name,mapLongitude,mapLatitude){
	this.init();
	this.panel.show("http://ms.ctospace.com/map/mapPanel.html?name="+encodeURI(name)+"&mapLongitude="+mapLongitude+"&mapLatitude="+mapLatitude);
}

MapPanel.close = function(){
	this.panel.close();
}

