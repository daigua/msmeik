// JavaScript Document
function digg(id,commentId,num){
	var param = {commentId:commentId};
	CommentApi1.digg(param,function(data){diggCallback(data,id,commentId,num)});
}
function diggCallback(data,id,commentId,num){
	$(id).html("("+(num+1)+")");
	$(id).removeAttr("onclick");
}

function deleteComment(commentId){	

	HtmlUi.Confirm.show("您确定删除吗？删除并会扣除相应的积分",{
							"确定":function(){
									Comment.del(commentId,function(data){
										deleteCommentCallback(data,commentId);	 
								},function(){
									HtmlUi.OverlayTip.show("正在删除，请稍后……");		  
								});
								},
							"取消":function(){}
						});	
}
function deleteCommentCallback(data,commentId){
	if(data.ok > 0){
		HtmlUi.OverlayTip.close();	
		$("#commentRow"+commentId).css("opacity",0.2);
		$("#commentRow"+commentId).click(function(){ return false;});
	}else{
		HtmlUi.OverlayTip.show("删除失败",true);
	}
}