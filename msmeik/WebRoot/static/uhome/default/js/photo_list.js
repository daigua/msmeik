// JavaScript Document
function photoManageMode(id){
	if($(id).html()=="管理图片"){
		$(id).html("取消管理");
		$(".photo_manage_menu").show();
		$(".photo_list .photo a").unbind("click");
		$(".photo_list .photo").click(function(){selectPhoto($(this));return false;});
	}else{
		$(id).html("管理图片");
		$(".photo_manage_menu").hide();
		$(".photo_list .photo a").zoomimage({centered:true});
	}
}

//全选 取消全选图片
function selectPhotoAll(id){
	if($(id).html()=="全选"){
		$(id).html("取消全选");
		$(".photo_list .photo").addClass("selected");
	}else{
		$(id).html("全选");
		$(".photo_list .photo").removeClass("selected");
	}
}
//选择图片
function selectPhoto(id){
	if($(id).hasClass("selected")){
		$(id).removeClass("selected");
	}else{
		$(id).addClass("selected");
	}
}

//删除图片
function deletePhoto(){
	if($(".photo_list .selected").size()==0){
		HtmlUi.OverlayTip.show("您还没有选择图片",true);
		return;
	}
	HtmlUi.Confirm.show("您确定删除所选图片吗？删除并会扣除相应的积分",{
							"确定":function(){
									Photo.del(getSelectedPhotos(),function(data){
																				HtmlUi.OverlayTip.show("删除成功",true);	
																				var i = parseInt($("#photoNum").html())-$(".photo_list .selected").size();
																				$(".photo_list .selected").remove();
																				$("#photoNum").html(i);
																				setInterval(function(){HtmlUi.OverlayTip.close()},500);
																			},function(){
																							HtmlUi.OverlayTip.show("正在删除，请稍后……");		  
																						});
								},
							"取消":function(){}
						});			
}
function getSelectedPhotos(){
	var photoIds = [];
	var selector = $(".photo_list .selected");
	$.each(selector,function(i,n){
												photoIds.push($(n).attr("photoId"));		  
											});
	return photoIds;
}