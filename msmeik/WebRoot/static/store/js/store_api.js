// JavaScript Document
var Store = {};
//店铺验证
Store.validate = function(field,value,callback){
	var value = $.trim(value.replaceHtmlTags());
	var result = "正确";
	var flag = true;
	if(field == "store.name"){
		if(value == ""){
			result="店铺名称不能为空";
			flag=false;
		}else
		if(value.byteLength()>40){
			result="店铺名称最多40个字母或20个汉字";
			flag = false;
		}
	}
	if(field=="store.address"){
		if(value == ""){
			result="地址不能为空";
			flag=false;
		}else
		if(value.byteLength()>100){
			result="地址最多100个字母或50个汉字";
			flag = false;
		}
	}
	if(field=="store.telephone"){
		if(value == ""){
			result="营业电话不能为空";
			flag=false;
		}else
		if(value.byteLength()>20){
			result="营业电话长度不能超过20";
			flag = false;
		}
	}
	if(field=="store.businessTime"){
		if(value.byteLength()>40){
			result="营业时间最多20个汉字或40个字母";
			flag = false;
		}
	}
	if(field=="store.diningType"){
		if(value.byteLength()>100){
			result="环境氛围最多100个字母或50个汉字";
			flag = false;
		}
	}
	if(field=="store.tags"){
		if(value.byteLength()>100){
			result="标签最多50个汉字或100个字母";
			flag = false;
		}
	}
	if(field=="store.website"){
		if(value.byteLength()>50){
			result="网站地址长度不能超过50";
			flag = false;
		}
	}
	if(field=="store.busLine"){
		if(value.byteLength()>100){
			result="公交线路最多50个汉字或100个字母";
			flag = false;
		}
	}
	if(field=="store.intersection"){
		if(value.byteLength()>100){
			result="交叉路口最多50个汉字或100个字母";
			flag = false;
		}
	}
	
	if(field=="store.introduction"){
		if(value.byteLength()>400){
			result="简介最多200个汉字或400个字符";
			flag = false;
		}
	}
	if(field=="store.foodType"){
		if(value == ""){
			result="主营菜系不能为空";
			flag=false;
		}else
		if(value.byteLength()>100){
			result="主营菜系最多50个汉字或100个字符";
			flag = false;
		}
	}
	if(field == "safeCode"){
		var val = $.trim($("input[name='safeCode']").val());
		if(val == ""){
			result = "验证码不能为空";
			flag = false;
		}else
		if(!/^[0-9A-Za-z]{4}$/.test(val)){
			result = "验证码格式输入错误";
			flag = false;
		}
	}
	
	if(field == "store.perPay"){
		if(value != "" && !/^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$/.test(value)){
			result="人均消费输入不正确";
			flag = false;
		}
	}
	
	if($.isFunction(callback)){
		callback(field,flag,result);
	}
	return {ok:flag,result:result};
}

//注册用户
Store.register = function(store,callback,validateCallback,beforeDoFunction){
	var self = this;
	if(this.registering == 1){
		return;
	}
	var flag = true;
	$.each(store,function(i,n){
				flag = Store.validate(i,n,validateCallback).ok && flag;			 
			});
	if(!flag)
		return;
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.registering =1;	
	var url = "/store/register";
	$.ajaxSetup({cache:false});
	$.post(url,store,function(data){
				self.registering = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Store.register(store,callback,validateCallback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}

//验证点名是否存在
Store.checkStoreExist = function(field,value,callback){
	if($.trim(value)=="")
		return;
	var data = {};
	if(field == "name"){
		data["name"] = value;
	}
	var url = "/store/checkStoreExist";
	$.ajaxSetup({cache:false});
	$.post(url,data,function(data){
					callback(data.storeId);
				},"json");
}

//获取storeList根据店铺名称模糊或者精确查找或根据店铺id
Store.getStoreListByNameOrId = function(param,callback){
	if(this.getStoreListByNameOrIding == 1)
		return;
	this.getStoreListByNameOrIding =1;
	var self = this;
	var url = "/store/getStoreListByNameOrId";
	$.ajaxSetup({cache:false});
	$.get(url,param,function(data){
				self.getStoreListByNameOrIding = 0;
				callback(data);
			},"json");
	
}

//认领店铺
Store.claimStore = function(storeClaim,callback){
	if(this.claimStoreing == 1)
		return;
	this.claimStoreing = 1;
	var self = this;
	var url = "/store/claimStore";
	$.ajaxSetup({cache:false});
	$.get(url,storeClaim,function(data){
				self.claimStoreing = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Store.claimStore(storeClaim,callback);		
															}))
					return;
				callback(data);
			},"json");
}

//删除店铺认领
Store.deleteStoreClaim = function(claimId,callback){
	if(this.deleteClaiming==1)
		return;
	this.deleteClaiming = 1;
	var self = this;
	var url = "/store/deleteStoreClaim";
	$.ajaxSetup({cache:false});
	$.get(url,{claimId:claimId},function(data){
				self.deleteClaiming = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Store.deleteStoreClaim(claimId,callback);		
															}))
					return;
				callback(data);
			},"json");
}

//更新店铺信息
Store.updateStore = function(store,callback,validateCallback,beforeDoFunction){
	var self = this;
	if(self.updateStoreing == 1){
		return;
	}
	var flag = true;
	$.each(store,function(i,n){
				flag = Store.validate(i,n,validateCallback).ok && flag;			 
			});
	if(!flag)
		return;
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.updateStoreing = 1;
	var url  = "/manage/store/updateStore";
	$.ajaxSetup({cache:false});
	$.post(url,store,function(data){
				self.updateStoreing = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Store.updateStore(store,callback,validateCallback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}

//根据店铺id取得店铺信息
Store.getStoreByStoreId = function(storeId,callback){
	var param = {storeId:storeId};
	var url = "/store/getStore";
	$.ajaxSetup({cache:false});
	$.get(url,param,function(data){
						var store = data.store;
						callback(store);
					},"json");
}

//设置店铺门面头像
Store.setStoreFace = function(form,callback,validateCallback,beforeDoFunction){
		var self = this;
		if(self.uploadStoreFaceing == 1){
			return ;
		}	
		var fileVal = $("#faceFile").val();
		if($.trim(fileVal)=="")
			return;
		validateCallback("");
		if(!/\.(jpg)|(jpeg)$/i.test(fileVal)){
			validateCallback("您选择的图片格式不正确");
			return;
		}
		if($.isFunction(beforeDoFunction)){
			beforeDoFunction();
		}
		this.uploadStoreFaceing=1;
		var iframe = $("<iframe id='uploadStoreFaceIframe' name='uploadStoreFaceIframe' style='display:none'></iframe>");
		$("body").append(iframe);
		iframe.load(function(){
					var data = eval("("+iframe.contents().find("body").html()+")");
					iframe.remove();
					self.uploadStoreFaceing = 0;
					if(!LoginPanel.checkLogin(data,function(){
																	Store.setStoreFace(form,callback,validateCallback,beforeDoFunction);		
																}))
						return;
					callback(data);		 
				});
		document.getElementById(form).target = "uploadStoreFaceIframe";
		document.getElementById(form).method = "post";
		document.getElementById(form).enctype  = "multipart/form-data";
		document.getElementById(form).action = "/manage/store/setStoreFace";
		document.getElementById(form).submit();
}
//刷新餐厅排名时间
Store.setUpdateTime = function(param,callback,beforeDoFunction){
	var self = this;
	if(self.setUpdateTimeing == 1){
		return;
	}	
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.setUpdateTimeing = 1;
	var url  = "/manage/store/setStoreUpdateTime";
	$.ajaxSetup({cache:false});
	$.post(url,param,function(data){
				self.setUpdateTimeing = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Store.setUpdateTime(param,callback,beforeDoFunction);		
															}))
					return;
				if(data.requestLimitReject){
					HtmlUi.OverlayTip.show(data.limitReason,true);
					return;
				}
				callback(data);
			},"json");
	
}

