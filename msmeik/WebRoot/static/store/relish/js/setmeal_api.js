// JavaScript Document
var SetMeal = {};

SetMeal.Category = {};
//category域的验证
SetMeal.Category.validate = function(field,value,callback){
	var value = $.trim(value.replaceHtmlTags());
	var result = "正确";
	var flag = true;
	if(field == "category.name"){
		if(value == ""){
			result="美味分类名不能为空";
			flag=false;
		}else
		if(value.byteLength()>40){
			result="美味分类名最多40个字母或20个汉字";
			flag = false;
		}
	}
	if(field == "category.orderIndex"){
		if(!/^[1-9]\d*$/.test(value)){
			result="排序值输入不正确";
			flag = false;
		}
	}
	if($.isFunction(callback)){
		callback(field,flag,result);
	}
	return {ok:flag,result:result};
}
//添加美食分类
SetMeal.Category.add = function(param,callback,validateCallback,beforeDoFunction){
	var self = this;
	if(this.addCategorying == 1){
		return;
	}
	var flag = true;
	$.each(param,function(i,n){
				flag = SetMeal.Category.validate(i,n,validateCallback).ok && flag;			 
			});
	if(!flag)
		return;
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.addCategorying =1;	
	var url = "/manage/store/relish/setmeal/category/add";
	$.ajaxSetup({cache:false});
	$.post(url,param,function(data){
				self.addCategorying = 0;
				if(!LoginPanel.checkLogin(data,function(){
																SetMeal.Category.add(param,callback,validateCallback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}

//修改美食分类名
SetMeal.Category.modify = function(param,callback,validateCallback,beforeDoFunction){
	var self = this;
	if(this.modifying == 1){
		return;
	}
	var flag = true;
	$.each(param,function(i,n){
				flag = SetMeal.Category.validate(i,n,validateCallback).ok && flag;			 
			});
	if(!flag)
		return;
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.modifying =1;	
	var url = "/manage/store/relish/setmeal/category/update";
	$.ajaxSetup({cache:false});
	$.post(url,param,function(data){
				self.modifying = 0;
				if(!LoginPanel.checkLogin(data,function(){
																SetMeal.Category.modify(param,callback,validateCallback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}

//删除美食分类
SetMeal.Category.del = function(param,callback,beforeDoFunction){	
	var self = this;
	if(this.deleting == 1){
		return;
	}
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.deleting = 1;
	var url = "/manage/store/relish/setmeal/category/delete";
	$.ajaxSetup({cache:false});
	$.post(url,param,function(data){
				self.deleting = 0;
				if(!LoginPanel.checkLogin(data,function(){
																SetMeal.Category.del(param,callback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}

//分类排序
SetMeal.Category.order = function(param,callback){
	var self = this;
	if(this.ordering == 1)
		return;
	this.ordering = 1;
	var url = "/manage/store/relish/setmeal/category/order";
	$.ajaxSetup({cache:false});
	$.post(url,param,function(data){

				self.ordering = 0;
				if(!LoginPanel.checkLogin(data,function(){
																SetMeal.Category.order(param,callback);		
															}))
					return;
				callback(data);
			},"json");
}


//美食域验证
SetMeal.validate = function(field,value,callback){
	if(typeof value == String)
		value = $.trim(value.replaceHtmlTags());
	var result = "正确";
	var flag = true;
	if(field == "setMeal.name"){
		if(value == ""){
			result="套餐名不能为空";
			flag=false;
		}else
		if(value.byteLength()>40){
			result="套餐名最多40个字母或20个汉字";
			flag = false;
		}
	}
	if(field == "setMeal.price"){
		if(value == ""){
			result="套餐价格不能为空";
			flag=false;
		}else
		if(!/^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$/.test(value)){
			result="价格输入不正确";
			flag = false;
		}
	}
	if(field == "_setMeal.specialPrice"){
		if(value != "0" && value != "" && !/^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$/.test(value)){
			result="优惠价格输入不正确";
			flag = false;
		}
	}
	if(field == "setMeal.tags"){
		if(value.byteLength()>40){
			result="标签最多40个字母或20个汉字";
			flag = false;
		}
	}
	if(field == "setMeal.introduction"){
		if(value.byteLength()>400){
			result="简介最多400个字母或者200个汉字";
			flag = false;
		}
	}
	if(field=="previewPhoto"){
		if(value != "" && !/\.(jpg)|(jpeg)$/i.test(value)){
			result="您选择的图片格式不正确";
			flag = false;
		}
	}
	if($.isFunction(callback)){
		callback(field,flag,result);
	}
	return {ok:flag,result:result};
}

//添加美食
SetMeal.add = function(param,callback,validateCallback,beforeDoFunction){
	var self = this;
	if(this.adding == 1){
		return;
	}
	var flag = true;	
	$.each(param,function(i,n){
				flag = SetMeal.validate(i,n,validateCallback).ok && flag;			 
			});
	if(!flag)
		return;
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.adding =1;	
	var url = "/manage/store/relish/setmeal/add";	
	$.ajaxSetup({cache:false});
	$.post(url,param,function(data){
				self.adding = 0;
				if(!LoginPanel.checkLogin(data,function(){
																SetMeal.add(param,callback,validateCallback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}

//添加美食
SetMeal.modify = function(param,callback,validateCallback,beforeDoFunction){
	var self = this;
	if(this.adding == 1){
		return;
	}
	var flag = true;	
	$.each(param,function(i,n){
				flag = SetMeal.validate(i,n,validateCallback).ok && flag;			 
			});
	if(!flag)
		return;
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.adding =1;	
	var url = "/manage/store/relish/setmeal/modify";	
	$.ajaxSetup({cache:false});
	$.post(url,param,function(data){
				self.adding = 0;
				if(!LoginPanel.checkLogin(data,function(){
																SetMeal.modify(param,callback,validateCallback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}


//添加美食
SetMeal.set = function(param,callback,validateCallback,beforeDoFunction){
	var self = this;
	if(this.manageing == 1){
		return;
	}

	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.manageing =1;	
	var url = "/manage/store/relish/setmeal/manage";	
	$.ajaxSetup({cache:false});
	$.post(url,param,function(data){
				self.manageing = 0;
				if(!LoginPanel.checkLogin(data,function(){
																SetMeal.manage(param,callback,validateCallback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}

//添加美食
SetMeal.setPreviewPhoto = function(form,callback,validateCallback,beforeDoFunction){
	var self = this;
	if(this.adding == 1){
		return;
	}
	if(!SetMeal.validate("previewPhoto",$("input[name='previewPhoto']").val(),validateCallback).ok)
		return;
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.adding =1;	
	var url = "/manage/store/relish/setmeal/setPreviewPhoto";	
	var iframe = $("<iframe id='setPreviewPhotoIframe' name='setPreviewPhotoIframe' style='display:none'></iframe>");
	$("body").append(iframe);
	iframe.load(addRelishCallback);
	document.getElementById(form).target = "setPreviewPhotoIframe";
	document.getElementById(form).method = "post";
	document.getElementById(form).enctype  = "multipart/form-data";
	document.getElementById(form).action = url;
	document.getElementById(form).submit();	
	//处理请求完成后
	function addRelishCallback(){
		//alert(iframe.contents().find("body").html());
		var data = eval("("+iframe.contents().find("body").html()+")");
		iframe.remove();
		self.adding = 0;
		if(!LoginPanel.checkLogin(data,function(){
														Relish.setPreviewPhoto(form,callback,validateCallback,beforeDoFunction);		
													}))
			return;
		callback(data);
	}
}

//删除美食
SetMeal.del = function(param,callback,beforeDoFunction){
	var self = this;
	if(this.deling == 1){
		return;
	}
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.deling =1;	
	var url = "/manage/store/relish/setmeal/delete";	
	$.ajaxSetup({cache:false});
	$.get(url,param,function(data){
							 //alert(data);
				self.deling = 0;
				if(!LoginPanel.checkLogin(data,function(){
																SetMeal.del(param,callback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}