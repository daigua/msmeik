// JavaScript Document
var Relish = {};

Relish.Category = {};
//category域的验证
Relish.Category.validate = function(field,value,callback){
	var value = $.trim(value.replaceHtmlTags());
	var result = "正确";
	var flag = true;
	if(field == "relishCategory.name"){
		if(value == ""){
			result="美味分类名不能为空";
			flag=false;
		}else
		if(value.byteLength()>40){
			result="美味分类名最多40个字母或20个汉字";
			flag = false;
		}
	}
	if($.isFunction(callback)){
		callback(field,flag,result);
	}
	return {ok:flag,result:result};
}
//添加美食分类
Relish.Category.add = function(category,callback,validateCallback,beforeDoFunction){
	var self = this;
	if(this.addCategorying == 1){
		return;
	}
	var flag = true;
	$.each(category,function(i,n){
				flag = Relish.Category.validate(i,n,validateCallback).ok && flag;			 
			});
	if(!flag)
		return;
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.addCategorying =1;	
	var url = "/manage/store/relish/category/add";
	$.ajaxSetup({cache:false});
	$.post(url,category,function(data){
				self.addCategorying = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Relish.Category.add(category,callback,validateCallback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}

//删除美食分类
Relish.Category.del = function(categoryId,storeId,callback,beforeDoFunction){	
	var self = this;
	if(this.deleting == 1){
		return;
	}
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.deleting = 1;
	var url = "/manage/store/relish/category/delete";
	$.ajaxSetup({cache:false});
	$.post(url,{categoryId:categoryId,storeId:storeId},function(data){
				self.deleting = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Relish.Category.del(categoryId,storeId,callback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}

//修改美食分类名
Relish.Category.modify = function(category,callback,validateCallback,beforeDoFunction){
	var self = this;
	if(this.modifying == 1){
		return;
	}
	var flag = true;
	$.each(category,function(i,n){
				flag = Relish.Category.validate(i,n,validateCallback).ok && flag;			 
			});
	if(!flag)
		return;
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.modifying =1;	
	var url = "/manage/store/relish/category/update";
	$.ajaxSetup({cache:false});
	$.post(url,category,function(data){
				self.modifying = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Relish.Category.modify(category,callback,validateCallback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}

//取得美食分类
Relish.Category.get = function(storeId,callback){
	var url = "/store/relish/category/list";
	$.ajaxSetup({cache:false});
	$.get(url,{storeId:storeId},callback,"json")
}

//美食域验证
Relish.validate = function(field,value,callback){
	var value = $.trim(value.replaceHtmlTags());
	var result = "正确";
	var flag = true;
	if(field == "relish.name"){
		if(value == ""){
			result="美味名不能为空";
			flag=false;
		}else
		if(value.byteLength()>40){
			result="美味名最多40个字母或20个汉字";
			flag = false;
		}
	}
	if(field == "relish.price"){
		if(value == ""){
			result="美味价格不能为空";
			flag=false;
		}else
		if(!/^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$/.test(value)){
			result="价格输入不正确";
			flag = false;
		}
	}
	if(field == "_relish.specialPrice"){
		if(value != "" && !/^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$/.test(value)){
			result="优惠价格输入不正确";
			flag = false;
		}
	}
	if(field == "relish.tags"){
		if(value.byteLength()>40){
			result="标签最多40个字母或20个汉字";
			flag = false;
		}
	}
	if(field == "relish.introduction"){
		if(value.byteLength()>400){
			result="简介最多400个字母或者200个汉字";
			flag = false;
		}
	}
	if(field=="previewPhoto"){
		if(value != "" && !/\.(jpg)|(jpeg)$/i.test(value)){
			result="您选择的图片格式不正确";
			flag = false;
		}
	}
	if($.isFunction(callback)){
		callback(field,flag,result);
	}
	return {ok:flag,result:result};
}

//添加美食
Relish.add = function(relish,callback,validateCallback,beforeDoFunction){
	var self = this;
	if(this.adding == 1){
		return;
	}
	var flag = true;	
	$.each(relish,function(i,n){
				flag = Relish.validate(i,n,validateCallback).ok && flag;			 
			});
	relish["relish.store.id"]=storeId+"";
	if(!flag)
		return;
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.adding =1;	
	var url = "/manage/store/relish/add";	
	$.ajaxSetup({cache:false});
	$.post(url,relish,function(data){
				self.adding = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Relish.add(relish,callback,validateCallback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}

//修改美食
Relish.update = function(relish,callback,validateCallback,beforeDoFunction){
	var self = this;
	if(this.modifying == 1){
		return;
	}
	var flag = true;	
	$.each(relish,function(i,n){
				flag = Relish.validate(i,n,validateCallback).ok && flag;			 
			});
	if(!flag)
		return;
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.modifying =1;	
	var url = "/manage/store/relish/update";	
	$.ajaxSetup({cache:false});
	$.post(url,relish,function(data){
				self.modifying = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Relish.update(relish,callback,validateCallback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}

//添加美食
Relish.setPreviewPhoto = function(form,callback,validateCallback,beforeDoFunction){
	var self = this;
	if(this.adding == 1){
		return;
	}
	if(!Relish.validate("previewPhoto",$("input[name='previewPhoto']").val(),validateCallback).ok)
		return;
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.adding =1;	
	var url = "/manage/store/relish/setPreviewPhoto";	
	var iframe = $("<iframe id='setPreviewPhotoIframe' name='setPreviewPhotoIframe' style='display:none'></iframe>");
	$("body").append(iframe);
	iframe.load(addRelishCallback);
	document.getElementById(form).target = "setPreviewPhotoIframe";
	document.getElementById(form).method = "post";
	document.getElementById(form).enctype  = "multipart/form-data";
	document.getElementById(form).action = url;
	document.getElementById(form).submit();	
	//处理请求完成后
	function addRelishCallback(){
		var data = eval("("+iframe.contents().find("body").html()+")");
		iframe.remove();
		self.adding = 0;
		if(!LoginPanel.checkLogin(data,function(){
														Relish.setPreviewPhoto(form,callback,validateCallback,beforeDoFunction);		
													}))
			return;
		callback(data);
	}
}

//设置美食
Relish.set = function(relishId,storeId,p,v,callback,beforeDoFunction){
	var self = this;
	if(this.setting == 1){
		return;
	}
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.setting =1;	
	var url = "/manage/store/relish/set";	
	$.ajaxSetup({cache:false});
	$.get(url,{relishId:relishId,storeId:storeId,p:p,v:v},function(data){
				self.setting = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Relish.set(relishId,storeId,p,v,callback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}

//删除美食
Relish.del = function(storeId,relishId,callback,beforeDoFunction){
	var self = this;
	if(this.deling == 1){
		return;
	}
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.deling =1;	
	var url = "/manage/store/relish/delete";	
	$.ajaxSetup({cache:false});
	$.get(url,{relishId:relishId,storeId:storeId},function(data){
				self.deling = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Relish.del(storeId,relishId,callback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}

//获取relish
Relish.list = function(param,callback){
	var url = "/store/relish/json/list";	
	$.ajaxSetup({cache:false});
	$.get(url,param,function(data){				
				callback(data);
			},"json");
}