// JavaScript Document
var SetMealApi1 = {};
//推荐美食
SetMealApi1.recommend = function(param,callback,beforeDoFunction){
	var self = this;
	if(this.recommending == 1){
		return;
	}
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.recommending =1;	
	var url = "/store/relish/setmeal/recommend";	
	$.ajaxSetup({cache:false});
	$.get(url,param,function(data){
				self.recommending = 0;
				if(!LoginPanel.checkLogin(data,function(){
																SetMealApi1.recommend(param,callback,beforeDoFunction);		
															}))				
					return;
				if(data.requestLimitReject){
					//HtmlUi.OverlayTip.show(data.limitReason,true,{overlayShow:false});
					//setTimeout(function(){HtmlUi.OverlayTip.close();},1000);
					return;
				}
				callback(data);
			},"json");
}
//美食打分
SetMealApi1.score = function(param,callback,beforeDoFunction){
	var self = this;
	if(this.scoreing == 1){
		return;
	}
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.scoreing =1;	
	var url = "/store/relish/setmeal/score";	
	$.ajaxSetup({cache:false});
	$.get(url,param,function(data){
				self.scoreing = 0;
				if(!LoginPanel.checkLogin(data,function(){
																SetMealApi1.score(param,callback,beforeDoFunction);		
															}))				
					return;
				if(data.requestLimitReject){
					//HtmlUi.OverlayTip.show(data.limitReason,true,{overlayShow:false});
					//setTimeout(function(){HtmlUi.OverlayTip.close();},1000);
					data.ok = 0;
				}
				callback(data);
			},"json");
}