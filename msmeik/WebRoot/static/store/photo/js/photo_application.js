// JavaScript Document

var PhotoUploadPanel = {};
PhotoUploadPanel.init =  function(){
	if(this.isInit){
		return;
	}
	this.isInit = true;
	this.uploaderPanel = new HtmlUi.IframePanel({width:480,height:350});
}
PhotoUploadPanel.show = function(storeId,targetType,targetKey,targetName,categoryId){
	this.init();
	this.uploaderPanel.show("/store/photo/upload.html?storeId="+storeId+"&targetKey="+targetKey+"&targetType="+targetType+"&targetName="+encodeURI(targetName)+"&categoryId="+categoryId);
}
