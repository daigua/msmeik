// JavaScript Document
var Photo = {};
Photo.Category = {};
//category域的验证
Photo.Category.validate = function(field,value,callback){
	var value = $.trim(value.replaceHtmlTags());
	var result = "正确";
	var flag = true;
	if(field == "photoCategory.name"){
		if(value == ""){
			result="图片分类名称不能为空";
			flag=false;
		}else
		if(value.byteLength()>40){
			result="图片分类名最多40个字母或20个汉字";
			flag = false;
		}
	}
	if($.isFunction(callback)){
		callback(field,flag,result);
	}
	return {ok:flag,result:result};
}
//添加图片分类
Photo.Category.add = function(category,callback,validateCallback,beforeDoFunction){
	var self = this;
	if(this.addCategorying == 1){
		return;
	}
	var flag = true;
	$.each(category,function(i,n){
				flag = Photo.Category.validate(i,n,validateCallback).ok && flag;			 
			});
	if(!flag)
		return;
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.addCategorying =1;	
	var url = "/manage/store/photo/category/add";
	$.ajaxSetup({cache:false});
	$.post(url,category,function(data){
				self.addCategorying = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Photo.Category.add(category,callback,validateCallback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}
//修改图片分类名
Photo.Category.modify = function(category,callback,validateCallback,beforeDoFunction){
	var self = this;
	if(this.modifying == 1){
		return;
	}
	var flag = true;
	$.each(category,function(i,n){
				flag = Photo.Category.validate(i,n,validateCallback).ok && flag;			 
			});
	if(!flag)
		return;
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.modifying =1;	
	var url = "/manage/store/photo/category/update";
	$.ajaxSetup({cache:false});
	$.post(url,category,function(data){
				self.modifying = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Photo.Category.modify(category,callback,validateCallback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}
//删除美食分类
Photo.Category.del = function(categoryId,storeId,callback,beforeDoFunction){	
	var self = this;
	if(this.deleting == 1){
		return;
	}
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.deleting = 1;
	var url = "/manage/store/photo/category/delete";
	$.ajaxSetup({cache:false});
	$.post(url,{categoryId:categoryId,storeId:storeId},function(data){
				self.deleting = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Photo.Category.del(categoryId,storeId,callback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}
//取得美食分类
Photo.Category.get = function(storeId,callback){
	var url = "/store/photo/category/list";
	$.ajaxSetup({cache:false});
	$.get(url,{storeId:storeId},callback,"json")
}
Photo.validate = function(field,value,callback){
	var value = $.trim(value.replaceHtmlTags());
	var result = "正确";
	var flag = true;
	if(field=="photoFile"){
		if(value == ""){
			result="请选择您要上传的图片";
			flag = false;
		}else
		if(!/\.(jpg)|(jpeg)$/i.test(value)){
			result="您选择的图片格式不正确";
			flag = false;
		}
	}
	if($.isFunction(callback)){
		callback(field,flag,result);
	}
	return {ok:flag,result:result};
}
//上传图片
Photo.upload = function(form,param,callback,validateCallback,beforeDoFunction){
	var self = this;
	if(this.uploading == 1){
		return;
	}
	if(!Photo.validate("photoFile",$("#"+form+" input[name='photoFile']").val(),validateCallback).ok)
		return;
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.uploading =1;	
	var url = "/store/photo/upload?"+$.param(param);	
	var iframe = $("<iframe id='uploadPhotoFileIframe' name='uploadPhotoFileIframe' style='display:none'></iframe>");
	$("body").append(iframe);
	iframe.load(uploadCallback);
	document.getElementById(form).target = "uploadPhotoFileIframe";
	document.getElementById(form).method = "post";
	document.getElementById(form).enctype  = "multipart/form-data";
	document.getElementById(form).action = url;
	document.getElementById(form).submit();	
	//处理请求完成后
	function uploadCallback(){
		//alert(iframe.contents().find("body").html());
		var data = eval("("+iframe.contents().find("body").html()+")");
		iframe.remove();
		self.uploading = 0;
		if(!LoginPanel.checkLogin(data,function(){
														Photo.upload(form,param,callback,validateCallback,beforeDoFunction);		
													}))
			return;
		if(data.requestLimitReject){
			HtmlUi.OverlayTip.show(data.limitReason,true);
			return;
		}
		callback(data);
	}
}

//图片管理
Photo.manage = function(storeId,photoIds,p,v,callback,beforeDoFunction){
	if(photoIds.length == 0)
		return;
	var self = this;
	if(this.manageing == 1){
		return;
	}
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.manageing =1;	
	var url = "/manage/store/photo/manage";	
	$.ajaxSetup({cache:false});
	$.get(url,{photoIds:photoIds,storeId:storeId,p:p,v:v},function(data){
				self.manageing = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Photo.manage(storeId,photoIds,p,v,callback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}

//删除图片
Photo.del = function(photoIds,callback,beforeDoFunction){
	if(photoIds.length == 0)
		return;
	var self = this;
	if(this.manageing == 1){
		return;
	}
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.manageing =1;	
	var url = "/manage/center/photo/delete";	
	$.ajaxSetup({cache:false});
	$.get(url,{photoIds:photoIds},function(data){
				self.manageing = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Photo.del(photoIds,callback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}