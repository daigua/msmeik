// JavaScript Document

var Promotion = {};

Promotion.validate = function(field,value,callback){
	var value = $.trim(value.replaceHtmlTags());
	var result = "正确";
	var flag = true;
	if(field == "promotion.name"){
		if(value == ""){
			result="优惠名称不能为空";
			flag=false;
		}else
		if(value.byteLength()>40){
			result="优惠名称最多40个字母或20个汉字";
			flag = false;
		}
	}
	if(field == "promotion.tags"){
		if(value.byteLength()>40){
			result="标签最多40个字母或20个汉字";
			flag = false;
		}
	}
	if(field == "promotion.introduction"){
		if(value.byteLength()>2000){
			result="简介最多2000个字母或者1000个汉字";
			flag = false;
		}
	}
	if(field == "promotion.beginTime"){
		if(value==""){
			result="开始日期不能为空";
			flag = false;
		}
	}
	if(field == "promotion.endTime"){
		if(value==""){
			result="结束日期不能为空";
			flag = false;
		}
	}
	if(field=="promotionImage"){
		if(value != "" && !/\.(jpg)|(jpeg)$/i.test(value)){
			result="您选择的图片格式不正确";
			flag = false;
		}
	}
	if($.isFunction(callback)){
		callback(field,flag,result);
	}
	return {ok:flag,result:result};
}

Promotion.add = function(promotion,callback,validateCallback,beforeDoFunction){
	var self = this;
	if(this.adding == 1){
		return;
	}
	var flag = true;
	$.each(promotion,function(i,n){
				flag = Promotion.validate(i,n,validateCallback).ok && flag;			 
			});
	if(!flag)
		return;
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.adding =1;	
	var url = "/manage/store/promotion/add";
	$.ajaxSetup({cache:false});
	$.post(url,promotion,function(data){
				self.adding = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Promotion.add(promotion,callback,validateCallback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}

Promotion.update = function(promotion,callback,validateCallback,beforeDoFunction){
	var self = this;
	if(this.updateing == 1){
		return;
	}
	var flag = true;
	$.each(promotion,function(i,n){
				flag = Promotion.validate(i,n,validateCallback).ok && flag;			 
			});
	if(!flag)
		return;
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.updateing =1;	
	var url = "/manage/store/promotion/update";
	$.ajaxSetup({cache:false});
	$.post(url,promotion,function(data){
				self.updateing = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Promotion.update(promotion,callback,validateCallback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}

Promotion.uploadImage = function(form,callback,validateCallback,beforeDoFunction){
	var self = this;
	if(this.adding == 1){
		return;
	}
	if(!Promotion.validate("promotionImage",$("input[name='promotionImage']").val(),validateCallback).ok)
		return;
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.adding =1;	
	var url = "/manage/store/promotion/uploadImage";	
	var iframe = $("<iframe id='uploadPromotionImageIframe' name='uploadPromotionImageIframe' style='display:none'></iframe>");
	$("body").append(iframe);
	iframe.load(uploadCallback);
	document.getElementById(form).target = "uploadPromotionImageIframe";
	document.getElementById(form).method = "post";
	document.getElementById(form).enctype  = "multipart/form-data";
	document.getElementById(form).action = url;
	document.getElementById(form).submit();	
	//处理请求完成后
	function uploadCallback(){
		var data = eval("("+iframe.contents().find("body").html()+")");
		iframe.remove();
		self.adding = 0;
		if(!LoginPanel.checkLogin(data,function(){
														Promotion.uploadImage(form,callback,validateCallback,beforeDoFunction);		
													}))
			return;
		callback(data);
	}
}

Promotion.set = function(promotionId,storeId,p,v,callback,beforeDoFunction){
	var self = this;
	if(this.setting == 1){
		return;
	}
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.setting =1;	
	var url = "/manage/store/promotion/set";	
	$.ajaxSetup({cache:false});
	$.get(url,{promotionId:promotionId,storeId:storeId,p:p,v:v},function(data){
				self.setting = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Promotion.set(promotionId,storeId,p,v,callback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}

//删除
Promotion.del = function(storeId,promotionId,callback,beforeDoFunction){
	var self = this;
	if(this.deling == 1){
		return;
	}
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.deling =1;	
	var url = "/manage/store/promotion/delete";	
	$.ajaxSetup({cache:false});
	$.get(url,{promotionId:promotionId,storeId:storeId},function(data){
				self.deling = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Promotion.del(promotionId,storeId,callback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}