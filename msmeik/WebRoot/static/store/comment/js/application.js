// JavaScript Document
var CommentPanel = {};
CommentPanel.init = function(){
	if(this.isInit){
		return;
	}
	this.isInit = true;
	this.panel = new HtmlUi.IframePanel({width:670,height:570});
}
CommentPanel.show = function(name,storeId){
	this.init();
	this.panel.show("/store/comment/dianping.html?name="+encodeURI(name)+"&storeId="+storeId);
}

CommentPanel.close = function(){
	this.panel.close();
}