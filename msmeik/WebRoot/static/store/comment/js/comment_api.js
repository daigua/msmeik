// JavaScript Document
var Comment = {};

Comment.validate = function(field,value,callback){
	var result = "正确";
	var flag = true;
	if(field == "comment.store.id"){
		if(value == ""){
			result="输入错误";
			flag=false;
		}else
		if(!/^[1-9][0-9]{0,19}$/.test(value)){
			result="输入错误";
			flag = false;
		}
	}
	if(field == "comment.perPay"){
		if(value != "" && !/^[1-9][0-9]{0,19}$/.test(value)){
			result="输入错误";
			flag=false;
		}
	}
	if(field=="comment.content"){
		value = $.trim(value.replaceHtmlTags())
		if(value == ""){
			result="点评";
			flag=false;
			result="点评内容不能为空";
		}else
		if(value.byteLength > 4000){
			result="点评内容太长，最多2000字或者400个字符";
			flag = false;
		}
	}
	if(field == "comment.tasteScore"){
		if(value == 0){
			result="请打分";
			flag=false;
		}
	}
	if(field == "comment.serveScore"){
		if(value == 0){
			result="请打分";
			flag=false;
		}
	}
	if(field == "comment.evmtScore"){
		if(value == 0){
			result="请打分";
			flag=false;
		}
	}
		
	if($.isFunction(callback)){
		callback(field,flag,result);
	}
	return {ok:flag,result:result};
}

Comment.add = function(param,callback,validateCallback,beforeDoFunction){
	var self = this;
	if(this.adding == 1){
		return;
	}
	var flag = true;	
	$.each(param,function(i,n){
				flag = Comment.validate(i,n,validateCallback).ok && flag;			 
			});
	if(!flag)
		return;
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.adding =1;	
	var url = "/store/comment/add";	
	$.ajaxSetup({cache:false});
	$.post(url,param,function(data){
				self.adding = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Comment.add(param,callback,validateCallback,beforeDoFunction);		
															}))
					return;
				if(data.requestLimitReject){
					HtmlUi.OverlayTip.show(data.limitReason,true);
					return;
				}
				callback(data);
			},"json");
}

Comment.del = function(commentId,callback,beforeDoFunction){
	var self = this;
	if(this.deling == 1){
		return;
	}
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.deling =1;	
	var url = "/manage/center/comment/delete";	
	$.ajaxSetup({cache:false});
	$.get(url,{commentId:commentId},function(data){
				self.deling = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Comment.del(commentId,callback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}