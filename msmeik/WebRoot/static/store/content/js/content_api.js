// JavaScript Document
var Content = {};

Content.validate = function(field,value,callback){
	var value = $.trim(value.replaceHtmlTags());
	var result = "正确";
	var flag = true;
	if(field == "content.title"){
		if(value == ""){
			result="标题不能为空";
			flag=false;
		}else
		if(value.byteLength()>40){
			result="标题最多20个字或者40个字母";
			flag = false;
		}
	}
	if(field == "content.content"){
		if(value == ""){
			result="内容不能为空";
			flag=false;
		}
	}	
	if($.isFunction(callback)){
		callback(field,flag,result);
	}
	return {ok:flag,result:result};
}

Content.add = function(content,callback,validateCallback,beforeDoFunction){
	var self = this;
	if(this.adding == 1){
		return;
	}
	var flag = true;	
	$.each(content,function(i,n){
				flag = Content.validate(i,n,validateCallback).ok && flag;			 
			});
	if(!flag)
		return;
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.adding =1;	
	var url = "/manage/store/content/add";	
	$.ajaxSetup({cache:false});
	$.post(url,content,function(data){
				self.adding = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Content.add(content,callback,validateCallback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}

Content.manage = function(storeId,contentId,type,value,callback,beforeDoFunction){
	var self = this;
	if(this.manageing == 1){
		return;
	}
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.manageing =1;	
	var url = "/manage/store/content/manage/"+type;	
	$.ajaxSetup({cache:false});
	$.get(url,{contentId:contentId,storeId:storeId,v:value},function(data){
				self.manageing = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Content.manage(storeId,contentId,type,value,callback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}

Content.modify = function(content,callback,validateCallback,beforeDoFunction){
	var self = this;
	if(this.modifying == 1){
		return;
	}
	var flag = true;	
	$.each(content,function(i,n){
				flag = Content.validate(i,n,validateCallback).ok && flag;			 
			});
	if(!flag)
		return;
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.modifying =1;	
	var url = "/manage/store/content/modify";	
	$.ajaxSetup({cache:false});
	$.post(url,content,function(data){
				self.modifying = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Content.modify(content,callback,validateCallback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}