// JavaScript Document
var Domain = {};
Domain.validate =function(field,value,callback){
	var value = $.trim(value.replaceHtmlTags());
	var result = "正确";
	var flag = true;
	if(field == "subdomain"){
		if(value == ""){
			result = "域名不能为空";
			flag = false;
		}else
		if(!/^[a-z]\w{0,9}$/.test(value)){
			result = "域名格式错误";
			flag = false;
		}	
	}
	if($.isFunction(callback)){
		callback(field,flag,result);
	}
	return {ok:flag,result:result};
}

//设置店铺域名
Domain.setSubdomain = function(param,callback,validateCallback,beforeDoFunction){
	var self = this;
	if(this.setDomaining == 1){
		return;
	}
	var flag = true;
	flag = Domain.validate("subdomain",param.subdomain,validateCallback).ok && flag;
	if(!flag)
		return;
	if($.isFunction(beforeDoFunction)){
		beforeDoFunction();
	}
	this.setDomaining =1;	
	var url = "/manage/store/vas/domain/setSubdomain";
	$.ajaxSetup({cache:false});
	$.post(url,param,function(data){
				self.setDomaining = 0;
				if(!LoginPanel.checkLogin(data,function(){
																Domain.setSubdomain(param,callback,validateCallback,beforeDoFunction);		
															}))
					return;
				callback(data);
			},"json");
}